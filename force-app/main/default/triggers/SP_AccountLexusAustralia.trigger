trigger SP_AccountLexusAustralia on Account (before delete, before insert, before update)
{
	// The Lexus Australia account has a special use and must not be updated, deleted or duplicated
/*
	list<Account> li_Account =	[
									select	id
									from	Account
									where	Name = :'Lexus Australia'
								];

	// Prevent a duplicate Lexus Australia account from being created
	if (trigger.isInsert)
	{
		for (Integer i = 0; i < Trigger.new.size(); i++)
		{
			// Prevent a user from adding a new 'Lexus Australia' account if one exists
			if (Trigger.new[i].Name == 'Lexus Australia' && !li_Account.isEmpty())
			{
				Trigger.new[i].Name.AddError('An Account called Lexus Australia already exists') ;
			}
		}
	}

	// Prevent a name change to or from Lexus Australia
	if (trigger.isUpdate)
	{
		for (Integer i = 0; i < Trigger.old.size(); i++)
		{
			// Prevent a user from renaming the 'Lexus Australia' account
			if (Trigger.old[i].Name == 'Lexus Australia' && Trigger.new[i].Name != 'Lexus Australia')
			{
				Trigger.new[i].Name.AddError('Cannot change the name of the Lexus Australia account') ;
			}
	
			// Prevent a user from renaming another Account to 'Lexus Australia'
			if (Trigger.new[i].Name == 'Lexus Australia' && (Trigger.old[i].Name != 'Lexus Australia'))
			{
				Trigger.new[i].Name.AddError('Cannot change the name of this Account to Lexus Australia') ;
			}
		}
	}

	// Prevent the Lexus Australia account from being deleted
	if (trigger.isDelete)
	{
		for (Integer i = 0; i < Trigger.old.size(); i++)
		{
			if (Trigger.old[i].Name == 'Lexus Australia')
			{
				Trigger.old[i].Name.AddError('Cannot delete the Lexus Australia account') ;
			}
		}		
	}
*/
}