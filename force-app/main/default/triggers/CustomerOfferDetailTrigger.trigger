/*******************************************************************************
@author:		Donnie Banez
@date:         	August 2019
@description:  	Trigger for Offer 
@Test Methods:	CustomerOfferDetailTriggerHandlerTest 
*******************************************************************************/
trigger CustomerOfferDetailTrigger on Customer_Offer_Detail__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    // Call Customer Offer Detail Trigger Handler 
    CustomerOfferDetailTriggerHandler.mainEntry(
        Trigger.isBefore, Trigger.isAfter, 
        Trigger.isInsert, Trigger.isUpdate, 
        Trigger.isDelete, Trigger.isUnDelete,  
        Trigger.new, Trigger.old,
        Trigger.newMap, Trigger.oldMap 
    );
}