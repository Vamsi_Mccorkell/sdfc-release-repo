trigger SP_ServiceIntegrationAddressCleaned on Service__c (before update)
{
	// This trigger checks if the Integration Address fields have been cleaned and if they have,
	// sets the Ready_For_Processing flag which will allow the Service record to be picked up by batch processing

	for (Service__c sService : trigger.new)
	{
		Service__c sOldService = trigger.oldMap.get(sService.Id);

		if (!sOldService.Address_Cleaned__c && sService.Address_Cleaned__c)
		{
			if (!sOldService.Ready_For_Processing__c)
			{
				sService.Ready_For_Processing__c = true;
			}
		}
	}
}