trigger SP_AccountManagement on Account (after update, before delete) 
{
	// Flag to check if this trigger is fired as a result of the updation of First Name, Last Name,
	// Email, Mobile and/or Password fields when Intelematics User updates the similar fields in Enform User
	boolean accountUpdatedTriggers = SP_Utilities.AccountUpdatedByEnformTrigger || 
									 SP_Utilities.AccountUpdatedByVOTrigger ||
									 SP_Utilities.AccountUpdatedByVOUpdateAccInfoTrigger ||
									 SP_Utilities.AccountUpdatedByWarrantyTrigger ||
									 SP_Utilities.AccountUpdatedByOwnerModelsTrigger ||
									 SP_Utilities.AccountUpdatedByPMAllocationTrigger ||
									 SP_Utilities.AccountUpdatedByCalcServValTrigger ||
									 SP_Utilities.AccountUpdatedByServRegoAssetServDateTrigger ||
									 SP_Utilities.AccountUpdatedBySurveyUpdOwnerTrigger ||
									 SP_Utilities.AccountUpdatedByCaseOptOutTrigger ||
									 SP_Utilities.AccountUpdatedByCaseSetSelTrigger;
	
	if (SP_Utilities.AccountUpdatedByEnformTrigger)
		SP_Utilities.AccountUpdatedByEnformTrigger = false;
		
	if (SP_Utilities.AccountUpdatedByVOTrigger)
		SP_Utilities.AccountUpdatedByVOTrigger = false;
		
	if (SP_Utilities.AccountUpdatedByVOUpdateAccInfoTrigger)
		SP_Utilities.AccountUpdatedByVOUpdateAccInfoTrigger = false;
		
	if (SP_Utilities.AccountUpdatedByWarrantyTrigger)
		SP_Utilities.AccountUpdatedByWarrantyTrigger = false;		
		
	if (SP_Utilities.AccountUpdatedByOwnerModelsTrigger)
		SP_Utilities.AccountUpdatedByOwnerModelsTrigger = false;		
		
	if (SP_Utilities.AccountUpdatedByPMAllocationTrigger)
		SP_Utilities.AccountUpdatedByPMAllocationTrigger = false;	
		
	if (SP_Utilities.AccountUpdatedByCalcServValTrigger)
		SP_Utilities.AccountUpdatedByCalcServValTrigger = false;			

	if (SP_Utilities.AccountUpdatedByServRegoAssetServDateTrigger)
		SP_Utilities.AccountUpdatedByServRegoAssetServDateTrigger = false;			

	if (SP_Utilities.AccountUpdatedBySurveyUpdOwnerTrigger)
		SP_Utilities.AccountUpdatedBySurveyUpdOwnerTrigger = false;		
		
	if (SP_Utilities.AccountUpdatedByCaseOptOutTrigger)
		SP_Utilities.AccountUpdatedByCaseOptOutTrigger = false;			

	if (SP_Utilities.AccountUpdatedByCaseSetSelTrigger)
		SP_Utilities.AccountUpdatedByCaseSetSelTrigger = false;			

	try
	{
		// Send notification to Intelematics whenever a non-Itelematics User makes changes to First Name, 
		// Last Name, Email, Mobile and/or Password fields
		if (UserInfo.getUserId() != SPCacheLexusEnformMetadata.getIntelematicsUser())
		{
			if (trigger.isUpdate)
			{
				SP_AccountManagement.doSyncNotification_AccounttoEnformUser(trigger.oldMap, trigger.newMap);
			}
			else if (trigger.isDelete)
			{
				SP_AccountManagement.doSyncNotification_AccounttoEnformUser(trigger.oldMap);
			}
		}
		// If this trigger is fired when an Intelematics User updates First Name, Last Name, Email, Mobile 
		// and/or Password fields of Enform User, nothing to do. 
		// But, when an Intelematics User directly updates an account record, it should be checked if the 
		// updated fields are alowed for updation by an Intelematics User. If an Intelematics User is not
		// supposed to edit any of the non-permitted field, an error should be thrown.
		else if (trigger.isUpdate && !accountUpdatedTriggers)
		{
			if (trigger.new[0].RecordTypeId == SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType())
			{
				set<String> set_FieldNames = new set<String>();
				set_FieldNames.add('enform_expiration_date_user_license__c');
				set_FieldNames.add('personhomephone');
				set_FieldNames.add('work_phone__c');
				set_FieldNames.add('fax');
				set_FieldNames.add('shippingcity');
				set_FieldNames.add('shippingcountry');
				set_FieldNames.add('shippingpostalcode');
				set_FieldNames.add('shippingstate');
				set_FieldNames.add('shippingstreet');
				set_FieldNames.add('billingcity');
				set_FieldNames.add('billingcountry');
				set_FieldNames.add('billingpostalcode');
				set_FieldNames.add('billingstate');
				set_FieldNames.add('billingstreet');	
				set_FieldNames.add('benefits_events_opt_out__c');
				set_FieldNames.add('product_information_opt_out__c');
				set_FieldNames.add('first_registered_date__c');
				
				string strField = SP_Utilities.checkFieldChangeAllowed(trigger.newMap, trigger.oldMap, set_FieldNames);	
				
				if (strField != null)	
					trigger.new[0].addError(' An Intelematics User is not allowed to edit ' + strField + '.');
			}
		}
	}
	catch(Exception ex)
	{
		system.debug('######## SYSTEM EXCEPTION: Trigger SP_AccountManagement on Account Failed.');
		SPException.LogSystemException(ex, SP_EnformUtilities.getAccountOwnerFromAccount(trigger.oldMap.keySet()).get(trigger.old[0].Id), trigger.old[0].Id); // Assume, there is only one record for the time being
		
		throw ex;
	}
}