trigger SP_CaseOptOut on Case (after insert)
{
	// Set of Owner / Prospect Accounts related to these new Cases
	set<id>				set_AccountIds			= new set<id>();

	// Map of Accounts
	map<id, Account>	map_Accounts			= new map<id, Account>();

	// List of Accounts to update
	list<Account>		li_AccountstoUpdate		= new list<Account>();

	// List of new Cases
	list<Case>			li_CasestoInsert		= new list<Case>();

	for (Case sCase : trigger.new)
	{
		if	(	sCase.Origin != SPCacheTWAMetadata.getCaseOriginWeb() &&
				sCase.Origin != SPCacheTWAMetadata.getCaseOriginDealerWeb() &&
				sCase.Origin != SPCacheTWAMetadata.getCaseOriginIPad()
			)
		{
			continue;
		}

		if	(	sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeAccessoriesEnquiry() ||
				sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeBuildandPriceEnquiry() ||
				sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeGeneralEnquiry() ||
				sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypePersonalEnquiry() ||
				sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeNewVehicleInterest()
			)
		{
			if (sCase.AccountId != null)
			{
				set_AccountIds.add(sCase.AccountId);
			}
		}
	}

	for (Account [] arrAccounts :	[	select	id,
												OwnerId,
												RecordTypeId,
												PersonContactId,
												// Opt-out flags
												Benefits_Events_Opt_Out__c,
												Do_Not_Contact__pc,
												PersonDoNotCall,
												Product_Information_Opt_Out__c
										from	Account
										where	id in :set_AccountIds
									])
	{
		for (Account sAccount : arrAccounts)
		{
			map_Accounts.put(sAccount.Id, sAccount);
		}
	}

	for (Case sCase : trigger.new)
	{
		if	(	sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeAccessoriesEnquiry() ||
				sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeBuildandPriceEnquiry() ||
				sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeGeneralEnquiry() ||
				sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypePersonalEnquiry() ||
				sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeNewVehicleInterest()
			)
		{

			if (!map_Accounts.containsKey(sCase.AccountId))
			{
				continue;
			}

			Account sAccount = map_Accounts.get(sCase.AccountId);

			// Handle a Case from a Dealer website
			if (sCase.Origin == SPCacheTWAMetadata.getCaseOriginDealerWeb())
			{
				if (sAccount.RecordTypeId == SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType())
				{
					// Existing Owner
				}
				else if (sAccount.RecordTypeId == SPCacheRecordTypeMetadata.getPersonAccountProspectRecordType())
				{
					// New prospect - set the opt-out flags
//					datetime dtFiveMinutesAgo = system.now().addMinutes(-5);
					
//					if (sAccount.CreatedDate > dtFiveMinutesAgo)
//					{
//						sAccount.Benefits_Events_Opt_Out__c		= true;
//						sAccount.Do_Not_Contact__pc				= true;
//						sAccount.PersonDoNotCall				= true;
//						sAccount.Product_Information_Opt_Out__c	= true;
						
//						li_AccountstoUpdate.add(sAccount);
//					}
				}
			}
			else
			{
				// Has the Product Opt Out choice changed?
				if (sAccount.Product_Information_Opt_Out__c != sCase.Product_Information_Opt_Out__c)
				{
					sAccount.Product_Information_Opt_Out__c = sCase.Product_Information_Opt_Out__c;
	
					li_AccountstoUpdate.add(sAccount);
					
					if (sAccount.RecordTypeId == SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType())
					{
						// Raise a Change Details Case
						Case sNewCase = new Case();
		
						sNewCase.RecordTypeId		= SPCachePotentiateMetadata.getOwnerDetailsRecordTypeID();
						sNewCase.ContactId			= sAccount.PersonContactId;
						sNewCase.Type				= SPCachePotentiateMetadata.getCaseTypeUpdateDetails();
						sNewCase.OwnerId			= sAccount.OwnerId;
						sNewCase.Status				= 'Closed';
						sNewCase.Origin				= 'Web';
						sNewCase.Subject			= 'Owner Update Details via Web';
						sNewCase.Description		= 'Update Owner Fields: Product Info Opt Out';
					
						li_CasestoInsert.add(sNewCase);
					}
					else if (sAccount.RecordTypeId == SPCacheRecordTypeMetadata.getPersonAccountProspectRecordType())
					{
						// We do not raise a Change Details Case for a Prospect
					}
				}
			}
		}
	}

	// Update Accounts
	if (!li_AccountstoUpdate.isEmpty())
	{
		SP_Utilities.AccountUpdatedByCaseOptOutTrigger = true;
		update li_AccountstoUpdate;
	}

	// Create new Cases
	if (!li_CasesToInsert.isEmpty())
	{
		insert li_CasesToInsert;
	}
}