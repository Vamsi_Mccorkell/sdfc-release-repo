/*******************************************************************************
@author:		Donnie Banez
@date:         	August 2019
@description:  	Trigger for VO Offer Detail 
@Test Methods:	VOOfferDetailTriggerHandlerTest 
*******************************************************************************/
trigger VOOfferDetailTrigger on VO_Offer_Detail__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    // Call VO Offer Detail Trigger Handler 
    VOOfferDetailTriggerHandler.mainEntry(
        Trigger.isBefore, Trigger.isAfter, 
        Trigger.isInsert, Trigger.isUpdate, 
        Trigger.isDelete, Trigger.isUnDelete,  
        Trigger.new, Trigger.old,
        Trigger.newMap, Trigger.oldMap 
    );
}