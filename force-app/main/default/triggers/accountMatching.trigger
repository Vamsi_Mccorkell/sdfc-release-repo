trigger accountMatching on Account (before insert)
{
	list<Account>	li_AccountsToUpdate	= new list<Account>();	

	for (Account sAccount : trigger.new)
	{
		if(	sAccount.RecordTypeId == SPCacheRecordTypeMetadata.getAccount_Dealer() || 
			sAccount.RecordTypeId == SPCacheRecordTypeMetadata.getAccount_Lexus())
		{
			String strAcctId = '';
			//MatchingAccount ma = new MatchingAccount();
			/*strAcctId = ma.customerMatching	(	'',
												sAccount.FirstName,
												sAccount.LastName,
												sAccount.PersonEmail,
												sAccount.PersonMobilePhone,
												sAccount.ShippingCity,
												sAccount.ShippingPostalCode,
												sAccount.ShippingStreet
											);*/
											
			strAcctId = SimplifiedMatchingAccount.customerMatching(	'',
												sAccount.FirstName,
												sAccount.LastName,
												sAccount.PersonEmail,
												sAccount.PersonMobilePhone,
												sAccount.ShippingCity,
												sAccount.ShippingPostalCode,
												sAccount.ShippingStreet, false
											);
			if(strAcctId != null)
			{
				sAccount.addError('Matching Account Found: CR1 ' + strAcctId );
			}

			li_AccountsToUpdate.add(sAccount);
		}
	}

	try
	{
		update li_AccountsToUpdate;
	}
	catch (exception e)
	{
		
	}

}