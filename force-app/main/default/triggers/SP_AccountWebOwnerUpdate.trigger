trigger SP_AccountWebOwnerUpdate on Account (after update)
{
	list<Case>			li_CasestoInsert	= new list<Case>();

	// Is this Account being updated by Potentiatethrough the API?
	if (Userinfo.getUserId().substring(0, 15) == SPCachePotentiateMetadata.getPotentiateUserID().substring(0, 15))
	{
		for (Account sAccountNew : trigger.new)
		{
			Account sAccountOld = trigger.oldMap.get(sAccountNew.Id);

			// Is the Account being updated an Owner Account?
			if (sAccountOld.RecordTypeId == SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType())
			{	
				// Look for changes that we need to report as an Owner Update
				if (	(sAccountOld.PersonEmail != sAccountNew.PersonEmail) ||
						(sAccountOld.PersonHomePhone != sAccountNew.PersonHomePhone) ||
						(sAccountOld.Work_Phone__c != sAccountNew.Work_Phone__c) ||	
						(sAccountOld.PersonMobilePhone != sAccountNew.PersonMobilePhone) ||
						(sAccountOld.BillingStreet != sAccountNew.BillingStreet) ||
						(sAccountOld.BillingCity != sAccountNew.BillingCity) ||
						(sAccountOld.BillingState != sAccountNew.BillingState) ||
						(sAccountOld.BillingPostalCode != sAccountNew.BillingPostalCode)
			)
				{
					Case sCase = new Case();

					sCase.RecordTypeId		= SPCachePotentiateMetadata.getOwnerDetailsRecordTypeID();
					sCase.ContactId			= sAccountNew.PersonContactId;
					sCase.Type				= SPCachePotentiateMetadata.getCaseTypeUpdateDetails();
					sCase.OwnerId			= sAccountNew.OwnerId;
					sCase.Status			= 'Closed';
					sCase.Origin			= SPCachePotentiateMetadata.getCaseOrigin();
					sCase.Subject			= 'Owner Update Details';
					sCase.Description		= 'Updated details: ';

					if (sAccountOld.PersonEmail != sAccountNew.PersonEmail)
					{
						sCase.Description += ' Email: ' + sAccountNew.PersonEmail;
					}

					if (sAccountOld.PersonHomePhone != sAccountNew.PersonHomePhone)
					{
						sCase.Description += ' Home Phone: ' + sAccountNew.PersonHomePhone;
					}

					if (sAccountOld.Work_Phone__c != sAccountNew.Work_Phone__c)
					{
						sCase.Description += ' Work Phone: ' + sAccountNew.Work_Phone__c;
					}

					if (sAccountOld.PersonMobilePhone != sAccountNew.PersonMobilePhone)
					{
						sCase.Description += ' Mobile: ' + sAccountNew.PersonMobilePhone;
					}

					if (sAccountOld.BillingStreet != sAccountNew.BillingStreet)
					{
						sCase.Description += ' Postal Street: ' + sAccountNew.BillingStreet;
					}

					if (sAccountOld.BillingCity != sAccountNew.BillingCity)
					{
						sCase.Description += ' Postal City: ' + sAccountNew.BillingCity;
					}

					if (sAccountOld.BillingState != sAccountNew.BillingState)
					{
						sCase.Description += ' Postal State: ' + sAccountNew.BillingState;
					}

					if (sAccountOld.BillingPostalCode != sAccountNew.BillingPostalCode)
					{
						sCase.Description += ' Postal Postcode: ' + sAccountNew.BillingPostalCode;
					}
			
					li_CasestoInsert.add(sCase);
				}
			}
		}
	}

	// Create new Teaks
	if (!li_CasesToInsert.isEmpty())
	{
		insert li_CasesToInsert;
	}

}