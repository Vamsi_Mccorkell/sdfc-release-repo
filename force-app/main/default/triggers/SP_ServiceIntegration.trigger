trigger SP_ServiceIntegration on Service__c (after insert)
{

	// AFV records to create
	list<Address_For_Validation__c>	li_AFVstoInsert = new list<Address_For_Validation__c>();


	for (Service__c sService : trigger.new)
	{
		Address_For_Validation__c sAFV = new Address_For_Validation__c();

		sAFV.Source_Id__c						= sService.Id;
		sAFV.Street_Address__c					= sService.Integration_Customer_Address__c;
		sAFV.Street_Address_Field_API_Name__c	= 'Integration_Customer_Address__c';
		sAFV.Suburb__c							= sService.Integration_Customer_Suburb__c;
		sAFV.Suburb_Field_API_Name__c			= 'Integration_Customer_Suburb__c';
		sAFV.State__c							= null;
		sAFV.State_Field_API_Name__c			= null;
		sAFV.Postcode__c						= sService.Integration_Customer_Postcode__c;
		sAFV.Postcode_Field_API_Name__c			= 'Integration_Customer_Postcode__c';
		sAFV.Ready_For_Cleaning__c				= true;

		li_AFVstoInsert.add(sAFV);
	}

	// Do the database work
	if (!li_AFVstoInsert.isEmpty())
	{
		insert li_AFVstoInsert;
	}
}