trigger SP_VehicleOwnershipManagement on Vehicle_Ownership__c (after insert, after update, before delete)
{
	if (trigger.isInsert)
	{
		system.debug('############### NEW REGISTRATION SOURCE : ' + trigger.new[0].Registration_Source__c);
		SP_VehicleOwnershipManagement.EnformRegistrationInsert(trigger.newMap);
	}
	if (trigger.isUpdate)
	{
		system.debug('############### OLD REGISTRATION SOURCE : ' + trigger.old[0].Registration_Source__c + ' ############ NEW REGISTRATION SOURCE : ' + trigger.new[0].Registration_Source__c);
		SP_VehicleOwnershipManagement.EnformRegistrationUpdate(trigger.oldMap, trigger.newMap);
	}
	else if (trigger.isDelete)
	{
		system.debug('############### SHOULD COME HERE ############');
		SP_VehicleOwnershipManagement.EnformRegistrationDelete(trigger.oldMap);
	}
}