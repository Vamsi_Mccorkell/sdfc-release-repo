trigger SP_CasePersonalPreview on Case (after insert)
{
    // Cases by Type
    //TODO map<string, Case>   map_CaseTypetoCase      = new map<string, Case>();
    map<string, list<Case>>   map_CaseTypetoCase      = new map<string, list<Case>>();

    // Set of Accounts (Selected Dealers) referenced by these new Cases
    set<id>             set_AccountIds          = new set<id>();

    // Maps of Sets of 'To' and 'CC' Contacts for those Accounts
    map<id, set<id>>    map_AccounttoToList     = new map<id, set<id>>();

    // Lists of Contact Ids for each Case record type / email template
    list<id>            li_ContactIds1          = new list<id>();
    list<id>            li_ContactIds2          = new list<id>();
    list<id>            li_ContactIds3          = new list<id>();
    list<id>            li_ContactIds4          = new list<id>();
    list<id>            li_ContactIds5          = new list<id>();
    list<id>            li_ContactIds6          = new list<id>();
    list<id>            li_ContactIds7          = new list<id>();
    list<id>            li_ContactIds8          = new list<id>(); 

    list<id>            li_ContactIds9          = new list<id>(); // Vehicle Information
    list<id>            li_ContactIds10          = new list<id>(); // Parts, Services, Warranty     
    list<id>            li_ContactIds11          = new list<id>(); // Lexus Financial Services
    list<id>            li_ContactIds12          = new list<id>(); // Feedback 
    
        
    // Lists of Case Ids
    list<id>            li_CaseIds1             = new list<id>();
    list<id>            li_CaseIds2             = new list<id>();
    list<id>            li_CaseIds3             = new list<id>();
    list<id>            li_CaseIds4             = new list<id>();
    list<id>            li_CaseIds5             = new list<id>();
    list<id>            li_CaseIds6             = new list<id>();
    list<id>            li_CaseIds7             = new list<id>();
    list<id>            li_CaseIds8             = new list<id>();

    list<id>            li_CaseIds9             = new list<id>(); // Vehicle Information   
    list<id>            li_CaseIds10             = new list<id>(); // Parts, Services, Warranty   
    list<id>            li_CaseIds11             = new list<id>(); // Lexus Financial Services  
    list<id>            li_CaseIds12             = new list<id>(); // Feedback   
    
        
    for (Case sCase : trigger.new)
    {
        if  (   sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeAccessoriesEnquiry() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeBuildandPriceEnquiry() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeGeneralEnquiry() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypePersonalEnquiry() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeNewVehicleInterest() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeConciergeOffer() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeNXPreLaunch() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeDomPerignonOffer() ||
                
                //Only Dealer Web or Dealer Mobile form submission trigger mass email for below case type
                //Vehicle Information
                ( sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeVehicleInformation() && sCase.Origin == 'Dealer Web' || sCase.Origin == 'Dealer Mobile') ||
                //Parts, Services, Warranty
                ( sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypePartsServicesWarranty() && sCase.Origin == 'Dealer Web' || sCase.Origin == 'Dealer Mobile') ||  
                //Lexus Financial Services             
                ( sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeLexusFinancialServices() && sCase.Origin == 'Dealer Web' || sCase.Origin == 'Dealer Mobile') ||
                 //Feedback             
                ( sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeFeedback() && sCase.Origin == 'Dealer Web' || sCase.Origin == 'Dealer Mobile') 
 
            )
        {
            //TODO map_CaseTypetoCase.put(sCase.Type, sCase);
            list<Case> listCases = map_CaseTypetoCase.get(sCase.Type);
            if (listCases == null)	listCases = new list<Case>();        	
            listCases.add(sCase);
            map_CaseTypetoCase.put(sCase.Type, listCases);

            if (sCase.Selected_Dealer__c != null)
            {
                set_AccountIds.add(sCase.Selected_Dealer__c);
            }
        }
    }


    set<ID> setDomPerignonOfferDistributionGroup = new set<ID>();
    
    for (Contact [] arrContact :    [   select  Id, Dom_Perignon_Offer_Distribution_Group__c,
                                                AccountId,
                                                Name
                                        from    Contact
                                        where   AccountId in :set_AccountIds
                                        and     (   To_be_Sent_on_Email_Distribution_List__c = true
                                                or  CCed_on_Distribution_List__c = true
                                                or Dom_Perignon_Offer_Distribution_Group__c = true)
                                    ])
    {
        for (Contact sContact : arrContact)
        {
            if (!map_AccounttoToList.containsKey(sContact.AccountId))
            {
                set<id> set_ToList = new set<id>();
                set_ToList.add(sContact.Id);
                map_AccounttoToList.put(sContact.AccountId, set_ToList);
            }
            else
            {
                set<id> set_ToList = map_AccounttoToList.remove(sContact.AccountId);
                set_ToList.add(sContact.Id);
                map_AccounttoToList.put(sContact.AccountId, set_ToList);
            }
            
            if (sContact.Dom_Perignon_Offer_Distribution_Group__c)
                setDomPerignonOfferDistributionGroup.add(sContact.Id);
        }
    }

    // Set up email list #1
    Messaging.MassEmailMessage mail1 = new Messaging.MassEmailMessage();
    mail1.setTemplateId(SPCacheEmailTemplateMetadata.getTemplateIdAccessoriesEnquiry());
    mail1.setSenderDisplayName('Lexus Australia');
    mail1.setReplyTo('noreply@lexus.com.au');

    // Set up email list #2
    Messaging.MassEmailMessage mail2 = new Messaging.MassEmailMessage();
    mail2.setTemplateId(SPCacheEmailTemplateMetadata.getTemplateIdBuildandPriceEnquiry());
    mail2.setSenderDisplayName('Lexus Australia');
    mail2.setReplyTo('noreply@lexus.com.au');

    // Set up email list #3
    Messaging.MassEmailMessage mail3 = new Messaging.MassEmailMessage();
    mail3.setTemplateId(SPCacheEmailTemplateMetadata.getTemplateIdGeneralEnquiry());
    mail3.setSenderDisplayName('Lexus Australia');
    mail3.setReplyTo('noreply@lexus.com.au');

    // Set up email list #4
    Messaging.MassEmailMessage mail4 = new Messaging.MassEmailMessage();
    mail4.setTemplateId(SPCacheEmailTemplateMetadata.getTemplateIdPersonalEnquiry());
    mail4.setSenderDisplayName('Lexus Australia');
    mail4.setReplyTo('noreply@lexus.com.au');

    // Set up email list #5
    Messaging.MassEmailMessage mail5 = new Messaging.MassEmailMessage();
    mail5.setTemplateId(SPCacheEmailTemplateMetadata.getTemplateIdNewVehicleInterest());
    mail5.setSenderDisplayName('Lexus Australia');
    mail5.setReplyTo('noreply@lexus.com.au');
    
    // Set up email list #6
    Messaging.MassEmailMessage mail6 = new Messaging.MassEmailMessage();
    mail6.setTemplateId(SPCacheEmailTemplateMetadata.getTemplateIdTemplateIDConciergeOffer());
    mail6.setSenderDisplayName('Lexus Australia');
    mail6.setReplyTo('noreply@lexus.com.au');
    
    // Set up email list #7
    Messaging.MassEmailMessage mail7 = new Messaging.MassEmailMessage();
    mail7.setTemplateId(SPCacheEmailTemplateMetadata.getTemplateIdTemplateIDDomPerignonOffer());
    mail7.setSenderDisplayName('Lexus Australia');
    mail7.setReplyTo('noreply@lexus.com.au');   
    
    // Set up email list #8
    Messaging.MassEmailMessage mail8 = new Messaging.MassEmailMessage();
    mail8.setTemplateId(SPCacheEmailTemplateMetadata.getTemplateIdTemplateIDNXPreLaunch());
    mail8.setSenderDisplayName('Lexus Australia');
    mail8.setReplyTo('noreply@lexus.com.au');   

    // Set up email list #9, Vehicle Information
    Messaging.MassEmailMessage mail9 = new Messaging.MassEmailMessage();
    mail9.setTemplateId(SPCacheEmailTemplateMetadata.getTemplateIdMassEmail());
    mail9.setSenderDisplayName('Lexus Australia');
    mail9.setReplyTo('noreply@lexus.com.au');         

    // Set up email list #10, Parts, Services, Warranty
    Messaging.MassEmailMessage mail10 = new Messaging.MassEmailMessage();
    mail10.setTemplateId(SPCacheEmailTemplateMetadata.getTemplateIdMassEmail());
    mail10.setSenderDisplayName('Lexus Australia');
    mail10.setReplyTo('noreply@lexus.com.au');    

    // Set up email list #11, Lexus Financial Services
    Messaging.MassEmailMessage mail11 = new Messaging.MassEmailMessage();
    mail11.setTemplateId(SPCacheEmailTemplateMetadata.getTemplateIdMassEmail());
    mail11.setSenderDisplayName('Lexus Australia');
    mail11.setReplyTo('noreply@lexus.com.au');    

    // Set up email list #11, Lexus Financial Services
    Messaging.MassEmailMessage mail12 = new Messaging.MassEmailMessage();
    mail12.setTemplateId(SPCacheEmailTemplateMetadata.getTemplateIdMassEmail());
    mail12.setSenderDisplayName('Lexus Australia');
    mail12.setReplyTo('noreply@lexus.com.au');  
    
    
    for (string strCaseType : map_CaseTypetoCase.keySet())
    {
        //TOOD Case sCase = map_CaseTypetoCase.get(strCaseType);
        for (Case sCase : map_CaseTypetoCase.get(strCaseType)) {
        
	        if (sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeAccessoriesEnquiry() && !map_AccounttoToList.isEmpty())
	        {
	            set<id> set_ToList = map_AccounttoToList.get(sCase.Selected_Dealer__c);
	            
	            for (id idContactId : set_ToList)
	            {
	                li_ContactIds1.add(idContactId); 
	                li_CaseIds1.add(sCase.Id); 
	            }
	        }
	
	        else if (sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeBuildandPriceEnquiry() && !map_AccounttoToList.isEmpty())
	        {
	            set<id> set_ToList = map_AccounttoToList.get(sCase.Selected_Dealer__c);
	            
	            for (id idContactId : set_ToList)
	            {
	                li_ContactIds2.add(idContactId);
	                li_CaseIds2.add(sCase.Id); 
	            }
	        }
	
	        else if (
	        	sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeGeneralEnquiry() && !map_AccounttoToList.isEmpty())
	        {
	            set<id> set_ToList = map_AccounttoToList.get(sCase.Selected_Dealer__c);
	            
	            for (id idContactId : set_ToList)
	            {
	                li_ContactIds3.add(idContactId);
	                li_CaseIds3.add(sCase.Id);
	            }
	        }

	        else if (sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypePersonalEnquiry() && !map_AccounttoToList.isEmpty())
	        {
	            set<id> set_ToList = map_AccounttoToList.get(sCase.Selected_Dealer__c);
	            
	            for (id idContactId : set_ToList)
	            {
	                li_ContactIds4.add(idContactId);
	                li_CaseIds4.add(sCase.Id);
	            }
	        }
	        
	
	        else if (sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeNewVehicleInterest() && !map_AccounttoToList.isEmpty())
	        {
	            set<id> set_ToList = map_AccounttoToList.get(sCase.Selected_Dealer__c);
	            
	            for (id idContactId : set_ToList)
	            {
	                li_ContactIds5.add(idContactId);
	                li_CaseIds5.add(sCase.Id);
	            }
	        }
	        
	        else if (sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeConciergeOffer() && !string.isBlank(SPCacheEmailTemplateMetadata.getConciergeEmailContactID()))
	        {
	            li_ContactIds6.add(SPCacheEmailTemplateMetadata.getConciergeEmailContactID());
	            li_CaseIds6.add(sCase.Id);
	        }
	        
	        else if (sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeDomPerignonOffer() && !map_AccounttoToList.isEmpty())
	        {
	            set<id> set_ToList = map_AccounttoToList.get(sCase.Selected_Dealer__c);
	            
	            for (id idContactId : set_ToList)
	            {
	                if (setDomPerignonOfferDistributionGroup.contains(idContactId))
	                {
	                    li_ContactIds7.add(idContactId);
	                    li_CaseIds7.add(sCase.Id);
	                }
	            }
	        } 
	        else if (sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeNXPreLaunch() && !map_AccounttoToList.isEmpty())
	        {
	            set<id> set_ToList = map_AccounttoToList.get(sCase.Selected_Dealer__c);
	            
	            for (id idContactId : set_ToList)
	            {
	                li_ContactIds8.add(idContactId);
	                li_CaseIds8.add(sCase.Id);
	            }
	        } 
	        else if (sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeVehicleInformation() && !map_AccounttoToList.isEmpty())
	        {
	            set<id> set_ToList = map_AccounttoToList.get(sCase.Selected_Dealer__c);
	            
	            for (id idContactId : set_ToList)
	            {
	                li_ContactIds9.add(idContactId);
	                li_CaseIds9.add(sCase.Id);
	            }
	        } 

	        else if (sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypePartsServicesWarranty() && !map_AccounttoToList.isEmpty())
	        {
	            set<id> set_ToList = map_AccounttoToList.get(sCase.Selected_Dealer__c);
	            
	            for (id idContactId : set_ToList)
	            {
	                li_ContactIds10.add(idContactId);
	                li_CaseIds10.add(sCase.Id);
	            }
	        } 

	        else if (sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeLexusFinancialServices() && !map_AccounttoToList.isEmpty())
	        {
	            set<id> set_ToList = map_AccounttoToList.get(sCase.Selected_Dealer__c);
	            
	            for (id idContactId : set_ToList)
	            {
	                li_ContactIds11.add(idContactId);
	                li_CaseIds11.add(sCase.Id);
	            }
	        } 	        
	        
	        else if (sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeFeedback() && !map_AccounttoToList.isEmpty())
	        {
	            set<id> set_ToList = map_AccounttoToList.get(sCase.Selected_Dealer__c);
	            
	            for (id idContactId : set_ToList)
	            {
	                li_ContactIds12.add(idContactId);
	                li_CaseIds12.add(sCase.Id);
	            }
	        } 		        
	        
	        	        
        }      
    }

    if (!li_ContactIds1.isEmpty())
    {
        // Target object Ids for merge fields
        mail1.setTargetObjectIds(li_ContactIds1);
        mail1.setWhatIds(li_CaseIds1);

        // Send the email
        Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail1 });
    }


    if (!li_ContactIds2.isEmpty())
    {
        // Target object Ids for merge fields
        mail2.setTargetObjectIds(li_ContactIds2);
        mail2.setWhatIds(li_CaseIds2);

        // Send the email
        Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail2 });
    }


    if (!li_ContactIds3.isEmpty())
    {
        // Target object Ids for merge fields
        mail3.setTargetObjectIds(li_ContactIds3);
        mail3.setWhatIds(li_CaseIds3);

        // Send the email
       Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail3 });
    }

/* 
// Request from LIDA.
// All Case created under Personal Preview/Test Drive Case Type will not get Mass email
// 19/05/2015 M&a

//Roll Back. 27/05/2015 M&a

    if (!li_ContactIds4.isEmpty())
    {
        // Target object Ids for merge fields
        mail4.setTargetObjectIds(li_ContactIds4);
        mail4.setWhatIds(li_CaseIds4);

        // Send the email
        Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail4 });
    }
*/
// Roll out. 29/05/2015

    if (!li_ContactIds5.isEmpty())
    {
        // Target object Ids for merge fields
        mail5.setTargetObjectIds(li_ContactIds5);
        mail5.setWhatIds(li_CaseIds5);

        // Send the email
        Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail5 });
    }
    
    if (!li_ContactIds6.isEmpty())
    {
        // Target object Ids for merge fields
        mail6.setTargetObjectIds(li_ContactIds6);
        mail6.setWhatIds(li_CaseIds6);

        // Send the email
        Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail6 });
    }
    
    if (!li_ContactIds7.isEmpty())
    {
        // Target object Ids for merge fields
        mail7.setTargetObjectIds(li_ContactIds7);
        mail7.setWhatIds(li_CaseIds7);

        // Send the email
        Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail7 });
    }

    if (!li_ContactIds8.isEmpty())
    {
        // Target object Ids for merge fields
        mail8.setTargetObjectIds(li_ContactIds8);
        mail8.setWhatIds(li_CaseIds8);

        // Send the email
        Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail8 });
    }
    

    //Vehicle Information
    if (!li_ContactIds9.isEmpty())
    {
       	// Target object Ids for merge fields
        mail9.setTargetObjectIds(li_ContactIds9);
        mail9.setWhatIds(li_CaseIds9);

        // Send the email
        Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail9 });
    } 

     //Parts, Services, Warranty
    if (!li_ContactIds10.isEmpty())
    {
       	// Target object Ids for merge fields
        mail10.setTargetObjectIds(li_ContactIds10);
        mail10.setWhatIds(li_CaseIds10);

        // Send the email
        Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail10 });
    } 
 
     //Lexus Financial Services
    if (!li_ContactIds11.isEmpty())
    {
       	// Target object Ids for merge fields
        mail11.setTargetObjectIds(li_ContactIds11);
        mail11.setWhatIds(li_CaseIds11);

        // Send the email
        Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail11 });
    } 
    
     //Feedback
    if (!li_ContactIds12.isEmpty())
    {
       	// Target object Ids for merge fields
        mail12.setTargetObjectIds(li_ContactIds12);
        mail12.setWhatIds(li_CaseIds12);

        // Send the email
        Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail12 });
    } 
           
    

}