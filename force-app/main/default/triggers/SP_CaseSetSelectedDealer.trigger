trigger SP_CaseSetSelectedDealer on Case (before insert)
{
	// Primarily for use by third-party applications (website, iPads) which do not know the Dealer Ids
	// This trigger finds the appropriate Dealer record for the Dealer Name provided

	// Build a map of Dealer Accounts and their database IDs
	map<string, id> map_DealerAccountNametoId = new map<string, id>();

	Account [] arrDealerAccount = [
										select	Id,
												Name
										from	Account
										where	RecordTypeId = :SPCacheRecordTypeMetadata.getAccount_Dealer()
										and		Status__c = 'Active'
									];

	for (Account sAccount : arrDealerAccount)
	{
		map_DealerAccountNametoId.put(sAccount.Name, sAccount.Id);
	}

	// Build a map of Prospect Accounts referenced by these new Cases
	set<id>				set_ProspectContactIds	= new set<id>();
	map<id, Account>	map_ProspectAccounts	= new map<id, Account>();
	list<Account>		li_AccountstoUpdate		= new list<Account>();
	
	for (Case sCase : trigger.new)
	{
		if (sCase.ContactId != null)
		{
			set_ProspectContactIds.add(sCase.ContactId);
		}
	}

	Account [] arrProspectAccount = [
										select	Id,
												PersonContactId,
												Preferred_Dealer__c
										from	Account
										where	RecordTypeId = :SPCacheRecordTypeMetadata.getPersonAccountProspectRecordType()
										and		PersonContactId in :set_ProspectContactIds
									];

	for (Account sAccount : arrProspectAccount)
	{
		map_ProspectAccounts.put(sAccount.PersonContactId, sAccount);
	}

	for (integer i = 0; i < trigger.new.size(); i++)
	{
		if	(	trigger.new[i].Selected_Dealer_Name__c != null &&
				trigger.new[i].Selected_Dealer_Name__c != '' &&
				trigger.new[i].Selected_Dealer__c == null
			)
		{
			if (map_DealerAccountNametoId.containsKey(trigger.new[i].Selected_Dealer_Name__c))
			{
				trigger.new[i].Selected_Dealer__c = map_DealerAccountNametoId.get(trigger.new[i].Selected_Dealer_Name__c);

				if (map_ProspectAccounts.containsKey(trigger.new[i].ContactId))
				{
					Account sAccount = map_ProspectAccounts.get(trigger.new[i].ContactId);
					sAccount.Preferred_Dealer__c = map_DealerAccountNametoId.get(trigger.new[i].Selected_Dealer_Name__c);
					
					li_AccountstoUpdate.add(sAccount);
				}
			}
			else
			{
				trigger.new[i].Selected_Dealer_Name__c.AddError('Unable to find this Dealer on the Salesforce database') ;
			}
		}
	}

	if (!li_AccountstoUpdate.isEmpty())
	{
		SP_Utilities.AccountUpdatedByCaseSetSelTrigger = true;
		update li_AccountstoUpdate;
	}
}