trigger SP_ServiceRegoAssetServiceDate on Service__c (before insert, after insert, after update) 
{
	// Build a set of Asset Ids for the new / updated Service records
	set<string> 			set_AssetIds				= new set<string>();

	for(Service__c sService : trigger.new)
	{
		if (sService.Asset__c != null)
			set_AssetIds.add(sService.Asset__c);
	}

	// For new Service records only - update a blank Rego with one from the Asset
	if (trigger.isBefore && trigger.isInsert)
	{
		// Map of Asset Ids to Registration numbers
		map<string, string>		map_AssettoRego				= new map<string, string>();
	
		Vehicle_Ownership__c[] arrVO = [	select	AssetID__c,
													Vehicle_Rego_Number__c
											from	Vehicle_Ownership__c
											where	AssetID__c in :set_AssetIds
											and		Status__c = 'Active'
											];
	
		for (Vehicle_Ownership__c sVO : arrVO)
		{
			map_AssettoRego.put(sVO.AssetID__c, sVO.Vehicle_Rego_Number__c);
		}

		for(Service__c sService : trigger.new)
		{
			if (map_AssettoRego.containsKey(sService.Asset__c) && sService.Vehicle_Rego_Number__c == null)
			{
				sService.Vehicle_Rego_Number__c = map_AssettoRego.get(sService.Asset__c);
			}
		}
	}

	// For after insert / after update, update the Asset object with the latest Service date
	if (trigger.isAfter && (trigger.isInsert || trigger.isUpdate))
	{
		// List of Assets for the Service records
		list<Asset__c>	li_Assets		= new list<Asset__c>();

		Asset__c[] arrAsset =	[	select	id,
											Latest_Service__c
									from	Asset__c
									where	id in :set_AssetIds
								];
		for (Asset__c sAsset : arrAsset)
		{
			li_Assets.add(sAsset);
		}

		// Map of latest Service records for these Assets
		map<id, Service__c>	map_AssettoLatestService	= new map<id, Service__c>();

		Service__c [] arrService =	[	select	Id,
												Asset__c,
												Service_Date__c
										from	Service__c
										where	Asset__c in :set_AssetIds
									];
		for (Service__c sService : arrService)
		{
			if (map_AssettoLatestService.containsKey(sService.Asset__c))
			{
				if (map_AssettoLatestService.get(sService.Asset__c).Service_Date__c
					< sService.Service_Date__c)
				{
					map_AssettoLatestService.remove(sService.Asset__c);
					map_AssettoLatestService.put(sService.Asset__c, sService);
				}
			}
			else
			{
				map_AssettoLatestService.put(sService.Asset__c, sService);
			}
		}

		// List of Assets to be updated
		list<Asset__c>	li_AssetstobeUpdate		= new list<Asset__c>();

		for (Asset__c sAsset : li_Assets)
		{
			if (map_AssettoLatestService.containsKey(sAsset.Id))
			{
				sAsset.Latest_Service__c = map_AssettoLatestService.get(sAsset.Id).Id;
				
				li_AssetstobeUpdate.add(sAsset);
			}
		}

		if (!li_AssetstobeUpdate.isEmpty())
		{
			SP_Utilities.AssetUpdatedByServRegoAssetServDateTrigger = true;
			update li_AssetstobeUpdate;
		}
	}


	// Update the Last Servicing Dealer field on the Owner Account
	if (trigger.isAfter)
	{
		// Map of Account Ids to Services
		map<id, id> map_AccounttoServicingDealer = new map<id, id>();

		for (Service__c sService : trigger.new)
		{
			if (sService.Customer_Account__c != null && sService.Servicing_Dealer_Branch__c != null)
			{
				map_AccounttoServicingDealer.put(sService.Customer_Account__c, sService.Servicing_Dealer_Branch__c);
			}
		}

		// List of Accounts to update
		list<Account> li_AccountstoUpdate = new list<Account>();
	
		Account[] arrAccount=	[	select	Id,
											Servicing_Dealer__c,
											Preferred_Dealer__c,
											Override_Preferred_Dealer__c
									from	Account
									where	Id in :map_AccounttoServicingDealer.keySet()
								];
	
		for (Account sAccount : arrAccount)
		{
			sAccount.Servicing_Dealer__c = map_AccounttoServicingDealer.get(sAccount.Id);

			string strPreferredDealer = sAccount.Preferred_Dealer__c;
			// Update the Selected Dealer too?
			if (	(	sAccount.Preferred_Dealer__c == null ||
						strPreferredDealer.substring(0, 15) == SPCacheRecordTypeMetadata.getLexusAustraliaRecordID().substring(0, 15)
					) && !sAccount.Override_Preferred_Dealer__c
				)
			{
				system.debug('############## TEST BY SHABU : SP_ServiceRegoAssetServiceDate ' + sAccount);
				sAccount.Preferred_Dealer__c = map_AccounttoServicingDealer.get(sAccount.Id);
			}

			li_AccountstoUpdate.add(sAccount);
		}

		if (!li_AccountstoUpdate.isEmpty())
		{
			SP_Utilities.AccountUpdatedByServRegoAssetServDateTrigger = true;
			update li_AccountstoUpdate;
		}
	}
}