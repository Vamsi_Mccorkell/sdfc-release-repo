/*******************************************************************************
@author:		Donnie Banez
@date:         	Aug 2019
@description:  	Trigger for Offer 
@Test Methods:	CustomerOfferTransactionTriggerHandlerTest 
*******************************************************************************/
trigger CustomerOfferTransactionTrigger on Customer_Offer_Transaction__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    // Call Contact Trigger Handler 
    CustomerOfferTransactionTriggerHandler.mainEntry(
        Trigger.isBefore, Trigger.isAfter, 
        Trigger.isInsert, Trigger.isUpdate, 
        Trigger.isDelete, Trigger.isUnDelete,  
        Trigger.new, Trigger.old,
        Trigger.newMap, Trigger.oldMap 
    );
}