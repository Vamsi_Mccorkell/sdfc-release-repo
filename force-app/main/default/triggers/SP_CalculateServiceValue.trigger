trigger SP_CalculateServiceValue on Service__c (after insert, after update)
{
	map<id, id>			map_oldAccountIds				= new map<id, id>();
	map<id, Account>	map_OldAccounts					= new map<id, Account>();
	map<id, Service__c>	map_OldAccountsToService		= new map<id, Service__c>();
	map<id, Service__c>	map_OldAccountsLosingService	= new map<id, Service__c>();
	list<id>			li_oldAccountIds				= new list<id>();

	map<id, id>			map_newAccountIds				= new map<id, id>();
	map<id, Account>	map_NewAccounts					= new map<id, Account>();
	map<id, Service__c>	map_NewAccountsToService		= new map<id, Service__c>();

	// Process the new / changed records now and adjust the lifetime service value

	// Build a map of Account ids from the New keyset
	for (Integer i = 0; i < Trigger.new.size(); i++)
	{
		if (Trigger.new[i].Customer_Account__c != null)
		{
			map_newAccountIds.put(Trigger.new[i].Customer_Account__c, Trigger.new[i].Customer_Account__c);
		}
	}

	// Build a map of all Accounts to be updated
	Account[] arrAccount =	[
								select	Id,
										Lastname,
										Service_Value__c,
										Preferred_Dealer__c,
										Override_Preferred_Dealer__c
								from	Account
								where	Id in :map_newAccountIds.keyset()
							];
	
	for (Account sAccount : arrAccount)
	{
		map_NewAccounts.put(sAccount.Id, sAccount);
	}

	// Build a map of all new Accounts and their most recent Service record
	Service__c[] arrNewService =	[
									select	Customer_Account__c,
											Asset__r.Name,				// = VIN
											Service_Date__c,
											Servicing_Dealer_Branch__c
									from	Service__c
									where	Customer_Account__c in :map_newAccountIds.keyset()
									order by Service_Date__c desc
									limit	200
								];
	
	for (Service__c sService : arrNewService)
	{
		// We now have all Service records for this Account
		// Need to remove Services for other vehicles and also the latest Service for this Vehicle		
		for (integer i = 0; i < Trigger.new.size(); i++)
		{
			if (Trigger.new[i].Asset__r.Name == sService.Asset__r.Name &&
				Trigger.new[i].Id != sService.Id)
			{
				map_NewAccountsToService.put(sService.Customer_Account__c, sService);
			}
		}
	}

	if (trigger.isUpdate)
	{
		// Only adjust the service value on an update if the Service record is being linked to another Account
		// Build a map of Account ids from the Old keyset

		for (Integer i = 0; i < Trigger.old.size(); i++)
		{
			if (Trigger.old[i].Customer_Account__c != null)
			{
				map_oldAccountIds.put(Trigger.old[i].Customer_Account__c, Trigger.old[i].Customer_Account__c);
			}
		}

		// Build a map of Account ids from the Old keyset that are not in the New keyset
		for (Integer i = 0; i < Trigger.new.size(); i++)
		{
			if (map_oldAccountIds.containsKey(Trigger.new[i].Customer_Account__c))
			{
				map_oldAccountIds.remove(Trigger.new[i].Customer_Account__c);
			}
		}

		if (!map_oldAccountIds.isEmpty() && !Trigger.oldMap.isEmpty())
		{
			// Now we can get only those Accounts which are having a Service record assigned AWAY from them
			Account[] arrOldAccount =	[
											select	Id,
													Lastname,
													Service_Value__c 
											from	Account
											where	Id in :map_oldAccountIds.keyset()
										];
			
			for (Account sAccount : arrOldAccount)
			{
				map_OldAccounts.put(sAccount.Id, sAccount);
				li_oldAccountIds.add(sAccount.Id);
			}
	
			// Build a map of all old Accounts and their most recent Service record
			Service__c[] arrOldService =	[
											select	Customer_Account__c,
													Asset__r.Name,				// = VIN
													Service_Date__c
											from	Service__c
											where	Customer_Account__c in :map_oldAccountIds.keyset()
											order by Service_Date__c desc
											limit	200
										];
			
			for (Service__c sService : arrOldService)
			{
				// We now have all Service records for this Account
				// Need to remove Services for other vehicles and also the latest Service for this Vehicle		
				for (integer i = 0; i < Trigger.old.size(); i++)
				{
					if (Trigger.old[i].Asset__r.Name == sService.Asset__r.Name &&
						Trigger.old[i].Id != sService.Id)
					{
						map_OldAccountsToService.put(sService.Customer_Account__c, sService);
					}
				}
			}
		}
	}

	// Now iterate through the trigger data and update the service value in the Account map
	// Exclude records where the service date is less than 9 months after the last service

	// New records first
	for (Integer i = 0; i < Trigger.new.size(); i++)
	{
		date dtNineMonthsSinceLastService;

		if (map_NewAccountsToService.containsKey(Trigger.new[i].Customer_Account__c))
		{
			Service__c sService = map_NewAccountsToService.get(Trigger.new[i].Customer_Account__c);
	
			if (sService.Service_Date__c.Month() < 4)
			{
				dtNineMonthsSinceLastService = sService.Service_Date__c.addMonths(9);
			}
			else
			{
				dtNineMonthsSinceLastService = sService.Service_Date__c.addMonths(-3).addYears(1);
			}
		}

		if (	dtNineMonthsSinceLastService == null ||										// no previous service record found
				!(Trigger.new[i].Service_Date__c < dtNineMonthsSinceLastService))			// Date in range
		{
			if (Trigger.new[i].Customer_Account__c != null)
			{
				Account sAccount = new Account();
				sAccount = map_NewAccounts.get(Trigger.new[i].Customer_Account__c);

				if (Trigger.new[i].Calculated_Service_Value__c != null)
				{
					// Check to see if this is the first service
					if (sAccount.Service_Value__c != null && sAccount.Service_Value__c != 0)
					{
						sAccount.Service_Value__c += Trigger.new[i].Calculated_Service_Value__c;
					}
					else
					{
						sAccount.Service_Value__c = Trigger.new[i].Calculated_Service_Value__c;
					}
	
					map_NewAccounts.remove(Trigger.new[i].Customer_Account__c);
					map_NewAccounts.put(Trigger.new[i].Customer_Account__c, sAccount);
				}
			}
		}
	}


	if (trigger.isUpdate)
	{
		// Now add the Service records that are being transferred
		for (Integer i = 0; i < Trigger.old.size(); i++)
		{
			for (Integer j = 0; j < li_oldAccountIds.size(); j++)
			{
				if (Trigger.old[i].Customer_Account__c == li_oldAccountIds[j])
				{
					map_OldAccountsLosingService.put(li_oldAccountIds[j], Trigger.old[i]);
				}
			}
		}

	
		// Now those Accounts having Service records removed
		for (Integer i = 0; i < li_oldAccountIds.size(); i++)
		{
			// This is the Service record from the old trigger map
			// We use the old one - even though the new version may have a different calculated Invoice total -
			// because the value on this is what was added to the Account in the past 
			Service__c sServiceBeingTransferred = map_OldAccountsLosingService.get(li_oldAccountIds[i]);

			// This is the last service for the old Account - other than the one being transferred
			Service__c sLastServiceForOldAccount = map_OldAccountsToService.get(li_oldAccountIds[i]);

			date dtNineMonthsSinceLastService;
			if (sLastServiceForOldAccount != null)
			{
				if (sLastServiceForOldAccount.Service_Date__c.Month() < 4)
				{
					dtNineMonthsSinceLastService = sLastServiceForOldAccount.Service_Date__c.addMonths(9);
				}
				else
				{
					dtNineMonthsSinceLastService = sLastServiceForOldAccount.Service_Date__c.addMonths(-3).addYears(1);
				}
			}
	
			if (	dtNineMonthsSinceLastService == null ||												// no previous service record found
					!(sServiceBeingTransferred.Service_Date__c < dtNineMonthsSinceLastService))			// Date in range
			{
				Account sAccount = new Account();
				sAccount = map_OldAccounts.get(li_oldAccountIds[i]);
	
				if (sServiceBeingTransferred.Calculated_Service_Value__c != null)
				{
					sAccount.Service_Value__c -= sServiceBeingTransferred.Calculated_Service_Value__c;
	
					map_OldAccounts.remove(li_oldAccountIds[i]);
					map_OldAccounts.put(li_oldAccountIds[i], sAccount);
				}
			}
		}
	}


	// Are we loading Service records from Integration?
	if (Userinfo.getUserId() == SPCacheRecordTypeMetadata.getIntegrationUserID())
	{

		// Check if this is the first owner transaction with a Dealer since Preferred Dealer was set to Lexus Australia
		for (Integer i = 0; i < Trigger.new.size(); i++)
		{
			if (map_NewAccounts.containsKey(Trigger.new[i].Customer_Account__c) &&
				map_NewAccountsToService.containsKey(Trigger.new[i].Customer_Account__c))
			{
				Account sAccount = map_NewAccounts.remove(Trigger.new[i].Customer_Account__c);
				Service__c sService = map_NewAccountsToService.get(Trigger.new[i].Customer_Account__c);
	
				// Must also NOT update if the 'Override Preferred Dealer' option is selected		
				if (sAccount.Preferred_Dealer__c == SPCacheRecordTypeMetadata.getLexusAustraliaRecordID() &&
					!sAccount.Override_Preferred_Dealer__c &&
					sService.Servicing_Dealer_Branch__c != null)
				{
					sAccount.Preferred_Dealer__c = sService.Servicing_Dealer_Branch__c;
				}
	
				map_NewAccounts.put(Trigger.new[i].Customer_Account__c, sAccount);
			}
		}
	}
	

	// Finally, build a list of Accounts to update
	list<Account> li_Accounts = map_OldAccounts.values();
	li_Accounts.addAll(map_NewAccounts.values());

	try
	{
		if (!li_Accounts.isEmpty())
			SP_Utilities.AccountUpdatedByCalcServValTrigger = true;
			
		update li_Accounts;
	}
	catch (exception e)
	{
		// No action
	}

}