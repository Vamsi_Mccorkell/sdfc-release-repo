/*******************************************************************************
@author:		Donnie Banez
@date:         	August 2019
@description:  	Trigger for Account 
@Test Methods:	AccountTriggerHandlerTest 
*******************************************************************************/
trigger AccountTrigger on Account (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    // Call Customer Offer Detail Trigger Handler 
    AccountTriggerHandler.mainEntry(
        Trigger.isBefore, Trigger.isAfter, 
        Trigger.isInsert, Trigger.isUpdate, 
        Trigger.isDelete, Trigger.isUnDelete,  
        Trigger.new, Trigger.old,
        Trigger.newMap, Trigger.oldMap 
    );
}