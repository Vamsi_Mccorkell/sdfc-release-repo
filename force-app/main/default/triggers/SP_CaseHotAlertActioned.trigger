trigger SP_CaseHotAlertActioned on Case (after update)
{
		set<id>				set_VINs					= new set<id>();
		map<id, Service__c>	map_AssetToLatestService	= new map<id, Service__c>();
		map<id,string>		map_AssetToVIN				= new map<id, string>();

	for (Case sCase : Trigger.new)
	{
		if (sCase.RecordTypeId == SPCachePotentiateMetadata.getHotAlertRecordTypeID() && sCase.Asset__c != null)
		{
			set_VINs.add(sCase.Asset__c);
		}
	}

	if (!set_VINs.isEmpty())
	{
		// Build a map of the most recent Service records
		Service__c [] arrService =	[
										select	Customer_Account__c,
												Asset__r.Name,				// = VIN
												Service_Date__c
										from	Service__c
										where	Asset__c in :set_VINs
										order by Service_Date__c desc
										limit	200
									];
		
		for (Service__c sService : arrService)
		{
			// We now have all Service records for this VO - save the latest
			if (sService.Service_Date__c != null)
			{
				map_AssetToLatestService.put(sService.Asset__c, sService);
			}
		}

		// Build a map of the Asset names (VINs)
		Asset__c [] arrAsset =	[
										select	Id,
												Name				// = VIN
										from	Asset__c
										where	Id in :set_VINs
									];
		
		for (Asset__c sAsset : arrAsset)
		{
			map_AssetToVIN.put(sAsset.Id, sAsset.Name);
		}
	}

	for (Case sCase : Trigger.new)
	{
		if (sCase.RecordTypeId == SPCachePotentiateMetadata.getHotAlertRecordTypeID())
		{
			string strAlertType;
			date dtServiceDate;
			string strServiceDate = '';
			string strActioned = '0';
			string strComments = '';

			if (sCase.Type == SPCachePotentiateMetadata.getCaseTypeSales())
			{
				strAlertType = '1';
			}
			else if (sCase.Type == SPCachePotentiateMetadata.getCaseTypeService())
			{
				strAlertType = '2';

				if (map_AssetToLatestService.containsKey(sCase.Asset__c))
				{
					dtServiceDate = map_AssetToLatestService.get(sCase.Asset__c).Service_Date__c;
					strServiceDate = dtServiceDate.day() + '/';
					string strMonth = '' + dtServiceDate.month();

					if (strMonth.length() == 1)
					{
						strServiceDate += '0' + strMonth;
					}
					else
					{
						strServiceDate += strMonth;
					}

					strServiceDate += '/' + dtServiceDate.year();
				}
			}

			string strVIN = '';

			if (sCase.Asset__c != null && map_AssetToVIN.containsKey(sCase.Asset__c))
			{
				strVIN = map_AssetToVIN.get(sCase.Asset__c);
			}

			if (sCase.Status == 'Closed')
			{
				strActioned = '1';
			}

			if (sCase.Description != null && sCase.Description != '')
			{
				strComments = sCase.Description.replaceAll(' ', '%20');
			}

			// make the asynchronous web service callout
			try
			{
				SP_PotentiateWebServices.TriggerHotAlertActionedRequest	(	strAlertType, 
																			strVIN,
																			strActioned,
																			strComments,
																			strServiceDate
																		);
			}
			catch (exception e)
			{
				
			}
		}
	}
}