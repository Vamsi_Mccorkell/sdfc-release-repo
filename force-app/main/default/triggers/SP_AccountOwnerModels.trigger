/***************************************
*    Last Modified by : Rakesh Muppiri - Valuelabs
*    Last Update      : lines 66-70, to avoid error while updating account field 'Models_Owned__c'
****************************************/

trigger SP_AccountOwnerModels on Account (before update)
{
    // Map of Account Ids to a set of Model names
    map<id, set<string>>    map_AccounttoOwnerModels    = new map<id, set<string>>();


    // Get all the VO records for these Accounts
    for (Vehicle_Ownership__c [] arrVOs :   [   select  id,
                                                        Customer__c,
                                                         AssetID__r.Vehicle_Model__r.Name
                                                from    Vehicle_Ownership__c
                                                where   Customer__c in :trigger.newMap.keySet()
                                            ])
    {
        for (Vehicle_Ownership__c sVO : arrVOs)
        {
            string strModelName = sVO.AssetID__r.Vehicle_Model__r.Name;
            
            if (!map_AccounttoOwnerModels.containsKey(sVO.Customer__c))
            {
                set<string> set_OwnerModels = new set<string>();
                set_OwnerModels.add(strModelName);
                map_AccounttoOwnerModels.put(sVO.Customer__c, set_OwnerModels);
            }
            else
            {
                set<string> set_OwnerModels = map_AccounttoOwnerModels.get(sVO.Customer__c);
                set_OwnerModels.add(strModelName);
                map_AccounttoOwnerModels.put(sVO.Customer__c, set_OwnerModels);
            }
        }
    }

    for (Account sAccount : trigger.new)
    {
        if (map_AccounttoOwnerModels.containsKey(sAccount.Id))
        {
            set<string> set_OwnerModels = map_AccounttoOwnerModels.get(sAccount.Id);
            list<string> li_OwnerModels = new list<string>();
        
            for (string strOwnerModels : set_OwnerModels)
            {
                li_OwnerModels.add(strOwnerModels);
            }
        
            li_OwnerModels.sort();
        
            sAccount.Models_Owned__c = '';
                    
            for (string strOwnerModels : li_OwnerModels)
            {
                sAccount.Models_Owned__c += strOwnerModels;
                sAccount.Models_Owned__c += ' ';
            }
        
            // Back up one place to remove the trailing space
            sAccount.Models_Owned__c = sAccount.Models_Owned__c.substring(0, sAccount.Models_Owned__c.length() - 1);
            
            //Below code truncates the data to 255 character only
            if(sAccount.Models_Owned__c.length() >254){
                sAccount.Models_Owned__c = sAccount.Models_Owned__c.subString(0,251);
                sAccount.Models_Owned__c = sAccount.Models_Owned__c.subString(0, sAccount.Models_Owned__c.lastIndexOf(' '))+'...';
            }
            
            //Below lines of code added by Rakesh Muppiri Valuelabs to truncate charater limit to 255 only
            SP_Utilities.AccountUpdatedByOwnerModelsTrigger = true;
        }
    }
}