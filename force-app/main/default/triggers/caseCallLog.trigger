trigger caseCallLog on Case (after insert, after update) 
{
//	This trigger automatically creates a task when a new case is created
	List<Task>		li_TasksForInsert = new List<Task>();
	list<Contact>	li_ContactsToUpdate = new list<Contact>();

	set<Id>			set_CustomerIds = new set<Id>();

	// Contact Id and fields to update description
	map<Id, String>	map_ContactToDescription = new map<Id, String>();

	for (Case sCase: Trigger.new)
	{
		String strComments = '';
		if (sCase.Description != null)
		{
			strComments = sCase.Type + '; ' + sCase.Subject + '; ' + sCase.Description;
		}
		else
		{
			strComments = sCase.Type + '; ' + sCase.Subject;
		}

		//for each new case create follow up task
		if (trigger.isInsert)
		{
			Task sTask = new Task();

			if (sCase.CreatedById == SPCacheRecordTypeMetadata.getWebUserId())
			{
				sTask.Subject	= 'Website Activity';
			}
			else
			{
				sTask.Subject	= 'Auto Call Log';
			}
			
			sTask.ActivityDate	= System.today();
			sTask.Status		= 'Completed'; 
			sTask.Type			= sCase.Origin;
			sTask.WhatId		= sCase.Id;
			sTask.WhoId			= sCase.ContactId;
			sTask.OwnerId		= sCase.OwnerId;
			sTask.Description	= strComments;

			li_TasksForInsert.add(sTask);
		}
			
		if (map_ContactToDescription.containsKey(sCase.ContactId))
		{
			if (map_ContactToDescription.get(sCase.ContactId)== 'update prospect status')
			{
				if (	(	sCase.Type == 'New Brochure Request' ||
							sCase.Type == 'Reissue Brochure Request' ||
							sCase.Type == 'eBrochure Request'
						) && sCase.isClosed
					)
				{
					continue;
				}

				if (	(	sCase.Type == 'Welcome Pack' ||
							sCase.Type == 'Reissue Welcome Pack'
						) && sCase.isClosed
					) 
				{
					map_ContactToDescription.remove(sCase.ContactId);
					map_ContactToDescription.put(sCase.ContactId, 'update both');
				}
			}
			
			if (map_ContactToDescription.get(sCase.ContactId)== 'update welcome pack sent date')
			{
				if (	(	sCase.Type == 'New Brochure Request' ||
							sCase.Type == 'Reissue Brochure Request' ||
							sCase.Type == 'eBrochure Request'
						) && sCase.isClosed
					)
				{	map_ContactToDescription.remove(sCase.ContactId);
					map_ContactToDescription.put(sCase.ContactId, 'update both');
				}

				if (	(	sCase.Type == 'Welcome Pack' ||
							sCase.Type == 'Reissue Welcome Pack'
						) && sCase.isClosed
					) 
				{
					continue;
				}
			}
		}
		else
		{
			if (	(	sCase.Type == 'New Brochure Request' ||
						sCase.Type == 'Reissue Brochure Request' ||
						sCase.Type == 'eBrochure Request'
					) && sCase.isClosed
				)
			{
				map_ContactToDescription.put(sCase.ContactId, 'update prospect status');
			}

			if (	(	sCase.Type == 'Welcome Pack' ||
						sCase.Type == 'Reissue Welcome Pack'
					) && sCase.isClosed
				)
			{
				map_ContactToDescription.put(sCase.ContactId, 'update welcome pack sent date');
			}
		}

		//create contact Id list
		if (set_CustomerIds.contains(sCase.ContactId) == false)
		{
			set_CustomerIds.add(sCase.ContactId);
		}
	}

	insert li_TasksForInsert;
	
	Contact[] arrContact =	[
								select	Id,
										Welcome_Pack_Sent_Date__c,
										Prospect_Status__c
								from	Contact
								where	Id in :set_CustomerIds
							];

	for (Contact sContact: arrContact)
	{
		if (map_ContactToDescription.containsKey(sContact.Id))
		{
			if (map_ContactToDescription.get(sContact.Id)=='update prospect status' )
			{
				sContact.Prospect_Status__c='Brochure Request';
			}

			if (map_ContactToDescription.get(sContact.Id)=='update welcome pack sent date' )
			{
				sContact.Welcome_Pack_Sent_Date__c = date.today();
			}

			if (map_ContactToDescription.get(sContact.Id)=='update both')
			{
				sContact.Prospect_Status__c='Brochure Request';
				sContact.Welcome_Pack_Sent_Date__c = date.today();
			}
			
			li_ContactsToUpdate.add(sContact);
		}
	}

	update li_ContactsToUpdate;

}