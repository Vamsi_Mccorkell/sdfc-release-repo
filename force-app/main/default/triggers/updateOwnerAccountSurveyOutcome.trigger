trigger updateOwnerAccountSurveyOutcome on Survey__c (after insert) {
/*

	*** Superceded by SP_SurveyUpdateOwner ***

	for(Survey__c survey : Trigger.new){
		date surveyDate = survey.Survey_Date__c;
	    Account ownerAccount = [select Id, Next_Sales_Survey_Eligibility_Date__c, Next_Service_Survey_Eligibility_Date__c from Account where Id =:survey.Customer__c limit 1];
		//Account ownerAccount = [select Id, Next_Service_Survey_Eligibility_Date__c from Account where Id =:survey.Customer__c limit 1];
		//Contact ownerContact = [select Id from Contact where AccountId =:survey.Customer__c limit 1];
				
		//Service Surveys
		if(survey.Survey_Type__c == 'Service'){
			if(survey.Contact_Code__c == 'Completed' || survey.Contact_Code__c == 'Abandoned' || survey.Contact_Code__c == 'Language Barrier' || survey.Contact_Code__c == 'Language'){
				ownerAccount.Last_Survey_Date__c = surveyDate;
				ownerAccount.Next_Service_Survey_Eligibility_Date__c = surveyDate.addMonths(12);
				update ownerAccount;
			} else if(survey.Contact_Code__c == 'No Marketing'){
				Contact ownerContact = [select Id from Contact where AccountId =:survey.Customer__c limit 1];
				ownerAccount.Last_Survey_Date__c = surveyDate;
				ownerAccount.Next_Service_Survey_Eligibility_Date__c = null;
				ownerAccount.Next_Sales_Survey_Eligibility_Date__c = null;
				ownerContact.Do_Not_Contact__c = true;
				update ownerAccount;
				update ownerContact;
			} else if(survey.Contact_Code__c == 'No Survey'){
				ownerAccount.Last_Survey_Date__c = surveyDate;
				ownerAccount.Service_CS_Survey_Opt_Out__c = true;
				ownerAccount.Sales_CS_Survey_Opt_Out__c = true;
				ownerAccount.Next_Service_Survey_Eligibility_Date__c = null;
				ownerAccount.Next_Sales_Survey_Eligibility_Date__c = null;
				update ownerAccount;
			} else if(survey.Contact_Code__c == 'Deceased'){
				ownerAccount.Last_Survey_Date__c = surveyDate;
				ownerAccount.Deceased__c = true;
				ownerAccount.Next_Service_Survey_Eligibility_Date__c = null;
				ownerAccount.Next_Sales_Survey_Eligibility_Date__c = null;
				update ownerAccount;
			} else if(survey.Contact_Code__c == 'Unserviced' || survey.Contact_Code__c == 'Incorrect Number'){
				ownerAccount.Last_Survey_Date__c = surveyDate;
				if(ownerAccount.Next_Service_Survey_Eligibility_Date__c < surveyDate.addMonths(4) || ownerAccount.Next_Service_Survey_Eligibility_Date__c == null){
					ownerAccount.Next_Service_Survey_Eligibility_Date__c = surveyDate.addMonths(4);
				}
				update ownerAccount;
			}
		}
		
		//Sales Surveys
		if(survey.Survey_Type__c == 'Sales'){
			if(survey.Contact_Code__c == 'Completed' || survey.Contact_Code__c == 'Abandoned' || survey.Contact_Code__c == 'Language Barrier'){
				ownerAccount.Last_Survey_Date__c = surveyDate;
				ownerAccount.Next_Sales_Survey_Eligibility_Date__c = surveyDate.addMonths(12);
				update ownerAccount;
			} else if(survey.Contact_Code__c == 'No Marketing'){
				Contact ownerContact = [select Id from Contact where AccountId =:survey.Customer__c limit 1];
				ownerAccount.Last_Survey_Date__c = surveyDate;
				ownerAccount.Next_Sales_Survey_Eligibility_Date__c = null;
				ownerAccount.Next_Service_Survey_Eligibility_Date__c = null;
				ownerContact.Do_Not_Contact__c = true;
				update ownerAccount;
				update ownerContact;
			} else if(survey.Contact_Code__c == 'No Survey'){
				ownerAccount.Last_Survey_Date__c = surveyDate;
				ownerAccount.Sales_CS_Survey_Opt_Out__c = true;
				ownerAccount.Service_CS_Survey_Opt_Out__c = true;
				ownerAccount.Next_Sales_Survey_Eligibility_Date__c = null;
				ownerAccount.Next_Service_Survey_Eligibility_Date__c = null; 
				update ownerAccount;
			} else if(survey.Contact_Code__c == 'Deceased'){
				ownerAccount.Last_Survey_Date__c = surveyDate;
				ownerAccount.Next_Sales_Survey_Eligibility_Date__c = null;
				ownerAccount.Next_Service_Survey_Eligibility_Date__c = null;
				ownerAccount.Deceased__c = true;
				update ownerAccount;
			} else if(survey.Contact_Code__c == 'Incorrect Number'){
				ownerAccount.Last_Survey_Date__c = surveyDate;
				if(ownerAccount.Next_Sales_Survey_Eligibility_Date__c < surveyDate.addMonths(4) || ownerAccount.Next_Sales_Survey_Eligibility_Date__c == null){
					ownerAccount.Next_Sales_Survey_Eligibility_Date__c = surveyDate.addMonths(4);
				}
				update ownerAccount;
			}
		}
	}

*/
}