// production
// 012900000000XAOAA2 Model of Interst Case RT
// 012900000000UvSAAU Case Collateral RT
// 012900000000UIMAA2 Case General Enquiry
// 012900000000Uvh Case Sales Enquiry
// New Brochure Request - case type
// Reissue Brochure Request - case type

trigger caseSplitModels on Case (after insert, after update) {

	String strModel = '';
	Id[] caseids = new Id[]{};
    Id[] pacctids = new Id[]{};
	List<String> lModel = new List<String>();
	Model_Of_Interest__c[] tempModel = new Model_Of_Interest__c[]{};

	for (Case ca : Trigger.new) {
	    if (ca.RecordTypeId == '012900000000UvSAAU' || ca.RecordTypeId == '012900000000Uvh'){      		
	        //if(ca.Type == 'New Brochure Request' || ca.Type == 'Reissue Brochure Request'){
				 caseids.add(ca.Id);
				if(ca.Model__c == null){continue;}
			        pacctids.add(ca.AccountId); 
			        strModel = ca.Model__c;	
			        lModel = strModel.split(';');
			        for(Integer i = 0; i< lModel.size(); i++){
			        	Model_Of_Interest__c mo = new Model_Of_Interest__c();
			        	mo.Account__c = ca.AccountId;
			        	mo.Case__c = ca.Id;
			        	mo.Type__c = ca.Type;
			        	mo.Model__c = lModel[i];
			        	mo.RecordTypeId = '012900000000XAOAA2';		        				        	
        				tempModel.add(mo);
        			}	        			        		  
	        //}               	
		}else if (ca.RecordTypeId == '012900000000UIMAA2'){      		
	       // if(ca.Type == 'Vehicle'){
				 caseids.add(ca.Id);
				if(ca.Model__c == null){continue;}
			        pacctids.add(ca.AccountId); 
			        strModel = ca.Model__c;	
			        lModel = strModel.split(';');
			        for(Integer i = 0; i< lModel.size(); i++){
			        	Model_Of_Interest__c mo = new Model_Of_Interest__c();
			        	mo.Account__c = ca.AccountId;
			        	mo.Case__c = ca.Id;
			        	mo.Type__c = ca.Type;
			        	mo.Model__c = lModel[i];
			        	mo.RecordTypeId = '012900000000XAOAA2';		        				        	
        				tempModel.add(mo);
        			}	        			        		  
	       // }               	
		}     	     	                    
	}
	
	Model_Of_Interest__c[] tempMo = new Model_Of_Interest__c[]{};
    if(caseids.size() > 0){
       	tempMo = [select Id from Model_Of_Interest__c where Case__c IN :caseids AND RecordTypeId = '012900000000XAOAA2']; 	
    	if(tempMo.size() > 0){
    		delete tempMo;
    	}
    }

	if(tempModel.size() > 0){
		insert tempModel;
	}
	
}