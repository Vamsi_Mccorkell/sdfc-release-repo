trigger SP_AssetRecordServicedYear on Asset__c (before update)
{
	boolean assetUpdatedByTrigger = SP_Utilities.AssetUpdatedByVOUpdateAccInfoTrigger ||
									SP_Utilities.AssetUpdatedByVOChangeOwnershipTrigger ||
									SP_Utilities.AssetUpdatedByServRegoAssetServDateTrigger;
	
	if (SP_Utilities.AssetUpdatedByVOUpdateAccInfoTrigger)
		SP_Utilities.AssetUpdatedByVOUpdateAccInfoTrigger = false;
		
	if (SP_Utilities.AssetUpdatedByVOChangeOwnershipTrigger)
		SP_Utilities.AssetUpdatedByVOChangeOwnershipTrigger = false;
		
	if (SP_Utilities.AssetUpdatedByServRegoAssetServDateTrigger)
		SP_Utilities.AssetUpdatedByServRegoAssetServDateTrigger = false;
		
	if (UserInfo.getUserId() == SPCacheLexusEnformMetadata.getIntelematicsUser())
	{
		set<String> set_FieldNames = new set<String>();
		set_FieldNames.add('current_rego__c');
		set_FieldNames.add('enform_equipped__c');
					
		string strField = SP_Utilities.checkFieldChangeAllowed(trigger.newMap, trigger.oldMap, set_FieldNames);	
					
		if (!assetUpdatedByTrigger && strField != null)	
			trigger.new[0].addError('Intelematics User is not allowed to edit ' + strField + '.');	
	}	
				
	// Map of Asset Ids to a set of Serviced Years
	map<id, set<string>>	map_AssettoServicedYear		= new map<id, set<string>>();


	// Get all the Service records for these Assets
	for (Service__c [] arrServices :	[	select	id,
													Asset__c,
													Service_Date__c
											from	Service__c
											where	Asset__c in :trigger.oldMap.keySet()
										])
	{
		for (Service__c sService : arrServices)
		{
			if (sService.Asset__c != null && sService.Service_Date__c != null)
			{
				string strServicedYear = sService.Service_Date__c.year().format();
				strServicedYear = strServicedYear.replaceAll(',', '');

				if (!map_AssettoServicedYear.containsKey(sService.Asset__c))
				{
					set<string> set_ServicedYear = new set<string>();
					set_ServicedYear.add(strServicedYear);
					map_AssettoServicedYear.put(sService.Asset__c, set_ServicedYear);
				}
				else
				{
					set<string> set_ServicedYear = map_AssettoServicedYear.get(sService.Asset__c);
					set_ServicedYear.add(strServicedYear);
					map_AssettoServicedYear.put(sService.Asset__c, set_ServicedYear);
				}
			}
		}
	}

system.debug('*** map_AssettoServicedYear ***' + map_AssettoServicedYear);

	for (Asset__c sAsset : trigger.new)
	{
		if (map_AssettoServicedYear.containsKey(sAsset.Id))
		{
			set<string> set_ServicedYear = map_AssettoServicedYear.get(sAsset.Id);
			list<string> li_ServicedYear = new list<string>();

			for (string strServicedYear : set_ServicedYear)
			{
				li_ServicedYear.add(strServicedYear);
			}

			li_ServicedYear.sort();

			sAsset.Serviced_In_Year__c = '';
			
			for (string strServicedYear : li_ServicedYear)
			{
				sAsset.Serviced_In_Year__c += strServicedYear;
				sAsset.Serviced_In_Year__c += ' ';
			}

			// Back up one place to remove the trailing space
			sAsset.Serviced_In_Year__c = sAsset.Serviced_In_Year__c.substring(0, sAsset.Serviced_In_Year__c.length() - 1);

system.debug('*** sAsset ***' + sAsset);
		}
	}
}