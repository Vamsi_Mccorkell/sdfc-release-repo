trigger SP_UpdateAccountInfo on Vehicle_Ownership__c (after insert, after update)
{
	List<String>					VO_IDList					= new List<String>();

	set<id>							set_CustomerIds				= new set<id>{};
	set<id>							set_OldCustomerIds			= new set<id>{};

	map<id, Vehicle_Ownership__c>	map_AssetIdsforUpdate		= new map<id, Vehicle_Ownership__c>{};
	set<id>							set_AssetIdsforWarranties	= new set<Id>{};
	set<string>						set_AssetCustomer			= new set<string>{};

	map<id, Vehicle_Ownership__c>	map_CustomertoVO			= new map<id, Vehicle_Ownership__c>();
	map<id, Vehicle_Ownership__c>	map_OldCustomertoVO			= new map<id, Vehicle_Ownership__c>();

	for (Vehicle_Ownership__c sVO : trigger.new)
	{
		set_CustomerIds.add(sVO.Customer__c);
		map_CustomertoVO.put(sVO.Customer__c, sVO);
	} 

	// Updates only
	if (trigger.isUpdate)
	{
		for (Vehicle_Ownership__c sVO : trigger.old)
		{
			set_OldCustomerIds.add(sVO.Customer__c);
			map_OldCustomertoVO.put(sVO.Customer__c, sVO);
		}
	}

	Integer iActiveVOCount = 0;
	Integer iCountVO = 0;

    // Account id and VO where factory warranty is latest
	map<Id,Vehicle_Ownership__c>	map_VOLatestWarranty			= new map<Id,Vehicle_Ownership__c>{};
	// Account id and VO of latest purchased vehicle
	map<Id,Vehicle_Ownership__c>	map_VOLatestPurchased			= new map<Id,Vehicle_Ownership__c>{};
	// Account id and VO of latest selling dealer which in not private sale VO
	map<Id,Vehicle_Ownership__c>	map_VOLatestSellingDealer		= new map<Id,Vehicle_Ownership__c>{};
	// Account id and inactive VO with latest ownership ends date
	map<ID,Vehicle_Ownership__c>	map_VOLatestOwned				= new map<Id,Vehicle_Ownership__c>{};
	// Account id and inactive VO with latest Factory warranty
	map<ID,Vehicle_Ownership__c>	map_VOInactiveLatestWarranty	= new map<Id,Vehicle_Ownership__c>{};

	// Lists of database records to be updated
	// Accounts to be updated
	list<Account>					li_AccountsToUpdate = new list<Account>();
	// Warranties to be updated
	list<Warranty__c>				li_WarrantiesToUpdate = new list<Warranty__c>();
	// Assets to be updated
	list<Asset__c>					li_AssetsToUpdate = new list<Asset__c>();

	Vehicle_Ownership__c [] arrVO =	[
										select	Id,
												Customer__c,
												Start_Date__c,
												Branch_of_Dealership__c,
												Status__c,
												End_Date__c,
												AssetID__r.Factory_Warranty_Expiration_Date__c,
												Vehicle_Type__c
										from	Vehicle_Ownership__c
										where	Customer__c in :set_CustomerIds
									];

	for (Vehicle_Ownership__c sVO: arrVO)
	{
		iCountVO += 1;

		if (sVO.Status__c == 'Active')
		{
			iActiveVOCount += 1;

			// search for latest warranty in active VOs
			if (map_VOLatestWarranty.containsKey(sVO.Customer__c))
			{
				if (map_VOLatestWarranty.get(sVO.Customer__c).AssetID__r.Factory_Warranty_Expiration_Date__c 
					< sVO.AssetID__r.Factory_Warranty_Expiration_Date__c)
				{
					map_VOLatestWarranty.remove(sVO.Customer__c);
					map_VOLatestWarranty.put(sVO.Customer__c, sVO);
				}
			}
			else
			{
				map_VOLatestWarranty.put(sVO.Customer__c, sVO);
			}
			
			//for most recently purchase vehicle,search only in non-private sale VO
			if (map_VOLatestPurchased.containsKey(sVO.Customer__c))
			{
				if (map_VOLatestPurchased.get(sVO.Customer__c).Start_Date__c
					< sVO.Start_Date__c)
				{
					map_VOLatestPurchased.remove(sVO.Customer__c);
					map_VOLatestPurchased.put(sVO.Customer__c, sVO);
				}
			}
			else
			{
				map_VOLatestPurchased.put(sVO.Customer__c, sVO);
			}

			//for last selling dealer search only in non private sale VO	
			if (sVO.Vehicle_Type__c != 'Private Sale')
			{
				if (map_VOLatestSellingDealer.containsKey(sVO.Customer__c))
				{
					if (map_VOLatestSellingDealer.get(sVO.Customer__c).Start_Date__c
						< sVO.Start_Date__c)
					{
						map_VOLatestSellingDealer.remove(sVO.Customer__c);
						map_VOLatestSellingDealer.put(sVO.Customer__c, sVO);
					}
				}
				else
				{
					map_VOLatestSellingDealer.put(sVO.Customer__c, sVO);
				}
			}
		}
		
		else
		{
			//find the latest owned vehicle in those Inactive VO
			if (map_VOLatestOwned.containsKey(sVO.Customer__c))
			{
				if (map_VOLatestOwned.get(sVO.Customer__c).End_Date__c 
					< sVO.End_Date__c)
				{
					map_VOLatestOwned.remove(sVO.Customer__c);
					map_VOLatestOwned.put(sVO.Customer__c,sVO);
				}
			}
			else
			{
				map_VOLatestOwned.put(sVO.Customer__c,sVO);	
			}
			
			//find last factory warranty in inactive VOs
			if (map_VOInactiveLatestWarranty.containsKey(sVO.Customer__c))
			{
				if (map_VOInactiveLatestWarranty.get(sVO.Customer__c).AssetID__r.Factory_Warranty_Expiration_Date__c 
					< sVO.AssetID__r.Factory_Warranty_Expiration_Date__c)
				{
					map_VOInactiveLatestWarranty.remove(sVO.Customer__c);
					map_VOInactiveLatestWarranty.put(sVO.Customer__c, sVO);
				}
			}
			else
			{
				map_VOInactiveLatestWarranty.put(sVO.Customer__c, sVO);
			}
		}		
	}


	// Insert only
	if (trigger.isInsert)
	{
		Account[] arrAccount =	[
									select	Id,
											Last_Selling_Dealer__c,
											Original_Selling_Dealer__c,
											Assets_Owned__c,
											Vehicle_With_Latest_Warranty__c,
											Most_Recently_Purchased_Model__c,
											Encore_Expiration_Date__c,
											Factory_Warranty_Expiration_Date__c,
											Preferred_Dealer__c,
											Override_Preferred_Dealer__c
									from	Account
									where	Id in :set_CustomerIds
								];

		for (Account sAccount: arrAccount)
		{
			// has active VO
			if (map_VOLatestWarranty.containsKey(sAccount.Id))
			{
				sAccount.Owners_End_Date__c						= null;
				sAccount.Vehicle_With_Latest_Warranty__c		= map_VOLatestWarranty.get(sAccount.Id).Id;
				// 21 JAN 2020: D BANEZ: Commented out as part of de-rdr trigger project; Field will be updated directly by Encore
                //sAccount.Encore_Expiration_Date__c				= map_VOLatestWarranty.get(sAccount.Id).AssetID__r.Factory_Warranty_Expiration_Date__c;
				sAccount.Factory_Warranty_Expiration_Date__c	= map_VOLatestWarranty.get(sAccount.Id).AssetID__r.Factory_Warranty_Expiration_Date__c;
				
			}
			// has no active Vo	
			else 
			{
				sAccount.Vehicle_With_Latest_Warranty__c		= null;
				sAccount.Factory_Warranty_Expiration_Date__c	= null;
				sAccount.Encore_Opt_Out__c						= true;
	
				if (map_VOLatestOwned.containsKey(sAccount.Id))
				{
					sAccount.Owners_End_Date__c					= map_VOLatestOwned.get(sAccount.Id).End_Date__c;
					if (sAccount.Owners_End_Date__c
						<= map_VOInactiveLatestWarranty.get(sAccount.Id).AssetID__r.Factory_Warranty_Expiration_Date__c)
					{
						// 21 JAN 2020: D BANEZ: Commented out as part of de-rdr trigger project; Field will be updated directly by Encore
                        //sAccount.Encore_Expiration_Date__c		= sAccount.Owners_End_Date__c;
					}
					else
					{
						// 21 JAN 2020: D BANEZ: Commented out as part of de-rdr trigger project; Field will be updated directly by Encore
                        //sAccount.Encore_Expiration_Date__c		= map_VOInactiveLatestWarranty.get(sAccount.Id).AssetID__r.Factory_Warranty_Expiration_Date__c;
					}
				}
			}
	
			//most recently purchased model
			if (map_VOLatestPurchased.containsKey(sAccount.Id))
			{
				sAccount.Most_Recently_Purchased_Model__c		= map_VOLatestPurchased.get(sAccount.Id).Id;
			}
			
			if (map_VOLatestSellingDealer.containsKey(sAccount.Id))
			{
				if (map_VOLatestSellingDealer.get(sAccount.Id).Branch_of_Dealership__c != null)
				{
					sAccount.Last_Selling_Dealer__c				= map_VOLatestSellingDealer.get(sAccount.Id).Branch_of_Dealership__c;
				}

				// Check if this is an Owner with no preferred Dealer, OR
				// the first owner transaction with a Dealer since Preferred Dealer was set to Lexus Australia
				// Must also NOT update if the 'Override Preferred Dealer' option is selected
				string strPreferredDealer = sAccount.Preferred_Dealer__c;
				if (	(	sAccount.Preferred_Dealer__c == null ||
							strPreferredDealer.substring(0, 15) == SPCacheRecordTypeMetadata.getLexusAustraliaRecordID().substring(0, 15)
						) && !sAccount.Override_Preferred_Dealer__c
					)
				{
					sAccount.Preferred_Dealer__c = map_VOLatestSellingDealer.get(sAccount.Id).Branch_of_Dealership__c;
				}

				//write in original selling dealer only for the first VO
				if (iCountVO == 1 && iActiveVOCount == 1)
				{
					sAccount.Original_Selling_Dealer__c			= map_VOLatestSellingDealer.get(sAccount.Id).Branch_of_Dealership__c;
				}		
			}
			else
			{
				if (iCountVO == 1 && iActiveVOCount == 1)
				{
					sAccount.Original_Selling_Dealer__c	= null;
					sAccount.Last_Selling_Dealer__c		= SPCacheRecordTypeMetadata.getLexusAustraliaRecordID();
					
					// Private sale - set the Preferred and Original Selling Dealer to Lexus Australia
					sAccount.Preferred_Dealer__c		= SPCacheRecordTypeMetadata.getLexusAustraliaRecordID();
					sAccount.Original_Selling_Dealer__c	= SPCacheRecordTypeMetadata.getLexusAustraliaRecordID();
				}
			}
			
			li_AccountsToUpdate.add(sAccount);
		}
	}


	// Updates only
	if (trigger.isUpdate)
	{
		Account[] arrAccount =	[
									select	Id,
											Last_Selling_Dealer__c,
											Original_Selling_Dealer__c,
											Assets_Owned__c,
											Vehicle_With_Latest_Warranty__c,
											Most_Recently_Purchased_Model__c,
											Encore_Expiration_Date__c,
											Factory_Warranty_Expiration_Date__c
									from	Account
									where	Id in :set_OldCustomerIds
								];
	
		for (Account sAccount: arrAccount)
		{
			Vehicle_Ownership__c sOldVO = map_OldCustomertoVO.get(sAccount.Id);
			Vehicle_Ownership__c sNewVO = map_CustomertoVO.get(sAccount.Id);
	
			//find vechicle with latest facotry warranty
			if(map_VOLatestWarranty.containsKey(sAccount.Id))
			{
				sAccount.Vehicle_With_Latest_Warranty__c		= map_VOLatestWarranty.get(sAccount.Id).Id;
				sAccount.Factory_Warranty_Expiration_Date__c	= map_VOLatestWarranty.get(sAccount.Id).AssetID__r.Factory_Warranty_Expiration_Date__c;
				// 21 JAN 2020: D BANEZ: Commented out as part of de-rdr trigger project; Field will be updated directly by Encore
                //sAccount.Encore_Expiration_Date__c				= map_VOLatestWarranty.get(sAccount.Id).AssetID__r.Factory_Warranty_Expiration_Date__c;
				sAccount.Owners_End_Date__c						= null;
			}
			else 
			{	
				sAccount.Vehicle_With_Latest_Warranty__c		= null;
				sAccount.Factory_Warranty_Expiration_Date__c	= null;
				sAccount.Encore_Opt_Out__c						= true;
	
				if (map_VOLatestOwned.containsKey(sAccount.Id))
				{
					sAccount.Owners_End_Date__c					= map_VOLatestOwned.get(sAccount.Id).End_Date__c;
					if (sAccount.Owners_End_Date__c 
						<= map_VOInactiveLatestWarranty.get(sAccount.Id).AssetID__r.Factory_Warranty_Expiration_Date__c)
					{
						// 21 JAN 2020: D BANEZ: Commented out as part of de-rdr trigger project; Field will be updated directly by Encore
                        //sAccount.Encore_Expiration_Date__c		= map_VOLatestOwned.get(sAccount.Id).End_Date__c;
					}
					else
					{
						// 21 JAN 2020: D BANEZ: Commented out as part of de-rdr trigger project; Field will be updated directly by Encore
                        //sAccount.Encore_Expiration_Date__c		= map_VOInactiveLatestWarranty.get(sAccount.Id).AssetID__r.Factory_Warranty_Expiration_Date__c;
					}
				}
			}
		
			//find new most recently purchased vehicle ownership id
			if (map_VOLatestPurchased.containsKey(sAccount.Id)) 
			{
				sAccount.Most_Recently_Purchased_Model__c		= map_VOLatestPurchased.get(sAccount.Id).Id;
			}
			else
			{
				sAccount.Most_Recently_Purchased_Model__c		= null;
			}
	
			if (sNewVO.Status__c == 'Active')
			{
				map_AssetIdsforUpdate.put(sNewVO.AssetID__c, sNewVO);
			}
	
			if ((sOldVO.Status__c == 'Active') && (sNewVO.Status__c == 'Inactive'))
			{
				// Is the updated VO marked as Inactive and end-dated?
				if (sNewVO.End_Date__c != null)
				{
					set_AssetIdsforWarranties.add(sOldVO.AssetID__c);
					string strAssetCustomer = '' + sOldVO.AssetID__c + sOldVO.Customer__c;
					set_AssetCustomer.add(strAssetCustomer);
				}
			}	
			else
			{
				if (map_VOLatestSellingDealer.containsKey(sAccount.Id))
				{
					if (map_VOLatestSellingDealer.get(sAccount.Id).Branch_of_Dealership__c != null)
					{
						sAccount.Last_Selling_Dealer__c = map_VOLatestSellingDealer.get(sAccount.Id).Branch_of_Dealership__c;			
					}
	
					//write in original selling dealer only for the first VO
					if (iCountVO == 1 && iActiveVOCount == 1)
					{
						sAccount.Original_Selling_Dealer__c = map_VOLatestSellingDealer.get(sAccount.Id).Branch_of_Dealership__c;
					}

					// Check if this is an Owner with no preferred Dealer, OR
					// the first owner transaction with a Dealer since Preferred Dealer was set to Lexus Australia
					// Must also NOT update if the 'Override Preferred Dealer' option is selected
					string strPreferredDealer = sAccount.Preferred_Dealer__c;
					if (	(	sAccount.Preferred_Dealer__c == null ||
								strPreferredDealer.substring(0,15) == SPCacheRecordTypeMetadata.getLexusAustraliaRecordID().substring(0,15)
							) && !sAccount.Override_Preferred_Dealer__c
						)
					{
						sAccount.Preferred_Dealer__c = map_VOLatestSellingDealer.get(sAccount.Id).Branch_of_Dealership__c;
					}

				}
				else
				{
					//if only has one active VO which is private sale
					if (iCountVO == 1 && iActiveVOCount == 1)
					{
						sAccount.Original_Selling_Dealer__c	= null;
						sAccount.Last_Selling_Dealer__c		= SPCacheRecordTypeMetadata.getLexusAustraliaRecordID();
						
					// Private sale - set the Preferred and Original Selling Dealer to Lexus Australia
					sAccount.Preferred_Dealer__c		= SPCacheRecordTypeMetadata.getLexusAustraliaRecordID();
					sAccount.Original_Selling_Dealer__c	= SPCacheRecordTypeMetadata.getLexusAustraliaRecordID();
					}
				}
			}
	
			li_AccountsToUpdate.add(sAccount); 
		}

		// Build list of Assets for update
		Asset__c [] arrAsset =	[
									select	Id,
											Current_Vehicle_Type__c,
											Current_Rego__c,
											Selling_Dealer__c
									from	Asset__c 
									where	Id in :map_AssetIdsforUpdate.keySet()
								];
	
		for (Asset__c sAsset : arrAsset)
		{
			Vehicle_Ownership__c sNewVO = map_AssetIdsforUpdate.get(sAsset.Id);

			// If this is a new Asset from RDR we want to set the Selling Dealer - only time we do this
			if (sAsset.Selling_Dealer__c == null && sNewVO.Branch_of_Dealership__c != null)
			{
	          	sAsset.Selling_Dealer__c = sNewVO.Branch_of_Dealership__c;
			}
			
			sAsset.Current_Vehicle_Type__c	= sNewVO.Vehicle_Type__c;

			if (sNewVO.Vehicle_Rego_Number__c != null && sNewVO.Vehicle_Rego_Number__c != '' && sNewVO.Vehicle_Rego_Number__c != ' ')
			{
				sAsset.Current_Rego__c			= sNewVO.Vehicle_Rego_Number__c;
			}

			li_AssetsToUpdate.add(sAsset);
		}

		Warranty__c [] arrWarranty =	[
												select	Id,
														Customer_Account__c,
														AssetId__c
												from	Warranty__c 
												where	AssetId__c in :set_AssetIdsforWarranties
											];
		for (Warranty__c sWarranty : arrWarranty)
		{
			string strAssetCustomer = '' + sWarranty.AssetId__c + sWarranty.Customer_Account__c;

			// Only clear the Account info for specific Asset / Customer Id pairings
			if (set_AssetCustomer.contains(strAssetCustomer))
			{
				sWarranty.Customer_Account__c = null;

				li_WarrantiesToUpdate.add(sWarranty);
			}
		}
	}


system.debug('*** map_VOLatestSellingDealer ***' + map_VOLatestSellingDealer);
system.debug('*** li_AccountsToUpdate ***' + li_AccountsToUpdate);

system.debug('*** VO_IDList ***' + VO_IDList);
system.debug('*** set_CustomerIds ***' + set_CustomerIds);
system.debug('*** set_OldCustomerIds ***' + set_OldCustomerIds);
system.debug('*** map_AssetIdsforUpdate ***' + map_AssetIdsforUpdate);
system.debug('*** set_AssetIdsforWarranties ***' + set_AssetIdsforWarranties);
system.debug('*** set_AssetCustomer ***' + set_AssetCustomer);
system.debug('*** map_CustomertoVO ***' + map_CustomertoVO);
system.debug('*** map_OldCustomertoVO ***' + map_OldCustomertoVO);


	// Finally do all our database updates
	if (!li_AccountsToUpdate.isEmpty())
		SP_Utilities.AccountUpdatedByVOUpdateAccInfoTrigger = true; 
		
	update li_AccountsToUpdate;
	
	if (!li_AssetsToUpdate.isEmpty())
		SP_Utilities.AssetUpdatedByVOUpdateAccInfoTrigger = true;
	
	update li_AssetsToUpdate;
	update li_WarrantiesToUpdate;
    
    // DEV NOTES: Added some extra lines that we need to remove once we refactor this code; This is addd to deploy it to Prod as quick as possible as instructed by Lexus Jan 2020
    String a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';
    a = 'coverage';

}