trigger SP_ChangeOwnership on Vehicle_Ownership__c (after insert)
{
	system.debug('############ SP_ChangeOwnership ' + trigger.new);
	
	// Build maps for related sObjects
	map<id, id>						m_mapVOtoContactId	= new map<id, id>();
	map<id, id>						m_mapContacttoVOId	= new map<id, id>();
	map<id, Asset__c>				m_mapAsset			= new map<id, Asset__c>();
	map<id, id>						m_mapAssettoVOId	= new map<id, id>();

	// Warranties
	map<id, list<Warranty__c>>		m_mapVOtoWarranties	= new map<id, list<Warranty__c>>();
	list<Warranty__c>				li_Warranty			= new list<Warranty__c>();

	// Sales Consultants
	map<id, map<string, id>>		m_mapVOtoConsultant	= new map<id, map<string, id>>();

	// Old VOs for this Asset
	map<id, list<Vehicle_Ownership__c>>	m_mapActiveVOs		= new map<id, list<Vehicle_Ownership__c>>();
	map<id, list<Vehicle_Ownership__c>>	m_mapInactiveVOs	= new map<id, list<Vehicle_Ownership__c>>();
	map<id, Vehicle_Ownership__c>	m_mapLatestVO			= new map<id, Vehicle_Ownership__c>();

	// Set of records to update
	Set<Vehicle_Ownership__c>		m_setVOsToUpdate			= new Set<Vehicle_Ownership__c>();
	Map<Id, Warranty__c>				m_mapWarrantiesToUpdate	= new Map<Id, Warranty__c>();
	Set<Asset__c>					m_setAssetsToUpdate		= new Set<Asset__c>();


	// Processing
	for (Vehicle_Ownership__c  sVO: Trigger.new)
	{
		m_mapVOtoContactId.put(sVO.Id, sVO.Customer__c);
		m_mapContacttoVOId.put(sVO.Customer__c, sVO.Id);
		m_mapAssettoVOId.put(sVO.AssetId__c, sVO.Id);
		//m_mapVOtoWarranties.put(sVO.Id, li_Warranty);
	}
	
	system.debug('########### m_mapAssettoVOId ' + m_mapAssettoVOId);

	Asset__c [] li_Assets =	[
										select	Id,
												Current_Rego__c,
												Current_Vehicle_Type__c
										from	Asset__c
										where	Id in :m_mapAssettoVOId.keySet()
									];
	for (Asset__c sAsset : li_Assets)
	{
		m_mapAsset.put(sAsset.Id, sAsset);
	}
	
	Warranty__c [] li_Warranties =	[
										select	Customer_Account__c,			// link to Account
												Customer__c,					// link to Contact
												AssetId__c
										from	Warranty__c
										where	AssetId__c in :m_mapAssettoVOId.keySet()
									];
	for (Warranty__c sWarranty : li_Warranties)
	{
		system.debug('########## sWarranty ' + sWarranty);
		id idVO = m_mapAssettoVOId.get(sWarranty.AssetId__c);
		
		system.debug('############ idVO ' + idVO);
		
		
		list<Warranty__c> li_Wrty = m_mapVOtoWarranties.get(idVO);
		if (li_Wrty == null)
			li_Wrty = new list<Warranty__c>();
		
		system.debug('############ li_Wrty Before' + li_Wrty);
		li_Wrty.add(sWarranty);
		m_mapVOtoWarranties.put(idVO, li_Wrty);
		
		system.debug('############ li_Wrty ' + li_Wrty);
		//list<Warranty__c> li_Wrty = m_mapVOtoWarranties.remove(idVO);
		//system.debug('############ li_Wrty ' + li_Wrty);
		//li_Wrty.add(sWarranty);
		//m_mapVOtoWarranties.put(idVO, li_Wrty);
	}

	Contact[] arrContact =	[
								select	Id,
										AccountId,
										Sales_Code__c
								from	Contact
								where	Status__c = 'Active'
								and		Sales_Code__c != null
								and		Sales_Code__c != ''
							];

	for (Contact sContact : arrContact)
	{
		map<string, id> map_CodetoContact = new map<string, id>();
		map_CodetoContact.put(sContact.Sales_Code__c, sContact.Id);
		m_mapVOtoConsultant.put(sContact.AccountId, map_CodetoContact);
	}

	// Get list of previous vehicle ownership records
	Vehicle_Ownership__c []  arrVO =	[
											select	Customer__c,
													Customer__r.Encore_Expiration_date__c,
													AssetID__c,
													Status__c,
													Vehicle_Rego_Number__c,
													Vehicle_Registration_Date__c,
													End_Date__c,
													Selling_Dealer__c,
													Branch_of_Dealership__c,
													CreatedDate
											from	Vehicle_Ownership__c
											where	AssetID__c in :m_mapAssettoVOId.keySet()
											order by End_Date__c desc
										];

	for (Vehicle_Ownership__c sVO :arrVO)
	{
		if (sVO.Status__c == 'Active')
		{
			if (!m_mapActiveVOs.containsKey(sVO.AssetID__c))
			{
				list<Vehicle_Ownership__c> li_VO = new list<Vehicle_Ownership__c>();
				li_VO.add(sVO);
				m_mapActiveVOs.put(sVO.AssetID__c, li_VO);
			}
			else
			{
				list<Vehicle_Ownership__c> li_VO = m_mapActiveVOs.remove(sVO.AssetID__c);
				li_VO.add(sVO);
				m_mapActiveVOs.put(sVO.AssetID__c, li_VO);
			}
		}
		else
		{
			if (!m_mapInActiveVOs.containsKey(sVO.AssetID__c))
			{
				list<Vehicle_Ownership__c> li_VO = new list<Vehicle_Ownership__c>();
				li_VO.add(sVO);
				m_mapInActiveVOs.put(sVO.AssetID__c, li_VO);
			}
			else
			{
				list<Vehicle_Ownership__c> li_VO = m_mapInActiveVOs.remove(sVO.AssetID__c);
				li_VO.add(sVO);
				m_mapInActiveVOs.put(sVO.AssetID__c, li_VO);
			}
		}
	}

	// Work out which is the latest VO to use
	for (id idAsset : m_mapAssettoVOId.keySet())
	{
		// Look at Active VOs first
		if (m_mapActiveVOs.containsKey(idAsset))
		{
			// Loop through the list of active VOs - should only be one
			for (Vehicle_Ownership__c sVO : m_mapActiveVOs.get(idAsset))
			{
				if (!m_mapLatestVO.containsKey(sVO.AssetID__c))
				{
					m_mapLatestVO.put(sVO.AssetID__c, sVO);
				}
				else
				{
					// We have > 1 - keep the most recently created and end-date the others
					Vehicle_Ownership__c oldVO = m_mapLatestVO.remove(sVO.AssetID__c);
					if (sVO.CreatedDate > oldVO.CreatedDate)
					{
						oldVO.Status__c = 'Inactive';
						oldVO.End_Date__c = system.today();
						m_setVOsToUpdate.add(oldVO);
						
						m_mapLatestVO.put(sVO.AssetID__c, sVO);
					}
					else
					{
						sVO.Status__c = 'Inactive';
						sVO.End_Date__c = system.today();
						m_setVOsToUpdate.add(sVO);
						
						m_mapLatestVO.put(sVO.AssetID__c, oldVO);
					}
				}
			}
		}
		else
		{
			// Didn't find an Active VO so use the latest Inactive one
			if (m_mapInActiveVOs.containsKey(idAsset))
			{
				list<Vehicle_Ownership__c> liVO = m_mapInActiveVOs.get(idAsset);

				Vehicle_Ownership__c sVO = liVO[0];

				m_mapLatestVO.put(sVO.AssetID__c, sVO);
			}
		}
	}

system.debug('*** m_mapAssettoVOId ***' + m_mapAssettoVOId);
system.debug('*** trigger.newMap.keyset() ***' + trigger.newMap.keyset());

	for (id idVO : trigger.newMap.keyset())
	{
		Vehicle_Ownership__c sVO = trigger.newMap.get(idVO);
		Vehicle_Ownership__c oldVO;

		// Step 1 - iterate through the old VO records and set them to inactive
		if (null != (oldVO = m_mapLatestVO.get(idVO)))
		{
			if (oldVO.Status__c == 'Active')
			{
				// Checks if Rego, Rego Date, Selling Dealer and Branch of Dealership are entered. 
				//If not, get them from last Active VO
				if (sVO.Vehicle_Rego_Number__c == null || sVO.Vehicle_Rego_Number__c == '' || sVO.Vehicle_Rego_Number__c == ' ')
				{
					sVO.Vehicle_Rego_Number__c = oldVO.Vehicle_Rego_Number__c;
				}
	
				if (sVO.Vehicle_Registration_Date__c == null)
				{
					sVO.Vehicle_Registration_Date__c = oldVO.Vehicle_Registration_Date__c;
				}

				//this is required since B records will not have dealer information through integration (RDR)
				//it is assumed that all B records will have a previous VO to obtain dealer information
				//it is assumed that the dealer who sold the vehicle previously is the same dealer who sold the B record
				if (sVO.Rec_Id__c == 'B' && UserInfo.getUserId() == SPCacheRecordTypeMetadata.getIntegrationUserID())
				{
					if (sVO.Selling_Dealer__c == null || sVO.Selling_Dealer__c == '')
					{
						sVO.Selling_Dealer__c = oldVO.Selling_Dealer__c;
					}
	
					if (sVO.Branch_of_Dealership__c == null || sVO.Branch_of_Dealership__c == '')
					{
						sVO.Branch_of_Dealership__c = oldVO.Branch_of_Dealership__c;
					}
	
					if (sVO.Sales_Code_Temp__c != null || sVO.Sales_Code_Temp__c != '')
					{
						sVO.Sales_Consultant__c = m_mapVOtoConsultant.get(sVO.Branch_of_Dealership__c).get(sVO.Sales_Code_Temp__c);
					}
				}
	
				oldVO.Status__c = 'Inactive';
				oldVO.End_Date__c = sVO.Start_Date__c;
				m_setVOsToUpdate.add(oldVO);
			}
		}

		// Step 2: Get the list of Warranties attached to the Asset and set to the new Customer's Name	
		list<Warranty__c> li_Wrty = m_mapVOtoWarranties.remove(sVO.Id);
	
		if (li_Wrty != null)
		{
			for (Warranty__c sWarranty: li_Wrty)
			{
				sWarranty.Customer_Account__c = sVO.Customer__c;
	            
	            System.debug('SP_ChangeOwnership sWarranty: ' + sWarranty);
				m_mapWarrantiesToUpdate.put(sWarranty.id, sWarranty);
			}
		}
	
		// Update Asset with Latest Rego
		Asset__c sAsset = m_mapAsset.get(sVO.AssetId__c);

		if (sVO.Vehicle_Rego_Number__c != null && sVO.Vehicle_Rego_Number__c != '' && sVO.Vehicle_Rego_Number__c != ' ')
		{
			sAsset.Current_Rego__c = sVO.Vehicle_Rego_Number__c;
		}

		sAsset.Current_Vehicle_Type__c = sVO.Vehicle_Type__c;
	
		m_setAssetsToUpdate.add(sAsset);
	}

	// Add elements from set to a list. (Set can make element unique, list is used to update operation)
	List<Vehicle_Ownership__c> m_liVOsToUpdate = new List<Vehicle_Ownership__c>();
	m_liVOsToUpdate.addAll(m_setVOsToUpdate);
	
	List<Asset__c> m_liAssetsToUpdate = new List<Asset__c>();
	m_liAssetsToUpdate.addAll(m_setAssetsToUpdate);
	
	System.debug('SP_ChangeOwnership m_setVOsToUpdate: ' + m_setVOsToUpdate);
	System.debug('SP_ChangeOwnership m_mapWarrantiesToUpdate.values(): ' + m_mapWarrantiesToUpdate.values());
	System.debug('SP_ChangeOwnership m_setAssetsToUpdate: ' + m_setAssetsToUpdate);
	
	System.debug('SP_ChangeOwnership m_liVOsToUpdate: ' + m_liVOsToUpdate);
	System.debug('SP_ChangeOwnership m_liAssetsToUpdate: ' + m_liAssetsToUpdate);
	
	// Finally ready to do the database updates
	update m_liVOsToUpdate;
	update m_mapWarrantiesToUpdate.values();
	
	if (!m_liAssetsToUpdate.isEmpty())
		SP_Utilities.AssetUpdatedByVOChangeOwnershipTrigger = true;
		
	update m_liAssetsToUpdate;

}