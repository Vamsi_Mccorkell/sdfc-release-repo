trigger SP_CasePersonalPreviewBeforeInsert on Case (before insert)
{
    set<id>             set_ContactIds  = new set<id>();
    map<id, Contact>    map_Contacts    = new map<id, Contact>();

    // Set of Accounts (Selected Dealers) referenced by these new Cases
    set<id>             set_AccountIds          = new set<id>();

    // Map of Dealers and their Contact email addresses
    map<id, string>     map_AccounttoEmailStrings   = new map<id, string>();

    // Build a map of Dealer Accounts and their database IDs
    map<string, id> map_DealerAccountNametoId = new map<string, id>();

    Account [] arrDealerAccount = [
                                        select  Id,
                                                Name
                                        from    Account
                                        where   RecordTypeId = :SPCacheRecordTypeMetadata.getAccount_Dealer()
                                        and     Status__c = 'Active'
                                    ];

    for (Account sAccount : arrDealerAccount)
    {
        map_DealerAccountNametoId.put(sAccount.Name, sAccount.Id);
    }


    for (Case sCase : trigger.new)
    {
        if  (   sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeAccessoriesEnquiry() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeBuildandPriceEnquiry() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeGeneralEnquiry() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypePersonalEnquiry() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeDomPerignonOffer() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeConciergeOffer() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeNXPreLaunch() ||             
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeNewVehicleInterest() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeVehicleInformation() ||		//Vehicle Information
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypePartsServicesWarranty() ||	//Parts, Services, Warranty
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeLexusFinancialServices() || 		//Lexus Financial Services
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeFeedback() 						//Feedback               
            )
        {
            if (sCase.ContactId != null)
            {
                set_ContactIds.add(sCase.ContactId);

                if (map_DealerAccountNametoId.containsKey(sCase.Selected_Dealer_Name__c))
                {
                    sCase.Selected_Dealer__c = map_DealerAccountNametoId.get(sCase.Selected_Dealer_Name__c);
                    set_AccountIds.add(map_DealerAccountNametoId.get(sCase.Selected_Dealer_Name__c));
                }
            }
        }
    }

    // Get Dealer contact information
    for (Contact [] arrContact :    [   select  Id,
                                                AccountId,
                                                Name,
                                                Email
                                        from    Contact
                                        where   AccountId in :set_AccountIds
                                        and     (   To_be_Sent_on_Email_Distribution_List__c = true
                                                or  CCed_on_Distribution_List__c = true
                                                or Dom_Perignon_Offer_Distribution_Group__c = true)
                                    ])
    {
        for (Contact sContact : arrContact)
        {
            // Contact email addresses - to be saved to a field on the Case so we can display it in the email template
            if (!map_AccounttoEmailStrings.containsKey(sContact.AccountId))
            {
                string strEmailString = 'This email has been sent to: ' + sContact.Email;
                map_AccounttoEmailStrings.put(sContact.AccountId, strEmailString);
            }
            else
            {
                string strEmailString = map_AccounttoEmailStrings.remove(sContact.AccountId);
                strEmailString += ', ' + sContact.Email;
                map_AccounttoEmailStrings.put(sContact.AccountId, strEmailString);
            }
        }
    }

    // Get information about the Prospect
    for (Contact [] arrContact :    [   select  Id,
                                                FirstName,
                                                LastName,
                                                Email,
                                                HomePhone,
                                                MobilePhone,
                                                Account.Work_Phone__c,
                                                Account.Product_Information_Opt_Out__c
                                        from    Contact
                                        where   Id in :set_ContactIds
                                    ])
    {
        for (Contact sContact : arrContact)
        {
            map_Contacts.put(sContact.Id, sContact);
        }
    }



    for (Case sCase : trigger.new)
    {
        if  (   sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeAccessoriesEnquiry() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeBuildandPriceEnquiry() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeGeneralEnquiry() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypePersonalEnquiry() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeDomPerignonOffer() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeConciergeOffer() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeNXPreLaunch() ||               
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeNewVehicleInterest() ||
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeVehicleInformation() ||		//Vehicle Information
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypePartsServicesWarranty() ||	//Parts, Services, Warranty
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeLexusFinancialServices() 	||	//Lexus Financial Services 
                sCase.Type == SPCacheEmailTemplateMetadata.getCaseTypeFeedback() 						//Feedback                 
            )
        {
            if (sCase.ContactId != null)
            {
                Contact sContact = map_Contacts.get(sCase.ContactId);

                sCase.TempFirstName__c                  = sContact.FirstName;
                sCase.TempLastName__c                   = sContact.LastName;
                sCase.TempEmail__c                      = sContact.Email;
                sCase.TempHomePhone__c                  = sContact.HomePhone;
                sCase.TempMobile__c                     = sContact.MobilePhone;
                sCase.TempWorkPhone__c                  = sContact.Account.Work_Phone__c;
        
                if (sContact.Account.Product_Information_Opt_Out__c)
                {
                    sCase.TempProductInformationOptOutYN__c = 'Yes';
                }
                else
                {
                    sCase.TempProductInformationOptOutYN__c = 'No';
                }

                if (map_AccounttoEmailStrings.containsKey(sCase.Selected_Dealer__c))
                {
                    sCase.TempEmailStrings__c = map_AccounttoEmailStrings.get(sCase.Selected_Dealer__c);
                }
            }
        }
    }
}