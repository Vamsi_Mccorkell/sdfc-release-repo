// Trigger to manage changes to Enform User records
// Author: Shabu, SP on 02.04.2013

trigger SP_EnformUserManagement on Enform_User__c (before insert, before update, after insert, after update) 
{
    /*
        Before inserting/updating an Enform User, the mandatory fields and Account/Lead lookups should be validated.
        
        After inserting an Enform User, a lead should be created, if it is a secondary user.
        
        After updating an Enform User, the related accounts (for primary user) and leads (for secondary users)
        should be updated.
    */
    try
    {
        if (SP_Utilities.EnformUpdatedByEnformTrigger)
        {
            SP_Utilities.EnformUpdatedByEnformTrigger = false;
            return;
        }
        
        // Flag to check if this trigger is fired as a result of the updation of First Name, Last Name,
        // Email and/or mobile fields when a non-Intelematics User updates the similar fields in Account or Lead    
        boolean enformUpdatedByLeadOrAccountTrigger = (SP_Utilities.EnformUpdatedByAccountTrigger || SP_Utilities.EnformUpdatedByLeadTrigger);
        
        if (SP_Utilities.EnformUpdatedByAccountTrigger)
            SP_Utilities.EnformUpdatedByAccountTrigger = false;
            
        if (SP_Utilities.EnformUpdatedByLeadTrigger)
            SP_Utilities.EnformUpdatedByLeadTrigger = false;    
            
        // Before updating Enform User, we have to make sure that the updation is authorized. 
        // It will be a non-Intelematics who will update First Name, Last Name, Mobile, Email and
        // Password of Enform User when he updated the similar fields in Account or Lead. A non-Intelematics
        // User should not be allowed to directly edit those five fields (but can directly edit other fields).
        // If it is an Intelematics User, definitely it is a direct attempt to update Enform User record.       
                    
        if (trigger.isBefore) 
        {
            string action = 'insert';
            if (trigger.isUpdate)
            {
                action = 'update';
                
                set<String> set_FieldNames = new set<String>();
                set_FieldNames.add('first_name__c');
                set_FieldNames.add('last_name__c');
                set_FieldNames.add('mobile_phone__c');
                set_FieldNames.add('email__c');
                set_FieldNames.add('password__c');
                
                // If an Intelematics User tries to directly update an Enform User reord, it should be checked 
                // if the updated fields are alowed for updation by an Intelematics User. If an Intelematics User 
                // is not supposed to edit any of the non-permitted fields, an error should be thrown.
                if (UserInfo.getUserId() == SPCacheLexusEnformMetadata.getIntelematicsUser())
                {
                    set_FieldNames.add('intelematics_id__c');
                    set_FieldNames.add('usage_frequency__c');
                    set_FieldNames.add('usage_level__c');
                    set_FieldNames.add('usage_last_update_date__c');
                    set_FieldNames.add('isactive__c');
                    
                    set_FieldNames.add('app_launches_concierge__c');
                    set_FieldNames.add('app_launches_fuel_finder__c');
                    set_FieldNames.add('app_launches_local_search__c');
                    set_FieldNames.add('app_launches_traffic__c');
                    set_FieldNames.add('app_launches_weather__c');
                    set_FieldNames.add('accepts_terms_and_conditions__c');
                 
                    set_FieldNames.add('last_updated_date_concierge__c');
                    set_FieldNames.add('last_updated_date_fuel_finder__c');
                    set_FieldNames.add('last_updated_date_local_search__c');
                    set_FieldNames.add('last_updated_date_traffic__c');
                    set_FieldNames.add('last_updated_date_weather__c');
                    
                    set_FieldNames.add('activation_source__c');
                    
                    string strField = SP_Utilities.checkFieldChangeAllowed(trigger.newMap, trigger.oldMap, set_FieldNames); 
                    
                    if (strField != null)   
                        trigger.new[0].addError(' An Intelematics User is not allowed to edit ' + strField + '.');  
                }   
                // If it is a non-Intelematics User, it should be checked if it is triggered as result of Account/Lead
                // updation or a direct attempt.
                // If due to Account/Lead trigger, nothing to do. But if it is a direct attempt, it should be
                // assured that he is not changing First Name, Last Name, Mobile, Email and Password
                else
                {
                    // if that is not from Account trigger and trying to change those four fields, throw error
                    string strField = SP_Utilities.checkFieldChangeRestricted(trigger.newMap, trigger.oldMap, set_FieldNames);
                    
                    if (!enformUpdatedByLeadOrAccountTrigger && strField != null)
                        trigger.new[0].addError(' A non-Intelematics User is not allowed to directly edit ' + strField + '.');  
                }   
            }
                
            SP_EnformUserManagement.doValidateEnformUser(trigger.new, action);
        }
        // After inserting an Enform User, corresponding Lead should be created/fetched and linked to
        // secondary enform user
        // If an Intelematics User updates First Name, Last Name, Mobile, Email and Password, the corresponding 
        // fields in Account/Lead should also be set.
        else 
        {
            if (trigger.isInsert)
            {
                system.debug('@@@@@@@ TRIGGER AFTER ENFORM USER');
                SP_EnformUserManagement.doCreateLeadFromEnformUsers(trigger.newMap);
            }
            else if (UserInfo.getUserId() == SPCacheLexusEnformMetadata.getIntelematicsUser())
                SP_EnformUserManagement.doUpdateAccountsAndLeadsForEnformUsers(trigger.newMap, trigger.oldMap);
        }
    }
    catch(SPException ex) 
    {
        system.debug('####### CUSTOM EXCEPTION: Trigger SP_EnformUserManagement on Enform User Failed.');
        ex.Log();
        
        trigger.new[0].addError(ex.getMessage()); // Assume there is one record in trigger data
    }
    catch(Exception ex)
    {
        system.debug('####### SYSTEM EXCEPTION: Trigger SP_EnformUserManagement on Enform User Failed.');
        SPException.LogSystemException(ex);
        
        trigger.new[0].addError(ex.getMessage()); // Assume there is one record in trigger data
    }
}