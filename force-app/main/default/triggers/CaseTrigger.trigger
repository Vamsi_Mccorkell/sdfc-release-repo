/*******************************************************************************
@author:		Donnie Banez
@date:         	February 2019
@description:  	Trigger for Case 
@Test Methods:	CaseTriggerHandlerTest 
*******************************************************************************/
trigger CaseTrigger on Case (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    // Call Lead Trigger Handler 
    CaseTriggerHandler.mainEntry(
        Trigger.isBefore, Trigger.isAfter, 
        Trigger.isInsert, Trigger.isUpdate, 
        Trigger.isDelete, Trigger.isUnDelete,  
        Trigger.new, Trigger.old,
        Trigger.newMap, Trigger.oldMap 
    );
}