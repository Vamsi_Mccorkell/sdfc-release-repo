trigger SP_QASAddressUpdate on Address_After_Validation__c (before insert, before update)
{
	// Set of AFV ids and map of AFVs by id
	set<id>								m_setAFVIds				= new set<id>();
	map<id, Address_For_Validation__c>	m_mapAFVs				= new map<id, Address_For_Validation__c>();

	// sObject name
	string m_ObjectName;
	set<id>								m_setObjectIds			= new set<id>();
	set<id>								m_setObjectIdstoQuery	= new set<id>();


	map<id, Address_For_Validation__c>	m_mapAFVbySourceId		= new map<id, Address_For_Validation__c>();
	map<string, map<id, Address_For_Validation__c>>	m_mapObjecttoAFVs	= new map<string, map<id, Address_For_Validation__c>>();

	map<id, Address_After_Validation__c>	m_mapAAVbySourceId	= new map<id, Address_After_Validation__c>();

	// Confidence levels
	set<string>							m_setHighConfidence		= new set<string>{'9', '8', '7'};
	set<string>							m_setLowConfidence		= new set<string>{'6', '5', '4', '3', '2', '1', '0'};

	// List of sObjects to update with good quality Addresses
	list<sObject>						m_liObjectstoUpdate		= new list<sObject>();

	// List of Tasks to create for bad quality Addresses
	list<Task>							m_liTaskstoInsert		= new list<Task>();

	// List and map of address for validation records to update
	list<Address_For_Validation__c>		m_liAFVstoUpdate		= new list<Address_For_Validation__c>();


	/* First build a map of sObjects and their prefixes so we can properly identify the associated record	*/
	// Ask the schema for a list of objects
	map<string, schema.sObjectType> m_mapGlobalDescribe = schema.getGlobalDescribe();

	// Make a map of these objects and their prefixes
	map<string, string> m_mapObjectPrefixtoName = new map<string, string>();

	for (string strObjectName : m_mapGlobalDescribe.keySet())
	{
		schema.sObjectType sObjType = m_mapGlobalDescribe.get(strObjectName);

		string strOpportunityKeyPrefix = sObjType.getDescribe().getKeyPrefix();
		string strObjectName2 = sObjType.getDescribe().getName();

		if (strOpportunityKeyPrefix != null)
		{
			m_mapObjectPrefixtoName.put(strOpportunityKeyPrefix, strObjectName2);
		}
	}

	/*  Now process the trigger object */

	for (Address_After_Validation__c sAAV : trigger.new)
	{
		m_setAFVIds.add(sAAV.Address_For_Validation__c);
	}


	for (Address_For_Validation__c [] arrAFV :	[	select	Id,
															Source_Id__c,
															Name,
															Postcode__c,
															Postcode_Field_API_Name__c,
															Ready_For_Cleaning__c,
															State__c,
															State_Field_API_Name__c,
															Street_Address__c,
															Street_Address_Field_API_Name__c,
															Suburb__c,
															Suburb_Field_API_Name__c
													from	Address_For_Validation__c
													where	Id in :m_setAFVIds
												])
	{
		for (Address_For_Validation__c sAFV : arrAFV)
		{
			sAFV.Ready_For_Cleaning__c = false;

			// Save to map
			m_mapAFVs.put(sAFV.Id ,sAFV);

			// Also save to a map keyed on the source record id
			m_mapAFVbySourceId.put(sAFV.Source_Id__c ,sAFV);

			// Identify the sObject that the Address for Cleaning came from
			string strSourceIdPrefix = sAFV.Source_Id__c.subString(0, 3);

			if (!m_mapObjecttoAFVs.containsKey(strSourceIdPrefix))
			{
				map<id, Address_For_Validation__c> map_AFVs = new map<id, Address_For_Validation__c>();
				map_AFVs.put(sAFV.Id, sAFV);

				m_mapObjecttoAFVs.put(strSourceIdPrefix, map_AFVs);
			}
			else
			{
				map<id, Address_For_Validation__c> map_AFVs = m_mapObjecttoAFVs.remove(strSourceIdPrefix);
				map_AFVs.put(sAFV.Id, sAFV);

				m_mapObjecttoAFVs.put(strSourceIdPrefix, map_AFVs);
			}

			m_liAFVstoUpdate.add(sAFV);
		}
	}


	// Check the address confidence level and take appropriate action
	for (Address_After_Validation__c sAAV : trigger.new)
	{
		// Also create a map of AAVs keyed on source object id
		Address_For_Validation__c sAFV = m_mapAFVs.get(sAAV.Address_For_Validation__c);

		m_mapAAVbySourceId.put(sAFV.Source_Id__c, sAAV);


		if (m_setHighConfidence.contains(sAAV.Confidence__c))
		{
			m_setObjectIds.add(sAAV.Address_For_Validation__c);
		}

		if (m_setLowConfidence.contains(sAAV.Confidence__c))
		{
			Task sTask = new Task();

			sTask.Description 	= 'Confidence level: ' + sAAV.Confidence__c;
			sTask.Priority 		= 'Normal';
			sTask.Status 		= 'In Progress';
			sTask.Subject 		= 'QAS reports low confidence in an address';
			sTask.WhatId 		= sAFV.Source_Id__c;

			m_liTaskstoInsert.add(sTask);
		}
	}

	for (string strSourceIdPrefix : m_mapObjecttoAFVs.keySet())
	{
		map<id, Address_For_Validation__c> map_AFVs = m_mapObjecttoAFVs.get(strSourceIdPrefix);

		string strQuery;
		m_setObjectIdstoQuery = new set<id>();

		for (id idAFVId : map_AFVs.keySet())
		{
			Address_For_Validation__c sAFV = map_AFVs.get(idAFVId);

			// Prepare the sObject query
			strQuery = ' select	Id';

			if (sAFV.Street_Address_Field_API_Name__c != null && sAFV.Street_Address_Field_API_Name__c != '' && sAFV.Street_Address_Field_API_Name__c != ' ')
			{
				strQuery +=	', ' + sAFV.Street_Address_Field_API_Name__c;
			}

			if (sAFV.Suburb_Field_API_Name__c != null && sAFV.Suburb_Field_API_Name__c != '' && sAFV.Suburb_Field_API_Name__c != ' ')
			{
				strQuery += ', ' + sAFV.Suburb_Field_API_Name__c;
			}

			if (sAFV.State_Field_API_Name__c != null && sAFV.State_Field_API_Name__c != '' && sAFV.State_Field_API_Name__c != ' ')
			{
				strQuery += ', ' + sAFV.State_Field_API_Name__c;
			}

			if (sAFV.Postcode_Field_API_Name__c != null && sAFV.Postcode_Field_API_Name__c != '' && sAFV.Postcode_Field_API_Name__c != ' ')
			{
				strQuery += ', ' + sAFV.Postcode_Field_API_Name__c;
			}

			strQuery +=	' from 		' + m_mapObjectPrefixtoName.get(strSourceIdPrefix) +
						' where 	id in :m_setObjectIdstoQuery';

			// We only came in here once to get the field api names so finish now
			break;
		}

		// Get the object ids
		for (id idAFVId : map_AFVs.keySet())
		{
			Address_For_Validation__c sAFV = map_AFVs.get(idAFVId);
			m_setObjectIdstoQuery.add(sAFV.Source_Id__c);
		}

		list<sObject> li_sObjects = database.Query(strQuery);

		for (sObject sObj : li_sObjects)
		{
			Address_For_Validation__c sAFV = m_mapAFVbySourceId.get(string.valueOf(sObj.get('Id')));
			Address_After_Validation__c sAAV = m_mapAAVbySourceId.get(string.valueOf(sObj.get('Id')));

			if (sAFV.Street_Address_Field_API_Name__c != null && sAFV.Street_Address_Field_API_Name__c != '' && sAFV.Street_Address_Field_API_Name__c != ' ')
			{
				sObj.put(sAFV.Street_Address_Field_API_Name__c, sAAV.Street_Address__c);
			}

			if (sAFV.Suburb_Field_API_Name__c != null && sAFV.Suburb_Field_API_Name__c != '' && sAFV.Suburb_Field_API_Name__c != ' ')
			{
				sObj.put(sAFV.Suburb_Field_API_Name__c, sAAV.Suburb__c);
			}

			if (sAFV.State_Field_API_Name__c != null && sAFV.State_Field_API_Name__c != '' && sAFV.State_Field_API_Name__c != ' ')
			{
				sObj.put(sAFV.State_Field_API_Name__c, sAAV.State__c);
			}

			if (sAFV.Postcode_Field_API_Name__c != null && sAFV.Postcode_Field_API_Name__c != '' && sAFV.Postcode_Field_API_Name__c != ' ')
			{
				sObj.put(sAFV.Postcode_Field_API_Name__c, sAAV.Postcode__c);
			}
			
			sObj.put('Address_Cleaned__c', true);

			m_liObjectstoUpdate.add(sObj);
		}
	}


	// Update sObjects
	if (!m_liObjectstoUpdate.isEmpty())
	{
		update m_liObjectstoUpdate;
	}

	// Create Tasks
	if (!m_liTaskstoInsert.isEmpty())
	{
		insert m_liTaskstoInsert;
	}

	// Clear flags on the cleaned records
	if (!m_liAFVstoUpdate.isEmpty())
	{
		update m_liAFVstoUpdate;
	}

}