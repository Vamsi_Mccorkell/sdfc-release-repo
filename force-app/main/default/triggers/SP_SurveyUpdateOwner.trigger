trigger SP_SurveyUpdateOwner on Survey__c (before insert, before update)
{
	// Set of related Account ids
	set<id>				set_AccountIds			= new set<id>();

	// Map of Accounts and Contacts
	map<id, Account>	map_Accounts			= new map<id, Account>();
	map<id, Contact>	map_Contacts			= new map<id, Contact>();

	// Accounts and Contacts to be updated
	list<Account>		li_AccountstoUpdate		= new list<Account>();
	list<Contact>		li_ContactstoUpdate		= new list<Contact>();


	for (Survey__c sSurvey : trigger.new)
	{
		if (sSurvey.Customer__c != null)
			set_AccountIds.add(sSurvey.Customer__c);
	}

	for (Account [] arrAccounts :	[	select	Id,
	    										Next_Sales_Survey_Eligibility_Date__c,
	    										Next_Service_Survey_Eligibility_Date__c
	    								from	Account
	    								where	Id in :set_AccountIds
	    								and		(	RecordTypeId = :SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType() or
													RecordTypeId = :SPCacheRecordTypeMetadata.getPersonAccountProspectRecordType()
												)
	    							])
	{
		for (Account sAccount : arrAccounts)
		{
			map_Accounts.put(sAccount.Id, sAccount);
		}
	}

	for (Contact [] arrContacts :	[	select	Id,
												AccountId,
	    										Do_Not_Contact__c
	    								from	Contact
	    								where	AccountId in :set_AccountIds
	    							])
	{
		for (Contact sContact : arrContacts)
		{
			map_Contacts.put(sContact.AccountId, sContact);
		}
	}


	for (Survey__c sSurvey : trigger.new)
	{
		// Ignore any Survey that does not have a valid Account id
		if (!map_Accounts.containsKey(sSurvey.Customer__c))
		{
			sSurvey.Customer__c.AddError('Error: this Account does not exist');
			continue;
		}

	    Account sAccount = map_Accounts.get(sSurvey.Customer__c);

		// We're dealing with Owners and Prospects so there should always be a Contact record, but check to be sure
		if (!map_Contacts.containsKey(sSurvey.Customer__c))
		{
			sSurvey.Customer__c.AddError('Error: this Account does not have a matching Contact');
			continue;
		}

	    Contact sContact = map_Contacts.get(sSurvey.Customer__c);

		if (sSurvey.Survey_Date__c == null)
		{
			sSurvey.Survey_Date__c.AddError('Error: Survey Date is required');
			continue;
		}
				
		//Service sSurveys
		if (sSurvey.Survey_Type__c == 'Service')
		{
			if (	sSurvey.Contact_Code__c == 'Completed' || 
					sSurvey.Contact_Code__c == 'Abandoned' || 
					sSurvey.Contact_Code__c == 'Language Barrier' || 
					sSurvey.Contact_Code__c == 'Language')
			{
				sAccount.Last_Survey_Date__c = sSurvey.Survey_Date__c;
				sAccount.Next_Service_Survey_Eligibility_Date__c = sSurvey.Survey_Date__c.addMonths(12);

				li_AccountstoUpdate.add(sAccount);
			}
			else if (sSurvey.Contact_Code__c == 'No Marketing')
			{

				sAccount.Last_Survey_Date__c = sSurvey.Survey_Date__c;
				sAccount.Next_Service_Survey_Eligibility_Date__c = null;
				sAccount.Next_Sales_Survey_Eligibility_Date__c = null;
				sContact.Do_Not_Contact__c = true;

				li_AccountstoUpdate.add(sAccount);
				li_ContactstoUpdate.add(sContact);
			}
			else if (sSurvey.Contact_Code__c == 'No Survey')
			{
				sAccount.Last_Survey_Date__c = sSurvey.Survey_Date__c;
				sAccount.Service_CS_Survey_Opt_Out__c = true;
				sAccount.Sales_CS_Survey_Opt_Out__c = true;
				sAccount.Next_Service_Survey_Eligibility_Date__c = null;
				sAccount.Next_Sales_Survey_Eligibility_Date__c = null;

				li_AccountstoUpdate.add(sAccount);
			}
			else if (sSurvey.Contact_Code__c == 'Deceased')
			{
				sAccount.Last_Survey_Date__c = sSurvey.Survey_Date__c;
				sAccount.Deceased__c = true;
				sAccount.Next_Service_Survey_Eligibility_Date__c = null;
				sAccount.Next_Sales_Survey_Eligibility_Date__c = null;

				li_AccountstoUpdate.add(sAccount);
			}
			else if (	sSurvey.Contact_Code__c == 'Unserviced' || 
						sSurvey.Contact_Code__c == 'Incorrect Number')
			{
				sAccount.Last_Survey_Date__c = sSurvey.Survey_Date__c;

				if (	sAccount.Next_Service_Survey_Eligibility_Date__c < sSurvey.Survey_Date__c.addMonths(4) || 
						sAccount.Next_Service_Survey_Eligibility_Date__c == null)
				{
					sAccount.Next_Service_Survey_Eligibility_Date__c = sSurvey.Survey_Date__c.addMonths(4);
				}

				li_AccountstoUpdate.add(sAccount);
			}
		}
		
		//Sales sSurveys
		if (sSurvey.Survey_Type__c == 'Sales')
		{
			if (	sSurvey.Contact_Code__c == 'Completed' || 
					sSurvey.Contact_Code__c == 'Abandoned' || 
					sSurvey.Contact_Code__c == 'Language Barrier' || 
					sSurvey.Contact_Code__c == 'Language')
				{
					sAccount.Last_Survey_Date__c = sSurvey.Survey_Date__c;
					sAccount.Next_Sales_Survey_Eligibility_Date__c = sSurvey.Survey_Date__c.addMonths(12);

					li_AccountstoUpdate.add(sAccount);
			}
			else if (sSurvey.Contact_Code__c == 'No Marketing')
			{
				sAccount.Last_Survey_Date__c = sSurvey.Survey_Date__c;
				sAccount.Next_Sales_Survey_Eligibility_Date__c = null;
				sAccount.Next_Service_Survey_Eligibility_Date__c = null;
				sContact.Do_Not_Contact__c = true;

				li_AccountstoUpdate.add(sAccount);
				li_ContactstoUpdate.add(sContact);
			}
			else if (sSurvey.Contact_Code__c == 'No Survey')
			{
				sAccount.Last_Survey_Date__c = sSurvey.Survey_Date__c;
				sAccount.Sales_CS_Survey_Opt_Out__c = true;
				sAccount.Service_CS_Survey_Opt_Out__c = true;
				sAccount.Next_Sales_Survey_Eligibility_Date__c = null;
				sAccount.Next_Service_Survey_Eligibility_Date__c = null; 

				li_AccountstoUpdate.add(sAccount);
			}
			else if (sSurvey.Contact_Code__c == 'Deceased')
			{
				sAccount.Last_Survey_Date__c = sSurvey.Survey_Date__c;
				sAccount.Next_Sales_Survey_Eligibility_Date__c = null;
				sAccount.Next_Service_Survey_Eligibility_Date__c = null;
				sAccount.Deceased__c = true;

				li_AccountstoUpdate.add(sAccount);
			}
			else if (sSurvey.Contact_Code__c == 'Incorrect Number')
			{
				sAccount.Last_Survey_Date__c = sSurvey.Survey_Date__c;

				if (	sAccount.Next_Sales_Survey_Eligibility_Date__c < sSurvey.Survey_Date__c.addMonths(4) || 
						sAccount.Next_Sales_Survey_Eligibility_Date__c == null)
				{
					sAccount.Next_Sales_Survey_Eligibility_Date__c = sSurvey.Survey_Date__c.addMonths(4);
				}

				li_AccountstoUpdate.add(sAccount);
			}
		}
	}

	// Do the database updates
	if (!li_AccountstoUpdate.isEmpty())
	{
		SP_Utilities.AccountUpdatedBySurveyUpdOwnerTrigger = true;
		update li_AccountstoUpdate;
	}

	if (!li_ContactstoUpdate.isEmpty())
	{
		update li_ContactstoUpdate;
	}
}