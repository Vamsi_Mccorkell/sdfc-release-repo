/*******************************************************************************
@author:		Donnie Banez
@date:         	March 2019
@description:  	Trigger for Guest Relationship 
@Test Methods:	GuestRelationshipTriggerHandlerTest 
*******************************************************************************/
trigger GuestRelationshipTrigger on Guest_Relationship__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    // Call Guest Relationshop Trigger Handler 
    GuestRelationshipTriggerHandler.mainEntry(
        Trigger.isBefore, Trigger.isAfter, 
        Trigger.isInsert, Trigger.isUpdate, 
        Trigger.isDelete, Trigger.isUnDelete,  
        Trigger.new, Trigger.old,
        Trigger.newMap, Trigger.oldMap 
    );
}