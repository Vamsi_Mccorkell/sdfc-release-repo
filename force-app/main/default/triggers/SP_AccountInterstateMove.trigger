trigger SP_AccountInterstateMove on Account (before update)
{
    list<Task>          li_TasksToInsert    = new list<Task>();
    list<Task>          li_TasksToDelete    = new list<Task>();

    map<id, list<Task>>     map_AccounttoTasks  = new map<id, list<Task>>();

    // Get all Tasks associated with the Accounts
    Task [] arrTask =   [
                            select  Id,
                                    Subject,
                                    Status,
                                    WhatId
                            from    Task
                            where   WhatId in :trigger.oldMap.keySet()
                            and     Subject = 'Owner has moved interstate'
                            and     Status  = 'Not Started'
                        ];

    for (Task sTask : arrTask)
    {
        if (!map_AccounttoTasks.containsKey(sTask.WhatId))
        {
            list<Task> li_Tasks = new list<Task>();
            li_Tasks.add(sTask);
            map_AccounttoTasks.put(sTask.WhatId, li_Tasks);
        }
        else
        {
            list<Task> li_Tasks = new list<Task>();
            li_Tasks = map_AccounttoTasks.remove(sTask.WhatId);
            li_Tasks.add(sTask);
            map_AccounttoTasks.put(sTask.WhatId, li_Tasks);
        }
    }


    for (id idAccountId : trigger.oldMap.keySet())
    {
        Account sAccountOld = trigger.oldMap.get(idAccountId);
        Account sAccountNew = trigger.newMap.get(idAccountId);
        
        if (sAccountOld.RecordTypeId == SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType())
        {   
            // Only look for a change to the home address
           if (sAccountOld.ShippingState != null && sAccountNew.ShippingState !=null && sAccountOld.ShippingState != sAccountNew.ShippingState) 
            {
                // Do not create a new Task if the address change is being made by the CSA
                if (Userinfo.getProfileId().substring(0, 15) != SPCacheRecordTypeMetadata.getCSAProfileID().substring(0, 15))
                {
                    Task sTask = new Task();
    
                    sTask.Subject       = 'Owner has moved interstate ';            
                    sTask.ActivityDate  = System.today();
                    sTask.Status        = 'Not Started';
                    sTask.WhatId        = sAccountNew.Id;
                    sTask.OwnerId       = sAccountNew.OwnerId;
    
                    sTask.Description   = ' Home Address change from ' +
                                                sAccountOld.ShippingState +
                                                ' to ' +
                                                sAccountNew.ShippingState;
    
                    li_TasksToInsert.add(sTask);
                }
                
                // Now add the older, unprocessed Interstate move Tasks to the delete list
                list<Task> li_Tasks = map_AccounttoTasks.get(sAccountNew.Id);

                if (li_Tasks != null)
                {
                    li_TasksToDelete.addAll(li_Tasks);
                }
            }
        }
    }

    // Create new Teaks
    if (!li_TasksToInsert.isEmpty())
    {
        insert li_TasksToInsert;
    }

    // Delete old Tasks
    if (!li_TasksToDelete.isEmpty())
    {
        delete li_TasksToDelete;
    }

}