/*
   This trigger is used for Assist_Australia integration. Only when "Roadside Assistance" case is created, 
   this will be triggered and update Receipt_Time__c,Completion_Time__c,Arrival_Time__c fields.
*/

trigger Integration_case_timezone on Case (before insert) {
	set<String> breakDownCodeList = new set<String>{};
	
	
	for(Case ca: trigger.new ){
	 //check record type, If not "Roadside Assistance" don't do anything.	
	 //Sandbox   : "Roadside Assistance" recordTypeId: 012O00000008UXC
	 //if(ca.RecordTypeId != '012O00000008UXC'){continue;}
	 
	 //Production: "Roadside Assistance" recordTypeId: 012900000000YXq
	 if(ca.RecordTypeId != '012900000000YXq'){continue;}
	 
	 if (ca.Breakdown_Code__c != null && (breakDownCodeList.contains(ca.Breakdown_Code__c)== false)){breakDownCodeList.add(ca.Breakdown_Code__c);} 	
		
   	 //check null
   	 if(ca.Integration_date__c == Null || ca.Integration_date__c == '') continue;
     if(ca.Integration_date__c.length()!=8) continue; 
     
     if(ca.Integration_Receipt_Time__c == Null || ca.Integration_Receipt_Time__c == '') continue;
     //0:15 length is 4, from the data file  
     if(ca.Integration_Receipt_Time__c.length()==4){ 
     	ca.Integration_Receipt_Time__c = '0'+ ca.Integration_Receipt_Time__c;
     }
     //10:25 length is 5, otherwise not correct format
     else if(ca.Integration_Receipt_Time__c.length()!=5){continue;}
     
     if(ca.Integration_Completion_Time__c == Null || ca.Integration_Completion_Time__c == '') continue;
     if(ca.Integration_Completion_Time__c.length()==4) {
        ca.Integration_Completion_Time__c = '0'+ca.Integration_Completion_Time__c;
     }
     else if(ca.Integration_Completion_Time__c.length()!=5){continue;}
     
     if(ca.Integration_Arrival_Time__c == Null || ca.Integration_Arrival_Time__c == '') continue;
     if(ca.Integration_Arrival_Time__c.length()==4) {
        ca.Integration_Arrival_Time__c = '0'+ ca.Integration_Arrival_Time__c;
     }
     else if(ca.Integration_Arrival_Time__c.length()!=5){continue;}
     
     String year  = ca.Integration_date__c.substring(0,4);	
     String month = ca.Integration_date__c.substring(4,6);
     String day   = ca.Integration_date__c.substring(6,8);
     String Casedate = 	year + '-' + month+ '-' + day;
     String seconds = ':00';
     String Receipt_TimeDay = Casedate+ ' '+    ca.Integration_Receipt_Time__c + seconds;
     String Completion_TimeDay = Casedate+ ' '+ ca.Integration_Completion_Time__c + seconds;
     String Arrival_TimeDay = Casedate+ ' '+    ca.Integration_Arrival_Time__c + seconds;
     
     Datetime ReceiptDate =  datetime.valueOf(Receipt_TimeDay);

     ca.Receipt_Time__c = datetime.valueOf(Receipt_TimeDay);
     ca.Completion_Time__c = datetime.valueOf(Completion_TimeDay);
     ca.Arrival_Time__c = datetime.valueOf(Arrival_TimeDay);
   
   }
   
   if (breakDownCodeList.size()>0){
	
		Configuration__c[] config = new Configuration__c[]{};
		map<String,Configuration__c> configList = new map<String,Configuration__c>{};
		config = [select Name, Description__c,Owner_Or_Vehicle__c,Breakdown_Catalogue__c from Configuration__c where Name in :breakDownCodeList];
		if (config.size()> 0)
		{
			for (Configuration__c cf: config){
				configList.put(cf.Name, cf);
			}
		}
		
		for (Case ca: trigger.new){
			//system.assertEquals(configList.get(ca.Breakdown_Code__c).Description__c, ca.Description);
			ca.Description = configList.get(ca.Breakdown_Code__c).Description__c;
			ca.Subject = configList.get(ca.Breakdown_Code__c).Owner_Or_Vehicle__c;
			
		}
	}

}