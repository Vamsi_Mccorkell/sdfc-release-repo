@IsTest (SeeAllData=true) // required to see the custom settings in account
 public class DerivedMagicLinkUtilityTest {
	static Vehicle_Ownership__c validForOfferVO;
    static Asset__c asset;
    static Account customer;
    
    static testmethod void testDerivedMagicLink(){
        Test.setMock(HttpCalloutMock.class, new MockDerivedLinkHttpRespGenerator());
        dataSetup();
    }
    
    static void dataSetup(){
    	asset = new Asset__c();
        asset.Original_Sales_Date__c = system.today();
        asset.Encore_Tier__c = 'Platinum';
        asset.Name = 'TESTVIN';
        insert asset; 
        
        Id CUSTOMERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        customer = new Account(FirstName = 'Test', LastName = 'Test', RecordTypeId = CUSTOMERACCTRECORDTYPEID);
        customer.Guest_Gigya_ID__c = 'TESTGIGYA';
        insert customer;
        
        validForOfferVO = new Vehicle_Ownership__c(Customer__c = customer.Id, AssetID__c = asset.Id, Status__c = 'Active');
        insert validForOfferVO;
    }
}