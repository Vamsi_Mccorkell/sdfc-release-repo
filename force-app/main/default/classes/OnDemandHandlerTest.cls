// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for account creating accounts need to view the custom settings data
@IsTest (SeeAllData = true)
public class OnDemandHandlerTest {
	static Account partnerCustomer;
    static Account partnerVehicle;
    static Offer__c newOffer;
    static Offer__c vehicleOffer;
    static Vehicle_Ownership__c validForOfferVO;
    static Asset__c asset;
    static Account customer;
    static Customer_Offer_Detail__c cod;
    
    static testmethod void getOnDemandAvailabilityRequiredFields(){
        List<GuestServicesWebService.OnDemandWSResponse> wsoutput = OnDemandHandler.getOnDemandAvailability(null, null, null);
        system.assertEquals(false, wsoutput[0].success);
        system.assertEquals('700', wsoutput[0].errCode);
    }
    
    static testmethod void getOnDemandAvailabilityAcctIdGigyaNotMatch(){
        dataQuerySetup();
         List<GuestServicesWebService.OnDemandWSResponse> wsoutput = OnDemandHandler.getOnDemandAvailability(customer.Id, 'DIFFERENTGIGYA', null);
        system.assertEquals(false, wsoutput[0].success);
        system.assertEquals('601', wsoutput[0].errCode);
    }
    
    static testmethod void getOnDemandAvailabilityNoCustOffer(){
        dataQuerySetup();
        List<GuestServicesWebService.OnDemandWSResponse> wsoutput = OnDemandHandler.getOnDemandAvailability(customer.Id, customer.Guest_Gigya_ID__c, customer.Id);// pass incorrect COD Id
        system.assertEquals(false, wsoutput[0].success);
        system.assertEquals('602', wsoutput[0].errCode);
    }
    
    static testmethod void getOnDemandAvailabilitySuccessful(){
        dataQuerySetup();
        List<GuestServicesWebService.OnDemandWSResponse> wsoutput = OnDemandHandler.getOnDemandAvailability(customer.Id, customer.Guest_Gigya_ID__c, null);// pass incorrect COD Id
        for (GuestServicesWebService.OnDemandWSResponse ws : wsoutput){
            //if (ws.recordId == cod.Id){
              //  system.assertEquals(true, ws.success);
            //}
        }
    }
    
    static testmethod void writeOnDemandTransactionsRequiredFields(){
        GuestServicesWebService.OnDemandWSResponse wsoutput = OnDemandHandler.writeOnDemandTransactions(null, null, null, null, null, null, null, null, null, null, null, null, 
                                                                                                        null, null, null, null, null);
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('700', wsoutput.errCode);
    }
    
    static testmethod void writeOnDemandTransactionsAcctIdGigyaNotMatch(){
        dataQuerySetup();
        Datetime startDt = system.now();
        GuestServicesWebService.OnDemandWSResponse wsoutput = OnDemandHandler.writeOnDemandTransactions(customer.Id,'DIFFERENTGIGYA', cod.Id, 'Car Hire', 'Test location', 1, startDt, 12, 'start text', startDt + 5, 12, 'end text', 'Model', 'B001', 'Description test', 'Booked', 'REGOTEST');
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('601', wsoutput.errCode);
    }
    
    static testmethod void writeOnDemandTransactionsNoCustomerOffer(){
        dataQuerySetup();
        Datetime startDt = system.now();
        GuestServicesWebService.OnDemandWSResponse wsoutput = OnDemandHandler.writeOnDemandTransactions(customer.Id,customer.Guest_Gigya_ID__c, customer.Id, 'Car Hire', 'Test location', 1, startDt, 12, 'start text', startDt + 5, 12, 'end text', 'Model', 'B001', 'Description test', 'Booked', 'REGOTEST');
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('602', wsoutput.errCode);
    }
    
    static testmethod void writeOnDemandTransactionsTypeNotMatch(){
        dataQuerySetup();
        Datetime startDt = system.now();
        GuestServicesWebService.OnDemandWSResponse wsoutput = OnDemandHandler.writeOnDemandTransactions(customer.Id,customer.Guest_Gigya_ID__c, cod.Id, 'Valet Service', 'Test location', 1, startDt, 12, 'start text', startDt + 5, 12, 'end text', 'Model', 'B001', 'Description test', 'Booked', 'REGOTEST');
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('603', wsoutput.errCode);
    }
    
    
    static testmethod void writeOnDemandTransactionsSuccessful(){
        dataQuerySetup();
        Datetime startDt = system.now();
        Customer_Offer_Detail__c codUpdate = new Customer_Offer_Detail__c(Id = cod.Id, Total_Credits_Allowed__c  = 10);
        update codUpdate;
        GuestServicesWebService.OnDemandWSResponse wsoutput = OnDemandHandler.writeOnDemandTransactions(customer.Id,customer.Guest_Gigya_ID__c, cod.Id, 'Car Hire', 'Test location', 1, startDt, 12, 'start text', startDt + 5, 12, 'end text', 'Model', 'B001', 'Description test', 'Booked', 'REGOTEST');
        system.assertEquals(null, wsoutput.errorDesc);
        system.assertEquals(true, wsoutput.success);
    }
    
    static testmethod void editOnDemandTransactionsRequiredFields(){
        GuestServicesWebService.OnDemandWSResponse wsoutput = OnDemandHandler.editOnDemandTransactions(null, null, null, null, null, null, null, null, null, null, null);
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('700', wsoutput.errCode);
    }
    
    static testmethod void editOnDemandTransactionsSuccessful(){
        dataQuerySetup();
        Datetime startDt = system.now();
        Customer_Offer_Detail__c codUpdate = new Customer_Offer_Detail__c(Id = cod.Id, Total_Credits_Allowed__c  = 10);
        update codUpdate;
        GuestServicesWebService.OnDemandWSResponse wsoutput = OnDemandHandler.writeOnDemandTransactions(customer.Id,customer.Guest_Gigya_ID__c, cod.Id, 'Car Hire', 'Test location', 1, startDt, 12, 'start text', startDt + 5, 12, 'end text', 'Model', 'B001', 'Description test', 'Booked', 'REGOTEST');
        system.assertEquals(null, wsoutput.errorDesc);
        system.assertEquals(true, wsoutput.success);
        
        wsoutput = OnDemandHandler.editOnDemandTransactions('B001', null, null, null, null, null, null, null, 'NEWMODEL', null, null);
        List<Customer_Offer_Transaction__c> cotResult = [select Id, Redemption_Model__c from Customer_Offer_Transaction__c where Booking_Reference__c = 'B001' limit 1];
        system.assertEquals('NEWMODEL', cotResult[0].Redemption_Model__c);
    }
    
    static testmethod void getGuestVoucherAvailabilityRequiredFields(){
        List<GuestServicesWebService.VoucherAvailabilityWSResponse> wsoutput = OnDemandHandler.getGuestVoucherAvailability(null, null);
        system.assertEquals(false, wsoutput[0].success);
        system.assertEquals('700', wsoutput[0].errCode);
    }
    
    static testmethod void getGuestVoucherAvailabilityNoMatchingAccount(){
        dataQuerySetup();
        List<GuestServicesWebService.VoucherAvailabilityWSResponse> wsoutput = OnDemandHandler.getGuestVoucherAvailability('NOTEXISTINGGIGYA', validForOfferVO.Id);
        system.assertEquals(false, wsoutput[0].success);
        system.assertEquals('606', wsoutput[0].errCode);
    }
    
    static testmethod void getGuestVoucherAvailabilityNoVOOfferDetails(){
        dataQuerySetup();
        List<GuestServicesWebService.VoucherAvailabilityWSResponse> wsoutput = OnDemandHandler.getGuestVoucherAvailability(customer.Guest_Gigya_ID__c, validForOfferVO.Id);
        system.assertEquals(false, wsoutput[0].success);
        system.assertEquals('605', wsoutput[0].errCode);
    }
    
    static testmethod void getGuestVoucherAvailabilitySuccesful(){
        dataQuerySetup();
        List<GuestServicesWebService.VoucherAvailabilityWSResponse> wsoutput = OnDemandHandler.getGuestVoucherAvailability(customer.Guest_Gigya_ID__c, validForOfferVO.Id);
    }
    
    
    static testmethod void writeOnDemandTransactionsNotEnoughCredits(){
        dataQuerySetup();
        Datetime startDt = system.now();
        GuestServicesWebService.OnDemandWSResponse wsoutput = OnDemandHandler.writeOnDemandTransactions(customer.Id,customer.Guest_Gigya_ID__c, cod.Id, 'Car Hire', 'Test location', 100000, startDt, 12, 'start text', startDt + 5, 12, 'end text', 'Model', 'B001', 'Description test', 'Booked', 'REGOTEST');
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('604', wsoutput.errCode);
        
    }
    
    static testmethod void getPartnersandBranchesSuccessful(){
        List<GuestServicesWebService.PartnerBranchesWSResponse> wsoutput = OnDemandHandler.getPartnersandBranches();
        //system.assertEquals(true, wsoutput.size() > 1);
        //system.assertEquals(true, wsoutput[0].success);
        
    }
    
    static testmethod void codeCompen(){
        OnDemandHandler.codeCompensate();
    }
    
    static testmethod void cancelOnDemandTransactionRequiredFields(){
        GuestServicesWebService.OnDemandWSResponse wsoutput = OnDemandHandler.cancelOnDemandTransaction(null, null, null, null);
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('700', wsoutput.errCode);
    }
    
    static testmethod void cancelOnDemandTransactionNoBookingRef(){
        dataQuerySetup();
        GuestServicesWebService.OnDemandWSResponse wsoutput = OnDemandHandler.cancelOnDemandTransaction(customer.Id, customer.Guest_Gigya_ID__c, 'DIFFERENTBOOKID', true);
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('609', wsoutput.errCode);
    }
    
    static testmethod void cancelOnDemandTransactionGigyaIdAccountIdNotMatch(){
        dataQuerySetup();
        Customer_Offer_Transaction__c ot = [select Id, Booking_Reference__c from Customer_Offer_Transaction__c where Booking_Status__c = 'Booked' and Booking_Reference__c != null limit 1];
        GuestServicesWebService.OnDemandWSResponse wsoutput = OnDemandHandler.cancelOnDemandTransaction(customer.Id, 'DIFFERENTGIGYA', ot.Booking_Reference__c, true);
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('601', wsoutput.errCode);
    }
    
    static testmethod void cancelOnDemandTransactionBookingNotMatchCustomer(){
        dataQuerySetup();
        Customer_Offer_Transaction__c ot = [select Id, Booking_Reference__c from Customer_Offer_Transaction__c where Booking_Status__c = 'Booked' and Booking_Reference__c != null limit 1];
        GuestServicesWebService.OnDemandWSResponse wsoutput = OnDemandHandler.cancelOnDemandTransaction(customer.Id, customer.Guest_Gigya_ID__c, ot.Booking_Reference__c, true);
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('611', wsoutput.errCode);
    }
    
    static testmethod void cancelOnDemandTransactionBookingSuccessful(){
        dataQuerySetup();
        Customer_Offer_Transaction__c ot = [select Id, Booking_Reference__c, CustomerOffer__r.Customer__c, CustomerOffer__r.Customer__r.Guest_Gigya_ID__c
                                            from Customer_Offer_Transaction__c where Booking_Status__c = 'Booked' and Booking_Reference__c != null limit 1];
        GuestServicesWebService.OnDemandWSResponse wsoutput = OnDemandHandler.cancelOnDemandTransaction(ot.CustomerOffer__r.Customer__c, ot.CustomerOffer__r.Customer__r.Guest_Gigya_ID__c, ot.Booking_Reference__c, true);
        //system.assertEquals(true, wsoutput.success);
        //system.assertEquals(ot.Id, wsoutput.recordId);
    }
    
    private static void dataQuerySetup(){
        // This is a workaround data setup since creating VO's on the fly calls a big SOQL that pushes the limit
        List<Account> newAcctList = new List<Account>();
        List<Vehicle_Ownership__c> voList = new List<Vehicle_Ownership__c>();
        List<Asset__c> assetList = new List<Asset__c>();
        
        Id PARTNERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner').getRecordTypeId();
        
        partnerCustomer = new Account(Name = 'Partner Cust', RecordTypeId = PARTNERACCTRECORDTYPEID, Partner_Class__c = 'Customer');
        partnerVehicle = new Account(Name = 'Partner Veh', RecordTypeId = PARTNERACCTRECORDTYPEID, Partner_Class__c = 'Vehicle');
        newAcctList.add(partnerCustomer);
        newAcctList.add(partnerVehicle);
        insert newAcctList;
        
        
        validForOfferVO = [select Id, Customer__c, AssetID__c from Vehicle_Ownership__c 
                          where Status__c = 'Active' and Valid_for_Offers__c = false limit 1]; // try to get 1 that isn't valid for offer yet
        Asset__c asst = new Asset__c(Id = validForOfferVO.AssetID__c);
        asst.Original_Sales_Date__c = system.today();
        asst.Encore_Tier__c = 'Platinum';
        update asst;    
        customer = [select Id, Guest_Gigya_ID__c from Account where Id = :validForOfferVO.Customer__c limit 1];
        customer.Guest_Gigya_ID__c = 'TESTGIGYA';
        update customer;
        validForOfferVO.Valet_Voucher_Enabled__c = true;
        validForOfferVO.Voucher_T_C_Accepted__c = true;
        validForOfferVO.Voucher_T_C_Accepted_Date__c = system.today();
        validForOfferVO.Asset_RDR_Date__c = system.today();
        update validForOfferVO;
        
        newOffer = new Offer__c(Status__c = 'Inactive');
        newOffer.Offer_Class__c = 'Customer';
        newOffer.Offer_Type__c = 'Car Hire';
        newOffer.Customer_Platinum_Limit__c = 10;
        newOffer.Partner_Account__c = partnerCustomer.Id;
        insert newOffer;
        newOffer.Status__c = 'Active';
        update newOffer;
        
        vehicleOffer = new Offer__c(Status__c = 'Inactive');
        vehicleOffer.Offer_Class__c = 'Vehicle';
        vehicleOffer.Offer_Type__c = 'Valet Service';
        vehicleOffer.Vehicle_Offer_Platinum_Limit__c = 10;
        vehicleOffer.Partner_Account__c = partnerVehicle.Id;
        insert vehicleOffer;
        vehicleOffer.Status__c = 'Active';
        update vehicleOffer;
        List<Customer_Offer_Detail__c> codList = [select Id from Customer_Offer_Detail__c where Offer__c = :newOffer.Id and Customer__c = :customer.Id limit 1];
        if (codList.size() > 0){
            cod = codList[0];
        }
        else {
            cod = new Customer_Offer_Detail__c(Offer__c = newOffer.Id, Customer__c = customer.Id);
            insert cod;
        }
    }
}