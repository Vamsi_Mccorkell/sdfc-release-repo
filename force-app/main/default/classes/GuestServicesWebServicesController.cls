/* DATE: September 2019
 * AUTHOR: D BANEZ / MCCORKELL
 * CLASS SUMMARY: Guest Services WS controller to allow LCAC to call the Guest Services Web Service methods
 * 					
 * VF PAGE USING THIS CLASS: GuestServicesWSTestPage
 * TEST CLASS: GuestServicesWebServicesControllerTest
 * */
public with sharing class GuestServicesWebServicesController {
    public String guestAcctId {get;set;}
    public String gigyaId {get;set;}
    public String codId {get;set;}
    
    public String sfWriteAcctId {get;set;}
    public String writeGigyaId {get;set;}
    public String writecodId {get;set;}
    public String redemptionLocation {get;set;}
    public String bookingReference {get;set;}
    public Integer redemptionCredits {get;set;}
    public String redemptionModel {get;set;} 
    public String redemptionDescription {get;set;}
    public String bookingStatus {get;set;}
    public String redemptionRego {get;set;}
    public Customer_Offer_Transaction__c redemption {get;set;}
    public Voucher__c voucher {get;set;}
    public String voodGigyaId {get;set;}
    public String voodSFVOId {get;set;}
    
    public String editBookReference {get;set;}
    public String editLocation {get;set;}
    public String editModel {get;set;} 
    public String editDescription {get;set;}
    public String editRego {get;set;}
    public Customer_Offer_Transaction__c editRedemption {get;set;}
    
    public String cancelSfAcctId {get;set;}
    public String cancelGigyaId {get;set;}
    public String cancelBookingReference {get;set;}
    public Boolean creditCustomer {get;set;}
    
    public List<GuestServicesWebService.OnDemandWSResponse> onDAvailabilityResponseList {get;set;}
    public GuestServicesWebService.OnDemandWSResponse writeOnDemandTrans {get;set;}
    public GuestServicesWebService.OnDemandWSResponse editOnDemandTrans {get;set;}
    public List<GuestServicesWebService.VoucherAvailabilityWSResponse> voodResultList {get;set;}
    public List<GuestServicesWebService.PartnerBranchesWSResponse> partnerBranchList {get;set;}
    public GuestServicesWebService.OnDemandWSResponse cancelOnDemandTrans {get;set;}
    
    public GuestServicesWebServicesController(){
        onDAvailabilityResponseList = new List<GuestServicesWebService.OnDemandWSResponse>();
        writeOnDemandTrans = new GuestServicesWebService.OnDemandWSResponse();
        editOnDemandTrans = new GuestServicesWebService.OnDemandWSResponse();
        voodResultList = new List<GuestServicesWebService.VoucherAvailabilityWSResponse>();
        partnerBranchList = new List<GuestServicesWebService.PartnerBranchesWSResponse>();
        cancelOnDemandTrans = new GuestServicesWebService.OnDemandWSResponse();
        creditCustomer = false;
        redemption = new Customer_Offer_Transaction__c();
        editRedemption = new Customer_Offer_Transaction__c();
        Id CUSTOMERVOUCHERRECORDTYPEID = Schema.SObjectType.Voucher__c.getRecordTypeInfosByDeveloperName().get('Customer_Voucher').getRecordTypeId();
        voucher = new Voucher__c(RecordTypeId = CUSTOMERVOUCHERRECORDTYPEID);
    }
    
    public PageReference getOnDemandAvailability(){
        Boolean validId = true;
        String errMessage = '';
        if (codId == '') codId = null;
        if (guestAcctId == '') guestAcctId = null;
        String idRegex = '[a-zA-Z0-9]{18}|[a-zA-Z0-9]{15}';
        if (codId != null){
            validId = Pattern.matches(idRegex, codId);
            errMessage = 'Invalid Customer Offer Detail ID format';
            
        }
        if (guestAcctId != null){
            validId = Pattern.matches(idRegex, guestAcctId);
            errMessage += '; Invalid Guest Account ID format';
            
        }
        if (!validId){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, errMessage));
        }
        
        if (validId) onDAvailabilityResponseList = GuestServicesWebService.getOnDemandAvailability(guestAcctId, gigyaId, codId);
        return null;
    }
    
    public PageReference writeOnDemandTransactions(){
        Boolean validId = true;
        String errMessage = '';
        
        if (sfWriteAcctId == '') sfWriteAcctId = null;
        if (writecodId == '') writecodId = null;
        
        String idRegex = '[a-zA-Z0-9]{18}|[a-zA-Z0-9]{15}';
        if (sfWriteAcctId != null && sfWriteAcctId != ''){
            validId = Pattern.matches(idRegex, sfWriteAcctId);
            errMessage = 'Invalid Guest Account ID format';
            
        }
        if (writecodId != null && writecodId != ''){
            validId = Pattern.matches(idRegex, writecodId);
            errMessage += '; Invalid Customer Offer Detail ID format';
            
        }
        if (!validId){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, errMessage));
        }
        
        if (validId) writeOnDemandTrans = GuestServicesWebService.writeOnDemandTransactions (sfWriteAcctId, writeGigyaId, writecodId, voucher.Voucher_Type__c, redemptionLocation, redemptionCredits, 
                                                                                redemption.Redemption_Start_Date_Time__c, redemption.Start_Date_Time_Offset__c, redemption.Start_Date_Time_Text__c,
                                                                                redemption.Redemption_End_Date_Time__c, redemption.End_Date_Time_Offset__c, redemption.End_Date_Time_Text__c, 
                                                                                redemptionModel, bookingReference, redemptionDescription, bookingStatus, redemptionRego);
        
        return null;
    }
    
    
    public PageReference editOnDemandTransactions(){
        editOnDemandTrans = GuestServicesWebService.editOnDemandTransactions(editBookReference, editLocation, editRedemption.Redemption_Start_Date_Time__c, editRedemption.Start_Date_Time_Offset__c, editRedemption.Start_Date_Time_Text__c, 
                                                                                      editRedemption.Redemption_End_Date_Time__c, editRedemption.End_Date_Time_Offset__c, editRedemption.End_Date_Time_Text__c, editModel, editDescription, editRego);
        system.debug(LoggingLevel.ERROR, '## editOnDemandTrans: ' + editOnDemandTrans);
        return null;
    }
    public PageReference getGuestVoucherAvailability(){
        Boolean validId = true;
        String errMessage = '';
        if (voodSFVOId == '') voodSFVOId = null;
        String idRegex = '[a-zA-Z0-9]{18}|[a-zA-Z0-9]{15}';
        if (voodSFVOId != null){
            validId = Pattern.matches(idRegex, voodSFVOId);
            errMessage = 'Invalid VO Offer Detail ID format';
            
        }
        if (!validId){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, errMessage));
        }
        
        if (validId) voodResultList = GuestServicesWebService.getGuestVoucherAvailability(voodGigyaId, voodSFVOId);
        return null;
    }
    
    public PageReference getPartnersandBranches(){
        partnerBranchList = GuestServicesWebService.getPartnersandBranches();
        return null;
    }
    
    public PageReference cancelOnDemandTransaction(){
        Boolean validId = true;
        String errMessage = '';
        
        if (cancelSfAcctId == '') cancelSfAcctId = null;
        String idRegex = '[a-zA-Z0-9]{18}|[a-zA-Z0-9]{15}';
        if (cancelSfAcctId != null){
            validId = Pattern.matches(idRegex, cancelSfAcctId);
            errMessage = 'Invalid Guest Account ID format';
            
        }
        if (!validId){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, errMessage));
        }
        
        if (validId) cancelOnDemandTrans = GuestServicesWebService.cancelOnDemandTransaction (cancelSfAcctId, cancelGigyaId, cancelBookingReference, creditCustomer);
        return null;
    }
    
}