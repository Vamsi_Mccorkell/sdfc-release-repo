public with sharing class SPCacheCustomerMatchingMetadata
{
	private static SPCacheCustomerMatchingMetadata__c CacheCustomerMatchingMetadata;
	
	/***********************************************************************************************************
		Lookup Cached information
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	public static string getDefaultCaseRecordType()
	{
		if(CacheCustomerMatchingMetadata == null)
			LoadMetadata(); 
			
		return CacheCustomerMatchingMetadata.DefaultCaseRecordType__c;
	}

	public static string getDefaultCaseType()
	{
		if(CacheCustomerMatchingMetadata == null)
			LoadMetadata(); 
			
		return CacheCustomerMatchingMetadata.DefaultCaseType__c;
	}

	public static string getDefaultCaseStatus()
	{
		if(CacheCustomerMatchingMetadata == null)
			LoadMetadata(); 
			
		return CacheCustomerMatchingMetadata.DefaultCaseStatus__c;
	}

	public static string getDefaultCaseOrigin()
	{
		if(CacheCustomerMatchingMetadata == null)
			LoadMetadata(); 
			
		return CacheCustomerMatchingMetadata.DefaultCaseOrigin__c;
	}

	public static string getIntegrationUserName()
	{
		if(CacheCustomerMatchingMetadata == null)
			LoadMetadata(); 
			
		return CacheCustomerMatchingMetadata.Integration_User_Name__c;
	}

	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	private static void LoadMetadata()
	{
		CacheCustomerMatchingMetadata = SPCacheCustomerMatchingMetadata__c.getInstance();
	}
	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	public static testmethod void TestSPCacheCustomerMatchingMetadata()
	{
		SPCacheCustomerMatchingMetadata.getDefaultCaseRecordType();
		CacheCustomerMatchingMetadata = null;

		SPCacheCustomerMatchingMetadata.getDefaultCaseType();
		CacheCustomerMatchingMetadata = null;

		SPCacheCustomerMatchingMetadata.getDefaultCaseStatus();
		CacheCustomerMatchingMetadata = null;

		SPCacheCustomerMatchingMetadata.getDefaultCaseOrigin();
		CacheCustomerMatchingMetadata = null;

		SPCacheCustomerMatchingMetadata.getIntegrationUserName();
		CacheCustomerMatchingMetadata = null;

	}
}