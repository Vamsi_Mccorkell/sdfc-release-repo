@IsTest (SeeAllData = true)
public class CaseTriggerHandlerTest {
    static testmethod void testCaseTrigger(){
        Account dealerAccount = new Account(Name = 'Test Dealer', Type = 'Dealer',
                                            RecordTypeId = SPCacheRecordTypeMetadata.getAccount_Dealer(),
                                            Status__c = 'Active');
        insert dealerAccount;
        Id DEALERCONTACTRECORDID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Dealer Contact').getRecordTypeId();
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test', 
                                  Email = 'contact@noreply.com', RecordTypeId = DEALERCONTACTRECORDID,
                                  AccountId = dealerAccount.Id, To_be_Sent_on_Email_Distribution_List__c = true);
        insert con;
        Case newCase = new Case(Subject = 'Test', Description ='Test', Type = 'Facebook Lead', AccountId = dealerAccount.Id,
                               ContactId = con.Id, Selected_Dealer_Name__c = 'Test Dealer');
        insert newCase;
        Case resultCase = [select Id, TempEmailStrings__c, TempFirstName__c from Case
                          where Id = :newCase.Id limit 1];
        //system.assertEquals('Test', resultCase.TempFirstName__c);
        //system.assertEquals('This email has been sent to: contact@noreply.com', resultCase.TempEmailStrings__c);
    }
}