// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for creating accounts need to view the custom settings data
@IsTest (SeeAllData=true)
public class MyLexusWebServicesControllerTest {
    /* DEVELOPER NOTES: This is solely a coverage test method class; The actual assertion test method classes are 
     * in the respective handler test method classes
	*/
    public static Account customer;
    public static Vehicle_Ownership__c vo;
    public static Asset__c vehicle;
    public static Case c;
    
    static testmethod void guestRegistration(){
        dataSetup();
        MyLexusWebServicesController cont = new MyLexusWebServicesController();
        cont.firstName = 'Henry';
        cont.lastName = 'Registration';
        cont.email ='email@email.com';
        cont.mobile = '0211111111';
        cont.gigyaId = 'gigyatest';
        cont.sfAcctId = null;
        cont.sfVOId = null;
        cont.guestRegistration();
    }
    
    static testmethod void vehicleAssociation(){
        dataSetup();
        MyLexusWebServicesController cont = new MyLexusWebServicesController();
        cont.vehicleAssocAcctId = customer.Id;
        cont.vinVehicleAssoc = vehicle.Name;
        cont.vehicleAssociation();
    }
    
    static testmethod void vehicleVerification(){
        dataSetup();
        MyLexusWebServicesController cont = new MyLexusWebServicesController();
        cont.vehicleVeriAcctId = customer.Id;
        cont.vinVehicleVerif = vehicle.Name;
        cont.batchNumber = 'Batch Number';
        cont.vehicleVeriVOId = vo.Id;
        cont.vehicleVerification();
    }
    
    static testmethod void createUpdateCase(){
        dataSetup();
        MyLexusWebServicesController cont = new MyLexusWebServicesController();
        cont.caseSfAcctId = customer.Id;
        cont.caseNumber = c.CaseNumber;
        cont.callBackPhone = '01111111';
        cont.timeSlot = 'Test Time Slot';
        cont.createUpdateCase();
    }
    
    static testmethod void deleteRegistration(){
        dataSetup();
        MyLexusWebServicesController cont = new MyLexusWebServicesController();
        cont.deleteGigyaId = customer.Guest_Gigya_ID__c;
        cont.deleteEmail = customer.PersonEmail;
        cont.deleteRegistration();
    }
    
    static testmethod void getCustomerDetails(){
        dataSetup();
        MyLexusWebServicesController cont = new MyLexusWebServicesController();
        cont.customerDetailGigyaId = customer.Guest_Gigya_ID__c;
        cont.getCustomerDetails();
    }
    
    static testmethod void writeCustomerDetails(){
        dataSetup();
        MyLexusWebServicesController cont = new MyLexusWebServicesController();
        cont.inputAcct.gigyaId = customer.Guest_Gigya_ID__c;
        cont.inputAcct.firstName = 'Test';
        cont.inputAcct.lastName = 'Test';
        cont.writeCustomerDetails();
    }
    
    static testmethod void writeCommunicationPreferences(){
        dataSetup();
        MyLexusWebServicesController cont = new MyLexusWebServicesController();
        cont.inputAcctCommPref.gigyaId = customer.Guest_Gigya_ID__c;
        cont.inputAcctCommPref.homeStreet = 'home street';
        cont.inputAcctCommPref.homeSuburb = 'home suburb';
        cont.inputAcctCommPref.postalSameAsHome = true;
        cont.writeCommunicationPreferences();
    }
    
    static testmethod void updateTermsPolicyVersion(){
        dataSetup();
        MyLexusWebServicesController cont = new MyLexusWebServicesController();
        cont.versionAcctId = customer.Id;
        cont.termsAndConditionsVersion = 'TCV1';
        cont.privacyPolicyVersion = 'PPV1';
        cont.updateTermsPolicyVersion();
    }
    
    static testmethod void updateStripeCustomerId(){
        dataSetup();
        MyLexusWebServicesController cont = new MyLexusWebServicesController();
        cont.stripeAcctId = customer.Id;
        cont.stripeCustomerId = 'stripeXXX';
        cont.updateStripeCustomerId();
    }
    
    static testmethod void getGuestVehicles(){
        dataSetup();
        MyLexusWebServicesController cont = new MyLexusWebServicesController();
        cont.guestVehicleGigyaId = 'GIGYA1111';
        cont.getGuestVehicles();
    }
    
    private static void dataSetup(){
        Id OWNERRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        customer = new Account(FirstName = 'Henry', LastName = 'Sample', PersonEmail = 'sample@henry.com',
                               Guest_Gigya_ID__c = 'GIGYA1111', RecordTypeId = OWNERRECORDTYPEID);
        insert customer;
        
        vehicle = new Asset__c(Name = 'VIN1111');
        insert vehicle;
        
        vo = new Vehicle_Ownership__c(Customer__c = customer.Id, AssetID__c = vehicle.Id, Start_Date__c = system.today(),
                                     Status__c = 'Active', Vehicle_Type__c = 'New Vehicle');
        insert vo;
        
        c = new Case(AccountId = customer.Id, Subject = 'Test');
        insert c;
        c = [select Id, CaseNumber from Case where Id = :c.Id limit 1];
    }
}