/* DATE: September 2019
 * AUTHOR: D BANEZ / MCCORKELL
 * CLASS SUMMARY: MyLexus WS controller to allow LCAC to call the My Lexus Web Service methods
 * 					
 * VF PAGE USING THIS CLASS: MyLexusWSTestPage
 * TEST CLASS: MyLexusWebServicesControllerTest
 * */
public with sharing class MyLexusWebServicesController {
    public String firstName {get;set;}
    public String lastName {get;set;}
    public String email {get;set;}
    public String mobile {get;set;}
    public String gigyaId {get;set;}
    public String sfAcctId {get;set;}
    public String sfVOId {get;set;}
    
    public String vehicleAssocAcctId {get;set;}
    public String vinVehicleAssoc {get;set;}
    
    public String vehicleVeriAcctId {get;set;}
    public String vinVehicleVerif {get;set;}
    public String batchNumber {get;set;}
    public String vehicleVeriVOId {get;set;}
    
    public String caseNumber {get;set;}
    public String callBackPhone {get;set;}
    public String timeSlot {get;set;}
    public String caseSfAcctId {get;set;}
    
    public String deleteGigyaId {get;set;}
    public String deleteEmail {get;set;}
    
    public String customerDetailGigyaId {get;set;}
    public String customerAcctId {get;set;}
    public String customerVoId {get;set;}
    
    public String versionAcctId {get;set;}
    public String termsAndConditionsVersion {get;set;}
    public String privacyPolicyVersion {get;set;}
    
    public String stripeAcctId {get;set;}
    public String stripeCustomerId {get;set;}
    
    public String guestVehicleGigyaId {get;set;}
    
    public MyLexusWebService.GuestAccount inputAcct {get;set;}
    public MyLexusWebService.GuestAccount inputAcctCommPref {get;set;}
    
    public MyLexusWebService.GRResponse guestRegResponse {get;set;}
    public MyLexusWebService.WSResponse updateVersionResponse {get;set;}
    public MyLexusWebService.WSResponse stripeCustomerIdResponse {get;set;}
    public MyLexusWebService.WSResponse vehicleAssocResponse {get;set;}
    public MyLexusWebService.WSResponse vehicleVeriResponse {get;set;}
    public MyLexusWebService.WSResponse caseUpdateCreateResponse {get;set;}
    public MyLexusWebService.WSResponse delRegistrationResponse {get;set;}
    
    public MyLexusWebService.GuestProfileResponse guestProfileResponse {get;set;}
    
    public MyLexusWebService.GuestProfileResponse writeCustDetailsResponse {get;set;}
    public MyLexusWebService.GuestProfileResponse writeCommPrefResponse {get;set;}
    
    public MyLexusWebService.GuestVehicleWSResponse guestVehicleResponse {get;set;}
    
    public MyLexusWebServicesController(){
       inputAcct = new MyLexusWebService.GuestAccount();  
       inputAcctCommPref = new MyLexusWebService.GuestAccount();  
    }
    
    public PageReference guestRegistration(){
        Boolean validId = true;
        String errMessage = '';
        
        if (sfAcctId == '') sfAcctId = null;
        if (sfVOId == '') sfVOId = null;
        String idRegex = '[a-zA-Z0-9]{18}|[a-zA-Z0-9]{15}';
        if (sfAcctId != null){
            validId = Pattern.matches(idRegex, sfAcctId);
            errMessage = 'Invalid Guest Account ID format';
            
        }
        
        if (sfVOId != null){
            validId = Pattern.matches(idRegex, sfVOId);
            errMessage += '; Invalid VO ID format';
            
        }if (!validId){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, errMessage));
        }
        if (validId) guestRegResponse = MyLexusWebService.guestRegistration(firstName, lastName, email, mobile, gigyaId, sfAcctId, sfVOId);
        return null;
    }
    
    public PageReference vehicleAssociation(){
        Boolean validId = true;
        String errMessage = '';
        
        if (vehicleAssocAcctId == '') vehicleAssocAcctId = null;
        String idRegex = '[a-zA-Z0-9]{18}|[a-zA-Z0-9]{15}';
        if (vehicleAssocAcctId != null){
            validId = Pattern.matches(idRegex, vehicleAssocAcctId);
            errMessage = 'Invalid Guest Account ID format';
            
        }
        if (!validId){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, errMessage));
        }
        if (validId) vehicleAssocResponse = MyLexusWebService.vehicleAssociation(vehicleAssocAcctId, vinVehicleAssoc);
        return null;
    }
    
    public PageReference vehicleVerification(){
        Boolean validId = true;
        String errMessage = '';
        
        if (vehicleVeriAcctId == '') vehicleVeriAcctId = null;
        if (vehicleVeriVOId == '') vehicleVeriVOId = null;
        String idRegex = '[a-zA-Z0-9]{18}|[a-zA-Z0-9]{15}';
        if (vehicleVeriAcctId != null){
            validId = Pattern.matches(idRegex, vehicleVeriAcctId);
            errMessage = 'Invalid Guest Account ID format';
            
        }
        if (vehicleVeriVOId != null){
            validId = Pattern.matches(idRegex, vehicleVeriVOId);
            errMessage += '; Invalid VO ID format';
            
        }
        if (!validId){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, errMessage));
        }
        if (validId) vehicleVeriResponse = MyLexusWebService.vehicleVerification(vehicleVeriAcctId, vinVehicleVerif, batchNumber, vehicleVeriVOId);
        return null;
    }
    
    public PageReference createUpdateCase(){
        Boolean validId = true;
        String errMessage = '';
        
        if (caseSfAcctId == '') caseSfAcctId = null;
        String idRegex = '[a-zA-Z0-9]{18}|[a-zA-Z0-9]{15}';
        if (caseSfAcctId != null){
            validId = Pattern.matches(idRegex, caseSfAcctId);
            errMessage = 'Invalid Guest Account ID format';
            
        }
        if (!validId){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, errMessage));
        }
        if (validId) caseUpdateCreateResponse = MyLexusWebService.createUpdateCase(caseNumber, callBackPhone, timeSlot, caseSfAcctId);
        return null;
    }
    
    public PageReference deleteRegistration(){
        delRegistrationResponse = MyLexusWebService.deleteRegistration(deleteGigyaId, deleteEmail);
        return null;
    }
    
    public PageReference getCustomerDetails(){
        Boolean validId = true;
        String errMessage = '';
        
        if (customerAcctId == '') customerAcctId = null;
        if (customerVoId == '') customerVoId = null;
        String idRegex = '[a-zA-Z0-9]{18}|[a-zA-Z0-9]{15}';
        if (customerAcctId != null){
            validId = Pattern.matches(idRegex, customerAcctId);
            errMessage = 'Invalid Guest Account ID format';
            
        }
        if (customerVoId != null){
            validId = Pattern.matches(idRegex, customerVoId);
            errMessage += '; Invalid VO ID format';
            
        }
        if (!validId){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, errMessage));
        }
        if (validId) guestProfileResponse = MyLexusWebService.getCustomerDetails(customerDetailGigyaId, customerAcctId, customerVoId);
        return null;
    }
    
    public PageReference writeCustomerDetails() {
        writeCustDetailsResponse = MyLexusWebService.writeCustomerDetails(inputAcct);
        return null;
    }
    
    public PageReference writeCommunicationPreferences() {
        writeCommPrefResponse = MyLexusWebService.writeCommunicationPreferences(inputAcctCommPref);
        return null;
    }
    
    public PageReference updateTermsPolicyVersion() {
        Boolean validId = true;
        String errMessage = '';
        
        if (versionAcctId == '') versionAcctId = null;
        String idRegex = '[a-zA-Z0-9]{18}|[a-zA-Z0-9]{15}';
        if (versionAcctId != null){
            validId = Pattern.matches(idRegex, versionAcctId);
            errMessage = 'Invalid Guest Account ID format';
            
        }
        if (!validId){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, errMessage));
        }
        if (validId) updateVersionResponse = MyLexusWebService.updateTermsPolicyVersion(versionAcctId, termsAndConditionsVersion, privacyPolicyVersion);
        return null;
    }
    
    public PageReference getGuestVehicles(){
        guestVehicleResponse = MyLexusWebService.getGuestVehicles(guestVehicleGigyaId);
        return null;
    }
    
    public PageReference updateStripeCustomerId(){
        Boolean validId = true;
        String errMessage = '';
        
        if (stripeAcctId == '') stripeAcctId = null;
        String idRegex = '[a-zA-Z0-9]{18}|[a-zA-Z0-9]{15}';
        if (stripeAcctId != null){
            validId = Pattern.matches(idRegex, stripeAcctId);
            errMessage = 'Invalid Guest Account ID format';
            
        }
        if (!validId){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, errMessage));
        }
        if (validId) stripeCustomerIdResponse = MyLexusWebService.updateStripeCustomerId(stripeAcctId, stripeCustomerId);
        return null;
    }
}