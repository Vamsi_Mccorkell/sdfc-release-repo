/*******************************************************************************
@author:        Donnie Banez
@date:          February 2019
@description:   Trigger Handler for Case Trigger.
@Revision(s):    
@Test Methods:  CaseTriggerHandlerTest
********************************************************************************/
public with sharing class CaseTriggerHandler {
    public static TriggerAutomations__c automationEnabled  = TriggerAutomations__c.getInstance(); 
    public static Boolean caseTriggerEnabled = automationEnabled.Case__c; 
    public static Boolean sendingFBEmailEnabled = automationEnabled.EnableEmailSendingonFBLeadCase__c ;
    //public static Id PROSPECTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
    
    public static void mainEntry(Boolean isBefore, Boolean isAfter, Boolean isInsert, Boolean isUpdate,Boolean isDelete, Boolean isUnDelete, 
                    List<SObject> newList, List<SObject> oldList, Map<ID, SObject> newmap, Map<ID, SObject> oldmap)
    {
        // In a test context always set this to true        
        if (Test.isrunningTest())  {
            caseTriggerEnabled = true;
            sendingFBEmailEnabled = true;
        }
        // Perform business logic
        if(caseTriggerEnabled){
            try{
                // INSERT TRIGGER
                if(isInsert){
                    // Before
                    if(isBefore){
                       populateMassEmailFields(newList);
                    }            
                    // After
                    if(isAfter){
                       if (sendingFBEmailEnabled) sendEmailsToFacebookLeadCases(newList);
                    }
                }
                // Update TRIGGER
                if(isUpdate){
                    // Before
                    if(isBefore){
                        
                    }            
                    // After
                    if(isAfter){
                        
                    }
                }
                // Delete TRIGGER
                if(isDelete){
                    // Before
                    if(isBefore){
                        
                    }            
                    // After
                    if(isAfter){
                                            
                    }
                }
                // UNDELETE Trigger
                if(isUnDelete){}  
            } catch (DmlException ex){
                /*if (isAfter){ // if After trigger, create the error log after saving
                    List<Task> errorTaskList = new List<Task>();
                    for (Integer i = 0; i < ex.getNumDml(); i++) {
                        Task errorTask = new Task();
                        errorTask.WhatId = ex.getDmlId(i);
                        errorTask.Status = 'Completed';
                        errorTask.ActivityDate = system.today();
                        errorTask.Subject = 'Error encountered';
                        errorTask.Type = 'Error in After Trigger';
                        String errorString = ex.getStackTraceString()+'--'+ ex.getTypeName()+'--'+ ex.getMessage();
                        errorTask.Description = ' After trigger error: ' + errorString;
                        errorTaskList.add(errorTask);
                    }
                    Database.insert(errorTaskList, false);
                }
                else { // if before, show the error 
                    String errorString = ex.getStackTraceString()+'--'+ ex.getTypeName()+'--'+ ex.getMessage();
                	system.debug('### Case Trigger Handler Exception Thrown = ' + errorString);
                	newList[0].addError(errorString);
                	
                }*/
            }
            catch (Exception ex){
                system.debug(LoggingLevel.ERROR, 'EXCEPTION SEND EMAIL');
                Task errorTask = new Task();
                errorTask.WhatId = newList[0].Id;
                Case c = [select Id, ContactId from Case where Id = :newList[0].Id limit 1];
                errorTask.WhoId = c.ContactId;
                errorTask.Status = 'Completed';
                errorTask.ActivityDate = system.today();
                errorTask.Subject = 'Error encountered';
                errorTask.Type = 'Error in After Trigger';
                String errorString = ex.getStackTraceString()+'--'+ ex.getTypeName()+'--'+ ex.getMessage();
                errorTask.Description = ' After trigger error: ' + errorString;
                system.debug(LoggingLevel.ERROR, 'ERROR TASK: ' + errorTask);
                insert errorTask;
                system.debug(LoggingLevel.ERROR, 'ERROR TASK ID: ' + errorTask.Id);
            }
        }
    }
    
    public static void populateMassEmailFields(List<Case> newList){
    	Set<Id>	set_ContactIds  = new Set<Id>();
        Map<Id, Contact> map_Contacts = new Map<Id, Contact>();
    
        // Set of Accounts (Selected Dealers) referenced by these new Cases
        Set<Id> set_AccountIds = new Set<Id>();
    
        // Map of Dealers and their Contact email addresses
        Map<Id, String> map_AccounttoEmailStrings = new Map<Id, String>();
    
        // Build a map of Dealer Accounts and their database IDs
        Map<String, Id> map_DealerAccountNametoId = new map<String, Id>();
    
        List<Account> arrDealerAccount = [
                                            select  Id,
                                                    Name
                                            from    Account
                                            where   RecordTypeId = :SPCacheRecordTypeMetadata.getAccount_Dealer()
                                            and     Status__c = 'Active'
                                        ];
    
        for (Account sAccount : arrDealerAccount){
            map_DealerAccountNametoId.put(sAccount.Name, sAccount.Id);
        }
    
        for (Case sCase : newList) {
            if  (sCase.Type == 'Facebook Lead') {
                if (sCase.ContactId != null){
                    set_ContactIds.add(sCase.ContactId);
                    if (map_DealerAccountNametoId.containsKey(sCase.Selected_Dealer_Name__c)){
                        sCase.Selected_Dealer__c = map_DealerAccountNametoId.get(sCase.Selected_Dealer_Name__c);
                        set_AccountIds.add(map_DealerAccountNametoId.get(sCase.Selected_Dealer_Name__c));
                    }
                }
            }
        }
    
        // Get Dealer contact information
        for (List<Contact> arrContact :    [   select  Id,
                                                    AccountId,
                                                    Name,
                                                    Email
                                            from    Contact
                                            where   AccountId in :set_AccountIds
                                            and     (   To_be_Sent_on_Email_Distribution_List__c = true
                                                    or  CCed_on_Distribution_List__c = true
                                                    or Dom_Perignon_Offer_Distribution_Group__c = true)
                                        ]) {
            for (Contact sContact : arrContact)
            {
                // Contact email addresses - to be saved to a field on the Case so we can display it in the email template
                if (!map_AccounttoEmailStrings.containsKey(sContact.AccountId)){
                    string strEmailString = 'This email has been sent to: ' + sContact.Email;
                    map_AccounttoEmailStrings.put(sContact.AccountId, strEmailString);
                }
                else{
                    string strEmailString = map_AccounttoEmailStrings.remove(sContact.AccountId);
                    strEmailString += ', ' + sContact.Email;
                    map_AccounttoEmailStrings.put(sContact.AccountId, strEmailString);
                }
            }
        }
    
        // Get information about the Prospect
        for (List<Contact> arrContact :    [   select  Id,
                                                    FirstName,
                                                    LastName,
                                                    Email,
                                                    HomePhone,
                                                    MobilePhone,
                                                    Account.Work_Phone__c,
                                                    Account.Product_Information_Opt_Out__c
                                            from    Contact
                                            where   Id in :set_ContactIds
                                        ]) {
            for (Contact sContact : arrContact) {
                map_Contacts.put(sContact.Id, sContact);
            }
        }
    
        for (Case sCase : newList)
        {
            if  (sCase.Type == 'Facebook Lead') {
                if (sCase.ContactId != null) {
                    Contact sContact = map_Contacts.get(sCase.ContactId);
                    sCase.TempFirstName__c = sContact.FirstName;
                    sCase.TempLastName__c = sContact.LastName;
                    sCase.TempEmail__c = sContact.Email;
                    sCase.TempHomePhone__c = sContact.HomePhone;
                    sCase.TempMobile__c = sContact.MobilePhone;
                    sCase.TempWorkPhone__c = sContact.Account.Work_Phone__c;
            
                    if (sContact.Account.Product_Information_Opt_Out__c){
                        sCase.TempProductInformationOptOutYN__c = 'Yes';
                    }
                    else{
                        sCase.TempProductInformationOptOutYN__c = 'No';
                    }
    
                    if (map_AccounttoEmailStrings.containsKey(sCase.Selected_Dealer__c) && sendingFBEmailEnabled){
                        sCase.TempEmailStrings__c = map_AccounttoEmailStrings.get(sCase.Selected_Dealer__c);
                    }
                }
            }
        }    
    }
    
    public static void sendEmailsToFacebookLeadCases(List<Case> newList){
        system.debug(LoggingLevel.ERROR, '## IN FB send email');
        Set<Id> accountIdSet = new Set<Id>();
        Map<String, List<Case>> caseTypeCaseListMap = new Map<String, List<Case>>();
        for (Case c : newList){
            if (c.Type == 'Facebook Lead'){
                List<Case> listCases = caseTypeCaseListMap.get(c.Type);
            	if (listCases == null)	listCases = new List<Case>();        	
                listCases.add(c);
                caseTypeCaseListMap.put(c.Type, listCases);
                if (c.Selected_Dealer__c != null) accountIdSet.add(c.Selected_Dealer__c);
            }
        }
        
        // assemble the recipients
        Map<Id, Set<Id>> mapAccounttoToList     = new Map<Id, Set<Id>>();
        List<Contact> contactDistList = [select  Id, Dom_Perignon_Offer_Distribution_Group__c,
                                                AccountId,
                                                Name
                                        from    Contact
                                        where   AccountId in :accountIdSet
                                        and     (To_be_Sent_on_Email_Distribution_List__c = true
                                                or  CCed_on_Distribution_List__c = true
                                                or Dom_Perignon_Offer_Distribution_Group__c = true)];
        
        for (Contact c : contactDistList){
            if (!mapAccounttoToList.containsKey(c.AccountId)){
                Set<Id> set_ToList = new Set<Id>();
                set_ToList.add(c.Id);
                mapAccounttoToList.put(c.AccountId, set_ToList);
            }
            else {
                Set<Id> set_ToList = mapAccounttoToList.remove(c.AccountId);
                set_ToList.add(c.Id);
                mapAccounttoToList.put(c.AccountId, set_ToList);
            }
        }
        List<Id> recipientIdList = new List<Id>();
        List<Id> caseIdList = new List<Id>();
        for (String caseType : caseTypeCaseListMap.keySet()){
            for (Case c : caseTypeCaseListMap.get(caseType)) {
                if (c.Type == 'Facebook Lead' && !mapAccounttoToList.isEmpty()){
                    Set<Id> setToList = mapAccounttoToList.get(c.Selected_Dealer__c);
	                for (Id conId : setToList){
                        recipientIdList.add(conId);
                        caseIdList.add(c.Id);
                    }
                }
            }
        }
        
        if (!recipientIdList.isEmpty()) {
            // set the mass email headers
            Messaging.MassEmailMessage fbMassEmail = new Messaging.MassEmailMessage();
            fbMassEmail.setTemplateId(SPCacheEmailTemplateMetadata.getTemplateIdPersonalEnquiry());
            fbMassEmail.setSenderDisplayName('Lexus Australia');
            fbMassEmail.setReplyTo('noreply@lexus.com.au');
            // Target object Ids for merge fields
            fbMassEmail.setTargetObjectIds(recipientIdList);
            fbMassEmail.setWhatIds(caseIdList);
            // Send the email
            system.debug(LoggingLevel.ERROR, '## fb mass email: ' + fbMassEmail);
            Messaging.sendEmail(new Messaging.MassEmailMessage[] { fbMassEmail });
        }
    }
}