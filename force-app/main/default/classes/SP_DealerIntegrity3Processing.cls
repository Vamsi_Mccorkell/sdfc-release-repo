public with sharing class SP_DealerIntegrity3Processing
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Exception Classes & Enums
	public class SP_Exception extends Exception{}

	public enum BatchParam {ALL, ALL_ASYNC}

	private static integer iQueryLimit = 200;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class used to pass arguments into the Batch
	public class SP_Args
	{
		public BatchParam 	BatchParam 		{get; set;}
		public List<id>		ProcessList		{get; set;}
		public boolean 		UseSavePoint	{get; set;}
		public boolean 		AllOrNone		{get; set;}

		public SP_Args(list<id> theProcessList)
		{
			this.ProcessList 	= theProcessList;
			this.UseSavePoint 	= true;
			this.AllOrNone 		= false;
		}

		public SP_Args(list<id> theProcessList, BatchParam theBatchParam)
		{
			this.ProcessList 	= theProcessList;
			this.BatchParam		= theBatchParam;
			this.UseSavePoint 	= true;
			this.AllOrNone 		= false;
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class used to pass information back to what called the batch
	public class SP_Ret
	{
		public boolean  	InError							{get; set;}
		public List<string> ErrorList						{get; set;}
				
		public SP_Ret()
		{
			this.InError = false;
			this.ErrorList = new List<string>();
		}
	}

	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Singular
	SP_Args 	mArgs;
	SP_Ret		mRet;
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Collections

	// List of exception records to be written
	list<Account>			li_AccountsToUpdate				= new list<Account>();

		
	/***********************************************************************************************************
		Constructor
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Our Constructor
	public SP_DealerIntegrity3Processing(SP_Args oArgs)
	{
		mRet 	= new SP_Ret();
		mArgs 	= oArgs;
	}
	
	/***********************************************************************************************************
		Access Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// The one and only entry point for the processor
	public SP_Ret ProcMain()
	{
		Savepoint savePoint;
		
		if(mArgs.UseSavePoint)
			savePoint = Database.setSavepoint();
		
		try
		{
			ProcessAccountObject(mArgs.ProcessList);
		}
		catch(Exception e)
		{
			mRet.InError = true;
			mRet.ErrorList.add(e.getMessage());
						
			if(savePoint != null)
				Database.rollback(savePoint);
			
			throw new SP_Exception('Fatal Exception - All changes have been rolled back - ' + e.getMessage());
		}
		
		return mRet;
	}
	
	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Process Account Objects
	private void ProcessAccountObject(list<id> li_AccountIdsIn)
	{
		// This process identifies Owner Accounts whose Selected Dealer allocation is incorrect	

		// Build a list of all Account records to be processed
		Account [] arrAccount =	[
										select	Id,
												Servicing_Dealer__c, Servicing_Dealer__r.Status__c,
												Original_Selling_Dealer__c, Original_Selling_Dealer__r.Status__c,
												Last_Selling_Dealer__c, Last_Selling_Dealer__r.Status__c,
												Preferred_Dealer__c, Preferred_Dealer__r.Status__c,
												(select Id, Vehicle_Type__c from Vehicle_Ownerships__r 
												order by Start_Date__c asc limit 1)
										from	Account
										where	Id in :li_AccountIdsIn];
										
		/* 	Logic
		
			Case 1: Both Selected dealer and Servicing Dealer are same (not null)
			
			In this case, we are not sure if this happened due to the error in trigger. So we have to take this record
			for futher checking
			
			In this case, correct selling dealer should be either last selling dealer or original selling dealer.
			If there is no last selling dealer or original selling dealer, we do not need to change the existing selected dealer.
			Also, even if there is a selling dealer (original or last selling) and it is Lexus Australia and there is already an active selected dealer,
			we do not need to change that selected dealer.
			
			In all other cases if the selling dealer (original or last selling) is different from the selected dealer,
			we should set selected dealer with that selling dealer.
			
			Case 2: Selected Dealer (should be active) and Servicing Dealer  are different (not null )
			
			We do not need to consider this record.
			
			Case 3: There is no Selected Dealer. But there is a Servicing Dealer
			
			In this case, correct selling dealer should be either of last selling dealer, original selling dealer and servicing dealer. 
			We should set selected dealer with that correct selling dealer.
			
			Case 4: There is an active Selected Dealer. But there is no Servicing Dealer
			
			Who cares? So we do not need to consider this record.
			
			Case 5: Both Selected Dealer (or inactive) and Servicing Dealer have no values 

			In this case, correct selling dealer should be either last selling dealer or original selling dealer.
			If there is no last selling dealer or original selling dealer, we can not set selected dealer.
			
			But if there is a selling dealer ((original or last selling), we should set selected dealer with that selling dealer.
		*/

		string strLexusAustralia = SPCacheRecordTypeMetadata.getLexusAustraliaRecordID().substring(0, 15);
		
		for (Account sAccount : arrAccount)
		{
			// Only Non-Private Sale
			//if (sAccount.Vehicle_Ownerships__r.size() == 0 || sAccount.Vehicle_Ownerships__r[0].Vehicle_Type__c == 'Private Sale')
			//	continue;
				
			string strSelectedDealer = sAccount.Preferred_Dealer__c != null ? String.valueOf(sAccount.Preferred_Dealer__c).substring(0, 15) : null;
			string strLastServicingDealer = sAccount.Servicing_Dealer__c != null ? String.valueOf(sAccount.Servicing_Dealer__c).substring(0, 15) : null;
			string strOriginalSellingDealer = sAccount.Original_Selling_Dealer__c != null ? String.valueOf(sAccount.Original_Selling_Dealer__c).substring(0, 15) : null;
			string strLastSellingDealer = sAccount.Last_Selling_Dealer__c != null ? String.valueOf(sAccount.Last_Selling_Dealer__c).substring(0, 15) : null;
				
			// If the owner has already an active, non-luxus australia selected dealer and either there is no service dealer yet or
			// the service dealer and selected dealer are different, there is no problem with this account and we can skip it.
			if ((strSelectedDealer != null && sAccount.Preferred_Dealer__r.Status__c == 'Active' && strSelectedDealer != strLexusAustralia)  && 
				(strLastServicingDealer == null || strLastServicingDealer != strSelectedDealer))
				continue;
				
			string correctSellingDealer = null;
			boolean isLexusAustralia = false;
			
			// If there is no selected dealer or if the selected dealer is inactive or the selected dealer is luxus australia and there is an
			// active last servicing dealer, the last servicing dealer can be the selected dealer (depending on original and last selling dealer)
			if ((strSelectedDealer == null || sAccount.Preferred_Dealer__r.Status__c != 'Active' || strSelectedDealer == strLexusAustralia) && 
				strLastServicingDealer != null && sAccount.Servicing_Dealer__r.Status__c == 'Active')
				correctSellingDealer = strLastServicingDealer;
			
			// If there is an active non-luxus australia original selling dealer, this original selling dealer can be the selected dealer
			// (depending on last selling dealer). 
			if (strOriginalSellingDealer != null && sAccount.Original_Selling_Dealer__r.Status__c == 'Active' &&
				strOriginalSellingDealer != strLexusAustralia)
				correctSellingDealer = strOriginalSellingDealer;
			else if (strOriginalSellingDealer == strLexusAustralia)
				isLexusAustralia = true;
			
			// If there is an active non-luxus australia last selling dealer, this last selling dealer will be the final selected dealer	
			if (strLastSellingDealer != null && sAccount.Last_Selling_Dealer__r.Status__c == 'Active' &&
				strLastSellingDealer != strLexusAustralia)
				correctSellingDealer = strLastSellingDealer;
			else if (strLastSellingDealer == strLexusAustralia)
				isLexusAustralia = true;
			
			// Even if we have searched among last selling dealer, original selling dealer and servicing dealer and we haven't got any selected
			// dealer, check if original selling dealer or last selling dealer is luxus australia. if so, selected dealer will be luxus australia	
			if (correctSellingDealer == null && isLexusAustralia)
				correctSellingDealer = SPCacheRecordTypeMetadata.getLexusAustraliaRecordID().substring(0, 15);
				
			if (strSelectedDealer != correctSellingDealer)
			{
				sAccount.Preferred_Dealer__c = correctSellingDealer;
				li_AccountsToUpdate.add(sAccount);			
			}	
			
			/*// If the correct selling dealer is Lexus Australia or correct selling dealer is null, 
			//skip that record as it an exception record	
			if (correctSellingDealer == null ||
				(sAccount.Preferred_Dealer__c != null && sAccount.Preferred_Dealer__r.Status__c == 'Active' &&
				 correctSellingDealer.substring(0, 15) == SPCacheRecordTypeMetadata.getLexusAustraliaRecordID().substring(0, 15)))
				continue;
				
			if (sAccount.Preferred_Dealer__c == null || sAccount.Preferred_Dealer__r.Status__c != 'Active' ||
				correctSellingDealer.substring(0, 15) != String.valueOf(sAccount.Preferred_Dealer__c).substring(0, 15))
			{
				sAccount.Preferred_Dealer__c = correctSellingDealer;
				li_AccountsToUpdate.add(sAccount);
			}*/
		}
		
		if (!li_AccountsToUpdate.isEmpty())
		{
			update li_AccountsToUpdate;
			li_AccountsToUpdate.clear();
			
			system.debug('########## Selected Dealers of Accounts (' + li_AccountIdsIn + ') have been corrected successfully.');
		}
	}

	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 
	public static testMethod void testSP_DealerIntegrity2Processing()
	{

	}
}