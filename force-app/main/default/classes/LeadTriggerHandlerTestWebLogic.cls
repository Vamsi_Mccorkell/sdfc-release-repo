@IsTest (SeeAllData=true)
public class LeadTriggerHandlerTestWebLogic {
    static testmethod void testLeadTransform(){
        Account person = new Account(FirstName = 'Test', LastName = 'Test', PersonEmail = 'test@noreply.com',
                                    BillingPostalCode = 'XXXX', PersonMobilePhone = '02111111111');
        insert person;
        Account dealer = new Account(Name = 'Test Dealer', RDR_Dealer_Code__c = '44444',
                                    RecordTypeId = SPCacheRecordTypeMetadata.getAccount_Dealer(),
                                    Status__c = 'Active', Type = 'Dealer');
        insert dealer;
        PMA__c pma = new PMA__c(Dealer_Name__c  = dealer.Id,
                                             Location__c = 'Location', Postcode__c = 'XXXX',
                                             State_Province__c = 'State');
        insert pma;
        Lead l = new Lead(FirstName = 'Test', LastName = 'Test', Email = 'test@noreply.com',
                          			PostalCode = 'XXXX', MobilePhone = '02111111111', Status = 'Open',
                         			LeadSource = 'Facebook');
        insert l;
        
        Lead l2 = new Lead(FirstName = 'Test2', LastName = 'Test222', Email = 'test2@noreply.com',
                          			PostalCode = 'XXXX', MobilePhone = '02111111111', Status = 'Open',
                         			LeadSource = 'Facebook');
        insert l2;
    }
}