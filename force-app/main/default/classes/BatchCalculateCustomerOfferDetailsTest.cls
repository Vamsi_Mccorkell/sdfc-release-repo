// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for creating accounts need to view the custom settings data;
// Recommend changing the other old Account triggers to not make it dependent on viewing all data
@IsTest (SeeAllData=true)
public class BatchCalculateCustomerOfferDetailsTest {
	static Offer__c newOffer;
    static Account customer;
    static Account partnerAcct;
    
    static testmethod void testBatchRunCalculateCOD(){
        dataSetup();
        Customer_Offer_Detail__c cod = new Customer_Offer_Detail__c();
        cod.Customer__c = customer.Id;
        cod.Offer__c = newOffer.Id;
        cod.First_Credit_Expiration__c = system.today()+5;
        cod.Total_Credits_Allowed__c = 10;
        cod.Total_Customer_Credits_Consumed__c = 0;
        insert cod;
        
        BatchCalculateCustomerOfferDetails br = new BatchCalculateCustomerOfferDetails(cod.Id);
        Database.executeBatch(br);
        
        // DEV NOTE: This just tests the batch class and just aims for coverage; It calls the GuestVoucherServicesUtilities.recalculateCODCounts(codIdSet) method
        // which is tested with assertions in the GuestVoucherServicesUtilitiesTest class
        
    }
    
    static void dataSetup(){
        List<Account> newAcctList = new List<Account>();
        List<Vehicle_Ownership__c> voList = new List<Vehicle_Ownership__c>();
        List<Asset__c> assetList = new List<Asset__c>();
        
        Id PARTNERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner').getRecordTypeId();
        Id CUSTOMERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        partnerAcct = new Account(Name = 'Partner Account', RecordTypeId = PARTNERACCTRECORDTYPEID, Partner_Class__c = 'Customer');
        newAcctList.add(partnerAcct);
        
        customer = new Account(FirstName = 'Test', LastName = 'Customer', RecordTypeId = CUSTOMERACCTRECORDTYPEID);
        newAcctList.add(customer);
        
        insert newAcctList;
        
        newOffer = new Offer__c(Partner_Account__c = partnerAcct.Id, Status__c = 'Inactive', Offer_Class__c = 'Customer', Customer_Platinum_Limit__c = 10,
                               Offer_Type__c = 'Car Hire');
        insert newOffer;
        newOffer.Status__c = 'Active';
        update newOffer;
    }
}