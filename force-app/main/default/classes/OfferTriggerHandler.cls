/*******************************************************************************
@author:        Donnie Banez
@date:          March 2019
@description:   Trigger Handler for Offer Trigger
@Revision(s):    
@Test Methods:  OfferTriggerHandlerTest
//VD - REmoved Offer Tirgger as functaionlty is movedto a Batch Process.
********************************************************************************/
public with sharing class OfferTriggerHandler {
    public static TriggerAutomations__c automationEnabled  = TriggerAutomations__c.getInstance(); 
    public static Boolean triggerEnabled = automationEnabled.Offer__c; 
    public static void mainEntry(Boolean isBefore, Boolean isAfter, Boolean isInsert, Boolean isUpdate,Boolean isDelete, Boolean isUnDelete, 
                    List<SObject> newList, List<SObject> oldList, Map<ID, SObject> newmap, Map<ID, SObject> oldmap)
    {
        if (Test.isrunningTest())  {
            triggerEnabled = true;
        }
        
        if (triggerEnabled){
            try{
                // INSERT TRIGGER
                if(isInsert){
                    // Before
                    if(isBefore){
                        
                    }            
                    // After
                    if(isAfter){
  						//createCODVINODandVOODRecords(null, newList); 11 OCT: Not on create
                    }
                }
                // Update TRIGGER
                if(isUpdate){
                    // Before
                /*    if(isBefore){
                        
                    }            
                    // After
                    if(isAfter){
  					//	createCODVINODandVOODRecords((Map<Id, Offer__c>) oldMap, newList);
                    }*/
                }
                // Delete TRIGGER
                if(isDelete){
                /*    // Before
                    if(isBefore){
                        //preventOfferDeletion(oldList);
                    }            
                    // After
                    if(isAfter){
   
                    }*/
                }
                // UNDELETE Trigger
                if(isUnDelete){
               /*     if (isBefore){
                        
                    }
                    if (isAfter){
  
                    }*/
                }  
            } catch (Exception ex){
                String errorString = ex.getStackTraceString()+'--'+ ex.getTypeName()+'--'+ ex.getMessage();
                system.debug('### Offer Trigger Handler Exception Thrown = ' + errorString);
                newList[0].addError(errorString);
            }
        }
        
    }
    
    //Commented as functionality is moved to Batch
    /*public static void createCODVINODandVOODRecords(Map<Id, Offer__c> oldMap, List<Offer__c> newList){
        Set<Id> offerIdSet = new Set<Id>();
        for (Offer__c o : newList){
            Offer__c oldOffer = null;
            if (oldMap != null){
            	oldOffer = oldMap.get(o.Id);    
            }
           /* if (/*oldOffer == null ||  NOT INCLUDING CREATE */ /*oldOffer.Status__c != o.Status__c || oldOffer.Offer_Class__c != o.Offer_Class__c){
            	offerIdSet.add(o.Id);
            }
        }
        
        if (offerIdSet.size() > 0){
            GuestVoucherServicesUtilities.createCODVINODVOODRecords(offerIdSet, null, null); // offerIdSet , vinIdSet = null, voIdSet = null  
        }
        
    }*/
    /* MAY NO LONGER BE NEEDED
    public static void preventOfferDeletion(List<Offer__c> oldList){
        for (Offer__c o : oldList){
            if (!o.Delete_Record__c){
                o.addError(Label.OfferDeletionError);
            }
        }
    }*/

}