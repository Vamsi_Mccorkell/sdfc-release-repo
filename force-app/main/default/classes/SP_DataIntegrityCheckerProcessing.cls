public with sharing class SP_DataIntegrityCheckerProcessing
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Exception Classes & Enums
	public class SP_Exception extends Exception{}

	public enum BatchParam {ALL, ALL_ASYNC}

	private static integer iQueryLimit = 10;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class used to pass arguments into the Batch
	public class SP_Args
	{
		public BatchParam 	BatchParam 		{get; set;}
		public List<id>		ProcessList		{get; set;}
		public boolean 		UseSavePoint	{get; set;}
		public boolean 		AllOrNone		{get; set;}

		public SP_Args(list<id> theProcessList)
		{
			this.ProcessList 	= theProcessList;
			this.UseSavePoint 	= true;
			this.AllOrNone 		= false;
		}

		public SP_Args(list<id> theProcessList, BatchParam theBatchParam)
		{
			this.ProcessList 	= theProcessList;
			this.BatchParam		= theBatchParam;
			this.UseSavePoint 	= true;
			this.AllOrNone 		= false;
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class used to pass information back to what called the batch
	public class SP_Ret
	{
		public boolean  	InError							{get; set;}
		public List<string> ErrorList						{get; set;}
				
		public SP_Ret()
		{
			this.InError = false;
			this.ErrorList = new List<string>();
		}
	}

	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Singular
	SP_Args 	mArgs;
	SP_Ret		mRet;
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Collections
	list<CSV_Data__c>	m_liCSVData					= new list<CSV_Data__c>();

	map<string, id>		m_mapVINtoServiceAccountId	= new map<string, id>();
	map<string, id>		m_mapVINtoServiceId			= new map<string, id>();
	map<string, id>		m_mapVINtoAccountId			= new map<string, id>();
	set<string>			m_setVINIds					= new set<string>();

//	map<string, Service__c>	m_mapVINtoLatestService		= new map<string, Service__c>();

	map<string, map<date, Service__c>>	m_mapVINtoAllServices = new map<string, map<date, Service__c>>();

	list<CSV_Data__c>	m_liCSVDataUpdate			= new list<CSV_Data__c>();

		
	/***********************************************************************************************************
		Constructor
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Our Constructor
	public SP_DataIntegrityCheckerProcessing(SP_Args oArgs)
	{
		mRet 	= new SP_Ret();
		mArgs 	= oArgs;
	}
	
	/***********************************************************************************************************
		Access Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// The one and only entry point for the processor
	public SP_Ret ProcMain()
	{
		Savepoint savePoint;
		
		if(mArgs.UseSavePoint)
			savePoint = Database.setSavepoint();
		
		try
		{
			ProcessCSVDataObject(mArgs.ProcessList);
		}
		catch(Exception e)
		{
			mRet.InError = true;
			mRet.ErrorList.add(e.getMessage());
						
			if(savePoint != null)
				Database.rollback(savePoint);
			
			throw new SP_Exception('Fatal Exception - All changes have been rolled back - ' + e.getMessage());
		}
		
		return mRet;
	}
	
	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Process CSV Data Objects
	private void ProcessCSVDataObject(list<id> li_CSVDataIdsIn)
	{
		// Build a list of all CSV Data records to be processed
		CSV_Data__c [] arrCSVData =	[
										select	Id,
												CSV_Header__c,
												Customer_Name__c,
												Customer_Address__c,
												Customer_Suburb__c,
												Customer_Postcode__c,
												Customer_Phone__c,
												VIN__c,
												Integrity_Check__c,
												Notes__c,
												ServiceId__c,
												Service_Date__c
										from	CSV_Data__c
										where	Id in :li_CSVDataIdsIn
										limit	:iQueryLimit
								];

		for (CSV_Data__c sCSVData : arrCSVData)
		{
			if (sCSVData.VIN__c != null)
			{
				m_setVINIds.add(sCSVData.VIN__c);
			}
		}

		// Get the CSV Header record
		CSV_Header__c sCSVHeader = [
										select	Id,
												End_Date__c,
												Processed__c
										from	CSV_Header__c
										where	Id = :arrCSVData[0].CSV_Header__c
									];

		// Get Service records for the specified time period
		Service__c [] arrService =	[
										select	Id,
												Asset__c,
												Asset__r.Name,
												Customer_Account__c,
												Service_Date__c
										from	Service__c
										where	Asset__r.Name in :m_setVINIds
									];

		for (Service__c sService : arrService)
		{
			// All Services by date for each VIN
			if (!m_mapVINtoAllServices.containsKey(sService.Asset__r.Name))
			{
				map<date, Service__c> map_DatetoService = new map<date, Service__c>();
				map_DatetoService.put(sService.Service_Date__c, sService);
				m_mapVINtoAllServices.put(sService.Asset__r.Name, map_DatetoService);
			}
			else
			{
				map<date, Service__c> map_DatetoService = m_mapVINtoAllServices.remove(sService.Asset__r.Name);

				map_DatetoService.put(sService.Service_Date__c, sService);
				m_mapVINtoAllServices.put(sService.Asset__r.Name, map_DatetoService);
			}
		}

		// Get Vehicle Ownerships for these assets
		Vehicle_Ownership__c [] arrVO =	[
											select	AssetID__c,
													AssetID__r.Name,
													Customer__c
											from	Vehicle_Ownership__c
											where	AssetID__r.Name in :m_setVINIds
											and		Status__c = 'Active'
										];

		for (Vehicle_Ownership__c sVO : arrVO)
		{
			m_mapVINtoAccountId.put(sVO.AssetID__r.Name, sVO.Customer__c);
		}

		// Now process the CSV data		
		for (CSV_Data__c sCSVData : arrCSVData)
		{			
			// Call the customer match rules
			//MatchingAccount ma = new MatchingAccount();
			string strMatchedAccountId;
			/*strMatchedAccountId = ma.customerMatching	(
															sCSVData.VIN__c,				// VIN
															'',								// First Name
															sCSVData.Customer_Name__c,		// Last name
															'',								// Email
															sCSVData.Customer_Phone__c,		// Phone
															sCSVData.Customer_Suburb__c,	// City
															sCSVData.Customer_Postcode__c,	// Postcode
															sCSVData.Customer_Address__c,	// Street
															false							// Don't create Cases
														);*/
														
			strMatchedAccountId = SimplifiedMatchingAccount.customerMatching(
															sCSVData.VIN__c,				// VIN
															'',								// First Name
															sCSVData.Customer_Name__c,		// Last name
															'',								// Email
															sCSVData.Customer_Phone__c,		// Phone
															sCSVData.Customer_Suburb__c,	// City
															sCSVData.Customer_Postcode__c,	// Postcode
															sCSVData.Customer_Address__c,	// Street
															false							// Don't create Cases
														);

			sCSVData.Notes__c = 'Customer matching rules returned ';

			if (strMatchedAccountId != null)
			{
				sCSVData.Notes__c += strMatchedAccountId;

				if (m_mapVINtoAllServices.containsKey(sCSVData.VIN__c))
				{
					map<date, Service__c> map_DatetoService = m_mapVINtoAllServices.get(sCSVData.VIN__c);
	
					list<string> li_Date = sCSVData.Service_Date__c.split('/');
					date dtServiceDate	= date.newInstance	(
																integer.valueOf('20' + li_Date.get(2)), 
																integer.valueOf(li_Date.get(1)), 
																integer.valueOf(li_Date.get(0))
															);
	
					if (map_DatetoService.containsKey(dtServiceDate))
					{
						Service__c sService = map_DatetoService.get(dtServiceDate);
	
						if (strMatchedAccountId == sService.Customer_Account__c)
						{
							sCSVData.Integrity_Check__c = 'Pass';
						}
						else
						{
							sCSVData.Integrity_Check__c = 'Fail';
	
							sCSVData.Notes__c += ' which does not match the Customer Id on the Service record ' +
													sService.Customer_Account__c;						
						}
					}
					else
					{
						sCSVData.Integrity_Check__c = 'Fail';
						sCSVData.Notes__c += ' No services found for this VIN';
					}
				}
				else
				{
					sCSVData.Integrity_Check__c = 'Fail';
					sCSVData.Notes__c += ' No services found for this VIN';
				}
			}
			else
			{
				sCSVData.Notes__c += 'null ';
				
				// See if we can match via the VO
				if (m_mapVINtoAccountId.containsKey(sCSVData.VIN__c))
				{
					sCSVData.Notes__c += 'Matched via the VO ';

					strMatchedAccountId = m_mapVINtoAccountId.get(sCSVData.VIN__c);

					if (m_mapVINtoAllServices.containsKey(sCSVData.VIN__c))
					{
						map<date, Service__c> map_DatetoService = m_mapVINtoAllServices.get(sCSVData.VIN__c);
		
						list<string> li_Date = sCSVData.Service_Date__c.split('/');
						date dtServiceDate	= date.newInstance	(
																	integer.valueOf('20' + li_Date.get(2)), 
																	integer.valueOf(li_Date.get(1)), 
																	integer.valueOf(li_Date.get(0))
																);
		
						if (map_DatetoService.containsKey(dtServiceDate))
						{
							Service__c sService = map_DatetoService.get(dtServiceDate);
		
							if (strMatchedAccountId == sService.Customer_Account__c)
							{
								sCSVData.Integrity_Check__c = 'Pass';
							}
							else
							{
								sCSVData.Integrity_Check__c = 'Fail';
		
								sCSVData.Notes__c += ' which does not match the Customer Id on the Service record ' +
														sService.Customer_Account__c;						
							}
						}
						else
						{
							sCSVData.Integrity_Check__c = 'Fail';
							sCSVData.Notes__c += ' No services found for this VIN';
						}
					}
					else
					{
						sCSVData.Integrity_Check__c = 'Fail';
						sCSVData.Notes__c += ' No services found for this VIN';
					}
				}
				else
				{
					sCSVData.Integrity_Check__c = 'Fail';
					sCSVData.Notes__c += ' VIN does not have an active VO';
				}
			}
	
			// Add the Service record Id
			sCSVData.ServiceId__c = m_mapVINtoServiceId.get(sCSVData.VIN__c);

			m_liCSVDataUpdate.add(sCSVData);

		}
	
		update m_liCSVDataUpdate;
	
	}

	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 
	public static testMethod void testSP_DataIntegrityCheckerProcessing()
	{
	// Create records to be used in this test method

		Product2 sProduct1 = new Product2();
		sProduct1.Name					= 'IS200';
		insert sProduct1;

		Product2 sProduct2 = new Product2();
		sProduct2.Name					= 'SC430';
		insert sProduct2;

		Asset__c sAsset1 = new Asset__c();
		sAsset1.Name					= 'ABC12345678';
		sAsset1.Vehicle_Model__c		= sProduct1.Id;
		insert sAsset1;

		Asset__c sAsset2 = new Asset__c();
		sAsset2.Name					= 'XYZ98765432';
		sAsset2.Vehicle_Model__c		= sProduct2.Id;
		insert sAsset2;

		Asset__c sAsset3 = new Asset__c();
		sAsset3.Name					= 'DDDD8765432';
		sAsset3.Vehicle_Model__c		= sProduct1.Id;
		insert sAsset3;

		Asset__c sAsset4 = new Asset__c();
		sAsset4.Name					= 'JJJJ8765432';
		sAsset4.Vehicle_Model__c		= sProduct2.Id;
		insert sAsset4;

		list<Account>	li_Account		= new list<Account>();

		Account sAccount1 = new Account();
		sAccount1.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccount1.LastName				= 'TestLast1';
		sAccount1.PersonEmail			= 'test1@email.com';
		li_Account.add(sAccount1);

		Account sAccount2 = new Account();
		sAccount2.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccount2.LastName				= 'TestLast2';
		sAccount2.PersonEmail			= 'test2@email.com';
		li_Account.add(sAccount2);

		Account sAccount3 = new Account();
		sAccount3.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccount3.LastName				= 'TestLast3';
		sAccount3.PersonEmail			= 'test3@email.com';
		li_Account.add(sAccount3);

		insert li_Account;

		// Now we need to hit the dataase to get the ids of the Accounts we just created
		Account[] arrAccount =	[
									select	Id
									from	Account
									where	LastName = :'TestLast1'
									or		LastName = :'TestLast2'
									or		LastName = :'TestLast3'
									limit	3
								];

		list<id>		li_AccountIds	= new list<id>();

		for (Account sAccount : arrAccount)
		{
			li_AccountIds.add(sAccount.Id);
		}

		// Create a number of Service records
		list<Service__c> liService = new list<Service__c>();

		// Two customers, each with one vehicle, all services to be counted
		for (integer i=1; i<4; i++)
		{
			Service__c sService1			= new Service__c(); 
			sService1.Customer_Account__c	= sAccount1.Id;
			sService1.Asset__c				= sAsset1.Id;
			sService1.Vehicle_Mileage__c	= 7000 * i;
			sService1.Service_Date__c		= system.today().addYears(-(4-i));

			liService.add(sService1);

			Service__c sService2			= new Service__c(); 
			sService2.Customer_Account__c	= sAccount2.Id;
			sService2.Asset__c				= sAsset2.Id;
			sService2.Vehicle_Mileage__c	= 12345 * i;
			sService2.Service_Date__c		= system.today().addYears(-(4-i));

			liService.add(sService2);
			
		}

		// Now one customer with two vehicles, not all services to be counted
		// First service - counted
		Service__c sService3			= new Service__c(); 
		sService3.Customer_Account__c	= sAccount3.Id;
		sService3.Asset__c				= sAsset3.Id;
		sService3.Vehicle_Mileage__c	= 12345;
		sService3.Service_Date__c		= date.newInstance(2008, 1, 1);
		liService.add(sService3);

		// 4999 km past due - count as 30,000km service
		Service__c sService4			= new Service__c(); 
		sService4.Customer_Account__c	= sAccount3.Id;
		sService4.Asset__c				= sAsset3.Id;
		sService4.Vehicle_Mileage__c	= 34999;
		sService4.Service_Date__c		= date.newInstance(2008, 10, 2);
		liService.add(sService4);

		// 5000 km past due - count as 40,000 service
		Service__c sService5			= new Service__c(); 
		sService5.Customer_Account__c	= sAccount3.Id;
		sService5.Asset__c				= sAsset3.Id;
		sService5.Vehicle_Mileage__c	= 35000;
		sService5.Service_Date__c		= date.newInstance(2009, 8, 3);
		liService.add(sService5);

		// Outside mileage range - use average
		Service__c sService6			= new Service__c(); 
		sService6.Customer_Account__c	= sAccount3.Id;
		sService6.Asset__c				= sAsset3.Id;
		sService6.Vehicle_Mileage__c	= 300000;
		sService6.Service_Date__c		= date.newInstance(2010, 12, 31);
		liService.add(sService6);

		// Different vehicle - should count first service
		Service__c sService7			= new Service__c(); 
		sService7.Customer_Account__c	= sAccount3.Id;
		sService7.Asset__c				= sAsset4.Id;
		sService7.Vehicle_Mileage__c	= 121000;
		sService7.Service_Date__c		= date.newInstance(2007, 10, 1);
		liService.add(sService7);			

		// Mileage is zero - use average cost for model
		// Also, date is > 9 months since last service for this vehicle but < 9 months since last service for this customer
		Service__c sService8			= new Service__c(); 
		sService8.Customer_Account__c	= sAccount3.Id;
		sService8.Asset__c				= sAsset4.Id;
		sService8.Vehicle_Mileage__c	= 0;
		sService8.Service_Date__c		= date.newInstance(2008, 9, 1);
		liService.add(sService8);

		// Service < 9 months since last
		Service__c sService9			= new Service__c(); 
		sService9.Customer_Account__c	= sAccount3.Id;
		sService9.Asset__c				= sAsset4.Id;
		sService9.Vehicle_Mileage__c	= 265000;
		sService9.Service_Date__c		= date.newInstance(2009, 4, 1);
		liService.add(sService9);

		// Service > 9 months since last, same day as other vehicle for same customer
		Service__c sService10			= new Service__c(); 
		sService10.Customer_Account__c	= sAccount3.Id;
		sService10.Asset__c				= sAsset4.Id;
		sService10.Vehicle_Mileage__c	= 165000;
		sService10.Service_Date__c		= date.newInstance(2010, 12, 31);
		liService.add(sService10);

		insert liService;

		CSV_Header__c sCSVHeader = new CSV_Header__c();
		insert sCSVHeader;

		list<id>	li_CSVDataIds		= new list<id>();

		CSV_Data__c sCSVData = new CSV_Data__c();
		sCSVData.CSV_Header__c = sCSVHeader.Id;
		insert sCSVData;

		li_CSVDataIds.add(sCSVData.Id);

		// Finally, do some testing!!

		// Set up argument and return parameters
		SP_Args						 	oArgs;
		SP_Ret 							oRet;
		SP_DataIntegrityCheckerProcessing	oProcessor;
		
		// Test
		oArgs		= new SP_Args(li_CSVDataIds);
		oProcessor 	= new SP_DataIntegrityCheckerProcessing(oArgs);
		oRet		= oProcessor.procMain();

	}
}