@isTest
global with sharing class MockHttpResponseGeneratorForIHttpErr implements HttpCalloutMock 
{
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) 
    {        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/xml');
        res.setBody('<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">' + 
   					'	<S:Body>' +
      				'		<ns2:status xmlns:ns2="http://www.example.org/SFPortal/">' +
         			'			<statusCode>0</statusCode>' +
         			'			<shortMessage>Success</shortMessage>' +
      				'		</ns2:status>' +
   					'	</S:Body>' + 
					'</S:Envelope>');
        res.setStatusCode(400);
        res.setStatus('Test Http Layer Error');
        return res;
    }
}