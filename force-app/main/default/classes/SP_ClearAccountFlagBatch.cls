global with sharing class SP_ClearAccountFlagBatch implements Database.Batchable<sObject>
{
	public class SP_ClearAccountFlagBatchException extends Exception{}

	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		string	strQuery;

		if (!test.isRunningTest())
		{	
			strQuery =	
				' select	Id'	+
				' from 		Account' +
				' where 	Batch_Processed__c = true' ;
		}
		else
		{
			strQuery =		
				' select	Id'	+
				' from 		Account' +
				' where 	Batch_Processed__c = true' +
				' limit		200';
		}

		return Database.getQueryLocator(strQuery);
	}


	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		list<id> theProcessList = new list<id>();

		for(sobject s : scope)
		{
			theProcessList.add((id)s.get('Id'));
		}

		SP_ClearAccountFlagProcessing.SP_Args oArgs = new SP_ClearAccountFlagProcessing.SP_Args(theProcessList);

		SP_ClearAccountFlagProcessing processor = new SP_ClearAccountFlagProcessing(oArgs);
		SP_ClearAccountFlagProcessing.SP_Ret oRet = processor.ProcMain();
	}


	global void finish(Database.BatchableContext BC)
	{
		system.debug('Finished');
	}

	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static testMethod void testBatch() 
	{
		// Set up some database records
        Account sAccount;

        sAccount			= new Account();
        sAccount.FirstName	= 'Test';
        sAccount.LastName	= 'Account';
        sAccount.RecordTypeId = SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
        sAccount.Batch_Processed__c = true;
        insert sAccount;

		SP_ClearAccountFlagBatch oClearAccountFlagBatch = new SP_ClearAccountFlagBatch();
		oClearAccountFlagBatch.start(null);
		oClearAccountFlagBatch.execute(null, new list<SObject>{sAccount});
		oClearAccountFlagBatch.finish(null);
	}
}