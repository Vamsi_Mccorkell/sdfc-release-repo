public with sharing class SP_ServiceIntegrationProcessing 
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Exception Classes & Enums
	public class SP_Exception extends Exception{}

	public enum BatchParam {ALL, ALL_ASYNC}

	private static integer iQueryLimit = 10;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class used to pass arguments into the Batch
	public class SP_Args
	{
		public BatchParam 	BatchParam 		{get; set;}
		public List<id>		ProcessList		{get; set;}
		public boolean 		UseSavePoint	{get; set;}
		public boolean 		AllOrNone		{get; set;}

		public SP_Args(list<id> theProcessList)
		{
			this.ProcessList 	= theProcessList;
			this.UseSavePoint 	= true;
			this.AllOrNone 		= false;
		}

		public SP_Args(list<id> theProcessList, BatchParam theBatchParam)
		{
			this.ProcessList 	= theProcessList;
			this.BatchParam		= theBatchParam;
			this.UseSavePoint 	= true;
			this.AllOrNone 		= false;
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class used to pass information back to what called the batch
	public class SP_Ret
	{
		public boolean  	InError							{get; set;}
		public List<string> ErrorList						{get; set;}
				
		public SP_Ret()
		{
			this.InError = false;
			this.ErrorList = new List<string>();
		}
	}

	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Singular
	SP_Args 	mArgs;
	SP_Ret		mRet;
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Collections
	list<Service__c>			m_liServicesForUpdate	= new list<Service__c>();
	map<string, SPVO>			m_mapVINtoSPVO			= new map<string, SPVO>();
	set<string>					m_setVINIds				= new set<string>();

	set<string>					m_setMatchingAccountIds	= new set<string>();

	list<SPService>				m_liSPServices			= new list<SPService>();
	map<id, Account>			m_mapAccounts			= new map<id, Account>();


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Internal classes

	//  This class combines a Service record and Account matching results
	public class SPService
	{
		public id			ServiceId			{get; set;}
		public string		AccountId			{get; set;}
		public Service__c	Service				{get; set;}
//		public boolean	BusinessGroup		{get; set;}

		public SPService	(	id idServiceId, string strAccountId, Service__c sService
							)
		{
			ServiceId		= idServiceId;
			AccountId		= strAccountId;
			Service			= sService;
		}   
	}


	// This class contains data relevent to a VO
	public class SPVO
	{
		public id			CustomerRecordTypeId	{get; set;}
		public string		CustomerType			{get; set;}
		public string		CustomerOwnerType		{get; set;}
		public id			AssetID					{get; set;}
		public string		AssetName				{get; set;}
		public id			Customer				{get; set;}
		
		public SPVO()
		{
			
		}
	}
	

	/***********************************************************************************************************
		Constructor
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Our Constructor
	public SP_ServiceIntegrationProcessing(SP_Args oArgs)
	{
		mRet 	= new SP_Ret();
		mArgs 	= oArgs;
	}
	
	/***********************************************************************************************************
		Access Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// The one and only entry point for the processor
	public SP_Ret ProcMain()
	{
		Savepoint savePoint;
		
		if(mArgs.UseSavePoint)
			savePoint = Database.setSavepoint();
		
		try
		{
			ProcessServiceObject(mArgs.ProcessList);
		}
		catch(Exception e)
		{
			mRet.InError = true;
			mRet.ErrorList.add(e.getMessage());
						
			if(savePoint != null)
				Database.rollback(savePoint);
			
			throw new SP_Exception('Fatal Exception - All changes have been rolled back - ' + e.getMessage());
		}

		return mRet;
	}
	
	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Process Service Objects
	public void ProcessServiceObject(list<id> li_ServiceIdsIn)
	{
		// Build a list of the Service records to be processed
		Service__c [] li_ServicesIn =	[
											select	Id,
													Asset__c,
													Asset__r.Name,
													Customer_Account__c,
													Integration_Customer_Address__c,
													Integration_Customer_Name__c,
													Integration_Customer_Phone__c,
													Integration_Customer_Postcode__c,
													Integration_Customer_Suburb__c,
													Integration_Customer_VIN__c,
													Integration_Error_Message__c,
													Ready_For_Processing__c,
													Requires_Manual_Check__c
											from	Service__c
											where	Id in :li_ServiceIdsIn
											limit	2000
										];

		for (Service__c sService : li_ServicesIn)
		{
			if (sService.Integration_Customer_VIN__c != null)
			{
				m_setVINIds.add(sService.Integration_Customer_VIN__c);
			}
		}

		// Get Vehicle Ownerships for these assets
		Vehicle_Ownership__c [] arrVO =	[
											select	Customer__r.RecordTypeId,
													Customer__r.Type,
													Customer__r.Owner_Type__c,
													AssetID__c,
													AssetID__r.Name,
													Customer__c
											from	Vehicle_Ownership__c
											where	AssetID__r.Name in :m_setVINIds
											and		Status__c = 'Active'
											limit	2000
										];

		for (Vehicle_Ownership__c sVO : arrVO)
		{
			SPVO cSPVO = new SPVO();
			
			cSPVO.AssetID				= sVO.AssetID__c;
			cSPVO.AssetName				= sVO.AssetID__r.Name;
			cSPVO.Customer				= sVO.Customer__c;
			cSPVO.CustomerOwnerType		= sVO.Customer__r.Owner_Type__c;
			cSPVO.CustomerType			= sVO.Customer__r.Type;
			cSPVO.CustomerRecordTypeId	= sVO.Customer__r.RecordTypeId;
			
			m_mapVINtoSPVO.put(sVO.AssetID__r.Name, cSPVO);
		}

		// Now process the new Service data		
		for (Service__c sService : li_ServicesIn)
		{			
			// Call the customer match rules
			MatchingAccount ma = new MatchingAccount();
			string strMatchedAccountId;
			/*strMatchedAccountId = ma.customerMatching	(
															sService.Integration_Customer_VIN__c,			// VIN
															'',												// First Name
															sService.Integration_Customer_Name__c,			// Last name
															'',												// Email
															sService.Integration_Customer_Phone__c,			// Phone
															sService.Integration_Customer_Suburb__c,		// City
															sService.Integration_Customer_Postcode__c,		// Postcode
															sService.Integration_Customer_Address__c,		// Street
															false											// Don't create Cases
														);*/
														
			strMatchedAccountId = SimplifiedMatchingAccount.customerMatching(
															sService.Integration_Customer_VIN__c,			// VIN
															'',												// First Name
															sService.Integration_Customer_Name__c,			// Last name
															'',												// Email
															sService.Integration_Customer_Phone__c,			// Phone
															sService.Integration_Customer_Suburb__c,		// City
															sService.Integration_Customer_Postcode__c,		// Postcode
															sService.Integration_Customer_Address__c,		// Street
															false											// Don't create Cases
											);
														

			// Save the result
			SPService cSPService = new SPService	(	sService.Id,
														strMatchedAccountId,
														sService
													);
				
			m_liSPServices.add(cSPService);

			if (strMatchedAccountId != null)
			{
				m_setMatchingAccountIds.add(strMatchedAccountId);
			}
		}

		// Get the Accounts
		for (Account [] arrAccount :	[
											select	Id,
													RecordTypeId,
													Type,
													Owner_Type__c
											from	Account
											where	Id in :m_setMatchingAccountIds
										])
		{
			for (Account sAccount : arrAccount)
			{
				m_mapAccounts.put(sAccount.Id, sAccount);
			}
		}


		for (SPService cSPService : m_liSPServices)
		{
			Service__c sService = cSPService.Service;

			// Clear the error message text
			sService.Integration_Error_Message__c = '';

			string strMatchedAccountId = cSPService.AccountId;

			boolean bVOLinkedtoProspect = false;
			boolean bVOLinkedtoExOwner = false;
			boolean bNoActiveVO = false;

			if (strMatchedAccountId != null)
			{
				Account sAccount = m_mapAccounts.get(strMatchedAccountId);

system.debug('*** sAccount ***' + sAccount);

				// Make sure we are not trying to attach to a Prospect or an ex-Owner
				if (sAccount.RecordTypeId != SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType())
				{
					sService.Integration_Error_Message__c = 'ATTENTION - VO is not linked to an Owner Account ';
					bVOLinkedtoProspect = true;
				}
					
				if (sAccount.Owner_Type__c != 'Current')
				{
					sService.Integration_Error_Message__c = 'ATTENTION - VO is linked to an Owner Account who is not a current Owner ';
					bVOLinkedtoExOwner = true;
				}

				// Lastly, check for an active VO for this vehicle before assigning to the owner
				if (!m_mapVINtoSPVO.containsKey(sService.Integration_Customer_VIN__c))
				{
					sService.Integration_Error_Message__c = 'ATTENTION - Matched on Name BUT this VIN has no active VO';
					bNoActiveVO = true;
				}

				if (!bVOLinkedtoProspect && !bVOLinkedtoExOwner && !bNoActiveVO)
				{
					sService.Customer_Account__c	= strMatchedAccountId;
					sService.Requires_Manual_Check__c = false;
				}
				else
				{
					sService.Requires_Manual_Check__c = true;
				}
			}
			else
			{
				sService.Requires_Manual_Check__c = true;

				// Find Owner via an active VO
				if (m_mapVINtoSPVO.containsKey(sService.Integration_Customer_VIN__c))
				{
					sService.Integration_Error_Message__c = 'Match found through the VO ';

					// Get the VO for this vehicle
					SPVO cSPVO = m_mapVINtoSPVO.get(sService.Integration_Customer_VIN__c);

system.debug('*** cSPVO ***' + cSPVO);

					// Make sure we are not trying to attach to a Prospect or an ex-Owner
					if (cSPVO.CustomerRecordTypeId != SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType())
					{
						sService.Integration_Error_Message__c += 'ATTENTION - VO is not linked to an Owner Account ';
						bVOLinkedtoProspect = true;
					}
					
					if (cSPVO.CustomerOwnerType != 'Current')
					{
						sService.Integration_Error_Message__c += 'ATTENTION - VO is linked to an Owner Account who is not a current Owner ';
						bVOLinkedtoExOwner = true;
					}

					if (!bVOLinkedtoProspect && !bVOLinkedtoExOwner)
					{
						sService.Customer_Account__c	=  cSPVO.Customer;
					}
				}
				else
				{
					sService.Integration_Error_Message__c = 'No active VO found for this VIN ';
				}

				// We didn't get a match through the rules, so indicate a potential COO
				sService.Integration_Error_Message__c += ' Potential COO as no matching Customer found: ' + 
														' Customer Name: ' 			+ sService.Integration_Customer_Name__c +
														' Customer Address: '		+ sService.Integration_Customer_Address__c +
														' Customer Suburb: '		+ sService.Integration_Customer_Suburb__c +
														' Customer Postcode: ' 		+ sService.Integration_Customer_Postcode__c +
														' Customer Phone Number '	+ sService.Integration_Customer_Phone__c;
			}

			sService.Ready_For_Processing__c = false;

			m_liServicesForUpdate.add(sService);
		}

		update m_liServicesForUpdate;

	}

	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 
	public static testMethod void testSP_ServiceIntegrationProcessing()
	{
	// Create records to be used in this test method

		Product2 sProduct1 = new Product2();
		sProduct1.Name					= 'IS200';
		insert sProduct1;

		Product2 sProduct2 = new Product2();
		sProduct2.Name					= 'SC430';
		insert sProduct2;

		Asset__c sAsset1 = new Asset__c();
		sAsset1.Name					= 'ABC12345678';
		sAsset1.Vehicle_Model__c		= sProduct1.Id;
		insert sAsset1;

		Asset__c sAsset2 = new Asset__c();
		sAsset2.Name					= 'XYZ98765432';
		sAsset2.Vehicle_Model__c		= sProduct2.Id;
		insert sAsset2;

		Asset__c sAsset3 = new Asset__c();
		sAsset3.Name					= 'DDDD8765432';
		sAsset3.Vehicle_Model__c		= sProduct1.Id;
		insert sAsset3;

		Asset__c sAsset4 = new Asset__c();
		sAsset4.Name					= 'JJJJ8765432';
		sAsset4.Vehicle_Model__c		= sProduct2.Id;
		insert sAsset4;

		list<Account>	li_Account		= new list<Account>();

		Account sAccount1 = new Account();
		sAccount1.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccount1.LastName				= 'TestLast1';
		sAccount1.PersonEmail			= 'test1@email.com';
		li_Account.add(sAccount1);

		Account sAccount2 = new Account();
		sAccount2.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccount2.LastName				= 'TestLast2';
		sAccount2.PersonEmail			= 'test2@email.com';
		li_Account.add(sAccount2);

		Account sAccount3 = new Account();
		sAccount3.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccount3.LastName				= 'TestLast3';
		sAccount3.PersonEmail			= 'test3@email.com';
		li_Account.add(sAccount3);

		insert li_Account;

		// Now we need to hit the dataase to get the ids of the Accounts we just created
		Account[] arrAccount =	[
									select	Id
									from	Account
									where	LastName = :'TestLast1'
									or		LastName = :'TestLast2'
									or		LastName = :'TestLast3'
									limit	3
								];

		list<id>		li_AccountIds	= new list<id>();

		for (Account sAccount : arrAccount)
		{
			li_AccountIds.add(sAccount.Id);
		}

		// Create a number of Service records
		list<Service__c> liService = new list<Service__c>();

		// Two customers, each with one vehicle, all services to be counted
		for (integer i=1; i<4; i++)
		{
			Service__c sService1			= new Service__c(); 
			sService1.Customer_Account__c	= sAccount1.Id;
			sService1.Asset__c				= sAsset1.Id;
			sService1.Vehicle_Mileage__c	= 7000 * i;
			sService1.Service_Date__c		= system.today().addYears(-(4-i));

			liService.add(sService1);

			Service__c sService2			= new Service__c(); 
			sService2.Customer_Account__c	= sAccount2.Id;
			sService2.Asset__c				= sAsset2.Id;
			sService2.Vehicle_Mileage__c	= 12345 * i;
			sService2.Service_Date__c		= system.today().addYears(-(4-i));

			liService.add(sService2);
			
		}

		// Now one customer with two vehicles, not all services to be counted
		// First service - counted
		Service__c sService3			= new Service__c(); 
		sService3.Customer_Account__c	= sAccount3.Id;
		sService3.Asset__c				= sAsset3.Id;
		sService3.Vehicle_Mileage__c	= 12345;
		sService3.Service_Date__c		= date.newInstance(2008, 1, 1);
		liService.add(sService3);

		// 4999 km past due - count as 30,000km service
		Service__c sService4			= new Service__c(); 
		sService4.Customer_Account__c	= sAccount3.Id;
		sService4.Asset__c				= sAsset3.Id;
		sService4.Vehicle_Mileage__c	= 34999;
		sService4.Service_Date__c		= date.newInstance(2008, 10, 2);
		liService.add(sService4);

		// 5000 km past due - count as 40,000 service
		Service__c sService5			= new Service__c(); 
		sService5.Customer_Account__c	= sAccount3.Id;
		sService5.Asset__c				= sAsset3.Id;
		sService5.Vehicle_Mileage__c	= 35000;
		sService5.Service_Date__c		= date.newInstance(2009, 8, 3);
		liService.add(sService5);

		// Outside mileage range - use average
		Service__c sService6			= new Service__c(); 
		sService6.Customer_Account__c	= sAccount3.Id;
		sService6.Asset__c				= sAsset3.Id;
		sService6.Vehicle_Mileage__c	= 300000;
		sService6.Service_Date__c		= date.newInstance(2010, 12, 31);
		liService.add(sService6);

		// Different vehicle - should count first service
		Service__c sService7			= new Service__c(); 
		sService7.Customer_Account__c	= sAccount3.Id;
		sService7.Asset__c				= sAsset4.Id;
		sService7.Vehicle_Mileage__c	= 121000;
		sService7.Service_Date__c		= date.newInstance(2007, 10, 1);
		liService.add(sService7);			

		// Mileage is zero - use average cost for model
		// Also, date is > 9 months since last service for this vehicle but < 9 months since last service for this customer
		Service__c sService8			= new Service__c(); 
		sService8.Customer_Account__c	= sAccount3.Id;
		sService8.Asset__c				= sAsset4.Id;
		sService8.Vehicle_Mileage__c	= 0;
		sService8.Service_Date__c		= date.newInstance(2008, 9, 1);
		liService.add(sService8);

		// Service < 9 months since last
		Service__c sService9			= new Service__c(); 
		sService9.Customer_Account__c	= sAccount3.Id;
		sService9.Asset__c				= sAsset4.Id;
		sService9.Vehicle_Mileage__c	= 265000;
		sService9.Service_Date__c		= date.newInstance(2009, 4, 1);
		liService.add(sService9);

		// Service > 9 months since last, same day as other vehicle for same customer
		Service__c sService10			= new Service__c(); 
		sService10.Customer_Account__c	= sAccount3.Id;
		sService10.Asset__c				= sAsset4.Id;
		sService10.Vehicle_Mileage__c	= 165000;
		sService10.Service_Date__c		= date.newInstance(2010, 12, 31);
		liService.add(sService10);

		list<id> liServiceIds = new list<id>();

		insert liService;

		for (Service__c sService : liService)
		{
			liServiceIds.add(sService.Id);
		}

		// Finally, do some testing!!

		// Set up argument and return parameters
		SP_Args						 	oArgs;
		SP_Ret 							oRet;
		SP_ServiceIntegrationProcessing	oProcessor;
		
		// Test
		oArgs		= new SP_Args(liServiceIds);
		oProcessor 	= new SP_ServiceIntegrationProcessing(oArgs);
		oRet		= oProcessor.procMain();

	}
}