// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for account creating accounts need to view the custom settings data
@IsTest (SeeAllData=true)
public class AccountControllerExtTest {
    static testmethod void testGetGuestVehicles(){
        Account a = new Account(FirstName = 'Test', LastName = 'Test');
        insert a;
        ApexPages.StandardController stdCon = new ApexPages.StandardController(a);
        AccountControllerExt cont = new AccountControllerExt(stdCon);
        
        a.Guest_Gigya_ID__c = 'TESTGIGYA';
        update a;
        
        cont.getGuestVehicles();
        
        // DEV NOTE: This just tests the extension and just aims for coverage; It calls the VehicleManagementHandler.getGuestVehicles(account.Guest_Gigya_ID__c) method
        // which is tested with assertions in the VehicleManagementHandlerTest class
    }
}