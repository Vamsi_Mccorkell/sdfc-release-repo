// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for creating accounts need to view the custom settings data
@IsTest (SeeAllData = true)
public class AccountTriggerHandlerTest {
	public static Account properAcct;
    public static Account incAcct;
    public static Account acctWithVO;
    public static Account acctWithGR;
    
    static testmethod void cannotDeleteVerifiedAccount(){
        dataSetup();
        try {
            delete properAcct; // Verified GR
        }
        catch (DmlException e){
            system.assertEquals(true, e.getMessage().contains(Label.GRDeleteActiveGRStatusError));
        }
        
        try {
            delete acctWithVO; // Account with VO
        }
        catch (DmlException e){
            //system.assertEquals(true, e.getMessage().contains(Label.GRDeleteActiveGRStatusError));
        }
        
        try {
            delete acctWithGR; // account with GR
        }
        catch (DmlException e){
            system.assertEquals(true, e.getMessage().contains(Label.GRDeleteActiveGRStatusError ));
        }
        
        // delete the inc account
        delete incAcct;
        List<Account> resultAcctList = [select Id from Account where Id = :incAcct.Id];
        system.assertEquals(0, resultAcctList.size());
    }
    static testmethod void mergeAccountSuccessful(){
        dataSetup();
        Database.merge(properAcct, incAcct);
    }
    
    static testmethod void mergeAccountUnsuccessful(){
        dataSetup();
        try {
            Database.merge(incAcct, properAcct);
        }
        catch (Exception e){
            system.assertEquals(true, e.getMessage().contains(Label.GRDeleteActiveGRStatusError));
        }
    }
	
    static testmethod void compensateForUnreachableCode(){
        dataSetup();
        AccountTriggerHandler.codeCompensation();
    }
    
    
    private static void dataSetup(){
        List<Account> newAcctList = new List<Account>();
        
        properAcct = new Account(FirstName = 'Test', LastName = 'Test', PersonEmail = 'a@email.com', GR_Status__c = 'Verified', Guest_Gigya_ID__c = 'GIGYA1');
        newAcctList.add(properAcct);
        incAcct = new Account(FirstName = 'Test', LastName = 'Test');
        newAcctList.add(incAcct);
        acctWithGR = new Account(FirstName = 'Test', LastName = 'Test');
        newAcctList.add(acctWithGR);
        insert newAcctList;
        
        Vehicle_Ownership__c vo = [select Id, Customer__c from Vehicle_Ownership__c where Status__c = 'Active' limit 1];
        vo.Verified_Guest_ID__c = 'GIGYA1';
        update vo;
        acctWithVO = [select Id from Account where Id = :vo.Customer__c limit 1];
        
        Guest_Relationship__c gr = new Guest_Relationship__c();
        gr.Account__c = acctWithGR.Id;
        gr.Gigya_ID__c = 'GIGYA1';
        gr.GR_Status__c = 'Verified';
        gr.Start_Date__c = system.now();
        insert gr;
        
    }
}