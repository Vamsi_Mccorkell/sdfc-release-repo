/* DATE: September 2019
 * AUTHOR: D BANEZ / MCCORKELL
 * CLASS SUMMARY: Account controller extension to allow LCAC to call the Get Guest Vehicles service 
 * 					from the account page via the AccountGuestVehicles VF page
 * VF PAGE USING THIS CLASS: AccountGuestVehicles
 * TEST CLASS: AccountControllerExtTest
 * */
public with sharing class AccountControllerExt {
    public Account account {get;set;}
    public MyLexusWebService.GuestVehicleWSResponse resp {get;set;}
    
    public AccountControllerExt(ApexPages.StandardController stdCon){
        this.account = (Account)stdCon.getRecord();
        this.account = [select Id, Name, Guest_Gigya_ID__c from Account where Id = :account.Id limit 1];
        resp = new MyLexusWebService.GuestVehicleWSResponse();
        if (this.account.Guest_Gigya_ID__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Guest ID is blank'));
        }
    }
    
    public PageReference getGuestVehicles(){
        resp = VehicleManagementHandler.getGuestVehicles(account.Guest_Gigya_ID__c);
        return null;
    }
}