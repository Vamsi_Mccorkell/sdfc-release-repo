/*******************************************************************************
@author:        Donnie Banez
@date:          August 2019
@description:   Trigger Handler for Customer Offer Detail Trigger
@Revision(s):    
@Test Methods:  CustomerOfferDetailTriggerHandlerTest
********************************************************************************/
public with sharing class CustomerOfferDetailTriggerHandler {
    public static TriggerAutomations__c automationEnabled  = TriggerAutomations__c.getInstance(); 
    public static Boolean triggerEnabled = automationEnabled.Customer_Offer_Detail__c; 
    public static void mainEntry(Boolean isBefore, Boolean isAfter, Boolean isInsert, Boolean isUpdate,Boolean isDelete, Boolean isUnDelete, 
                    List<SObject> newList, List<SObject> oldList, Map<ID, SObject> newmap, Map<ID, SObject> oldmap)
    {
        if (Test.isrunningTest())  {
            triggerEnabled = true;
        }
        
        if (triggerEnabled){
            try{
                // INSERT TRIGGER
                if(isInsert){
                    // Before
                    if(isBefore){
                        validateUniqueCOD(newList);
                    }            
                    // After
                    if(isAfter){
  						
                    }
                }
                // Update TRIGGER
                if(isUpdate){
                    // Before
                    if(isBefore){
                        
                    }            
                    // After
                    if(isAfter){
  
                    }
                }
                // Delete TRIGGER
                if(isDelete){
                    // Before
                    if(isBefore){
                        
                    }            
                    // After
                    if(isAfter){
   
                    }
                }
                // UNDELETE Trigger
                if(isUnDelete){
                    if (isBefore){
                        
                    }
                    if (isAfter){
  
                    }
                }  
            } catch (Exception ex){
                String errorString = ex.getStackTraceString()+'--'+ ex.getTypeName()+'--'+ ex.getMessage();
                system.debug('### Customer Offer Detail Trigger Handler Exception Thrown = ' + errorString);
                newList[0].addError(errorString);
            }
        }
    }
    
    public static void validateUniqueCOD(List<Customer_Offer_Detail__c> newList){
        Set<Id> customerIdSet = new Set<Id>();
        Set<Id> offerIdSet = new Set<Id>();
        for (Customer_Offer_Detail__c cod : newList){
           customerIdSet.add(cod.Customer__c);
           offerIdSet.add(cod.Offer__c); 
        }
        List<Customer_Offer_Detail__c> codList = [select Id, Customer__c, Offer__c
                                                  from Customer_Offer_Detail__c
                                                  where Customer__c in :customerIdSet
                                                  and Offer__c in :offerIdSet];
		for (Customer_Offer_Detail__c cod : newList){
            for (Customer_Offer_Detail__c existingCOD : codList){
                if (cod.Customer__c == existingCOD.Customer__c && cod.Offer__c == existingCOD.Offer__c){ // if there is the same
                    cod.addError(Label.DuplicateCODError);
                    break;
                }
            } 
        }
    }
    
    /*COMMENTED OUT AND MAY NO LONGER BE NEEDED
    public static void preventCODDeletion(List<Customer_Offer_Detail__c> oldList){
        for (Customer_Offer_Detail__c cod : oldList){
            if (!cod.Delete_Record__c){
                cod.addError(Label.CODDeletionError);
            }
        }
    }*/
}