// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for account creating accounts need to view the custom settings data
@IsTest (SeeAllData=true)
public class GuestServicesWebServicesControllerTest {
	// DEVELOPER NOTES: These are just all coverage test methods; The assertion test methods are in the corresponding
	// test method classes for each of the handler classes
	public static Account customer;
    public static Customer_Offer_Detail__c cod;
    
    static testmethod void getOnDemandAvailability(){
        dataSetup();
        GuestServicesWebServicesController cont = new GuestServicesWebServicesController();
        cont.guestAcctId = customer.Id;
        cont.gigyaId = customer.Guest_Gigya_ID__c;
        cont.codId = null;
        cont.getOnDemandAvailability();
    }
    
    static testmethod void writeOnDemandTransactions(){
        dataSetup();
        GuestServicesWebServicesController cont = new GuestServicesWebServicesController();
        cont.sfWriteAcctId = customer.Id;
        cont.writeGigyaId = customer.Guest_Gigya_ID__c;
        cont.writecodId = cod.Id;
        cont.voucher.Voucher_Type__c = 'Car Hire';
        cont.redemptionLocation = 'test location';
        cont.redemptionCredits = 1;
        cont.redemption.Redemption_Start_Date_Time__c = system.now();
        cont.redemption.Start_Date_Time_Offset__c = 12;
        cont.redemption.Start_Date_Time_Text__c = 'red start time';
        cont.redemption.Redemption_End_Date_Time__c = system.now() + 5;
        cont.redemption.End_Date_Time_Offset__c = 12;
        cont.redemption.End_Date_Time_Text__c = 'red end time';
        cont.redemptionModel = 'Model';
        cont.bookingReference = 'B001';
        cont.redemptionDescription = 'Test booking';
        cont.bookingStatus = 'Booked';
        cont.redemptionRego = 'REGOTEST';
        cont.writeOnDemandTransactions();
    }
    
    static testmethod void editOnDemandTransactions(){
        dataSetup();
        GuestServicesWebServicesController cont = new GuestServicesWebServicesController();
        cont.editBookReference = 'B001';
        cont.editLocation = 'test location';
        cont.editRedemption.Redemption_Start_Date_Time__c = system.now();
        cont.editRedemption.Start_Date_Time_Offset__c = 12;
        cont.editRedemption.Start_Date_Time_Text__c = 'red start time';
        cont.editRedemption.Redemption_End_Date_Time__c = system.now() + 5;
        cont.editRedemption.End_Date_Time_Offset__c = 12;
        cont.editRedemption.End_Date_Time_Text__c = 'red end time';
        cont.editModel = 'Model';
        cont.editDescription = 'Test booking';
        cont.editRego = 'REGOTEST';
        cont.editOnDemandTransactions();
          
    }
    
    static testmethod void getGuestVoucherAvailability(){
        dataSetup();
        GuestServicesWebServicesController cont = new GuestServicesWebServicesController();
        cont.voodGigyaId = customer.Guest_Gigya_ID__c;
        cont.voodSFVOId = null;
        cont.getGuestVoucherAvailability();
    }
    
    static testmethod void getPartnersandBranches(){
        dataSetup();
        GuestServicesWebServicesController cont = new GuestServicesWebServicesController();
        cont.getPartnersandBranches();
    }
    
    static testmethod void cancelOnDemandTransaction(){
        dataSetup();
        GuestServicesWebServicesController cont = new GuestServicesWebServicesController();
        cont.cancelSfAcctId = customer.Id;
        cont.cancelGigyaId = customer.Guest_Gigya_ID__c;
        cont.cancelBookingReference = 'B001';
        cont.creditCustomer = true;
        cont.cancelOnDemandTransaction();
    }
    
    private static void dataSetup(){
        List<Account> newAcctList = new List<Account>();
        Id PARTNERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner').getRecordTypeId();
        Id CUSTOMERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        Account partnerAcct = new Account(Name = 'Partner Account', RecordTypeId = PARTNERACCTRECORDTYPEID, Partner_Class__c = 'Customer');
        newAcctList.add(partnerAcct);
        
        customer = new Account(FirstName = 'Test', LastName = 'Customer', RecordTypeId = CUSTOMERACCTRECORDTYPEID, Guest_Gigya_ID__c = 'TESTGIGYA');
        newAcctList.add(customer);
        
        insert newAcctList;
        
        Offer__c newOffer = new Offer__c(Partner_Account__c = partnerAcct.Id, Status__c = 'Inactive', Offer_Class__c = 'Customer', Customer_Platinum_Limit__c = 10,
                               Offer_Type__c = 'Car Hire');
        insert newOffer;
        newOffer.Status__c = 'Active';
        update newOffer;
        
        cod = new Customer_Offer_Detail__c();
        cod.Customer__c = customer.Id;
        cod.Offer__c = newOffer.Id;
        cod.First_Credit_Expiration__c = system.today()+5;
        cod.Total_Credits_Allowed__c = 10;
        cod.Total_Customer_Credits_Consumed__c = 0;
        insert cod;
    }
}