public with sharing class SP_VehicleOwnershipManagement
{
	
	public static set<id>				m_setNewRegistrationOwnerIds		= new set<id>();
	public static set<id>				m_setUpdateEnformExpiryOwnerIds		= new set<id>();
	
	public static list<Enform_User__c>	m_liEnformUserstoInsert				= new list<Enform_User__c>();
	
	
	
	public static void EnformRegistrationInsert(map<id, Vehicle_Ownership__c> mapNew)
	{
		m_setNewRegistrationOwnerIds		= new set<id>();
		m_liEnformUserstoInsert				= new list<Enform_User__c>();
		m_setUpdateEnformExpiryOwnerIds		= new set<id>();
				
		for (Vehicle_Ownership__c sVO : mapNew.values())
		{
			if (sVO.Enform_Registered__c)
			{
				m_setNewRegistrationOwnerIds.add(sVO.Customer__c);
			}
		}
		
		updateEnformData();
	}


	public static void EnformRegistrationUpdate(map<id, Vehicle_Ownership__c> mapOld, map<id, Vehicle_Ownership__c> mapNew)
	{
		m_setNewRegistrationOwnerIds		= new set<id>();
		m_liEnformUserstoInsert				= new list<Enform_User__c>();
		m_setUpdateEnformExpiryOwnerIds		= new set<id>();
		
		for (Vehicle_Ownership__c sVOOld : mapOld.values())
		{
			Vehicle_Ownership__c sVONew = mapNew.get(sVOOld.Id);
			
			system.debug('############# OLD ' + sVOOld + ',  ######  NEW ' + sVONew);

			if (!sVOOld.Enform_Registered__c && sVONew.Enform_Registered__c)
			{
				m_setNewRegistrationOwnerIds.add(sVONew.Customer__c);
			}

			if (sVOOld.Enform_Registered__c && !sVONew.Enform_Registered__c)
			{
				m_setUpdateEnformExpiryOwnerIds.add(sVOOld.Customer__c);
			}
			
			if (sVOOld.Status__c == 'Inactive' && sVONew.Status__c == 'Active' && sVONew.Enform_Registered__c)
			{
				m_setUpdateEnformExpiryOwnerIds.add(sVONew.Customer__c);
				m_setUpdateEnformExpiryOwnerIds.add(sVOOld.Customer__c);
			}
			
			// ******************* IMPORTANT NOTE ***********************//
			/*
				Since the change in Owner due to merging of accounts won't fire this trigger, there is no
				use in keeping this logic here. It has been moved to Account trigger
			*/
			// Owner changed as well?
			/*if (sVOOld.Customer__c != sVONew.Customer__c)
			{
				
				if (sVOOld.Enform_Registered__c)
				{
					//m_setUpdateEnformExpiryOwnerIds.add(sVOOld.Customer__c);
					m_setUpdateEnformExpiryOwnerIds.add(sVONew.Customer__c);
				}

				if (sVONew.Enform_Registered__c)
				{
					m_setNewRegistrationOwnerIds.add(sVONew.Customer__c);
				}
			}*/
		}
		
		updateEnformData();
	}	
	
	
	public static void EnformRegistrationDelete(map<id, Vehicle_Ownership__c> mapOld)
	{
		m_setNewRegistrationOwnerIds		= new set<id>();
		m_liEnformUserstoInsert				= new list<Enform_User__c>();
		m_setUpdateEnformExpiryOwnerIds		= new set<id>();
		
		for (Vehicle_Ownership__c sVO : mapOld.values())
		{
			if (sVO.Enform_Registered__c)
			{
				m_setUpdateEnformExpiryOwnerIds.add(sVO.Customer__c);
			}
		}
		
		updateEnformData(mapOld.keySet());
	}
	
	public static void updateEnformData()
	{
		updateEnformData(null);
	}
	
	
	public static void updateEnformData(set<ID> excVOIds)
	{

		
		// Check that an Enform User record exists for Owners with newly Enform registered vehicles
		set<id> set_OwnersNeedingEnformUserRecords = new set<id>();
		
		if (!m_setNewRegistrationOwnerIds.isEmpty())
		{
			set<id> set_OwnerswithEnformUserRecords = new set<id>();

			for (Enform_User__c [] arrEnformUsers :	[	select	id,
																Owner_id__c
														from	Enform_User__c
														where	Owner_Id__c in :m_setNewRegistrationOwnerIds
													])
			{
				for (Enform_User__c sEnformUser : arrEnformUsers)
				{
					set_OwnerswithEnformUserRecords.add(sEnformUser.Owner_id__c);
				}
			}
			
			for (id idOwnerId : m_setNewRegistrationOwnerIds)
			{
				if (!set_OwnerswithEnformUserRecords.contains(idOwnerId))
				{
					set_OwnersNeedingEnformUserRecords.add(idOwnerId);
				}
			}
			
			// Get details from the Owner records
			for (Account [] arrOwners :	[	select	id,
													FirstName,
													LastName,
													PersonEmail,
													PersonMobilePhone
											from	Account
											where	id in :set_OwnersNeedingEnformUserRecords
										])
			{
				for (Account sOwner : arrOwners)
				{
					Enform_User__c sEnformUser = new Enform_User__c();
					sEnformUser.Email__c							= sOwner.PersonEmail;
					sEnformUser.First_Name__c						= sOwner.FirstName;
					sEnformUser.IsDealer__c							= false;
					sEnformUser.IsPrimaryUser__c					= true;
					sEnformUser.Last_Name__c						= sOwner.LastName;
					sEnformUser.Mobile_Phone__c						= sOwner.PersonMobilePhone;
					sEnformUser.Owner_ID__c							= sOwner.Id;
					sEnformUser.IsActive__c							= true;

					m_liEnformUserstoInsert.add(sEnformUser);
				}
			}
			
			if (!m_liEnformUserstoInsert.isEmpty())
			{
				insert m_liEnformUserstoInsert;
			}
			
			// Add these OwnerIds to the set for which we will recalculate expiry dates
			m_setUpdateEnformExpiryOwnerIds.addAll(m_setNewRegistrationOwnerIds);
		}

		// Now fire the future method to recalculate Enform details held on the Owner records
		if (system.isBatch() || system.isFuture() || system.isScheduled() || test.isRunningTest())
			SP_EnformUtilities.UpdateEnformDetailOwnerIdsNoFuture(m_setUpdateEnformExpiryOwnerIds, excVOIds);
		else
			SP_EnformUtilities.UpdateEnformDetailOwnerIds(m_setUpdateEnformExpiryOwnerIds, excVOIds);
	}
	
	/*@isTest(SeeAllData=true)
	public static void testMethodSP_VehicleOwnershipManagement()
	{
		Test.startTest();
		
		List<Account> li_Accounts = new List<Account>();
		
		Account sAccountWithEU = new Account();
		sAccountWithEU.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccountWithEU.LastName				= 'TestLast1';
		sAccountWithEU.PersonEmail			= 'test1@email.com';
			
		li_Accounts.add(sAccountWithEU);
		
		Account sAccountNew = new Account();
		sAccountNew.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccountNew.LastName				= 'TestLast2';
		sAccountNew.PersonEmail				= 'test1@email2.com';
		sAccountNew.PersonMobilePhone 		= '12345467';
		
		li_Accounts.add(sAccountNew);
		
		insert li_Accounts;
		
		Enform_User__c testEUAcc = new Enform_User__c(Owner_Id__c = sAccountWithEU.Id,
												   Mobile_Phone__c = '045678923',
												   Email__c = 'aaa@aa.com',
												   First_Name__c = 'First',
												   Last_Name__c = 'Last',
												   IsPrimaryUser__c = true);
		insert testEUAcc;
		
		
		List<Asset__c> li_assets = new List<Asset__c>();
		
		Asset__c testAsset1 = new Asset__c(Name = 'test1', Original_Sales_Date__c = Date.today().addDays(-50));
		li_assets.add(testAsset1);
		
		Asset__c testAsset2 = new Asset__c(Name = 'test2', Original_Sales_Date__c = Date.today().addDays(-25));
		li_assets.add(testAsset2);
		
		insert li_assets;
		
		List<Vehicle_Ownership__c> li_VOs = new List<Vehicle_Ownership__c>();
		
		Vehicle_Ownership__c testVO1 = new Vehicle_Ownership__c(Enform_Registered__c = true,
																Customer__c = sAccountWithEU.Id,
																Status__c = 'Active',
																AssetID__c = testAsset1.Id);
		li_VOs.add(testVO1);	
		
		Vehicle_Ownership__c testVO2 = new Vehicle_Ownership__c(Enform_Registered__c = true,
																Customer__c = sAccountNew.Id,
																Status__c = 'Active',
																AssetID__c = testAsset2.Id);
		li_VOs.add(testVO2);	
		
		insert li_VOs;
		
		testVO1.Enform_Registered__c = false;
		testVO2.Enform_Registered__c = false;
		
		update li_VOs; 
		
		testVO1.Enform_Registered__c = true;
		testVO2.Enform_Registered__c = true;
		
		update li_VOs;
		
		set<ID> ownerIds = new set<ID>();
		ownerIds.add(sAccountWithEU.Id);
		ownerIds.add(sAccountNew.Id);
		
		system.debug('################# BEGIN TEST ################ ' + ownerIds);
		SP_EnformUtilities.UpdateEnformDetailOwnerIdsNoFuture(ownerIds);
		system.debug('################# END TEST ################');
		
		delete li_VOs;
		
		Test.stopTest();
	}	*/	
}