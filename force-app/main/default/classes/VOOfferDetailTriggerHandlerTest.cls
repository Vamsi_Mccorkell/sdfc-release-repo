// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for creating accounts need to view the custom settings data
@IsTest (SeeAllData=true)
public class VOOfferDetailTriggerHandlerTest {
	static Offer__c newOffer;
    static Account customer;
    static Account partnerAcct;
    static Vehicle_Ownership__c validForOfferVO;
    
    static testmethod void checkDuplicateVOOD(){
        dataSetup();
        VO_Offer_Detail__c vood = [select Id from VO_Offer_Detail__c where Vehicle_Ownership__c = :validForOfferVO.Id and Offer__c = :newOffer.Id limit 1];
        try {
            delete vood;
        }
        catch (DmlException e){
            system.assertEquals(true, e.getMessage().contains(Label.VOODDeletionError));
        }
        
    }
    
    static testmethod void preventDeletion(){
        dataSetup();
        VO_Offer_Detail__c vood = new VO_Offer_Detail__c();
        vood.Vehicle_Ownership__c = validForOfferVO.Id;
        vood.Offer__c = newOffer.Id;
        
        try {
            insert vood;
        }
        catch (DmlException e){
            system.assertEquals(true, e.getMessage().contains(Label.DuplicateVOODError));
        }
        
    }
    
    static testmethod void recalculateCounts(){
        dataSetup();
        VO_Offer_Detail__c vood = [select Id, Credit_Limit_Adjustment__c, Credit_Limit_Comment__c from VO_Offer_Detail__c where Vehicle_Ownership__c = :validForOfferVO.Id and Offer__c = :newOffer.Id limit 1];
        vood.Credit_Limit_Adjustment__c  = 2;
        vood.Credit_Limit_Comment__c = 'test';
        update vood;
    }
    
    static void dataSetup(){
        Id PARTNERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner').getRecordTypeId();
        
        partnerAcct = new Account(Name = 'Partner Cust', RecordTypeId = PARTNERACCTRECORDTYPEID, Partner_Class__c = 'Customer');
        insert partnerAcct;
        
        
        validForOfferVO = [select Id, Customer__c, AssetID__c from Vehicle_Ownership__c 
                          where Status__c = 'Active' and Valid_for_Offers__c = false limit 1]; // try to get 1 that isn't valid for offer yet
        Asset__c asst = new Asset__c(Id = validForOfferVO.AssetID__c);
        asst.Original_Sales_Date__c = system.today();
        asst.Encore_Tier__c = 'Platinum';
        update asst;    
        customer = [select Id, Guest_Gigya_ID__c from Account where Id = :validForOfferVO.Customer__c limit 1];
        customer.Guest_Gigya_ID__c = 'TESTGIGYA';
        update customer;
        validForOfferVO.Valet_Voucher_Enabled__c = true;
        validForOfferVO.Voucher_T_C_Accepted__c = true;
        validForOfferVO.Voucher_T_C_Accepted_Date__c = system.today();
        validForOfferVO.Asset_RDR_Date__c = system.today();
        update validForOfferVO;
        
        newOffer = new Offer__c(Status__c = 'Inactive');
        newOffer.Offer_Class__c = 'Customer';
        newOffer.Offer_Type__c = 'Car Hire';
        newOffer.Customer_Platinum_Limit__c = 10;
        newOffer.Partner_Account__c = partnerAcct.Id;
        insert newOffer;
        newOffer.Status__c = 'Active';
        update newOffer;
        
        
        
    }
}