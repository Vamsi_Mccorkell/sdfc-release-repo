/* Author: D BANEZ / M&A 
*  Purpose: Web service used by MyLexus
* ERROR CODES SUMMARY:
* 700: Required fields missing
* 800: Write failed (any exception)
* 60x: Logic and business scenarios issue
    601: SF acct ID and SF VO ID provided but they do not match
    602: VO Id provided is invalid or does not exist
    603: Account matched but Gigya ID provided is not the same as the gigya Id in the account
    604: Multiple Account Matches on provided information
    605: No VIN found in the system
    606: Multiple VIN Matches found. Raised a case for investigation
    607: VIN has an active VO whose account does not match the guest account
	608: VIN exists but is not associated with a customer
	609: VO Id account does not match the provided account ID for Vehicle Verification
	610: An account is found with active GR records but no Gigya ID 
	611: No case found for case number provided
	612: Gigya ID and email do not match
	613: Multiple matches on Gigya ID and Email
	614: Registration link expired
    615: No existing account with the gigya ID provided
5/2/20 : VD added new field Encore Tier to  Guest Account method
*/
global class MyLexusWebService {
    
    //- 25/2/20 Added for hashed  id lookup during guest registration
    global class GRHashLookupResponse {
        webservice Boolean success {get;set;}
        webservice String  sfHashedId {get;set;}
        webservice String  sfAcctId {get;set;}
        webservice String  sfVOId {get;set;}
        webservice String  errCode {get;set;}
        webservice String  errorDesc {get;set;}
                
        global GRHashLookupResponse(){
            success = false;
            sfHashedId = null;
            sfAcctId = null;
            sfVOId = null;
            errCode = null;
            errorDesc = null;
         }
    }
    
    global class WSResponse {
        webservice Boolean success {get;set;}
        webservice Id recordId {get;set;}
        webservice String errCode {get;set;}
        webservice String errorDesc {get;set;}
        webservice String caseNumber {get;set;}
        
        global WSResponse(){
            success = false;
            recordId = null;
            errCode = null;
            errorDesc = null;
            caseNumber = null;
        }
    }
   
    global class GRResponse {
        webservice Boolean success {get;set;}
        webservice Id recordId {get;set;}
        webservice String errCode {get;set;}
        webservice String errorDesc {get;set;}
        webservice String caseNumber {get;set;}
        webservice String firstName {get;set;}
        webservice String lastName {get;set;}
        webservice String email {get;set;}
        webservice String mobile {get;set;}
        webservice String termsAndConditionsVersion  {get;set;}
        webservice String privacyPolicyVersion {get;set;}
                
        global GRResponse(){
            success = false;
            recordId = null;
            errCode = null;
            errorDesc = null;
            caseNumber = null;
            firstName = null;
            lastName = null;
            email = null;
            mobile = null;
            termsAndConditionsVersion = null;
            privacyPolicyVersion = null;
        }
    }
    
    global class GuestProfileResponse {
        webservice GuestAccount outputAcct {get;set;}
        webservice String caseNumber {get;set;}
        webservice String errCode {get;set;}
        webservice String errorDesc {get;set;}
        webservice Id recordId {get;set;}
        webservice Boolean success {get;set;}
        
        global GuestProfileResponse(){
            outputAcct = new GuestAccount();
            success = false;
            caseNumber = null;
            errCode = null;
            errorDesc = null;
            recordId = null;
        }
    }
    
    global class GuestAccount {
        webservice String gigyaId {get;set;}
        webservice String customerTypeStatus {get;set;}
        webservice String firstName {get;set;}
        webservice String lastName {get;set;}
        webservice String email {get;set;}
        webservice String mobilePhone {get;set;}
        webservice String homeStreet {get;set;}
        webservice String homeSuburb {get;set;}
        webservice String homeState {get;set;}
        webservice String homePostCode {get;set;}
        webservice String postalStreet {get;set;}
        webservice String postalSuburb {get;set;}
        webservice String postalState {get;set;}
        webservice String postalPostCode {get;set;}
        webservice String interests {get;set;}
        webservice Boolean postalSameAsHome {get;set;}
        webservice Boolean doNotCall {get;set;}
        webservice Boolean doNotContact {get;set;}
        webservice Boolean productInfoOptOut {get;set;}
        webservice Boolean benefitEventsOptOut {get;set;}
        webservice String preferredDealer {get;set;}
        webservice String stripeCustomerId {get;set;}
        webservice String encoreTier {get;set;}
        
        global GuestAccount(){
            gigyaId = null;
            customerTypeStatus = null;
            firstName = null;
            lastName = null;
            email = null;
            mobilePhone = null;
            homeStreet = null;
            homeSuburb = null;
            homeState = null;
            homePostCode = null;
            postalStreet = null;
            postalSuburb = null;
            postalState = null;
            postalPostCode = null;
            interests = null;
            postalSameAsHome = false;
            doNotCall = false;
            doNotContact = false;
            productInfoOptOut = false;
            benefitEventsOptOut = false;
            preferredDealer = null;
            stripeCustomerId = null;
            encoreTier = null;
        }
    }
    
    global class GuestVehicleWSResponse {
        webservice Boolean success {get;set;}
        webservice Id recordId {get;set;}
        webservice String errCode {get;set;}
        webservice String errorDesc {get;set;}
        webservice String caseNumber {get;set;}
        webservice String guestEncoreStatus {get;set;}
        webservice Date guestEncoreExpiration {get;set;}
        webservice Date facWarrantyExpDate {get;set;}
        
        webservice List<GuestRelationshipVehicles> grVehiclesList {get;set;}
        
        
        global GuestVehicleWSResponse(){
            success = false;
            recordId = null;
            errCode = null;
            errorDesc = null;
            caseNumber = null;
            guestEncoreStatus = null;
            guestEncoreExpiration = null;
            grVehiclesList = new List<GuestRelationshipVehicles>();
        }
    }
    
    global class GuestRelationshipVehicles {
        webservice String grStatus {get;set;}
        webservice Boolean grActive {get;set;}
        webservice String grRego {get;set;}
        webservice String grVIN {get;set;}
        webservice String grModel {get;set;}
        webservice Date grfactWrntExpDate {get;set;}
        webservice Id grVO {get;set;}
        webservice String grCaseNumber {get;set;}
        
        global GuestRelationshipVehicles(){
            grStatus = null;
            grActive = false;
            grRego = null;
            grVIN = null;
            grfactWrntExpDate = null;
            grModel = null;
            grVO = null;
            grCaseNumber = null;
        }
    }
    
    //GR Hashed Lookup
     webservice static GRHashLookupResponse getHashedIdLookup(String sfHashedId, String sfAcctId, String sfVOId){
         return GuestRegistrationHandler.getHashedIdLookup(sfHashedId, sfAcctId, sfVOId);
     }
    
    // Guest Profile methods
    webservice static GuestProfileResponse getCustomerDetails(String gigyaId, Id sfAcctId, Id sfVOId){
        return GuestProfileHandler.getCustomerDetails(gigyaId, sfAcctId, sfVOId);
    }
    
    webservice static GuestProfileResponse writeCustomerDetails(GuestAccount ga){
        return GuestProfileHandler.writeCustomerDetails(ga);
    }
    
    webservice static GuestProfileResponse writeCommunicationPreferences(GuestAccount ga){
        return GuestProfileHandler.writeCommunicationPreferences(ga);
    }
    
    // Guest and Vehicle Registration, Verification and Association methods
    webservice static GRResponse guestRegistration(String firstName, String lastName, String email, String mobile, String gigyaId, Id sfAcctId, Id sfVOId){
        return GuestRegistrationHandler.guestRegistration(firstName, lastName, email, mobile, gigyaId, sfAcctId, sfVOId);
    }
    
    
    webservice static WSResponse vehicleVerification(Id sfAccountId, String vin, String batchNumber, Id sfVOId){
        return GuestRegistrationHandler.vehicleVerification(sfAccountId, vin, batchNumber, sfVOId);
    }
    
    
    webservice static WSResponse vehicleAssociation(Id sfAccountId, String vin){
        return GuestRegistrationHandler.vehicleAssociation(sfAccountId, vin);
    }
    
    
    webservice static WSResponse deleteRegistration(String gigyaId, String email){
        return GuestRegistrationHandler.deleteRegistration(gigyaId, email);
    }
    
    webservice static WSResponse createUpdateCase(String caseNumber, String callBackPhone, String timeSlot, Id sfAcctId){
        return GuestRegistrationHandler.createUpdateCase(caseNumber, callBackPhone, timeSlot, sfAcctId);
    }
    
    webservice static WSResponse updateTermsPolicyVersion(Id sfAcctId, String termsAndConditionsVersion, String privacyPolicyVersion){
        return GuestRegistrationHandler.updateTermsPolicyVersion(sfAcctId, termsAndConditionsVersion, privacyPolicyVersion);
    }
    
    webservice static WSResponse updateStripeCustomerId(Id sfAcctId, String stripeCustomerId){
        return GuestRegistrationHandler.updateStripeCustomerId(sfAcctId, stripeCustomerId);
    }
    
    
    // Vehicle Management methods
    webservice static GuestVehicleWSResponse getGuestVehicles(String gigyaId){
        return VehicleManagementHandler.getGuestVehicles(gigyaId);
    }
}