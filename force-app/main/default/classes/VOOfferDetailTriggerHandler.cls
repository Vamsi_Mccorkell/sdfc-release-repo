/*******************************************************************************
@author:        Donnie Banez
@date:          Aug 2019
@description:   Trigger Handler for VO Offer Detail Trigger
@Revision(s):    
@Test Methods:  VOOfferDetailTriggerHandlerTest
********************************************************************************/
public with sharing class VOOfferDetailTriggerHandler {
    public static TriggerAutomations__c automationEnabled  = TriggerAutomations__c.getInstance(); 
    public static Boolean triggerEnabled = automationEnabled.VO_Offer_Detail__c; 
    public static void mainEntry(Boolean isBefore, Boolean isAfter, Boolean isInsert, Boolean isUpdate,Boolean isDelete, Boolean isUnDelete, 
                    List<SObject> newList, List<SObject> oldList, Map<ID, SObject> newmap, Map<ID, SObject> oldmap)
    {
        if (Test.isrunningTest())  {
            triggerEnabled = true;
        }
        
        if (triggerEnabled){
            try{
                // INSERT TRIGGER
                if(isInsert){
                    // Before
                    if(isBefore){
                        validateUniqueVOOD(newList);
                    }            
                    // After
                    if(isAfter){
  						
                    }
                }
                // Update TRIGGER
                if(isUpdate){
                    // Before
                    if(isBefore){
                        
                    }            
                    // After
                    if(isAfter){
  						recalculateCounts((Map<Id, VO_Offer_Detail__c>) oldMap, newList);
                    }
                }
                // Delete TRIGGER
                if(isDelete){
                    // Before
                    if(isBefore){
                        preventVOODDeletion(oldList);
                    }            
                    // After
                    if(isAfter){
   
                    }
                }
                // UNDELETE Trigger
                if(isUnDelete){
                    if (isBefore){
                        
                    }
                    if (isAfter){
  
                    }
                }  
            } catch (Exception ex){
                String errorString = ex.getStackTraceString()+'--'+ ex.getTypeName()+'--'+ ex.getMessage();
                system.debug('### VO Offer Detail Trigger Handler Exception Thrown = ' + errorString);
                newList[0].addError(errorString);
            }
        }
        
    }
	
    public static void recalculateCounts(Map<Id, VO_Offer_Detail__c> oldMap, List<VO_Offer_Detail__c> newList){
        Set<Id> codIdSet = new Set<Id>();
        for (VO_Offer_Detail__c vood : newList){
            VO_Offer_Detail__c oldVOOD = null;
            if (oldMap != null){
                oldVOOD = oldMap.get(vood.Id);
            }
            if (oldVOOD.Adjusted_Credit_Offer_Limit__c != vood.Adjusted_Credit_Offer_Limit__c && vood.CustomerOfferDetail__c != null){
                codIdSet.add(vood.CustomerOfferDetail__c);
            }
        }
        
        if (codIdSet.size() > 0){
            GuestVoucherServicesUtilities.recalculateCODCounts(codIdSet);
        }
    }
    
    public static void validateUniqueVOOD(List<VO_Offer_Detail__c> newList){
        Set<Id> voIdSet = new Set<Id>();
        Set<Id> offerIdSet = new Set<Id>();
        for (VO_Offer_Detail__c vood : newList){
           voIdSet.add(vood.Vehicle_Ownership__c);
           offerIdSet.add(vood.Offer__c); 
        }
        List<VO_Offer_Detail__c> voodList = [select Id, Offer__c, Vehicle_Ownership__c
                                                  from VO_Offer_Detail__c
                                                  where Vehicle_Ownership__c in :voIdSet
                                                  and Offer__c in :offerIdSet];
		for (VO_Offer_Detail__c vood : newList){
            for (VO_Offer_Detail__c existingVOOD : voodList){
                if (vood.Vehicle_Ownership__c == existingVOOD.Vehicle_Ownership__c 
                    && vood.Offer__c == existingVOOD.Offer__c){ // if there is the same
                    vood.addError(Label.DuplicateVOODError);
                    break;
                }
            } 
        }
    }
    
    public static void preventVOODDeletion(List<VO_Offer_Detail__c> oldList){
        for (VO_Offer_Detail__c vood : oldList){
            if (!vood.Delete_Record__c){
                vood.addError(Label.VOODDeletionError);
            }
        }
    }
}