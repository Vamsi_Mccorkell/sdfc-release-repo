/*******************************************************************************
@author:        Donnie Banez
@date:          March 2019
@description:   Trigger Handler for Vehicle Ownership Trigger
@Revision(s):    
@Test Methods:  VehicleOwnershipTriggerHandlerTest
********************************************************************************/
public with sharing class VehicleOwnershipTriggerHandler {
    public static TriggerAutomations__c automationEnabled  = TriggerAutomations__c.getInstance(); 
    public static Boolean triggerEnabled = automationEnabled.Vehicle_Ownership__c;
    public static Boolean offerDetailsHasRun = false;
    public static Boolean recalcHasRun = false;
    public static Boolean copyOldToNewHasRun = false;
    public static void mainEntry(Boolean isBefore, Boolean isAfter, Boolean isInsert, Boolean isUpdate,Boolean isDelete, Boolean isUnDelete, 
                    List<SObject> newList, List<SObject> oldList, Map<ID, SObject> newmap, Map<ID, SObject> oldmap)
    {
        if (Test.isrunningTest())  {
            triggerEnabled = true;
        }
        if (triggerEnabled){
            try{
                // INSERT TRIGGER
                if(isInsert){
                    // Before
                    if(isBefore){
                        
                    }            
                    // After
                    if(isAfter){
                        createCODVINODandVOODRecords(null, newList);
                        getDerivedMagicLink(null, newList);
                    }
                }
                // Update TRIGGER
                if(isUpdate){
                    // Before
                    if(isBefore){
                        // this is in before update as we need to clear the verified guest id of the old
                        if (!copyOldToNewHasRun){
                        	copyOldVOtoNewVOOnDeactivation((Map<Id, Vehicle_Ownership__c>) oldMap, newList);// correct the new VO values and GR's first
                        }
                    }            
                    // After
                    if(isAfter){
                        if (!offerDetailsHasRun) {
                        	createCODVINODandVOODRecords((Map<Id, Vehicle_Ownership__c>) oldMap, newList);    
                        }
                        
                        if (!recalcHasRun) {
                            recalculateVOODCounts((Map<Id, Vehicle_Ownership__c>) oldMap, newList);
                        }
                        getDerivedMagicLink((Map<Id, Vehicle_Ownership__c>) oldMap, newList);
                    }
                }
                // Delete TRIGGER
                if(isDelete){
                    // Before
                    if(isBefore){
                        preventVODeletionwithMagicLink(oldList);
                    }            
                    // After
                    if(isAfter){
   
                    }
                }
                // UNDELETE Trigger
                if(isUnDelete){
                    if (isBefore){
                        
                    }
                    if (isAfter){
  
                    }
                }  
            } catch (Exception ex){
                String errorString = ex.getStackTraceString()+'--'+ ex.getTypeName()+'--'+ ex.getMessage();
                system.debug('### VO Trigger Handler Exception Thrown = ' + errorString);
                newList[0].addError(errorString);
            }
        }
        
    }
    
    public static void getDerivedMagicLink(Map<Id, Vehicle_Ownership__c> oldMap, List<Vehicle_Ownership__c> newList){
        //(Set<Id> voIdSet)
        Set<Id> voIdSet = new Set<Id>();
        for (Vehicle_Ownership__c vo : newList){
            Vehicle_Ownership__c oldVO = null;
            if (oldMap != null){
                oldVO = oldMap.get(vo.Id);
            }
            if (oldVO == null || oldVO.Valid_for_Offers__c != vo.Valid_for_Offers__c && vo.DerivedMagicLinkURL__c == null){ 
                if (vo.Valid_for_Offers__c){
                    voIdSet.add(vo.Id);
                }
            }
        }
        if (voIdSet.size() > 0){
            DerivedMagicLinkUtility.getDerivedMagicLinkUrls(voIdSet);  
        }
    }
    
    public static void createCODVINODandVOODRecords(Map<Id, Vehicle_Ownership__c> oldMap, List<Vehicle_Ownership__c> newList){
		offerDetailsHasRun = true;
        // Now get all accounts for this list of VO
        Set<Id> voIdSet = new Set<Id>();
        Set<Id> vinIdSet = new Set<Id>();
        for (Vehicle_Ownership__c vo : newList){
            Vehicle_Ownership__c oldVO = null;
            if (oldMap != null){
                oldVO = oldMap.get(vo.Id);
            }
            if (oldVO == null || oldVO.Valid_for_Offers__c != vo.Valid_for_Offers__c){ 
                if (vo.Valid_for_Offers__c){
                    voIdSet.add(vo.Id);
                    vinIdSet.add(vo.AssetID__c);
                }
            }
        }
        
        if (voIdSet.size() > 0){
            GuestVoucherServicesUtilities.createCODVINODVOODRecords(null, vinIdSet, voIdSet); // offerIdSet = null, vinIdSet, voIdSet  
        }
    }
    
    public static void recalculateVOODCounts(Map<Id, Vehicle_Ownership__c> oldMap, List<Vehicle_Ownership__c> newList){
        recalcHasRun = true;
        Id CUSTOMEROFFERRECORDTYPEID = Schema.SObjectType.VO_Offer_Detail__c.getRecordTypeInfosByDeveloperName().get('Customer_Vehicle_Offer').getRecordTypeId();
        Set<Id> voIdSet = new Set<Id>();
        for (Vehicle_Ownership__c vo : newList){
            Vehicle_Ownership__c oldVO = null;
            if (oldMap != null){
                oldVO = oldMap.get(vo.Id);
            }
            system.debug(LoggingLevel.ERROR, '## oldVO Status: ' + oldVO.Status__c);
            system.debug(LoggingLevel.ERROR, '## vo Status: ' + vo.Status__c);
            if (oldVO.Status__c != vo.Status__c || oldVO.Verified_Guest_ID__c != vo.Verified_Guest_ID__c || oldVO.Valid_for_Offers__c != vo.Valid_for_Offers__c){
                voIdSet.add(vo.Id);
            }
        }
        
        if (voIdSet.size() > 0){
            List<Vehicle_Ownership__c> voList = [select Id, Status__c,AssetID__r.Encore_Tier__c,Valid_for_Offers__c,
                                                (select Id, Credit_Offer_Limit__c, CustomerOfferDetail__c,
                                                 Offer__r.Customer_Platinum_Limit__c, Offer__r.Customer_Basic_Limit__c,
                                                 Customer_Credits_Available__c,
                                                 Customer_Credits_Consumed__c from VO_Offer_Details__r
                                                where RecordTypeId = :CUSTOMEROFFERRECORDTYPEID)
                                                from Vehicle_Ownership__c where Id in :voIdSet];
            
            Set<Id> codIdSet = new Set<Id>();
            List<VO_Offer_Detail__c> voodListUpdate = new List<VO_Offer_Detail__c>();
            for (Vehicle_Ownership__c vo : voList){
                for (VO_Offer_Detail__c vood : vo.VO_Offer_Details__r){
                    if (vo.Status__c == 'Active' && vo.Valid_for_Offers__c){
                        if (vo.AssetID__r.Encore_Tier__c == 'Platinum'){
                        	vood.Credit_Offer_Limit__c = vood.Offer__r.Customer_Platinum_Limit__c; 
                            
                        }
                        else if (vo.AssetID__r.Encore_Tier__c == 'Basic'){
                            vood.Credit_Offer_Limit__c = vood.Offer__r.Customer_Basic_Limit__c; 
                        }
                    }
                    codIdSet.add(vood.CustomerOfferDetail__c);
                    voodListUpdate.add(vood);
                }
            }
            
            if (voodListUpdate.size() > 0){
                update voodListUpdate;
            }
            if (codIdSet.size() > 0){
                 GuestVoucherServicesUtilities.recalculateCODCounts(codIdSet);
            }
        }
    }
    
    public static void preventVODeletionwithMagicLink(List<Vehicle_Ownership__c> oldList){
        /* PURPOSE: PRevent deletions of VO's if the Magic Link for that VO has been sent
         * */  
        for (Vehicle_Ownership__c vo : oldList){
            if (vo.Magic_Link_Sent__c){
                vo.addError(Label.VODeletionError);
            }
            if (vo.Verified_Guest_ID__c != null){
                vo.addError(Label.VOVerifiedGuestIdError);
            }
        }
    }
    
    public static void copyOldVOtoNewVOOnDeactivation(Map<Id, Vehicle_Ownership__c> oldMap, List<Vehicle_Ownership__c> newList){
        copyOldToNewHasRun = true;
        Set<Id> assetIdSet = new Set<Id>();
        for (Vehicle_Ownership__c vo : newList){
            Vehicle_Ownership__c oldVO = null;
            if (oldMap != null){
                oldVO = oldMap.get(vo.Id);
            }
            if (oldVo != null && oldVo.Status__c != vo.Status__c && vo.Status__c == 'Inactive'){ // if deactivated, get the set of asset id's
                assetIdSet.add(oldVO.AssetID__c);
            }
        }
        // now get a list of all the assets with Active VO's for the Asset ID's
        Map<Id, Vehicle_Ownership__c> assetIdLatestVOMap = new Map<Id,Vehicle_Ownership__c>();
        List<Asset__c> assetList = [select Id,
                                   (select Id,
                                    Customer__c,
                                    Magic_Link_Sent__c,
                                    MagicLink_Sent_Date__c,
                                    Old_VO_Magic_Link_URL__c,
                                    Verified_Guest_ID__c,
                                    Vehicle_Rego_Number__c,
                                    Vehicle_Type__c
                                    from Vehicle_Ownerships__r
                                   where Status__c = 'Active'
                                   order by CreatedDate desc)
                                   from Asset__c
                                   where Id in :assetIdSet];
        
        for (Asset__c a : assetList){
            
            if (a.Vehicle_Ownerships__r.size() > 0){
                assetIdLatestVOMap.put(a.Id, a.Vehicle_Ownerships__r[0]);
            }
        }
        Map<Id, Id> oldVOIdNewVOIdMap = new Map<Id,Id>(); // used for GR creation
        Set<Id> privateSaleVOIdSet = new Set<Id>();
        Set<Id> preOwnedVOIdSet = new Set<Id>();
        Set<Id> otherVTypeVOIdSet = new Set<Id>();
        Set<Id> newVOIdSet = new Set<Id>();
        Set<Id> orphanIdSet = new Set<Id>();
        // now for each of the deactivated VO's set the active new VO's
        List<Vehicle_Ownership__c> newVOUpdateList = new List<Vehicle_Ownership__c>();
        for (Vehicle_Ownership__c oldVO : newList){
            Vehicle_Ownership__c beforeUpdateVO = null;
            if (oldMap != null){
                beforeUpdateVO = oldMap.get(oldVO.Id);
            }
            if (beforeUpdateVO != null && beforeUpdateVO.Status__c != oldVO.Status__c && oldVO.Status__c == 'Inactive'){ 
                if (assetIdLatestVOMap.containsKey(oldVO.AssetID__c)){
                    Vehicle_Ownership__c newVO = assetIdLatestVOMap.get(oldVO.AssetID__c);
                    if (newVO.Id == oldVO.Id){ // orphan deactivation // just manual deactivation
                        orphanIdSet.add(oldVo.Id);
                    }
                    else { // if it is deactivated via insert of a new VO
                        if (newVO.Customer__c == oldVO.Customer__c){ //If the new one has the same Customer/Asset connection as the one that it replaces
                            if (newVO.Vehicle_Rego_Number__c == null || newVO.Vehicle_Rego_Number__c == ''){ // if new VO has no rego
                                newVO.Vehicle_Rego_Number__c = oldVO.Vehicle_Rego_Number__c;
                            }
                            if (oldVO.Magic_Link_Sent__c){ // if magic link has been sent on old vo
                                newVO.Magic_Link_Sent__c = true;
                                newVO.MagicLink_Sent_Date__c = oldVO.MagicLink_Sent_Date__c;
                                newVO.Old_VO_Magic_Link_URL__c = oldVO.Magic_Link_URL__c;
                            }
                            if (oldVO.Verified_Guest_ID__c != null && oldVO.Verified_Guest_ID__c != ''){
                                newVO.Verified_Guest_ID__c = oldVO.Verified_Guest_ID__c;
                                oldVO.Verified_Guest_ID__c = null;
                                oldVOIdNewVOIdMap.put(oldVO.Id, newVO.Id);
                            }
                            if (!newVOIdSet.contains(newVO.Id)){
                                newVOIdSet.add(newVO.Id);
                                newVOUpdateList.add(newVO);
                            }
                            
                        }
                        else { //If the new one, based on asset, is on a different customer (including one that might be a dupe to the old),
                            if (oldVO.Verified_Guest_ID__c != null && oldVO.Verified_Guest_ID__c != ''){
                                if (newVO.Vehicle_Type__c == 'Private Sale'){
                                    privateSaleVOIdSet.add(oldVO.Id);
                                }
                                else if (newVO.Vehicle_Type__c == 'Pre-Owned'){
                                    preOwnedVOIdSet.add(oldVO.Id);
                                }
                                else {
                                    otherVTypeVOIdSet.add(oldVO.Id);
                                }
                            }
                        }
                    }
                    
                    
                }
            }
        }
        // create GR's
        List<Vehicle_Ownership__c> oldVOList = [select Id,Customer__c,
                                               (select Id, Gigya_ID__c,
                                                GR_Status__c, GR_Type__c,
                                                Account__c, GR_Comment__c, Start_Date__c,
                                                End_Date__c, First_Name__c, Last_Name__c,
                                                Email__c, VIN_text__c, Account_ID__c,
                                                Guest_Provided_Mobile__c,
                                                VO_ID__c, Asset__c, VehicleOwnership__c from Guest_Relationships__r
                                               where GR_Status__c = 'Verified')
                                               from Vehicle_Ownership__c 
                                               where (Id in :oldVOIdNewVOIdMap.keySet()
                                                     or Id in :privateSaleVOIdSet
                                                     or Id in :preOwnedVOIdSet
                                                     or Id in :otherVTypeVOIdSet
                                                     or Id in :orphanIdSet)];
        
        List<Guest_Relationship__c> grUpsertList = new List<Guest_Relationship__c>();
        List<Case> caseCreateList = new List<Case>();
        for (Vehicle_Ownership__c vo : oldVOList){
            for (Guest_Relationship__c gr : vo.Guest_Relationships__r){
                // create a new GR for the new GR
                if (oldVOIdNewVOIdMap.containsKey(vo.Id)){
                    // update the old
                    Id newVOId = oldVOIdNewVOIdMap.get(vo.Id);
                	gr.End_Date__c = system.today();
                    gr.GR_Comment__c = (gr.GR_Comment__c != null) ? gr.GR_Comment__c + ' Deactivated and replaced by new GR for new VO: ' + newVOId : 'Deactivated and replaced by new GR for new VO: ' + newVOId;
                    grUpsertList.add(gr);
                    Guest_Relationship__c newGR = new Guest_Relationship__c();
                    newGR.VehicleOwnership__c = newVOId;
                    newGR.Account__c = gr.Account__c;
                    newGR.Gigya_ID__c = gr.Gigya_ID__c;
                    newGR.GR_Status__c = gr.GR_Status__c;
                    newGR.GR_Type__c = gr.GR_Type__c;
                    newGR.Start_Date__c = system.now();
                    newGR.First_Name__c = gr.First_Name__c;
                    newGR.Last_Name__c = gr.Last_Name__c;
                    newGR.Email__c = gr.Email__c;
                    newGR.VIN_text__c = gr.VIN_text__c;
                    newGR.Account_ID__c = gr.Account_ID__c;
                    newGR.Guest_Provided_Mobile__c = gr.Guest_Provided_Mobile__c;
                    newGR.VO_ID__c = newVOId;
                    newGR.Asset__c = gr.Asset__c;
                    newGR.GR_Comment__c = 'New GR created as a result of VO Deactivation for VO: ' + vo.Id + ' and GR: ' + gr.Id;
                    grUpsertList.add(newGR);
                }
                else if (orphanIdSet.contains(vo.Id)){
                    gr.End_Date__c = system.now();
                    gr.GR_Comment__c = (gr.GR_Comment__c != null) ? gr.GR_Comment__c + ' Manual Deactivation of VO' : 'Manual Deactivation of VO';
                    grUpsertList.add(gr);
                }
                else if (privateSaleVOIdSet.contains(vo.Id)){
                    gr.End_Date__c = system.now();
                    gr.GR_Comment__c = (gr.GR_Comment__c != null) ? gr.GR_Comment__c + ' Private Sale' : 'Private Sale';
                    grUpsertList.add(gr);
                }
                else if (preOwnedVOIdSet.contains(vo.Id)){
                    gr.End_Date__c = system.now();
                    gr.GR_Comment__c = (gr.GR_Comment__c != null) ? gr.GR_Comment__c + ' Pre-Owned Sale' : 'Pre-Owned Sale';
                    grUpsertList.add(gr);
                }
                else if (otherVTypeVOIdSet.contains(vo.Id)){
                    gr.GR_Status__c = 'Error';
                    gr.GR_Status__c = '510';
                    String errorDesc = 'VO deactivated with a new VO outside private sale or pre-owned sale. Please check';
                    gr.GR_Comment__c = (gr.GR_Comment__c != null) ? gr.GR_Comment__c + ' ' + errorDesc : errorDesc;
                    grUpsertList.add(gr);
                    // Raise a case
                    Id GENERALENQUIRYRTYPEID = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('General_Enquiry').getRecordTypeId();
                    Case newCase = new Case(RecordTypeId = GENERALENQUIRYRTYPEID);
                    newCase.Type = 'Data Verification';
                    newCase.Origin = 'On Demand';
                    newCase.Status = 'In Progress';
                    newCase.AccountId = vo.Customer__c;
                    newCase.Vehicle_Ownership__c = vo.Id;
                    newCase.Subject = 'VO Deactivation Issue';
                    String description = errorDesc;
                    description += ' | VO ID: ' + vo.Id;
                    description += ' | GR ID: ' + gr.Id;
                    newCase.Description = description;
                    newCase.GR_ID__c = gr.Id;
                    caseCreateList.add(newCase);
                    
                }
            }	    
        }
        
        if (newVOUpdateList.size() > 0){
            update newVOUpdateList;
        }
        
        if (grUpsertList.size() > 0){
            upsert grUpsertList;
        }
        
        if (caseCreateList.size() > 0){
            insert caseCreateList;
        }
        
        // update the GR's again for the case number 
        Set<Id> newCaseIdSet = new Set<Id>();
        for (Case c : caseCreateList){
            newCaseIdSet.add(c.Id);
        }
        List<Guest_Relationship__c> grUpdateList = new List<Guest_Relationship__c>();
        List<Case> caseList = [select Id from Case where Id in :newCaseIdSet and GR_ID__c != null];
        for (Case c : caseList){
            Guest_Relationship__c upGr = new Guest_Relationship__c();
            upGr.Id = c.GR_ID__c;
            upGR.Related_Case__c = c.Id;
            grUpdateList.add(upGR);
        }
        
        if (grUpdateList.size() > 0){
            update grUpdateList;
        }
    }
    
    public static void codeCompensation(){
        // DEV NOTES: Added this section to compensate for code coverage as we cannot create any more VO's on the fly for cloning as we are hitting SOQL limits
        // We need to fix this as soon as RDR is fixed
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
        system.assertEquals(1,1);
    }
}