@isTest
private class SP_TestCalculateServiceValue_Trigger
{
//	This test class excercises the SP_CalculateServiceValue trigger
/*
    static testMethod void myTestCalculateServiceValue_Trigger()
    {
		// Create some test data
		Product2 sProduct1 = new Product2();
		sProduct1.Name					= 'IS200';
		insert sProduct1;

		Asset__c sAsset1 = new Asset__c();
		sAsset1.Name					= 'ABC12345678';
		sAsset1.Vehicle_Model__c		= sProduct1.Id;
		insert sAsset1;

		Account sAccount = new Account();
		sAccount.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccount.LastName				= 'TestLast1';
		sAccount.PersonEmail			= 'test1@email.com';
		insert sAccount;

		Account sAccount2 = new Account();
		sAccount2.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccount2.LastName				= 'TestLast2';
		sAccount2.PersonEmail			= 'test2@email.com';
		insert sAccount2;

		// Now do some testing

		// Test case  1 - simple first service
		Service__c sService1 = new Service__c(); 
		sService1.Customer_Account__c	= sAccount.Id;
		sService1.Asset__c				= sAsset1.Id;
		sService1.Vehicle_Mileage__c	= 123000;
		sService1.Service_Date__c		= system.today().addYears(-3);
		insert sService1;
		
		Account acct1 = [
							select	Service_Value__c 
							from	Account
							where	Id = :sAccount.Id
						];

//		system.assertEquals(1367.03, acct1.Service_Value__c, 'Test case 1 failed');

		// Test case  2 - add a second service
		Service__c sService2 = new Service__c(); 
		sService2.Customer_Account__c	= sAccount.Id;
		sService2.Asset__c				= sAsset1.Id;
		sService2.Vehicle_Mileage__c	= 187000;
		sService2.Service_Date__c		= system.today().addYears(-2);
		insert sService2;
		
		Account acct2 = [
							select	Service_Value__c 
							from	Account
							where	Id = :sAccount.Id
						];

//		system.assertEquals(1650.40, acct2.Service_Value__c, 'Test case 2 failed');

		// Test case  3 - change second service to another Account
		sService2.Customer_Account__c	= sAccount2.Id;
		update sService2;
		
		Account acct3a = [
							select	Service_Value__c 
							from	Account
							where	Id = :sAccount.Id
						];

		Account acct3b = [
							select	Service_Value__c 
							from	Account
							where	Id = :sAccount2.Id
						];

//		system.assertEquals(1367.03, acct3a.Service_Value__c, 'Test case 3a failed');
//		system.assertEquals(283.37, acct3b.Service_Value__c, 'Test case 3b failed');

		// Test case  4 - add a third service
		Service__c sService4 = new Service__c(); 
		sService4.Customer_Account__c	= sAccount.Id;
		sService4.Asset__c				= sAsset1.Id;
		sService4.Vehicle_Mileage__c	= 217000;
		sService4.Service_Date__c		= date.newinstance(2010, 2, 17);
		insert sService4;
		
		Account acct4 = [
							select	Service_Value__c 
							from	Account
							where	Id = :sAccount.Id
						];

//		system.assertEquals(1367.03, acct4.Service_Value__c, 'Test case 4 failed');

		// Test case  5 - add another service
		Service__c sService5 = new Service__c(); 
		sService5.Customer_Account__c	= sAccount.Id;
		sService5.Asset__c				= sAsset1.Id;
		sService5.Vehicle_Mileage__c	= 123000;
		sService5.Service_Date__c		= date.newinstance(2011, 2, 17);
		insert sService5;
		
		Account acct5 = [
							select	Service_Value__c 
							from	Account
							where	Id = :sAccount.Id
						];

//		system.assertEquals(2734.06, acct5.Service_Value__c, 'Test case 5 failed');

		// Test case  6 - change last service to another Account
		sService5.Customer_Account__c	= sAccount2.Id;
		update sService5;
		
		Account acct6a = [
							select	Service_Value__c 
							from	Account
							where	Id = :sAccount.Id
						];

		Account acct6b = [
							select	Service_Value__c 
							from	Account
							where	Id = :sAccount2.Id
						];

//		system.assertEquals(1367.03, acct6a.Service_Value__c, 'Test case 6a failed');
//		system.assertEquals(1650.40, acct6b.Service_Value__c, 'Test case 6b failed');

    }
*/
}