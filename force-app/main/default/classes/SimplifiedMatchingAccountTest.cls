/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class SimplifiedMatchingAccountTest {

/*		Matching rules

		Action depends on the sequence/combination of field matches - same VIN, same email, same mobile, same last name
		and similar shipping address


					E	M
		T C			m	o	L	A
		e a		V	a	b	a	d
		s s		I	i	P	s	d
		t e		N	l	h	t	r	Action
		=========================================
		  1		Y	Y	Y	Y	Y	Customer match
		  2		Y	Y	Y	Y	N	Same customer - update address
		  3		Y	Y	Y	N	Y	Same customer - update name
		  4		Y	Y	Y	N	N	Raise Case
		  5		Y	Y	N	Y	Y	Same customer - update mobile
		  6		Y	Y	N	Y	N	Same customer - update address and mobile
		  7		Y	Y	N	N	Y	Same customer - update name and mobile
		  8		Y	Y	N	N	N	Raise Case
				....
		  9		Y	N	Y	Y	Y	Same customer - update email
		 10		Y	N	Y	Y	N	Same customer - update address and email
		 11		Y	N	Y	N	Y	Same customer - update name and email
		 12		Y	N	Y	N	N	Raise Case
		 13		Y	N	N	Y	Y	Customer match	<--- RULE CHANGE 02DEC10 was raise Case
		 14		Y	N	N	Y	N	Raise Case
		 15		Y	N	N	N	Y	Raise Case
		 16		Y	N	N	N	N	Different customer
				....
		 17		N	Y	Y	Y	Y	Customer match
		 18		N	Y	Y	Y	N	Same customer - update address
		 19		N	Y	Y	N	Y	Same customer - update name
		 20		N	Y	Y	N	N	Raise Case
		 21		N	Y	N	Y	Y	Same customer - update mobile
		 22		N	Y	N	Y	N	Same customer - update address and mobile
		 23		N	Y	N	N	Y	Raise Case
		 24		N	Y	N	N	N	Raise Case
				....
		 25		N	N	Y	Y	Y	Customer match
		 26		N	N	Y	Y	N	Same customer - update address and email
		 27		N	N	Y	N	Y	Raise Case
		 28		N	N	Y	N	N	Raise Case
		 29		N	N	N	Y	Y	No Match	<--- RULE CHANGE 02DEC10 was raise Case
		 30		N	N	N	Y	N	Different customer
		 31		N	N	N	N	Y	Different customer
		 32		N	N	N	N	N	Different customer

	*/
	@isTest
    static void TestRule01()   
    {
        // TO DO: implement unit test
        Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYYYYXY', 'XXXXXXXXXX', 'YYYYYYYYYY', 'xxxxx@yyyy.com', 
        															'0xxxxxxxxx', 'Sydney', '7896', '333 XXXXX St', true);
        											
        system.assertEquals(string.valueOf(acc.Id).substring(0, 15), matchId.substring(0, 15));
    }
    
	// VIN Match, Email Match, Phone Match, Last Name  Match Address Match
	@isTest
    static void TestRule02()   
    {
        // TO DO: implement unit test
        Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYYYYXY', 'XXXXXXXXXX', 'YYYYYYYYYY', 'xxxxx@yyyy.com', 
        															'0xxxxxxxxx', 'Sydney', '7896', '333 YYYYY St', true);
        											
        system.assertEquals(string.valueOf(acc.Id).substring(0, 15), matchId.substring(0, 15));
    }    
    
    @isTest
    static void TestRule03()   
    {
        // TO DO: implement unit test
        Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYYYYXY', 'XXXXXXXXXX', 'YYYYYXXXXX', 'xxxxx@yyyy.com', 
        															'0xxxxxxxxx', 'Sydney', '7896', '3 33 XXXXX St', true);
        											
        system.assertEquals(string.valueOf(acc.Id).substring(0, 15), matchId.substring(0, 15));
    } 
    
    @isTest
    static void TestRule04()   
    {
        // TO DO: implement unit test
       	Account acc = createAccount(true);
       
       	Vehicle_Ownership__c vo = createVO(acc.Id);
        

       	Test.startTest();
	   	string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYYYYXY', 'XXXXXXXXXX', 'YYYYYXXXXX', 'xxxxx@yyyy.com', 
	        															'0xxxxxxxxx', 'Sydney', '7896', '3 33 YYYYY St', true);
	  	Test.stopTest();
	        											
	   	system.assertEquals(null, matchId);
        
        list<Case> cas = [select Description from Case where AccountId = :acc.Id];
        system.assertEquals(1, cas.size());
        system.assertEquals(true, cas[0].Description.contains('Potential Matches: ' + acc.Id));
        system.assertEquals(true, cas[0].Description.contains('Matching Rule: YYYNN'));
    }      
    // Y	Y	N	Y	Y	Same customer
	@isTest
    static void TestRule05()   
    {
        // TO DO: implement unit test
        Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYYYYXY', 'XXXXXXXXXX', 'YYYYYYYYYY', 'xxxxx@yyyy.com', 
        															'0yyyyyyyyy', 'Sydney', '7896', '333 XXXXX St', true);
        											
        system.assertEquals(string.valueOf(acc.Id).substring(0, 15), matchId.substring(0, 15));
    }   
    
    // Y	Y	N	Y	N	Same customer
	@isTest
    static void TestRule06()   
    {
        // TO DO: implement unit test
        Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYYYYXY', 'XXXXXXXXXX', 'YYYYYYYYYY', 'xxxxx@yyyy.com', 
        															'0yyyyyyyyy', 'Sydney', '7896', '333 YYYYY St', true);
        											
        system.assertEquals(string.valueOf(acc.Id).substring(0, 15), matchId.substring(0, 15));
    }   
    
     // Y	Y	N	N	Y	Same customer
   	@isTest
    static void TestRule07()   
    {
        // TO DO: implement unit test
        Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYYYYXY', 'XXXXXXXXXX', 'YYYYYYXXXXXX', 'xxxxx@yyyy.com', 
        															'0yyyyyyyyy', 'Sydney', '7896', '333 XXXXX St', true);
        											
        system.assertEquals(string.valueOf(acc.Id).substring(0, 15), matchId.substring(0, 15));
    }
    
    // Y	Y	N	N	N	Same customer
   	@isTest
    static void TestRule08()   
    {
       	Account acc = createAccount(true);
       	Vehicle_Ownership__c vo = createVO(acc.Id);
        

      	Test.startTest();
	  	string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYYYYXY', 'XXXXXXXXXX', 'YYYYYXXXXX', 'xxxxx@yyyy.com', 
	        															'0yyyyyyyyy', 'Sydney', '7896', '3 33 YYYYY St', true);
	   	Test.stopTest();
	        											
	 	system.assertEquals(null, matchId);
        list<Case> cas = [select Description from Case where AccountId = :acc.Id];
        system.assertEquals(1, cas.size());
        system.assertEquals(true, cas[0].Description.contains('Potential Matches: ' + acc.Id));
        system.assertEquals(true, cas[0].Description.contains('Matching Rule: YYNNN'));
    }
    
 
 	// Y	N	Y	Y	Y	Same customer
 	@isTest
    static void TestRule09()   
    {
        // TO DO: implement unit test
        Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYYYYXY', 'XXXXXXXXXX', 'YYYYYYYYYY', 'yyyyy@yyyy.com', 
        															'0xxxxxxxxx', 'Sydney', '7896', '333 XXXXX St', true);
        											
        system.assertEquals(string.valueOf(acc.Id).substring(0, 15), matchId.substring(0, 15));
    }
    
	// Y	N	Y	Y	N	Same customer
	@isTest
    static void TestRule10()   
    {
        // TO DO: implement unit test
        Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYYYYXY', 'XXXXXXXXXX', 'YYYYYYYYYY', 'yyyyy@yyyy.com', 
        															'0xxxxxxxxx', 'Sydney', '7896', '333 YYYYY St', true);
        											
        system.assertEquals(string.valueOf(acc.Id).substring(0, 15), matchId.substring(0, 15));
    }   
    
    // Y	N	Y	N	Y	Same customer
    @isTest
    static void TestRule11()   
    {
        // TO DO: implement unit test
        Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYYYYXY', 'XXXXXXXXXX', 'YYYYYXXXXX', 'yyyyy@yyyy.com', 
        															'0xxxxxxxxx', 'Sydney', '7896', '3 33 XXXXX St', true);
        											
        system.assertEquals(string.valueOf(acc.Id).substring(0, 15), matchId.substring(0, 15));
    } 
    
    // Y	N	Y	N	N	Raise Case
    @isTest
    static void TestRule12()   
    {
        // TO DO: implement unit test
       	Account acc = createAccount(true);
       	Vehicle_Ownership__c vo = createVO(acc.Id);
        

     	Test.startTest();
	    string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYYYYXY', 'XXXXXXXXXX', 'YYYYYXXXXX', 'yyyyy@yyyy.com', 
	        														'0xxxxxxxxx', 'Sydney', '7896', '3 33 YYYYY St', true);
	    Test.stopTest();
	        											
	    system.assertEquals(null, matchId);
         
        list<Case> cas = [select Description from Case where AccountId = :acc.Id];
        system.assertEquals(1, cas.size());
        system.assertEquals(true, cas[0].Description.contains('Potential Matches: ' + acc.Id));
        system.assertEquals(true, cas[0].Description.contains('Matching Rule: YNYNN'));
    }      
    // Y	N	N	Y	Y	Same customer
	@isTest
    static void TestRule13()   
    {
        // TO DO: implement unit test
        Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYYYYXY', 'XXXXXXXXXX', 'YYYYYYYYYY', 'yyyyy@yyyy.com', 
        															'0yyyyyyyyy', 'Sydney', '7896', '333 XXXXX St', true);
        											
        system.assertEquals(string.valueOf(acc.Id).substring(0, 15), matchId.substring(0, 15));
    }   
    
    // Y	N	N	Y	N	Raise Case
	@isTest
    static void TestRule14()   
    {
       	Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        

        Test.startTest();
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYYYYXY', 'XXXXXXXXXX', 'YYYYYYYYYY', 'yyyyy@yyyy.com', 
        															'0yyyyyyyyy', 'Sydney', '7896', '333 YYYYY St', true);
	    Test.stopTest();
	        											
	  	system.assertEquals(null, matchId);
        
        list<Case> cas = [select Description from Case where AccountId = :acc.Id];
        system.assertEquals(1, cas.size());
        system.assertEquals(true, cas[0].Description.contains('Potential Matches: ' + acc.Id));
        system.assertEquals(true, cas[0].Description.contains('Matching Rule: YNNYN'));
    }   
    
     // Y	N	N	N	Y	Raise Case
   	@isTest
    static void TestRule15()   
    {
       	Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);       

        Test.startTest();
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYYYYXY', 'XXXXXXXXXX', 'YYYYYYXXXXXX', 'yyyyy@yyyy.com', 
        															'0yyyyyyyyy', 'Sydney', '7896', '333 XXXXX St', true);
	  	Test.stopTest();
	        											
	  	system.assertEquals(null, matchId);
        
        list<Case> cas = [select Description from Case where AccountId = :acc.Id];
        system.assertEquals(1, cas.size());
        system.assertEquals(true, cas[0].Description.contains('Potential Matches: ' + acc.Id));
        system.assertEquals(true, cas[0].Description.contains('Matching Rule: YNNNY'));
    }
    
    // Y	N	N	N	N	Same customer
   	@isTest
    static void TestRule16()  
    {
       	Account acc = createAccount(true);
       	Vehicle_Ownership__c vo = createVO(acc.Id);
        

      	Test.startTest();
	  	string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYYYYXY', 'XXXXXXXXXX', 'YYYYYXXXXX', 'yyyyy@yyyy.com', 
	        															'0yyyyyyyyy', 'Sydney', '7896', '3 33 YYYYY St', true);
	   	Test.stopTest();
	        											
	 	system.assertEquals(null, matchId);
        list<Case> cas = [select Description from Case where AccountId = :acc.Id];
        system.assertEquals(0, cas.size());
    } 
	//	N	Y	Y	Y	Y	Customer match
	@isTest
    static void TestRule17()   
    {
        // TO DO: implement unit test
        Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYKKKKKK', 'XXXXXXXXXX', 'YYYYYYYYYY', 'xxxxx@yyyy.com', 
        															'0xxxxxxxxx', 'Sydney', '7896', '333 XXXXX St', true);
        											
        system.assertEquals(string.valueOf(acc.Id).substring(0, 15), matchId.substring(0, 15));
    }
    
	// N	Y	Y	Y	N	Same customer
	@isTest
    static void TestRule18()   
    {
        // TO DO: implement unit test
        Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYKKKKKK', 'XXXXXXXXXX', 'YYYYYYYYYY', 'xxxxx@yyyy.com', 
        															'0xxxxxxxxx', 'Sydney', '7896', '333 YYYYY St', true);
        											
        system.assertEquals(string.valueOf(acc.Id).substring(0, 15), matchId.substring(0, 15));
    }    
    
    // N	Y	Y	N	Y	Same customer
    @isTest
    static void TestRule19()   
    {
        // TO DO: implement unit test
        Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYKKKKKK', 'XXXXXXXXXX', 'YYYYYXXXXX', 'xxxxx@yyyy.com', 
        															'0xxxxxxxxx', 'Sydney', '7896', '3 33 XXXXX St', true);
        											
        system.assertEquals(string.valueOf(acc.Id).substring(0, 15), matchId.substring(0, 15));
    } 
    
    // N	Y	Y	N	N	Raise Case
    @isTest
    static void TestRule20()   
    {
        // TO DO: implement unit test
       	Account acc = createAccount(true);
      	Vehicle_Ownership__c vo = createVO(acc.Id);

        Test.startTest();
	  	string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYKKKKKK', 'XXXXXXXXXX', 'YYYYYXXXXX', 'xxxxx@yyyy.com', 
	        														'0xxxxxxxxx', 'Sydney', '7896', '3 33 YYYYY St', true);
	    Test.stopTest();
	        											
	   	system.assertEquals(null, matchId);
        
        list<Case> cas = [select Description from Case where AccountId = :acc.Id];
        system.assertEquals(1, cas.size());
        system.assertEquals(true, cas[0].Description.contains('Potential Matches: ' + acc.Id));
        system.assertEquals(true, cas[0].Description.contains('Matching Rule: NYYNN'));
    }      
    // N	Y	N	Y	Y	Same customer
	@isTest
    static void TestRule21()   
    {
        // TO DO: implement unit test
        Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYKKKKKK', 'XXXXXXXXXX', 'YYYYYYYYYY', 'xxxxx@yyyy.com', 
        															'0yyyyyyyyy', 'Sydney', '7896', '333 XXXXX St', true);
        											
        system.assertEquals(string.valueOf(acc.Id).substring(0, 15), matchId.substring(0, 15));
    }   
    
    // N	Y	N	Y	N	Same customer
	@isTest
    static void TestRule22()   
    {
        // TO DO: implement unit test
       	Account acc = createAccount(true);
      	Vehicle_Ownership__c vo = createVO(acc.Id);

        Test.startTest();
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYKKKKKK', 'XXXXXXXXXX', 'YYYYYYYYYY', 'xxxxx@yyyy.com', 
        															'0yyyyyyyyy', 'Sydney', '7896', '333 YYYYY St', true);
        											
        system.assertEquals(string.valueOf(acc.Id).substring(0, 15), matchId.substring(0, 15));
    }   
    
     // N	Y	N	N	Y	Raise Case
   	@isTest
    static void TestRule23()   
    {
        // TO DO: implement unit test
       	Account acc = createAccount(true);
      	Vehicle_Ownership__c vo = createVO(acc.Id);

        Test.startTest();
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYKKKKKK', 'XXXXXXXXXX', 'YYYYYYXXXXXX', 'xxxxx@yyyy.com', 
        															'0yyyyyyyyy', 'Sydney', '7896', '333 XXXXX St', true);
  	    Test.stopTest();
	        											
	   	system.assertEquals(null, matchId);
        
        list<Case> cas = [select Description from Case where AccountId = :acc.Id];
        system.assertEquals(1, cas.size());
        system.assertEquals(true, cas[0].Description.contains('Potential Matches: ' + acc.Id));
        system.assertEquals(true, cas[0].Description.contains('Matching Rule: NYNNY'));
    }
    
    // N	Y	N	N	N	Raise Case
   	@isTest
    static void TestRule24()   
    {
       	Account acc = createAccount(true);
       	Vehicle_Ownership__c vo = createVO(acc.Id);
        

      	Test.startTest();
	  	string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYKKKKKK', 'XXXXXXXXXX', 'YYYYYXXXXX', 'xxxxx@yyyy.com', 
	        															'0yyyyyyyyy', 'Sydney', '7896', '3 33 YYYYY St', true);
	   	Test.stopTest();
	        											
	 	system.assertEquals(null, matchId);
        list<Case> cas = [select Description from Case where AccountId = :acc.Id];
        system.assertEquals(1, cas.size());
        system.assertEquals(true, cas[0].Description.contains('Potential Matches: ' + acc.Id));
        system.assertEquals(true, cas[0].Description.contains('Matching Rule: NYNNN'));
    }
    
 
 	// N	N	Y	Y	Y	Customer match
 	@isTest
    static void TestRule25()   
    {
        // TO DO: implement unit test
        Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYKKKKKK', 'XXXXXXXXXX', 'YYYYYYYYYY', 'yyyyy@yyyy.com', 
        															'0xxxxxxxxx', 'Sydney', '7896', '333 XXXXX St', true);
        											
        system.assertEquals(string.valueOf(acc.Id).substring(0, 15), matchId.substring(0, 15));
    }
    
	// N	N	Y	Y	N	Same customer
	@isTest
    static void TestRule26()   
    {
        // TO DO: implement unit test
        Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYKKKKKK', 'XXXXXXXXXX', 'YYYYYYYYYY', 'yyyyy@yyyy.com', 
        															'0xxxxxxxxx', 'Sydney', '7896', '333 YYYYY St', true);
        											
        system.assertEquals(string.valueOf(acc.Id).substring(0, 15), matchId.substring(0, 15));
    }   
    
    // N	N	Y	N	Y	Raise Case
    @isTest
    static void TestRule27()   
    {
        // TO DO: implement unit test
       	Account acc = createAccount(true);
       	Vehicle_Ownership__c vo = createVO(acc.Id);
        

      	Test.startTest();
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYKKKKKK', 'XXXXXXXXXX', 'YYYYYXXXXX', 'yyyyy@yyyy.com', 
        															'0xxxxxxxxx', 'Sydney', '7896', '3 33 XXXXX St', true);
	   	Test.stopTest();
	        											
	 	system.assertEquals(null, matchId);
        list<Case> cas = [select Description from Case where AccountId = :acc.Id];
        system.assertEquals(1, cas.size());
        system.assertEquals(true, cas[0].Description.contains('Potential Matches: ' + acc.Id));
        system.assertEquals(true, cas[0].Description.contains('Matching Rule: NNYNY'));
    } 
    
    // N	N	Y	N	N	Raise Case
    @isTest
    static void TestRule28()   
    {
        // TO DO: implement unit test
       	Account acc = createAccount(true);
       	Vehicle_Ownership__c vo = createVO(acc.Id);
        

     	Test.startTest();
	    string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYKKKKKK', 'XXXXXXXXXX', 'YYYYYXXXXX', 'yyyyy@yyyy.com', 
	        														'0xxxxxxxxx', 'Sydney', '7896', '3 33 YYYYY St', true);
	    Test.stopTest();
	        											
	    system.assertEquals(null, matchId);
         
        list<Case> cas = [select Description from Case where AccountId = :acc.Id];
        system.assertEquals(1, cas.size());
        system.assertEquals(true, cas[0].Description.contains('Potential Matches: ' + acc.Id));
        system.assertEquals(true, cas[0].Description.contains('Matching Rule: NNYNN'));
    }  
        
    // N	N	N	Y	Y	Match, if First Name and street number matches
	@isTest
    static void TestRule29A()   
    {
        // TO DO: implement unit test
        Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYKKKKKK', 'XXXXXXXXXX', 'YYYYYYYYYY', 'yyyyy@yyyy.com', 
        															'0xxxxxxxxx', 'Sydney', '7896', '333 XXXXX St', true);
        											
        system.assertEquals(string.valueOf(acc.Id).substring(0, 15), matchId.substring(0, 15));
    }  
    
    // N	N	N	Y	Y	Mismatch, if First Name does not match
	@isTest
    static void TestRule29B()   
    {
        // TO DO: implement unit test
        Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYKKKKKK', 'XXXXXXYYYY', 'YYYYYYYYYY', 'yyyyy@yyyy.com', 
        															'0yyyyyyyyy', 'Sydney', '7896', '333 XXXXX St', true);
        											
        system.assertEquals(null, matchId);
    }
    
     // N	N	N	Y	Y	Mismatch, if Street does not match
	@isTest
    static void TestRule29C()   
    {
        // TO DO: implement unit test
        Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        
        
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYKKKKKK', 'XXXXXXXXXX', 'YYYYYYYYYY', 'yyyyy@yyyy.com', 
        															'0yyyyyyyyy', 'Sydney', '7896', '33A XXXXX St', true);
        											
        system.assertEquals(null, matchId);
    }       
        
    
    // N	N	N	Y	N	Different customer
	@isTest
    static void TestRule30()   
    {
       	Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);
        

        Test.startTest();
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYKKKKKK', 'XXXXXXXXXX', 'YYYYYYYYYY', 'yyyyy@yyyy.com', 
        															'0yyyyyyyyy', 'Sydney', '7896', '333 YYYYY St', true);
	    Test.stopTest();
	        											
	  	system.assertEquals(null, matchId);
        
        list<Case> cas = [select Description from Case where AccountId = :acc.Id];
        system.assertEquals(0, cas.size());
    }   
    
     // N	N	N	N	Y	Different customer
   	@isTest
    static void TestRule31()    
    {
       	Account acc = createAccount(true);
        Vehicle_Ownership__c vo = createVO(acc.Id);       

        Test.startTest();
        string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYKKKKKK', 'XXXXXXXXXX', 'YYYYYYXXXXXX', 'yyyyy@yyyy.com', 
        															'0yyyyyyyyy', 'Sydney', '7896', '333 XXXXX St', true);
	  	Test.stopTest();
	        											
	  	system.assertEquals(null, matchId);
        
        list<Case> cas = [select Description from Case where AccountId = :acc.Id];
        system.assertEquals(0, cas.size());
    }
    
    // N	N	N	N	N	Different customer
   	@isTest
    static void TestRule32()  
    {
       	Account acc = createAccount(true);
       	Vehicle_Ownership__c vo = createVO(acc.Id);

      	Test.startTest();
	  	string matchId = MatchingAccount_WS.MatchingAccountCreateCase('XXXXXYYKKKKKK', 'XXXXXXXXXX', 'YYYYYXXXXX', 'yyyyy@yyyy.com', 
	        															'0yyyyyyyyy', 'Sydney', '7896', '3 33 YYYYY St', true);
	   	Test.stopTest();
	        											
	 	system.assertEquals(null, matchId);
        list<Case> cas = [select Description from Case where AccountId = :acc.Id];
        system.assertEquals(0, cas.size());
    }
        
    private static Account createAccount(boolean isOwner)
    {
 		Account acct = new Account();
		acct.RecordTypeId			= (isOwner ? 	SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType() : 
													SPCacheRecordTypeMetadata.getPersonAccountProspectRecordType());
													
		acct.FirstName				= 'XXXXXXXXXX';
		acct.LastName				= 'YYYYYYYYYY';
		acct.PersonEmail			= 'xxxxx@yyyy.com';
		acct.PersonMobilePhone		= '0xxxxxxxxx';
		acct.ShippingCity			= 'Sydney';
		acct.ShippingStreet			= '333 XXXXX St'; 
		acct.ShippingPostalCode		= '7896';

		insert acct;  
		
		return acct;	
    }
    
    private static Vehicle_Ownership__c createVO(ID ownerId)
    {
    			// Create some test data
		Asset__c asst = new Asset__c	(
											Name = 'XXXXXYYYYYXY'
										);
										
		insert asst;
		
		Vehicle_Ownership__c vo = new Vehicle_Ownership__c	(
																Customer__c	= ownerId,
																AssetID__c = asst.Id
															);
		insert vo;	
		
		return vo;	
    }
}