global with sharing class SP_DeleteAllAFVBatch implements Database.Batchable<sObject>
{
	public class SP_DeleteAllAFVBatchException extends Exception{}

	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		string	strQuery;

		if (!test.isRunningTest())
		{	
			strQuery =	
				' select	Id'	+
				' from 		Address_For_Validation__c';
		}
		else
		{
			strQuery =		
				' select	Id'	+
				' from 		Address_For_Validation__c' +
				' limit		1';
		}

		return Database.getQueryLocator(strQuery);
	}


	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		list<id> theProcessList = new list<id>();

		for(sobject s : scope)
		{
			theProcessList.add((id)s.get('Id'));
		}

		SP_DeleteAllAFVProcessing.SP_Args oArgs = new SP_DeleteAllAFVProcessing.SP_Args(theProcessList);

		SP_DeleteAllAFVProcessing processor = new SP_DeleteAllAFVProcessing(oArgs);
		SP_DeleteAllAFVProcessing.SP_Ret oRet = processor.ProcMain();
	}


	global void finish(Database.BatchableContext BC)
	{
		system.debug('Finished');
	}

	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static testMethod void testBatch() 
	{
		// Set up some database records
        Address_For_Validation__c sAFV;

        sAFV			= new Address_For_Validation__c();

        insert sAFV;

		SP_DeleteAllAFVBatch oDeleteAllAFVBatch = new SP_DeleteAllAFVBatch();
		oDeleteAllAFVBatch.start(null);
		oDeleteAllAFVBatch.execute(null, new list<SObject>{sAFV});
		oDeleteAllAFVBatch.finish(null);
	}
}