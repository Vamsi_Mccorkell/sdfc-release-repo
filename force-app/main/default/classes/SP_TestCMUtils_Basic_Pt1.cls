@isTest
private class SP_TestCMUtils_Basic_Pt1
{
//	This test class excercises cases 1 - 10 of the Customer Matching Rules
//	using the Basic call to the utility

    static testMethod void myTestCustomerMatchingUtils()
    {
		/*String strlastName = '';
		String strEmail = ''; 
		String strMobile = ''; 
		String strCity = ''; 
		String strPostcode = ''; 
		String strStreet = '';

		// Create some test data
		Asset__c asst = new Asset__c	(
											Name = 'ABC12345678'
										);
		insert asst;

		Account acct1 = new Account();
		acct1.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		acct1.LastName				= 'TestLast1';
		acct1.PersonEmail			= 'test1@email.com';
		acct1.PersonMobilePhone		= '194837465';
		acct1.ShippingCity			= 'Sydney';
		acct1.ShippingStreet		= 'George';
		acct1.ShippingPostalCode	= '2001';
		acct1.BillingCity			= 'Melbourne';
		acct1.BillingStreet			= 'Collins';
		acct1.BillingPostalCode		= '3001';

		insert acct1;

		Vehicle_Ownership__c vo = new Vehicle_Ownership__c	(
																Customer__c	= acct1.Id,
																AssetID__c = asst.Id
															);
		insert vo;*/

/*		Matching rules

		Action depends on the sequence/combination of field matches - same VIN, same email, same mobile, same last name
		and similar shipping address

					E	M
		T C			m	o	L	A
		e a		V	a	b	a	d
		s s		I	i	P	s	d
		t e		N	l	h	t	r	Action
		=========================================
		  1		Y	Y	Y	Y	Y	Customer match
		  2		Y	Y	Y	Y	N	Same customer - update address
		  3		Y	Y	Y	N	Y	Same customer - update name
		  4		Y	Y	Y	N	N	Raise Case
		  5		Y	Y	N	Y	Y	Same customer - update mobile
		  6		Y	Y	N	Y	N	Same customer - update address and mobile
		  7		Y	Y	N	N	Y	Same customer - update name and mobile
		  8		Y	Y	N	N	N	Raise Case
				....
		  9		Y	N	Y	Y	Y	Same customer - update email
		 10		Y	N	Y	Y	N	Same customer - update address and email
		 11		Y	N	Y	N	Y	Same customer - update name and email
		 12		Y	N	Y	N	N	Raise Case
		 13		Y	N	N	Y	Y	Raise Case
		 14		Y	N	N	Y	N	Raise Case
		 15		Y	N	N	N	Y	Raise Case
		 16		Y	N	N	N	N	Different customer
				....
		 17		N	Y	Y	Y	Y	Customer match
		 18		N	Y	Y	Y	N	Same customer - update address
		 19		N	Y	Y	N	Y	Same customer - update name
		 20		N	Y	Y	N	N	Raise Case
		 21		N	Y	N	Y	Y	Same customer - update mobile
		 22		N	Y	N	Y	N	Same customer - update address and mobile
		 23		N	Y	N	N	Y	Raise Case
		 24		N	Y	N	N	N	Raise Case
				....
		 25		N	N	Y	Y	Y	Customer match
		 26		N	N	Y	Y	N	Same customer - update address and email
		 27		N	N	Y	N	Y	Raise Case
		 28		N	N	Y	N	N	Raise Case
		 29		N	N	N	Y	Y	Raise Case
		 30		N	N	N	Y	N	Different customer
		 31		N	N	N	N	Y	Different customer
		 32		N	N	N	N	N	Different customer

*/

		/*// Test case  1 - exact match
		string strResult1 = MatchingAccount_WS.MatchingAccount(
			'ABC12345678', 'DummyFirstName',							// Matching VIN
			'TestLast1', 'test1@email.com', '194837465', 'Sydney', '2001', 'George');

		// Exact match
		system.debug('*** strResult1 ***' + strResult1);
		system.assertEquals(acct1.Id, strResult1, 'Test case 1 failed');

		// Test case  2
		string strResult2 = MatchingAccount_WS.MatchingAccount(
			'ABC12345678', 'DummyFirstName',							// Matching VIN
			'TestLast1', 'test1@email.com', '194837465', 'Hobart', '', '');

		// Check that address has been marked for change
		system.debug('*** strResult2 ***' + strResult2);
		system.assertEquals(acct1.Id, strResult2, 'Test case 2 failed');

		// Test case  3
		string strResult3 = MatchingAccount_WS.MatchingAccount(
			'ABC12345678', 'DummyFirstName',							// Matching VIN
			'TestLastDifferent1', 'test1@email.com', '194837465', 'Melbourne', '3001', 'Collins');

		// Check that last name has been marked for change
		system.debug('*** strResult3 ***' + strResult3);
		system.assertEquals(acct1.Id, strResult3, 'Test case 3 failed');

		// Test case  4
		string strResult4 = MatchingAccount_WS.MatchingAccount(
			'ABC12345678', 'DummyFirstName',							// Matching VIN
			'TestLastDifferent1', 'test1@email.com', '194837465', 'Hobart', '', '');

		// Check that null is returned
		system.debug('*** strResult4 ***' + strResult4);
		system.assertEquals(null, strResult4, 'Test case 4 failed');

		// Test case  5
		string strResult5 = MatchingAccount_WS.MatchingAccount(
			'ABC12345678', 'DummyFirstName',							// Matching VIN
			'TestLast1', 'test1@email.com', '222222222', 'Melbourne', '3001', 'Collins');

		// Check that mobile has been marked for change
		system.debug('*** strResult5 ***' + strResult5);
		system.assertEquals(acct1.Id, strResult5, 'Test case 5 failed');

		// Test case  6
		string strResult6 = MatchingAccount_WS.MatchingAccount(
			'ABC12345678', 'DummyFirstName',							// Matching VIN
			'TestLast1', 'test1@email.com', '222222222', 'Hobart', '', '');

		// Check that address and mobile have been marked for change
		system.debug('*** strResult6 ***' + strResult6);
		system.assertEquals(acct1.Id, strResult6, 'Test case 6 failed');

		// Test case  7
		string strResult7 = MatchingAccount_WS.MatchingAccount(
			'ABC12345678', 'DummyFirstName',							// Matching VIN
			'TestLastDifferent1', 'test1@email.com', '222222222', 'Sydney', '2001', 'George');

		// Check that last name and mobile have been marked for change
		system.debug('*** strResult7 ***' + strResult7);
		system.assertEquals(acct1.Id, strResult7, 'Test case 7 failed');

		// Test case  8
		string strResult8 = MatchingAccount_WS.MatchingAccount(
			'ABC12345678', 'DummyFirstName',							// Matching VIN
			'TestLastDifferent1', 'test1@email.com', '222222222', 'Hobart', '', '');

		// Check that null is returned
		system.debug('*** strResult8 ***' + strResult8);
		system.assertEquals(null, strResult8, 'Test case 8 failed');*/
/*
		// Test case  9
		string strResult9 = MatchingAccount_WS.MatchingAccount(
			'ABC12345678', 'DummyFirstName',							// Matching VIN
			'TestLast1', 'test1@different.com', '194837465', 'Melbourne', '3001', 'Collins');

		// Check that email has been marked for change
		system.debug('*** strResult9 ***' + strResult9);
		system.assertEquals(acct1.Id, strResult9, 'Test case 9 failed');

		// Test case 10
		string strResult10 = MatchingAccount_WS.MatchingAccount(
			'ABC12345678', 'DummyFirstName',							// Matching VIN
			'TestLast1', 'test1@different.com', '194837465', 'Hobart', '', '');

		// Check that email and address have been marked for change
		system.debug('*** strResult10 ***' + strResult10);
		system.assertEquals(acct1.Id, strResult10, 'Test case 10 failed');
*/
    }

}