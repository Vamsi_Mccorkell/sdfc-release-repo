// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for creating accounts need to view the custom settings data
@isTest (SeeAllData=true)
private class VoucherTriggerHandler_Test {
    static Account partnerAcctVehicle;
    static Account partnerAcctCustomer;
    static Account customer;
    static VO_Offer_Detail__c vood;
    static VO_Offer_Detail__c custvood;
    static Offer__c customerOffer;
    static Offer__c vehicleOffer;
    static Customer_Offer_Detail__c cood;
    static Customer_Offer_Transaction__c ot;
    static Vehicle_Ownership__c validForOfferVO;
    
    static testmethod void testcalculateCOODCreditsRemaining(){
        dataSetupCOD();
        Id CUSTOMERVOUCHERRECORDTYPEID = Schema.SObjectType.Voucher__c.getRecordTypeInfosByDeveloperName().get('Customer_Voucher').getRecordTypeId();
        Id CUSTOMERVEHICLEOFFERRECORDTYPEID = Schema.SObjectType.VO_Offer_Detail__c.getRecordTypeInfosByDeveloperName().get('Customer_Vehicle_Offer').getRecordTypeId();
        // get Credits available on COOD
        cood = [select Id, Total_Credits_Available__c from Customer_Offer_Detail__c where Id = :cood.Id limit 1];
        Decimal creditsAvailable = cood.Total_Credits_Available__c;
        
        
        cood = [select Id, Total_Credits_Available__c, First_Credit_Expiration__c from Customer_Offer_Detail__c where Id = :cood.Id limit 1];
        // should have been reduced by 1
        //system.assertEquals(creditsAvailable - 1, cood.Total_Credits_Available__c);
        validForOfferVO = [select Id, Valet_Voucher_Expiry_Date__c from Vehicle_Ownership__c where Id = :validForOfferVO.Id limit 1];
        //system.assertEquals(validForOfferVO.Valet_Voucher_Expiry_Date__c, cood.First_Credit_Expiration__c);
    }
    
    static testmethod void testEvaluatePurchasePrice(){
        dataSetupVOOD();
        Id VOVOUCHERRECORDTYPEID = Schema.SObjectType.Voucher__c.getRecordTypeInfosByDeveloperName().get('VO_Voucher').getRecordTypeId();
        // test Full Value Voucher
        Branch_Offer_Detail__c bod = new Branch_Offer_Detail__c();
        bod.Partner_Branch__c = partnerAcctVehicle.Id;
        bod.Voucher_Price_Type__c = 'Full Value';
        bod.Voucher_Price__c = 10;
        bod.Offer__c = vehicleOffer.Id;
        insert bod;
        
        Voucher__c fullValueVoucher = new Voucher__c(RecordTypeId = VOVOUCHERRECORDTYPEID);
        fullValueVoucher.Partner_Branch__c = partnerAcctVehicle.Id;
        fullValueVoucher.Voucher_Type__c = 'Valet Service';
        fullValueVoucher.VO_Offer_Details__c = vood.Id;
        insert fullValueVoucher;
        
        bod.Voucher_Price_Type__c = 'Fixed Discount';
        bod.Voucher_Price__c = 20;
        update bod;
        Voucher__c fixedDiscVoucher = new Voucher__c(RecordTypeId = VOVOUCHERRECORDTYPEID);
        fixedDiscVoucher.Partner_Branch__c = partnerAcctVehicle.Id;
        fixedDiscVoucher.Voucher_Type__c = 'Valet Service';
        fixedDiscVoucher.VO_Offer_Details__c = vood.Id;
        fixedDiscVoucher.Service_Price__c = 50;
        insert fixedDiscVoucher;
        //resultVoucher = [select Id, Voucher_Price__c from Voucher__c where Id = :fixedDiscVoucher.Id limit 1];
        //system.assertEquals(30, resultVoucher.Voucher_Price__c); // Service price less voucher price fixed discount
        // test Percent Discount
        bod.Voucher_Price_Type__c = 'Percent Discount';
        bod.Voucher_Price__c = null;
        bod.Voucher_Percent_Discount__c = 50;
        update bod;
        
        Voucher__c percentDiscVoucher = new Voucher__c(RecordTypeId = VOVOUCHERRECORDTYPEID);
        percentDiscVoucher.Partner_Branch__c = partnerAcctVehicle.Id;
        percentDiscVoucher.Voucher_Type__c = 'Valet Service';
        percentDiscVoucher.VO_Offer_Details__c = vood.Id;
        percentDiscVoucher.Service_Price__c = 90;
        insert percentDiscVoucher;
        
        
        
    }
    
    static void dataSetupCOD(){
        List<Account> newAcctList = new List<Account>();
        List<Offer__c> offerList = new List<Offer__c>();
        List<Asset__c> assetList = new List<Asset__c>();
        
        Id PARTNERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner').getRecordTypeId();
        Id CUSTOMERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        partnerAcctVehicle = new Account(Name = 'Partner Vehicle', RecordTypeId = PARTNERACCTRECORDTYPEID, Partner_Class__c = 'Vehicle');
        Account partnerCustomer = new Account(Name = 'Partner Customer', RecordTypeId = PARTNERACCTRECORDTYPEID, Partner_Class__c = 'Customer');
        newAcctList.add(partnerAcctVehicle);
        newAcctList.add(partnerCustomer);
        insert newAcctList;
        validForOfferVO = [select Id, Customer__c, AssetID__c from Vehicle_Ownership__c 
                          where Status__c = 'Active' and Valid_for_Offers__c = false limit 1]; // try to get 1 that isn't valid for offer yet
        Asset__c asst = new Asset__c(Id = validForOfferVO.AssetID__c);
        asst.Original_Sales_Date__c = system.today();
        asst.Encore_Tier__c = 'Platinum';
        update asst;    
        customer = [select Id, Guest_Gigya_ID__c from Account where Id = :validForOfferVO.Customer__c limit 1];
        customer.Guest_Gigya_ID__c = 'TESTGIGYA';
        update customer;
        customerOffer = new Offer__c(Status__c = 'Inactive');
        customerOffer.Offer_Class__c = 'Customer';
        customerOffer.Offer_Type__c = 'Car Hire';
        customerOffer.Customer_Platinum_Limit__c = 10;
        customerOffer.Partner_Account__c = partnerCustomer.Id;
        insert customerOffer;
        customerOffer.Status__c = 'Active';
        update customerOffer;
        
        cood = [select Id from Customer_Offer_Detail__c where Offer__c = :customerOffer.Id and Customer__c = :customer.Id limit 1];
        ot = new Customer_Offer_Transaction__c();
        
        ot.CustomerOffer__c = cood.Id;
        ot.Offer__c = customerOffer.Id;
        ot.Redemption_Credits__c = 1;
        ot.Redemption_Type__c = 'Car Hire';
        insert ot;
        
    }
    
    static void dataSetupVOOD(){
        List<Account> newAcctList = new List<Account>();
        List<Offer__c> offerList = new List<Offer__c>();
        List<Asset__c> assetList = new List<Asset__c>();
        
        Id PARTNERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner').getRecordTypeId();
        Id CUSTOMERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        partnerAcctVehicle = new Account(Name = 'Partner Vehicle', RecordTypeId = PARTNERACCTRECORDTYPEID, Partner_Class__c = 'Vehicle');
        Account partnerCustomer = new Account(Name = 'Partner Customer', RecordTypeId = PARTNERACCTRECORDTYPEID, Partner_Class__c = 'Customer');
        newAcctList.add(partnerAcctVehicle);
        newAcctList.add(partnerCustomer);
        insert newAcctList;
        validForOfferVO = [select Id, Customer__c, AssetID__c from Vehicle_Ownership__c 
                          where Status__c = 'Active' and Valid_for_Offers__c = false limit 1]; // try to get 1 that isn't valid for offer yet
        Asset__c asst = new Asset__c(Id = validForOfferVO.AssetID__c);
        asst.Original_Sales_Date__c = system.today();
        asst.Encore_Tier__c = 'Platinum';
        update asst;    
        customer = [select Id, Guest_Gigya_ID__c from Account where Id = :validForOfferVO.Customer__c limit 1];
        customer.Guest_Gigya_ID__c = 'TESTGIGYA';
        update customer;
        
        vehicleOffer = new Offer__c(Status__c = 'Inactive');
        vehicleOffer.Offer_Class__c = 'Vehicle';
        vehicleOffer.Offer_Type__c = 'Valet Service';
        vehicleOffer.Vehicle_Offer_Platinum_Limit__c = 10;
        vehicleOffer.Partner_Account__c = partnerAcctVehicle.Id;
        insert vehicleOffer;
        vehicleOffer.Status__c = 'Active';
        update vehicleOffer;
        vood = [select Id from VO_Offer_Detail__c where Vehicle_Ownership__c = :validForOfferVO.Id and Offer__c = :vehicleOffer.Id limit 1];
        
        
    }
}