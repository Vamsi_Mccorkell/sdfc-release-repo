// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for account creating accounts need to view the custom settings data
@IsTest (SeeAllData=true)
public class VoucherServicesWSHandlerTest {
	public static Account partnerVehicle;
    public static Offer__c vehicleOffer;
    public static Vehicle_Ownership__c validForOfferVO;
    
    static testmethod void attendantLoginRequiredFields(){
        VoucherServicesWebService.VoucherServiceResult wsoutput = VoucherServicesWSHandler.attendantLogin(null, null);
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('700', wsoutput.errCode);
    }
    
    static testmethod void attendantLoginNotSuccessful(){
        VoucherServicesWebService.VoucherServiceResult wsoutput = VoucherServicesWSHandler.attendantLogin('someusername', 'somepassword');
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('601', wsoutput.errCode);
    }
    
    static testmethod void attendantLoginSuccessful(){
        dataSetup();
        VoucherServicesWebService.VoucherServiceResult wsoutput = VoucherServicesWSHandler.attendantLogin(partnerVehicle.Partner_Branch_Username__c, partnerVehicle.Partner_Branch_Password__c);
        system.assertEquals(true, wsoutput.success);
        system.assertEquals(partnerVehicle.Id, wsoutput.recordId);
    }
    
    static testmethod void resendPasswordRequiredFields(){
        VoucherServicesWebService.VoucherServiceResult wsoutput = VoucherServicesWSHandler.resendPassword(null);
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('700', wsoutput.errCode);
    }
    
    static testmethod void resendPasswordNoPartnerBranch(){
        VoucherServicesWebService.VoucherServiceResult wsoutput = VoucherServicesWSHandler.resendPassword('some other username');
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('602', wsoutput.errCode);
    }
    
    static testmethod void resendPasswordSuccessful(){
        dataSetup();
        VoucherServicesWebService.VoucherServiceResult wsoutput = VoucherServicesWSHandler.resendPassword(partnerVehicle.Partner_Branch_Username__c);
        system.assertEquals(true, wsoutput.success);
        system.assertEquals(partnerVehicle.Id, wsoutput.recordId);
    }
    
    static testmethod void redeemVoucherRequiredFields(){
        VoucherServicesWebService.VoucherServiceResult wsoutput = VoucherServicesWSHandler.redeemVoucher(null, null, null, false, null, null);
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('700', wsoutput.errCode);
    }
    
    static testmethod void redeemVoucherPartnerNotValid(){
        VoucherServicesWebService.VoucherServiceResult wsoutput = VoucherServicesWSHandler.redeemVoucher(UserInfo.getUserId(), 'Valet Service', UserInfo.getUserId(), false, null, null); // pass valid id formats but not account and vo
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('603', wsoutput.errCode);
    }
    
    static testmethod void redeemVoucherVOODNotValid(){
        dataSetup();
        VoucherServicesWebService.VoucherServiceResult wsoutput = VoucherServicesWSHandler.redeemVoucher(partnerVehicle.Id, 'Valet Service', UserInfo.getUserId(), false, null, null); // pass valid id formats but not account and vo
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('604', wsoutput.errCode);
    }
    
    static testmethod void redeemVoucherSuccessful(){
        dataSetup();
        VO_Offer_Detail__c vood = [select Id from VO_Offer_Detail__c where Offer__c = :vehicleOffer.Id and Vehicle_Ownership__c = :validForOfferVO.Id limit 1];
        VoucherServicesWebService.VoucherServiceResult wsoutput = VoucherServicesWSHandler.redeemVoucher(partnerVehicle.Id, 'Valet Service', vood.Id, true, 'Test comp', 'errcode'); // pass valid id formats but not account and vo
        system.assertEquals(null, wsoutput.errCode);
        system.assertEquals(true, wsoutput.success);
    }
    
    static testmethod void getVoucherAvailabilityRequiredFields(){
        List<VoucherServicesWebService.VoucherAvailabilityResult> wsoutput = VoucherServicesWSHandler.getVoucherAvailability(null, null, null);
        system.assertEquals(false, wsoutput[0].success);
        system.assertEquals('700', wsoutput[0].errCode);
    }
    
    static testmethod void getVoucherAvailabilityNoPartnerBranchFound(){
        List<VoucherServicesWebService.VoucherAvailabilityResult> wsoutput = VoucherServicesWSHandler.getVoucherAvailability(UserInfo.getUserId(), 'REGO', 'Valet Service');
        system.assertEquals(false, wsoutput[0].success);
        system.assertEquals('605', wsoutput[0].errCode);
    }
    
    static testmethod void getVoucherAvailabilityNoActiveOffers(){
        dataSetup();
        vehicleOffer.Status__c = 'Inactive';
        update vehicleOffer;
        List<VoucherServicesWebService.VoucherAvailabilityResult> wsoutput = VoucherServicesWSHandler.getVoucherAvailability(partnerVehicle.Id, 'REGO1', 'Valet Service');
        system.assertEquals(false, wsoutput[0].success);
        system.assertEquals('606', wsoutput[0].errCode);
    }
    
    static testmethod void getVoucherAvailabilityNoActiveVOREGO(){
        dataSetup();
        List<VoucherServicesWebService.VoucherAvailabilityResult> wsoutput = VoucherServicesWSHandler.getVoucherAvailability(partnerVehicle.Id, 'REGO2TEST', 'Valet Service');
        system.assertEquals(false, wsoutput[0].success);
        system.assertEquals('607', wsoutput[0].errCode);
    }
    
    static testmethod void getVoucherAvailabilitySuccessful(){
        dataSetup();
        List<VoucherServicesWebService.VoucherAvailabilityResult> wsoutput = VoucherServicesWSHandler.getVoucherAvailability(partnerVehicle.Id, 'REGO1', 'Valet Service');
        system.assertEquals(true, wsoutput.size() > 0);
        system.assertEquals(null, wsoutput[0].errCode);
        system.assertEquals(true, wsoutput[0].success);
    }
    
    private static void dataSetup(){
        Id PARTNERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner').getRecordTypeId();
        Account partner = new Account(Name = 'Partner Branch', RecordTypeId = PARTNERACCTRECORDTYPEID, Partner_Class__c = 'Vehicle',Status__c = 'Active', Type = 'Partner');
        insert partner;
        Id PARTNERBRANCHACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Branch').getRecordTypeId();
        partnerVehicle = new Account(ParentId = partner.Id, Name = 'Partner', RecordTypeId = PARTNERBRANCHACCTRECORDTYPEID, Partner_Class__c = 'Vehicle',
                                    Partner_Branch_Username__c ='partner@user.com', Partner_Branch_Password__c = 'password',
                                    BillingCity = 'Test', BillingState = 'Test', Status__c = 'Active', Type = 'Partner Branch');
        insert partnerVehicle;
        
        validForOfferVO = [select Id, Customer__c, AssetID__c from Vehicle_Ownership__c 
                          where Status__c = 'Active' and Valid_for_Offers__c = false limit 1]; // try to get 1 that isn't valid for offer yet
        Asset__c asst = new Asset__c(Id = validForOfferVO.AssetID__c);
        asst.Original_Sales_Date__c = system.today();
        asst.Encore_Tier__c = 'Platinum';
        update asst;    
        Account customer = [select Id, Guest_Gigya_ID__c from Account where Id = :validForOfferVO.Customer__c limit 1];
        customer.Guest_Gigya_ID__c = 'TESTGIGYA';
        update customer;
        validForOfferVO.Valet_Voucher_Enabled__c = true;
        validForOfferVO.Voucher_T_C_Accepted__c = true;
        validForOfferVO.Voucher_T_C_Accepted_Date__c = system.today();
        validForOfferVO.Asset_RDR_Date__c = system.today();
        validForOfferVO.Rego_clean__c = 'REGO1';
        update validForOfferVO;
        
        vehicleOffer = new Offer__c(Status__c = 'Inactive');
        vehicleOffer.Offer_Class__c = 'Vehicle';
        vehicleOffer.Offer_Type__c = 'Valet Service';
        vehicleOffer.Vehicle_Offer_Platinum_Limit__c = 10;
        vehicleOffer.Partner_Account__c = partner.Id;
        insert vehicleOffer;
        vehicleOffer.Status__c = 'Active';
        update vehicleOffer;
    }
}