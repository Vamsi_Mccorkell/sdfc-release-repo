public class SPCacheLexusEnformMetadata 
{
	private static SPCacheLexusEnformMetadata__c CacheLexusEnformMetadata;
	
		/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	private static void LoadMetadata()
	{
		CacheLexusEnformMetadata = SPCacheLexusEnformMetadata__c.getInstance();
	}
	
	public static ID getIntelematicsUser()
	{
		if (CacheLexusEnformMetadata == null)
			LoadMetadata(); 
		
		if (CacheLexusEnformMetadata.Intelematics_User__c != null)	
			return CacheLexusEnformMetadata.Intelematics_User__c.substring(0, 15); 
		else
			return null;
	}
	
	public static ID getSqwarePegSupportEmail()
	{
		if (CacheLexusEnformMetadata == null)
			LoadMetadata(); 
		
		return CacheLexusEnformMetadata.Sqware_Peg_Support_Email__c;
	}
	
	public static string getDefaultCaseRecordType()
	{
		if (CacheLexusEnformMetadata == null)
			LoadMetadata(); 
			
		return CacheLexusEnformMetadata.DefaultCaseRecordType__c;
	}

	public static string getDefaultCaseType()
	{
		if (CacheLexusEnformMetadata == null)
			LoadMetadata(); 
			
		return CacheLexusEnformMetadata.DefaultCaseType__c;
	}

	public static string getDefaultCaseStatus()
	{
		if (CacheLexusEnformMetadata == null)
			LoadMetadata(); 
			
		return CacheLexusEnformMetadata.DefaultCaseStatus__c;
	}

	public static string getDefaultCaseOrigin()
	{
		if (CacheLexusEnformMetadata == null)
			LoadMetadata(); 
			
		return CacheLexusEnformMetadata.DefaultCaseOrigin__c;
	}	
	
	// Test Methods
	public static testmethod void TestSPCacheVehicleTypeMetadata()
	{
		SPCacheLexusEnformMetadata.getIntelematicsUser();
		CacheLexusEnformMetadata = null;
		
		SPCacheLexusEnformMetadata.getSqwarePegSupportEmail();
		CacheLexusEnformMetadata = null;
	}
	
}