/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 @isTest
public class SP_EnformUserManagementTest 
{
	private static Account CreateTestEUOwnerAccount()
	{
		Account TestEUOwnerAccount = new Account();
		TestEUOwnerAccount.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		TestEUOwnerAccount.LastName				= 'TestLast1';
		TestEUOwnerAccount.PersonEmail			= 'test1@email.com';
		TestEUOwnerAccount.PersonMobilePhone = '12335';	
		
		insert TestEUOwnerAccount;
		
		return TestEUOwnerAccount;
	}
	
	private static Account CreateTestOwnerAccountMatch()
	{
		Account TestEUOwnerAccount = new Account();
		TestEUOwnerAccount.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		TestEUOwnerAccount.LastName				= 'Matchlast';
		TestEUOwnerAccount.PersonEmail			= 'testMatch@email.com';
		TestEUOwnerAccount.PersonMobilePhone = '1234567';	
		
		insert TestEUOwnerAccount;
		
		return TestEUOwnerAccount;
	}
	
	private static Account CreateTestOwnerAccountMatchNullMobile()
	{
		Account TestEUOwnerAccount = new Account();
		TestEUOwnerAccount.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		TestEUOwnerAccount.LastName				= 'Matchlast';
		TestEUOwnerAccount.PersonEmail			= 'testMatch@email.com';
		
		insert TestEUOwnerAccount;
		
		return TestEUOwnerAccount;
	}
	
	private static Account CreateTestOwnerAccountMatchNullEmail()
	{
		Account TestEUOwnerAccount = new Account();
		TestEUOwnerAccount.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		TestEUOwnerAccount.LastName				= 'Matchlast';
		TestEUOwnerAccount.PersonMobilePhone = '1234567';	
		
		insert TestEUOwnerAccount;
		
		return TestEUOwnerAccount;
	}	
	
	private static Lead CreateTestMatchingLead()
	{
		Lead testLead = new Lead(LastName='Test1',
										MobilePhone = '1234567',
										Email = 'testMatch@email.com');
		insert testLead;
		
		return testLead;
	}
	
	private static Lead CreateTestLeadMatchNullMobile()
	{
		Lead TestLead = new Lead(LastName='Matchlast',
										Email = 'testMatch@email.com');
		
		insert TestLead;
		
		return TestLead;
	}
	
	private static Lead CreateTestLeadMatchNullEmail()
	{
		Lead TestLead = new Lead(LastName='Matchlast',
										MobilePhone = '1234567');
		
		insert TestLead;
		
		return TestLead;
	}		
	
	// 	-	No Missing Mandatory fields - Insert Secondary Enform User
	//	-	No Matching Owner/Prospect Account
	//	-	No Matching Lead
	
	// Result:
	
	// Enform User Record should be created
	// Lead should be created
	// Enform User should be linked to new Lead
	@isTest(SeeAllData=true)
    public static void TestCase001() 
    {
    	Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
    	
    	Enform_User__c TestEUNoMissFields = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '045678923',
							   Email__c = 'aaa@aa.com',
							   First_Name__c = 'First',
							   Last_Name__c = 'Last',
							   IsPrimaryUser__c = false);
							   
 		try
		{
			insert TestEUNoMissFields;
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not created');
		}
		
		Test.stopTest();
		
		system.assertNotEquals(TestEUNoMissFields.Id, null);
		
		TestEUNoMissFields = [select First_Name__c, Last_Name__c, Lead_ID__c from Enform_User__c 
							where Id = :TestEUNoMissFields.Id];
	
		List<Lead> Leads = [select Id, FirstName, LastName from Lead 
							where FirstName = :TestEUNoMissFields.First_Name__c 
							and LastName = :TestEUNoMissFields.Last_Name__c];
							
		//system.assertEquals(Leads.size(), 1);
		//system.assertEquals(Leads[0].Id, TestEUNoMissFields.Lead_ID__c);
    }
    
    // 	-	No Missing Mandatory fields - Update Existing Primary Enform User
    // 	-	No Missing Mandatory fields - Update Existing Secondary Enform User
    
    // Result
    
    // Enform User should be updated
    // Owner Account should be updated - Primary Enform User
    // Lead should be updated - Secondary Enform User
    
	@isTest(SeeAllData=true)
    public static void TestCase002() 
    {
    	Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
     	
    	Enform_User__c TestEUPrimary = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '045678923',
							   Email__c = 'abbb@bb.com',
							   First_Name__c = 'FirstP',
							   Last_Name__c = 'LastP',
							   IsPrimaryUser__c = true);
    	
    	Enform_User__c  TestEUNoMissFields = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '045678923',
							   Email__c = 'aaa@aa.com',
							   First_Name__c = 'First',
							   Last_Name__c = 'Last',
							   IsPrimaryUser__c = false);
							   
 		try
		{
			insert TestEUNoMissFields;
			insert TestEUPrimary;
			
    		SPCacheLexusEnformMetadata__c metaData = [select Id,Intelematics_User__c from SPCacheLexusEnformMetadata__c];
    		User currUser = [select Id from User where Id = :metaData.Intelematics_User__c];
    		//metaData.Intelematics_User__c = UserInfo.getUserId().substring(0, 15);
    		
    		//update metaData;
			
			system.runAs(currUser)
			{
				TestEUNoMissFields.First_Name__c = 'First_1';
				update TestEUNoMissFields;
				
				TestEUPrimary.First_Name__c = 'FirstP_1';
				update TestEUPrimary;
			}
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not updated');
		}
		
		Test.stopTest();
		
		//List<Lead> Leads = [select FirstName from Lead where FirstName = :TestEUNoMissFields.First_Name__c];
		//system.assertEquals(Leads.size(), 1);
		
		List<Account> Accounts = [select Id from Account where FirstName = :TestEUPrimary.First_Name__c];
		system.assertEquals(Accounts[0].Id, PrimaryUserAccount.Id);
		
    }    
    
  	// 	-	Missing Mandatory fields - Insert Secondary Enform User - Negative
	//	-	No Matching Owner/Prospect Account
	//	-	No Matching Lead
	
	// Result
	
	// Validation Exception
	// Enform User not inserted
   @isTest(SeeAllData=true)
    public static void TestCase003() 
    {
    	Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
    	
    	Enform_User__c TestEUNoMissFields = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '045678923',
							   Email__c = 'aaa@aa.com',
							   IsPrimaryUser__c = false);
							   
 		try
		{
			insert TestEUNoMissFields;
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not created' + ex.getMessage());
		}
		
		Test.stopTest();
		
		system.assertEquals(TestEUNoMissFields.Id, null);
    } 
    
   	// 	-	Missing Mandatory fields - Update Existing Primary Enform User - Negative
    
    // Result
    
    // Validation Exception
    // Enform User not updated
 	@isTest(SeeAllData=true)
    public static void TestCase004() 
    {
    	Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
     	
    	Enform_User__c TestEUPrimary = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '045678923',
							   Email__c = 'abbb@bb.com',
							   First_Name__c = 'FirstP',
							   Last_Name__c = 'LastP',
							   IsPrimaryUser__c = true);

 		try
		{
			insert TestEUPrimary;
			
			TestEUPrimary.Last_Name__c = '';
			TestEUPrimary.First_Name__c = 'FirstP_1';
			
			update TestEUPrimary;
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not updated ' + ex.getMessage());
		}
		
		TestEUPrimary = [select First_Name__c from Enform_User__c where Id = :TestEUPrimary.Id];
		
		system.assertEquals(TestEUPrimary.First_Name__c, 'FirstP');
		
		Test.stopTest();
    }  
    
         
     
 	// 	-	No Missing Mandatory fields - Insert Secondary Enform User
	//	-	Matching Owner/Prospect Account: Both Email and Mobile matching
	//	-	No Matching Lead
	
	// Result
	
	// Enform User should be created
	// Its Owner Prospect ID should be set with matching Owner/Propect Account ID
	// New Lead should not be created
	// New Case should be created
	@isTest(SeeAllData=true)
    static void TestCase005() 
    {
    	Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
     	Account MatchAccount = CreateTestOwnerAccountMatch();
    	
    	Enform_User__c TestEUNoMissFields = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '1234567',
							   Email__c = 'testMatch@email.com',
							   First_Name__c = 'First',
							   Last_Name__c = 'Last',
							   IsPrimaryUser__c = false);
							   
 		try
		{
			insert TestEUNoMissFields;
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not created');
		}
		
		Test.stopTest();
		
		system.assertNotEquals(TestEUNoMissFields.Id, null);
		
		TestEUNoMissFields = [select First_Name__c, Last_Name__c, Owner_Prospect_ID__c from Enform_User__c 
							where Id = :TestEUNoMissFields.Id];
							
		system.assertEquals(TestEUNoMissFields.Owner_Prospect_ID__c, MatchAccount.Id);
	
		List<Lead> Leads = [select Id, FirstName, LastName from Lead 
							where FirstName = :TestEUNoMissFields.First_Name__c 
							and LastName = :TestEUNoMissFields.Last_Name__c];
							
		system.assertEquals(Leads.size(), 0);
		
		List<Case> Cases = [select Id, Subject from Case 
							where AccountId = :PrimaryUserAccount.Id];
							
		system.assertEquals(Cases.size(), 1);  
		system.assertEquals(Cases[0].Subject, 'PROSPECT/OWNER MATCHING INFORMATION - PERFECT ACCOUNT MATCH');    
    }   
    
  	// 	-	No Missing Mandatory fields - Insert Secondary Enform User
	//	-	Matching Owner/Prospect Account: Email matching, Null Mobile
	//	-	No Matching Lead
	
	// Result
	
	// Enform User should be created
	// Its Owner Prospect ID should be set with matching Owner/Propect Account ID
	// New Lead should not be created
	// New case should be created
    @isTest(SeeAllData=true)
    static void TestCase006() 
    {
     	Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
     	Account MatchAccount = CreateTestOwnerAccountMatchNullMobile();
    	
    	Enform_User__c TestEUNoMissFields = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '1234567',
							   Email__c = 'testMatch@email.com',
							   First_Name__c = 'First',
							   Last_Name__c = 'Last',
							   IsPrimaryUser__c = false);
							   
 		try
		{
			insert TestEUNoMissFields;
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not created');
		}
		
		Test.stopTest();
		
		system.assertNotEquals(TestEUNoMissFields.Id, null);
		
		TestEUNoMissFields = [select First_Name__c, Last_Name__c, Owner_Prospect_ID__c from Enform_User__c 
							where Id = :TestEUNoMissFields.Id];
							
		//system.assertEquals(TestEUNoMissFields.Owner_Prospect_ID__c, MatchAccount.Id); // SP [27-05-2015]
	
		List<Lead> Leads = [select Id, FirstName, LastName from Lead 
							where FirstName = :TestEUNoMissFields.First_Name__c 
							and LastName = :TestEUNoMissFields.Last_Name__c];
							
		system.assertEquals(Leads.size(), 0);      
		
		List<Case> Cases = [select Id, Subject from Case 
							where AccountId = :PrimaryUserAccount.Id];

		system.assertEquals(Cases.size(), 1);  
		//system.assertEquals(Cases[0].Subject, 'PROSPECT/OWNER MATCHING INFORMATION - EMAIL MATCH, NO ACCOUNT MOBILE');      
							
    } 
    
  	// 	-	No Missing Mandatory fields - Insert Secondary Enform User
	//	-	Matching Owner/Prospect Account: Email matching, Different Mobile
	//	-	No Matching Lead
	
	// Result
	
	// Enform User should be created
	// Its Owner Prospect ID should be set with matching Owner/Propect Account ID
	// New Lead should not be created
	// New case should be created
    @isTest(SeeAllData=true)
    static void TestCase007() 
    {
    	Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
     	Account MatchAccount = CreateTestOwnerAccountMatch();
    	
    	Enform_User__c TestEUNoMissFields = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '1234568',
							   Email__c = 'testMatch@email.com',
							   First_Name__c = 'First',
							   Last_Name__c = 'Last',
							   IsPrimaryUser__c = false);
							   
 		try
		{
			insert TestEUNoMissFields;
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not created');
		}
		
		Test.stopTest();
		
		system.assertNotEquals(TestEUNoMissFields.Id, null);
		
		TestEUNoMissFields = [select First_Name__c, Last_Name__c, Owner_Prospect_ID__c from Enform_User__c 
							where Id = :TestEUNoMissFields.Id];
							
		system.assertEquals(TestEUNoMissFields.Owner_Prospect_ID__c, MatchAccount.Id);
	
		List<Lead> Leads = [select Id, FirstName, LastName from Lead 
							where FirstName = :TestEUNoMissFields.First_Name__c 
							and LastName = :TestEUNoMissFields.Last_Name__c];
							
		system.assertEquals(Leads.size(), 0);      
		
		List<Case> Cases = [select Id, Subject from Case 
							where AccountId = :PrimaryUserAccount.Id];

		system.assertEquals(Cases.size(), 1);  
		system.assertEquals(Cases[0].Subject, 'PROSPECT/OWNER MATCHING INFORMATION - EMAIL MATCH, DIFFERENT ACCOUNT MOBILE');      
    }     
    
  	// 	-	No Missing Mandatory fields - Insert Secondary Enform User
	//	-	Matching Owner/Prospect Account: Null Email, Matching Mobile
	//	-	No Matching Lead
		
	// Result
	
	// Enform User should be created
	// Its Owner Prospect ID should be set with matching Owner/Propect Account ID
	// New Lead should not be created
	// New case should be created
    @isTest(SeeAllData=true)
    static void TestCase008() 
    {
    	Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
     	Account MatchAccount = CreateTestOwnerAccountMatchNullEmail();
    	
    	Enform_User__c TestEUNoMissFields = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '1234567',
							   Email__c = 'testMatch@email.com',
							   First_Name__c = 'First',
							   Last_Name__c = 'Last',
							   IsPrimaryUser__c = false);
							   
 		try
		{
			insert TestEUNoMissFields;
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not created');
		}
		
		Test.stopTest();
		
		system.assertNotEquals(TestEUNoMissFields.Id, null);
		
		TestEUNoMissFields = [select First_Name__c, Last_Name__c, Owner_Prospect_ID__c from Enform_User__c 
							where Id = :TestEUNoMissFields.Id];
							
		//system.assertEquals(TestEUNoMissFields.Owner_Prospect_ID__c, MatchAccount.Id); // SP [27-05-2015]
	
		List<Lead> Leads = [select Id, FirstName, LastName from Lead 
							where FirstName = :TestEUNoMissFields.First_Name__c 
							and LastName = :TestEUNoMissFields.Last_Name__c];
							
		system.assertEquals(Leads.size(), 0);      
 		
		List<Case> Cases = [select Id, Subject from Case 
							where AccountId = :PrimaryUserAccount.Id];

		system.assertEquals(Cases.size(), 1);  
		//system.assertEquals(Cases[0].Subject, 'PROSPECT/OWNER MATCHING INFORMATION - MOBILE MATCH, NO ACCOUNT EMAIL'); // SP [27-05-2015]      
		
    }     
    
  	// 	-	No Missing Mandatory fields - Insert Secondary Enform User
	//	-	Matching Owner/Prospect Account: Diffferent Email, Matching Mobile
	//	-	No Matching Lead
	
	// Result
	
	// Enform User should be created
	// Its Owner Prospect ID should be set with matching Owner/Propect Account ID
	// New Lead should not be created
	// New case should be created
    @isTest(SeeAllData=true)
    static void TestCase009() 
    {
    	Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
     	Account MatchAccount = CreateTestOwnerAccountMatch();
    	
    	Enform_User__c TestEUNoMissFields = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '1234567',
							   Email__c = 'testMatch1@email.com',
							   First_Name__c = 'First',
							   Last_Name__c = 'Last',
							   IsPrimaryUser__c = false);
							   
 		try
		{
			insert TestEUNoMissFields;
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not created');
		}
		
		Test.stopTest();
		
		system.assertNotEquals(TestEUNoMissFields.Id, null);
		
		TestEUNoMissFields = [select First_Name__c, Last_Name__c, Owner_Prospect_ID__c from Enform_User__c 
							where Id = :TestEUNoMissFields.Id];
							
		//system.assertEquals(TestEUNoMissFields.Owner_Prospect_ID__c, MatchAccount.Id); // SP [27-05-2015]
	
		List<Lead> Leads = [select Id, FirstName, LastName from Lead 
							where FirstName = :TestEUNoMissFields.First_Name__c 
							and LastName = :TestEUNoMissFields.Last_Name__c];
							
		system.assertEquals(Leads.size(), 0);      
		
		List<Case> Cases = [select Id, Subject from Case 
							where AccountId = :PrimaryUserAccount.Id];

		system.assertEquals(Cases.size(), 1);  
		//system.assertEquals(Cases[0].Subject, 'PROSPECT/OWNER MATCHING INFORMATION - MOBILE MATCH, DIFFERENT ACCOUNT EMAIL'); // SP [27-05-2015]      
		
		
    }                
 
   	// 	-	No Missing Mandatory fields - Insert Secondary Enform User - Negative
	//	-	Matching Owner/Prospect Account: More than one exact matching record
	//	-	No Matching Lead
	
	// Result
	
	// Enform User should be created
	// Its Owner Prospect ID should not be set
	// New Lead should not be created
	// New case should be created	
    @isTest(SeeAllData=true)
    static void TestCase010() 
    {
  		Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
     	
     	Account PerfectMatchAccount = CreateTestOwnerAccountMatch();
     	Account MatchAccount = CreateTestOwnerAccountMatch();
    	
     	Enform_User__c TestEUNoMissFields = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '1234567',
							   Email__c = 'testMatch@email.com',
							   First_Name__c = 'First',
							   Last_Name__c = 'Last',
							   IsPrimaryUser__c = false);
							   
 		try
		{
			insert TestEUNoMissFields;
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not created');
		}
		
		Test.stopTest();
		
		system.assertNotEquals(TestEUNoMissFields.Id, null);
		
		TestEUNoMissFields = [select First_Name__c, Last_Name__c, Owner_Prospect_ID__c from Enform_User__c 
							where Id = :TestEUNoMissFields.Id];
							
		system.assertEquals(TestEUNoMissFields.Owner_Prospect_ID__c, null);
	
		List<Lead> Leads = [select Id, FirstName, LastName from Lead 
							where FirstName = :TestEUNoMissFields.First_Name__c 
							and LastName = :TestEUNoMissFields.Last_Name__c];
							
		system.assertEquals(Leads.size(), 0);      
		
		
		List<Case> Cases = [select Id, Subject from Case 
							where AccountId = :PrimaryUserAccount.Id];

		system.assertEquals(Cases.size(), 1);  
		system.assertEquals(Cases[0].Subject, 'PROSPECT/OWNER MATCHING EXCEPTION - MORE THAN ONE ACCOUNT MATCH WITH SAME MOBILE AND EMAIL');      
		
    }  
    
   	// 	-	No Missing Mandatory fields - Insert Secondary Enform User
	//	-	Matching Owner/Prospect Account: More than one mobile matching records
	//	-	No Matching Lead
	
	// Result

	// Enform User should be created
	// Its Owner Prospect ID should not be set
	// New Lead should not be created
	// New case should be created	
    @isTest(SeeAllData=true)
    static void TestCase011() 
    {
    	Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
     	
     	Account MatchAccount = CreateTestOwnerAccountMatchNullEmail();
     	Account AnotherMatchAccount = CreateTestOwnerAccountMatchNullEmail();
    	
    	Enform_User__c TestEUNoMissFields = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '1234567',
							   Email__c = 'testMatch@email.com',
							   First_Name__c = 'First',
							   Last_Name__c = 'Last',
							   IsPrimaryUser__c = false);
							   
 		try
		{
			insert TestEUNoMissFields;
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not created');
		}
		
		Test.stopTest();
		
		system.assertNotEquals(TestEUNoMissFields.Id, null);
		
		TestEUNoMissFields = [select First_Name__c, Last_Name__c, Owner_Prospect_ID__c from Enform_User__c 
							where Id = :TestEUNoMissFields.Id];
							
		system.assertEquals(TestEUNoMissFields.Owner_Prospect_ID__c, null);
	
		List<Lead> Leads = [select Id, FirstName, LastName from Lead 
							where FirstName = :TestEUNoMissFields.First_Name__c 
							and LastName = :TestEUNoMissFields.Last_Name__c];
							
		system.assertEquals(Leads.size(), 0);      
		
		
		List<Case> Cases = [select Id, Subject from Case 
							where AccountId = :PrimaryUserAccount.Id];

		system.assertEquals(Cases.size(), 1);  
		system.assertEquals(Cases[0].Subject, 'PROSPECT/OWNER MATCHING EXCEPTION - MORE THAN ONE ACCOUNT MATCH WITH SAME MOBILE');      
		
    }  
    
   	// 	-	No Missing Mandatory fields - Insert Secondary Enform User
	//	-	Matching Owner/Prospect Account: More than one email matching records
	//	-	No Matching Lead
	
	// Result

	// Enform User should be created
	// Its Owner Prospect ID should not be set
	// New Lead should not be created
	// New case should be created	
    @isTest(SeeAllData=true)
    static void TestCase012() 
    {
    	Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
     	
     	Account MatchAccount = CreateTestOwnerAccountMatchNullMobile();
     	Account AnotherMatchAccount = CreateTestOwnerAccountMatchNullMobile();
    	
    	Enform_User__c TestEUNoMissFields = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '1234567',
							   Email__c = 'testMatch@email.com',
							   First_Name__c = 'First',
							   Last_Name__c = 'Last',
							   IsPrimaryUser__c = false);
							   
 		try
		{
			insert TestEUNoMissFields;
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not created');
		}
		
		Test.stopTest();
		
		system.assertNotEquals(TestEUNoMissFields.Id, null);
		
		TestEUNoMissFields = [select First_Name__c, Last_Name__c, Owner_Prospect_ID__c from Enform_User__c 
							where Id = :TestEUNoMissFields.Id];
							
		system.assertEquals(TestEUNoMissFields.Owner_Prospect_ID__c, null);
	
		List<Lead> Leads = [select Id, FirstName, LastName from Lead 
							where FirstName = :TestEUNoMissFields.First_Name__c 
							and LastName = :TestEUNoMissFields.Last_Name__c];
							
		system.assertEquals(Leads.size(), 0);      
		
		List<Case> Cases = [select Id, Subject from Case 
							where AccountId = :PrimaryUserAccount.Id];

		system.assertEquals(Cases.size(), 1);  
		//system.assertEquals(Cases[0].Subject, 'PROSPECT/OWNER MATCHING EXCEPTION - MORE THAN ONE ACCOUNT MATCH WITH SAME EMAIL'); // SP [27-05-2015]      
		
    }   
    
 	// 	-	No Missing Mandatory fields - Insert Secondary Enform User
 	//	- 	No Matching Prospect/Owner
	//	-	Matching Lead: Both Email and Mobile matching
	
	// Result
	
	// Enform User should be created
	// Its Lead ID should be set with matching Lead ID
	// New Lead should not be created
	// New Case should be created
	@isTest(SeeAllData=true)
    static void TestCase013() 
    {
    	Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
     	Lead MatchLead = CreateTestMatchingLead();
    	
    	Enform_User__c TestEUNoMissFields = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '1234567',
							   Email__c = 'testMatch@email.com',
							   First_Name__c = 'First',
							   Last_Name__c = 'Last',
							   IsPrimaryUser__c = false);
							   
 		try
		{
			insert TestEUNoMissFields;
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not created');
		}
		
		Test.stopTest();
		
		system.assertNotEquals(TestEUNoMissFields.Id, null);
		
		TestEUNoMissFields = [select First_Name__c, Last_Name__c, Lead_ID__c from Enform_User__c 
							where Id = :TestEUNoMissFields.Id];
							
		system.debug('############## UPDATED ENFORM ' + TestEUNoMissFields);		
							
		//system.assertEquals(TestEUNoMissFields.Lead_ID__c, MatchLead.Id); // SP [27-05-2015]
	
		List<Lead> Leads = [select Id, FirstName, LastName from Lead 
							where FirstName = :TestEUNoMissFields.First_Name__c 
							and LastName = :TestEUNoMissFields.Last_Name__c and
							Id <> :MatchLead.Id];
							
		system.debug('############## STRANGE CASE ' + Leads);					
							
		system.assertEquals(Leads.size(), 0);
		
		
		List<Case> Cases = [select Id, Subject from Case 
							where AccountId = :PrimaryUserAccount.Id];

		system.assertEquals(Cases.size(), 1);  
		//system.assertEquals(Cases[0].Subject, 'LEAD MATCHING INFORMATION - PERFECT LEAD MATCH'); // SP [27-05-2015]      
		
    }
    
  	// 	-	No Missing Mandatory fields - Insert Secondary Enform User
	//	-	Matching Lead: Email matching, Null Mobile
	//	-	No Matching Prospect/Owner Account
	
	// Result
	
	// Enform User should be created
	// Its Lead ID should be set with matching Lead ID
	// New Lead should not be created
	// New case should be created
    @isTest(SeeAllData=true)
    static void TestCase014() 
    {
     	Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
     	Lead MatchLead = CreateTestLeadMatchNullMobile();
    	
    	Enform_User__c TestEUNoMissFields = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '1234567',
							   Email__c = 'testMatch@email.com',
							   First_Name__c = 'First',
							   Last_Name__c = 'Last',
							   IsPrimaryUser__c = false);
							   
 		try
		{
			insert TestEUNoMissFields;
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not created');
		}
		
		Test.stopTest();
		
		system.assertNotEquals(TestEUNoMissFields.Id, null);
		
		TestEUNoMissFields = [select First_Name__c, Last_Name__c, Lead_ID__c from Enform_User__c 
							where Id = :TestEUNoMissFields.Id];
							
		//system.assertEquals(TestEUNoMissFields.Lead_ID__c, MatchLead.Id); // SP [27-05-2015]
	
		List<Lead> Leads = [select Id, FirstName, LastName from Lead 
							where FirstName = :TestEUNoMissFields.First_Name__c 
							and LastName = :TestEUNoMissFields.Last_Name__c  and
							Id <> :MatchLead.Id];
							
		system.assertEquals(Leads.size(), 0);      
		
		List<Case> Cases = [select Id, Subject from Case 
							where AccountId = :PrimaryUserAccount.Id];

		system.assertEquals(Cases.size(), 1);  
		//system.assertEquals(Cases[0].Subject, 'LEAD MATCHING INFORMATION - EMAIL MATCH, NO LEAD MOBILE'); // SP [27-05-2015]      		
							
    }
    
   	// 	-	No Missing Mandatory fields - Insert Secondary Enform User
	//	-	Matching Lead: Email matching, Different Mobile
	//	-	No Matching Prospect/Owner Account
	
	// Result
	
	// Enform User should be created
	// Its Lead ID should be set with matching Lead ID
	// New Lead should not be created
	// New case should be created
    @isTest(SeeAllData=true)
    static void TestCase015() 
    {
    	Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
     	Lead MatchLead = CreateTestMatchingLead();
    	
    	Enform_User__c TestEUNoMissFields = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '1234568',
							   Email__c = 'testMatch@email.com',
							   First_Name__c = 'First',
							   Last_Name__c = 'Last',
							   IsPrimaryUser__c = false);
							   
 		try
		{
			insert TestEUNoMissFields;
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not created');
		}
		
		Test.stopTest();
		
		system.assertNotEquals(TestEUNoMissFields.Id, null);
		
		TestEUNoMissFields = [select First_Name__c, Last_Name__c, Lead_ID__c from Enform_User__c 
							where Id = :TestEUNoMissFields.Id];
							
		system.assertEquals(TestEUNoMissFields.Lead_ID__c, MatchLead.Id);
	
		List<Lead> Leads = [select Id, FirstName, LastName from Lead 
							where FirstName = :TestEUNoMissFields.First_Name__c 
							and LastName = :TestEUNoMissFields.Last_Name__c  and
							Id <> :MatchLead.Id];
							
		system.assertEquals(Leads.size(), 0);      
		
		
		List<Case> Cases = [select Id, Subject from Case 
							where AccountId = :PrimaryUserAccount.Id];

		system.assertEquals(Cases.size(), 1);  
		system.assertEquals(Cases[0].Subject, 'LEAD MATCHING INFORMATION - EMAIL MATCH, DIFFERENT LEAD MOBILE'); 		 
    }
    
   	// 	-	No Missing Mandatory fields - Insert Secondary Enform User
	//	-	Matching Lead: Null Email, Matching Mobile
	//	-	No Matching Prospect/Owner Account
		
	// Result
	
	// Enform User should be created
	// Its Lead ID should be set with matching Lead ID
	// New Lead should not be created
	// New case should be created
    @isTest(SeeAllData=true)
    static void TestCase016() 
    {
    	Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
     	Lead MatchLead = CreateTestLeadMatchNullEmail();
    	
    	Enform_User__c TestEUNoMissFields = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '1234567',
							   Email__c = 'testMatch@email.com',
							   First_Name__c = 'First',
							   Last_Name__c = 'Last',
							   IsPrimaryUser__c = false);
							   
 		try
		{
			insert TestEUNoMissFields;
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not created');
		}
		
		Test.stopTest();
		
		system.assertNotEquals(TestEUNoMissFields.Id, null);
		
		TestEUNoMissFields = [select First_Name__c, Last_Name__c, Lead_ID__c from Enform_User__c 
							where Id = :TestEUNoMissFields.Id];
							
		//system.assertEquals(TestEUNoMissFields.Lead_ID__c, MatchLead.Id); // SP [27-05-2015]
	
		List<Lead> Leads = [select Id, FirstName, LastName from Lead 
							where FirstName = :TestEUNoMissFields.First_Name__c 
							and LastName = :TestEUNoMissFields.Last_Name__c  and
							Id <> :MatchLead.Id];
							
		system.assertEquals(Leads.size(), 0);      
		
		
		List<Case> Cases = [select Id, Subject from Case 
							where AccountId = :PrimaryUserAccount.Id];

		system.assertEquals(Cases.size(), 1);  
		//system.assertEquals(Cases[0].Subject, 'LEAD MATCHING INFORMATION - MOBILE MATCH, NO LEAD EMAIL'); // SP [27-05-2015] 				
    }
    
  	// 	-	No Missing Mandatory fields - Insert Secondary Enform User
	//	-	Matching Lead: Different Email, Matching Mobile
	//	-	No Matching Prospect/Owner
	
	// Result
	
	// Enform User should be created
	// Its Lead ID should be set with matching Lead ID
	// New Lead should not be created
	// New case should be created
    @isTest(SeeAllData=true)
    static void TestCase017() 
    {
    	Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
     	Lead MatchLead = CreateTestMatchingLead();
    	
    	Enform_User__c TestEUNoMissFields = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '1234567',
							   Email__c = 'testMatch1@email.com',
							   First_Name__c = 'First',
							   Last_Name__c = 'Last',
							   IsPrimaryUser__c = false);
							   
 		try
		{
			insert TestEUNoMissFields;
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not created');
		}
		
		Test.stopTest();
		
		system.assertNotEquals(TestEUNoMissFields.Id, null);
		
		TestEUNoMissFields = [select First_Name__c, Last_Name__c, Lead_ID__c from Enform_User__c 
							where Id = :TestEUNoMissFields.Id];
							
		//system.assertEquals(TestEUNoMissFields.Lead_ID__c, MatchLead.Id); // SP [27-05-2015]
	
		List<Lead> Leads = [select Id, FirstName, LastName from Lead 
							where FirstName = :TestEUNoMissFields.First_Name__c 
							and LastName = :TestEUNoMissFields.Last_Name__c  and
							Id <> :MatchLead.Id];
							
		system.assertEquals(Leads.size(), 0);      
		
		
		List<Case> Cases = [select Id, Subject from Case 
							where AccountId = :PrimaryUserAccount.Id];

		system.assertEquals(Cases.size(), 1);  
		//system.assertEquals(Cases[0].Subject, 'LEAD MATCHING INFORMATION - MOBILE MATCH, DIFFERENT LEAD EMAIL'); // SP [27-05-2015]		
    }  

   	// 	-	No Missing Mandatory fields - Insert Secondary Enform User
	//	-	Matching Lead: More than one exact matching record
	//	-	No Matching Prospect/Owner
	
	// Result
	
	// Enform User should be created
	// Its Lead ID should not be set
	// New Lead should not be created
	// New case should be created	
    @isTest(SeeAllData=true)
    static void TestCase018() 
    {
  		Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
     	
     	Lead PerfectMatchLead1 = CreateTestMatchingLead();
     	Lead PerfectMatchLead2 = CreateTestMatchingLead();
    	
     	Enform_User__c TestEUNoMissFields = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '1234567',
							   Email__c = 'testMatch@email.com',
							   First_Name__c = 'First',
							   Last_Name__c = 'Last',
							   IsPrimaryUser__c = false);
							   
 		try
		{
			insert TestEUNoMissFields;
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not created');
		}
		
		Test.stopTest();
		
		system.assertNotEquals(TestEUNoMissFields.Id, null);
		
		TestEUNoMissFields = [select First_Name__c, Last_Name__c, Lead_ID__c from Enform_User__c 
							where Id = :TestEUNoMissFields.Id];
							
		system.assertEquals(TestEUNoMissFields.Lead_ID__c, null);
	
		List<Lead> Leads = [select Id, FirstName, LastName from Lead 
							where FirstName = :TestEUNoMissFields.First_Name__c 
							and LastName = :TestEUNoMissFields.Last_Name__c  and
							Id <> :PerfectMatchLead1.Id and Id <> :PerfectMatchLead2.Id];
							
		system.assertEquals(Leads.size(), 0);      
	
		List<Case> Cases = [select Id, Subject from Case 
							where AccountId = :PrimaryUserAccount.Id];

		system.assertEquals(Cases.size(), 1);  
		//system.assertEquals(Cases[0].Subject, 'LEAD MATCHING EXCEPTION - MORE THAN ONE LEAD MATCH WITH SAME MOBILE AND EMAIL'); // SP [27-05-2015]		
		
    }
    
    // 	-	No Missing Mandatory fields - Insert Secondary Enform User
	//	-	Matching Lead: More than one mobile matching records
	//	-	No Matching Prospect/Owner
	
	// Result

	// Enform User should be created
	// Its Lead ID should not be set
	// New Lead should not be created
	// New case should be created	
    @isTest(SeeAllData=true)
    static void TestCase019() 
    {
    	Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
     	
     	Lead MatchLead = CreateTestLeadMatchNullEmail();
     	Lead AnotherMatchLead = CreateTestLeadMatchNullEmail();
    	
    	Enform_User__c TestEUNoMissFields = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '1234567',
							   Email__c = 'testMatch@email.com',
							   First_Name__c = 'First',
							   Last_Name__c = 'Last',
							   IsPrimaryUser__c = false);
							   
 		try
		{
			insert TestEUNoMissFields;
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not created');
		}
		
		Test.stopTest();
		
		system.assertNotEquals(TestEUNoMissFields.Id, null);
		
		TestEUNoMissFields = [select First_Name__c, Last_Name__c, Lead_ID__c from Enform_User__c 
							where Id = :TestEUNoMissFields.Id];
							
		system.assertEquals(TestEUNoMissFields.Lead_ID__c, null);
	
		List<Lead> Leads = [select Id, FirstName, LastName from Lead 
							where FirstName = :TestEUNoMissFields.First_Name__c 
							and LastName = :TestEUNoMissFields.Last_Name__c and
							Id <> :MatchLead.Id and Id <> :AnotherMatchLead.Id];
							
		system.assertEquals(Leads.size(), 0);      
		
	
		List<Case> Cases = [select Id, Subject from Case 
							where AccountId = :PrimaryUserAccount.Id];

		system.assertEquals(Cases.size(), 1);  
		//system.assertEquals(Cases[0].Subject, 'LEAD MATCHING EXCEPTION - MORE THAN ONE LEAD MATCH WITH SAME MOBILE'); // SP [27-05-2015]		
		
    }
    
    // 	-	No Missing Mandatory fields - Insert Secondary Enform User
	//	-	Matching Lead: More than one email matching records
	//	-	No Matching Prospect/Owner
	
	// Result

	// Enform User should be created
	// Its Lead ID should not be set
	// New Lead should not be created
	// New case should be created	
    @isTest(SeeAllData=true)
    static void TestCase020() 
    {
    	Test.startTest();
										   
     	Account PrimaryUserAccount = CreateTestEUOwnerAccount();
     	
     	Lead MatchLead = CreateTestLeadMatchNullMobile();
     	Lead AnotherMatchLead = CreateTestLeadMatchNullMobile();
    	
    	Enform_User__c TestEUNoMissFields = new Enform_User__c(Owner_Id__c = PrimaryUserAccount.Id,
							   Mobile_Phone__c = '1234567',
							   Email__c = 'testMatch@email.com',
							   First_Name__c = 'First',
							   Last_Name__c = 'Last',
							   IsPrimaryUser__c = false);
							   
 		try
		{
			insert TestEUNoMissFields;
		}
		catch(Exception ex)
		{
			system.debug('##### TEST CASE FAILED: Enform User is not created');
		}
		
		Test.stopTest();
		
		system.assertNotEquals(TestEUNoMissFields.Id, null);
		
		TestEUNoMissFields = [select First_Name__c, Last_Name__c, Lead_ID__c from Enform_User__c 
							where Id = :TestEUNoMissFields.Id];
							
		system.assertEquals(TestEUNoMissFields.Lead_ID__c, null);
	
		List<Lead> Leads = [select Id, FirstName, LastName from Lead 
							where FirstName = :TestEUNoMissFields.First_Name__c 
							and LastName = :TestEUNoMissFields.Last_Name__c and
							Id <> :MatchLead.Id and Id <> :AnotherMatchLead.Id];
							
		system.assertEquals(Leads.size(), 0);      
		
		
		List<Case> Cases = [select Id, Subject from Case 
							where AccountId = :PrimaryUserAccount.Id];

		system.assertEquals(Cases.size(), 1);  
		//system.assertEquals(Cases[0].Subject, 'LEAD MATCHING EXCEPTION - MORE THAN ONE LEAD MATCH WITH SAME EMAIL'); // SP [27-05-2015]		
		
    }  
                                                   
}