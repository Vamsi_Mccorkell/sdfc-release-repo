// VD DEV Notes: We had to set to SeeAllData=true because other test triggers for creating accounts need to view the custom settings data;
// 
@IsTest (SeeAllData=true)

public class  CallBatchCreateOfferDetailsExtTest {
	static Offer__c newOffer;
    static Account customer;
    static Account partnerAcct;

    static testmethod void testcallCreateOfferDtlBatch(){
        dataSetup();
      
        ApexPages.StandardController stdCon = new ApexPages.StandardController(newOffer);
        CallBatchCreateOfferDetailsExt cont = new CallBatchCreateOfferDetailsExt(stdCon);
        
        cont.callCreateOfferDtlBatch();

        // DEV NOTE: This just tests the batch class and just aims for coverage; It calls the GuestVoucherServicesUtilities.recalculateCODCounts(codIdSet) method
        // which is tested with assertions in the GuestVoucherServicesUtilitiesTest class
        
    }
    
    static void dataSetup(){
        List<Account> newAcctList = new List<Account>();
        List<Vehicle_Ownership__c> voList = new List<Vehicle_Ownership__c>();
        List<Asset__c> assetList = new List<Asset__c>();
        
        Id PARTNERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner').getRecordTypeId();
        Id CUSTOMERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        partnerAcct = new Account(Name = 'Partner Account', RecordTypeId = PARTNERACCTRECORDTYPEID, Partner_Class__c = 'Vehicle');
        newAcctList.add(partnerAcct);
        
        customer = new Account(FirstName = 'Test', LastName = 'Customer', RecordTypeId = CUSTOMERACCTRECORDTYPEID);
        newAcctList.add(customer);
        
        insert newAcctList;
        
        newOffer = new Offer__c(Partner_Account__c = partnerAcct.Id, Status__c = 'Inactive', Offer_Class__c = 'Vehicle', Vehicle_Offer_Platinum_Limit__c = 10,
                               Offer_Type__c = 'Valet Service');
        insert newOffer;
        newOffer.Status__c = 'Active';
        update newOffer;
    }
}