@IsTest (SeeAllData = true)
public class LeadTriggerHandlerTest {
    static testMethod void testSpecialCharacters(){
        Lead l = new Lead(FirstName = '表外漢字', Status = 'Open', LastName = 'test', MobilePhone = 'p:+33333', PostalCode = '1010', LeadSource = 'Facebook');
        insert l;
        Lead resultLead = [select Id, Status from Lead where Id = :l.Id limit 1];
        system.assertEquals('Process Error', resultLead.Status);
    }
    
    static testMethod void doNotRunTriggerLogic(){
        Lead l = new Lead(FirstName = 'Test', Status = 'Open', LastName = 'test', MobilePhone = 'p:+33333', PostalCode = '1010'
                           ); // not facebook
        insert l;
        Lead resultLead = [select Id, MatchedPersonAccount__c, Status from Lead where Id = :l.Id limit 1];
        system.assertEquals('Open', resultLead.Status);
        system.assertEquals(null, resultLead.MatchedPersonAccount__c);
    }
    
    static testmethod void findMatchedAccount(){
        Account person = new Account(FirstName = 'Test', LastName = 'Test', PersonEmail = 'test@noreply.com',
                                    BillingPostalCode = 'XXTEST1010', PersonMobilePhone = '02111111111');
        insert person;
        Account dealer = new Account(Name = 'Test Dealer', RDR_Dealer_Code__c = '44444',
                                    RecordTypeId = SPCacheRecordTypeMetadata.getAccount_Dealer(),
                                    Status__c = 'Active', Type = 'Dealer');
        insert dealer;
        
        Lead l = new Lead(FirstName = 'Test', LastName = 'Test', Email = 'test@noreply.com',
                          			Preferred_Dealer__c = 'NSW - Test Dealer',
                                    PostalCode = '2000', MobilePhone = '02111111111', Status = 'Open',
                         			LeadSource = 'Facebook', FB_Campaign_Name__c = 'RX Test Form', FB_Form_Name__c = 'RX Test Form');
        insert l;
        Lead resultLead = [select Id, FB_Lead_Error_Type__c , MatchedPersonAccount__c, Lead_Trigger_Log__c, Status,
                           FB_Form_Name__c, LeadSource, Selected_Dealer__c from Lead where Id = :l.Id limit 1];
        system.assertEquals(null, resultLead.FB_Lead_Error_Type__c );
        system.assertEquals('Processed', resultLead.Status);
        system.assertEquals(true, resultLead.Lead_Trigger_Log__c.contains('Matched Person Account found'));
        system.assertEquals(person.Id, resultLead.MatchedPersonAccount__c);
        //system.assertEquals(dealer.Id, resultLead.Selected_Dealer__c);
        
        Case createdCase = [select Id, SourceLead__c, Type, Selected_Dealer__c, Subject, Model__c, Source__c, Origin, AccountId from Case 
                            where SourceLead__c = : l.Id limit 1];
        system.assertEquals(resultLead.MatchedPersonAccount__c, createdCase.AccountId);
        system.assertEquals('Facebook Lead', createdCase.Type);
        system.assertEquals(l.FB_Form_Name__c.left(2), createdCase.Model__c);
        system.assertEquals(l.LeadSource, createdCase.Origin);
        system.assertEquals(l.LeadSource, createdCase.Source__c);
        
    }
    
    static testmethod void invalidState(){
        Account person = new Account(FirstName = 'Test', LastName = 'Test', PersonEmail = 'test@noreply.com',
                                    BillingPostalCode = 'XXTEST1010', PersonMobilePhone = '02111111111');
        insert person;
        Account dealer = new Account(Name = 'Test Dealer', RDR_Dealer_Code__c = '44444',
                                    RecordTypeId = SPCacheRecordTypeMetadata.getAccount_Dealer(),
                                    Status__c = 'Active', Type = 'Dealer');
        insert dealer;
        
        Lead l = new Lead(FirstName = 'Test', LastName = 'Test', Email = 'test@noreply.com',
                          			Preferred_Dealer__c = 'XXX - Test Dealer',
                                    PostalCode = 'XXTEST1010', MobilePhone = '02111111111', Status = 'Open',
                         			LeadSource = 'Facebook', FB_Campaign_Name__c = 'RX Test Form', FB_Form_Name__c = 'RX Test Form');
        insert l;
        Lead resultLead = [select Id, FB_Lead_Error_Type__c , MatchedPersonAccount__c, Lead_Trigger_Log__c, Status,
                           FB_Form_Name__c, LeadSource, Selected_Dealer__c from Lead where Id = :l.Id limit 1];
        system.assertEquals('Invalid State', resultLead.FB_Lead_Error_Type__c );
        system.assertEquals('Processed', resultLead.Status);
        Case createdCase = [select Id, SourceLead__c, Type, Selected_Dealer__c, Subject, Model__c, Source__c, Origin, AccountId from Case 
                            where SourceLead__c = : l.Id limit 1];
        system.assertEquals('Facebook Error', createdCase.Type);
    }
    
    static testmethod void dealerDoesNotMatchAccount(){
        Account person = new Account(FirstName = 'Test', LastName = 'Test', PersonEmail = 'test@noreply.com',
                                    BillingPostalCode = 'XXTEST1010', PersonMobilePhone = '02111111111');
        insert person;
        Account dealer = new Account(Name = 'Test Dealer', RDR_Dealer_Code__c = '44444',
                                    RecordTypeId = SPCacheRecordTypeMetadata.getAccount_Dealer(),
                                    Status__c = 'Active', Type = 'Dealer');
        insert dealer;
        
        Lead l = new Lead(FirstName = 'Test', LastName = 'Test', Email = 'test@noreply.com',
                          			Preferred_Dealer__c = 'XXX - No Match Account',
                                    PostalCode = 'XXTEST1010', MobilePhone = '02111111111', Status = 'Open',
                         			LeadSource = 'Facebook', FB_Campaign_Name__c = 'RX Test Form', FB_Form_Name__c = 'RX Test Form');
        insert l;
        Lead resultLead = [select Id, FB_Lead_Error_Type__c , MatchedPersonAccount__c, Lead_Trigger_Log__c, Status,
                           FB_Form_Name__c, LeadSource, Selected_Dealer__c from Lead where Id = :l.Id limit 1];
        system.assertEquals('Dealer does not match an account', resultLead.FB_Lead_Error_Type__c );
        system.assertEquals('Processed', resultLead.Status);
        Case createdCase = [select Id, SourceLead__c, Type, Selected_Dealer__c, Subject, Model__c, Source__c, Origin, AccountId from Case 
                            where SourceLead__c = : l.Id limit 1];
        system.assertEquals('Facebook Error', createdCase.Type);
    }
    
    static testmethod void campaignCreation(){
        Account person = new Account(FirstName = 'Test', LastName = 'Test', PersonEmail = 'test@noreply.com',
                                    BillingPostalCode = '1010', PersonMobilePhone = '02111111111');
        insert person;
        PMA__c pma = [select Id, PostCode__c, Dealer_Name__c from PMA__c
                      where Postcode__c = '1010' limit 1];
        // delete all campaigns to make sure non exists
        String expectedName = system.now().format('yyyy.MM') + ' Facebook ' + system.now().format('MMMM');
        delete [select Id from Campaign where Name = :expectedName];
        Lead l = new Lead(FirstName = 'Test', LastName = 'Test', Email = 'test@noreply.com',
                                    PostalCode = '1010', MobilePhone = '02111111111', Status = 'Open',
                         			LeadSource = 'Facebook', FB_Form_Name__c = 'RX Test Form');
        insert l;
        Lead resultLead = [select Id, Source_Campaign__c from Lead where Id = :l.Id limit 1];
        system.assertNotEquals(null, resultLead.Source_Campaign__c);
        Case createdCase = [select Id, Campaign_Source__c from Case 
                            where SourceLead__c = : l.Id limit 1];
        system.assertEquals(resultLead.Source_Campaign__c, createdCase.Campaign_Source__c);
        Campaign resultCampaign = [select Id, Name from Campaign where Id = :resultLead.Source_Campaign__c limit 1];
        system.assertEquals(expectedName, resultCampaign.Name);
        
        // now verify that the next lead inserted will be associated to the same campaign
         Lead l2 = new Lead(FirstName = 'Test', LastName = 'Test', Email = 'test@noreply.com',
                                    PostalCode = '1010', MobilePhone = '02111111111', Status = 'Open',
                         			LeadSource = 'Facebook', FB_Form_Name__c = 'RX Test Form');
        insert l2;
        Lead resultLead2 = [select Id, Source_Campaign__c from Lead where Id = :l2.Id limit 1];
        system.assertNotEquals(null, resultLead2.Source_Campaign__c);
        Case createdCase2 = [select Id, Campaign_Source__c from Case 
                            where SourceLead__c = : l2.Id limit 1];
        system.assertEquals(resultLead2.Source_Campaign__c, createdCase2.Campaign_Source__c);
        system.assertEquals(createdCase.Campaign_Source__c, createdCase2.Campaign_Source__c);
    }
	
    static testmethod void noMatchedAccount(){
        Account person = new Account(FirstName = 'Test', LastName = 'Test', PersonEmail = 'test@noreply.com',
                                    BillingPostalCode = 'XXTEST1010', PersonMobilePhone = '02111111111');
        insert person;
        Account dealer = new Account(Name = 'Test Dealer', RDR_Dealer_Code__c = '44444',
                                    RecordTypeId = SPCacheRecordTypeMetadata.getAccount_Dealer(),
                                    Status__c = 'Active', Type = 'Dealer');
        insert dealer;
        
        Lead l = new Lead(FirstName = 'NoM', LastName = 'Match', Email = 'nomatch@nomatch.com',
                          			Preferred_Dealer__c = 'NSW - Test Dealer',
                                    PostalCode = '2000', MobilePhone = '02111111111', Status = 'Open',
                         			LeadSource = 'Facebook', FB_Campaign_Name__c = 'RX Test Form', FB_Form_Name__c = 'RX Test Form');
        insert l;
        Lead resultLead = [select Id, FB_Lead_Error_Type__c , MatchedPersonAccount__c, Lead_Trigger_Log__c, Status,
                           FB_Form_Name__c, LeadSource, Selected_Dealer__c from Lead where Id = :l.Id limit 1];
        
        system.assertEquals(null, resultLead.FB_Lead_Error_Type__c );
        system.assertEquals('Processed', resultLead.Status);
        system.assertEquals(true, resultLead.Lead_Trigger_Log__c.contains('No Match found, creating a new one'));
        //system.assertEquals(dealer.Id, resultLead.Selected_Dealer__c);
        Case createdCase = [select Id, SourceLead__c, Type, Selected_Dealer__c, Subject, Model__c, Source__c, Origin, AccountId from Case 
                            where SourceLead__c = : l.Id limit 1];
        system.assertEquals(resultLead.MatchedPersonAccount__c, createdCase.AccountId);
        system.assertEquals('Facebook Lead', createdCase.Type);
        system.assertEquals(l.FB_Form_Name__c.left(2), createdCase.Model__c);
        system.assertEquals(l.LeadSource, createdCase.Origin);
        system.assertEquals(l.LeadSource, createdCase.Source__c);
    }
}