public with sharing class SP_HistoricalServiceDataProcessing
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Exception Classes & Enums
	public class SP_Exception extends Exception{}

	public enum BatchParam {ALL, ALL_ASYNC}

	private static integer iQueryLimit = 10;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class used to pass arguments into the Batch
	public class SP_Args
	{
		public BatchParam 	BatchParam 		{get; set;}
		public List<id>		ProcessList		{get; set;}
		public boolean 		UseSavePoint	{get; set;}
		public boolean 		AllOrNone		{get; set;}

		public SP_Args(list<id> theProcessList)
		{
			this.ProcessList 	= theProcessList;
			this.UseSavePoint 	= true;
			this.AllOrNone 		= false;
		}

		public SP_Args(list<id> theProcessList, BatchParam theBatchParam)
		{
			this.ProcessList 	= theProcessList;
			this.BatchParam		= theBatchParam;
			this.UseSavePoint 	= true;
			this.AllOrNone 		= false;
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class used to pass information back to what called the batch
	public class SP_Ret
	{
		public boolean  	InError							{get; set;}
		public List<string> ErrorList						{get; set;}
				
		public SP_Ret()
		{
			this.InError = false;
			this.ErrorList = new List<string>();
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class used for Service data
	public class SP_Service
	{
		public id			AccountId;
		public string		VIN;
		public datetime		ServiceDate;
		public decimal		Mileage;
		public string		Model;
		
		public SP_Service(id idAccountId, string strVIN, datetime dtServiceDate, decimal dMileage, string strModel)
		{
			AccountId		= idAccountId;
			VIN				= strVIN;
			ServiceDate		= dtServiceDate;
			Mileage			= dMileage;
			Model			= strModel;
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Singular
	SP_Args 	mArgs;
	SP_Ret		mRet;
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Collections
	public map<id, Account>						m_mapAccounts				=	new map<id, Account>();
	public list<SP_Service>						m_liServices				=	new list<SP_Service>();
	public set<string>							m_setVINs					=	new set<string>();
	public list<SP_Service>						m_liServicesForVIN			=	new list<SP_Service>();
	public map<string, list<SP_Service>>		m_mapVINToServices			=	new map<string, list<SP_Service>>();
	public list<Account>						m_liAccountsToUpdate		=	new list<Account>();

	public list<Case>	m_liCases				= new list<Case>();

	public list<decimal>						m_li10KServiceCost			=	new list<decimal>();
	public list<decimal>						m_li15KServiceCost			=	new list<decimal>();

	public map<string, list<decimal>>			m_mapModel10kServiceCost	=	new map<string, list<decimal>>();
	public map<string, list<decimal>>			m_mapModel15kServiceCost	=	new map<string, list<decimal>>();

	public map<string, decimal>					m_mapModelServiceAverage	=	new map<string, decimal>();

		
	/***********************************************************************************************************
		Constructor
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Our Constructor
	public SP_HistoricalServiceDataProcessing(SP_Args oArgs)
	{
		mRet 	= new SP_Ret();
		mArgs 	= oArgs;
	}
	
	/***********************************************************************************************************
		Access Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// The one and only entry point for the processor
	public SP_Ret ProcMain()
	{
		Savepoint savePoint;
		
		if(mArgs.UseSavePoint)
			savePoint = Database.setSavepoint();
		
		try
		{
			if(mArgs.BatchParam == BatchParam.ALL_ASYNC)
			{
				FireAsyncProcess();
				mRet.InError = false;
			}
			else
			{
				ProcessAccountObject(mArgs.ProcessList);
			}

		}
		catch(Exception e)
		{
			mRet.InError = true;
			mRet.ErrorList.add(e.getMessage());
						
			if(savePoint != null)
				Database.rollback(savePoint);
			
			throw new SP_Exception('Fatal Exception - All changes have been rolled back - ' + e.getMessage());
		}
		
		return mRet;
	}
	
	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	@future

	private static void FireAsyncProcess()
	{
/*
		SP_HistoricalServiceDataProcessing.SP_Args 	oArgs;
		SP_HistoricalServiceDataProcessing.SP_Ret	oRet;
		SP_HistoricalServiceDataProcessing			oProcessor;
		
		oArgs 				= new SP_HistoricalServiceDataProcessing.SP_Args(null, SP_HistoricalServiceDataProcessing.BatchParam.ALL);
		oArgs.AllOrNone 	= false;
		oArgs.UseSavePoint 	= true;
		
		oProcessor = new SP_HistoricalServiceDataProcessing(oArgs);
		
		oRet = oProcessor.ProcMain();
*/
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Process Account Objects
	private void ProcessAccountObject(list<id> li_AccountIdsIn)
	{
		// Set up the Service Cost data
		buildServiceCostData();
		
		// Build a list of all Accounts to be updated
		Account[] arrAccount =	[
									select	Id,
											Lastname,
											Service_Value__c 
									from	Account
									where	Id in :li_AccountIdsIn
									and		Service_Value__c = null
									and		RecordTypeId = :SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType()
									limit	:iQueryLimit
								];
		
		for (Account sAccount : arrAccount)
		{
			sAccount.Service_Value__c = 0;
			m_mapAccounts.put(sAccount.Id, sAccount);
		}

		// Build a list of Accounts and their Service records
		Service__c[] arrService =	[
										select	Customer_Account__c,
												Asset__r.Name,
												Service_Date__c,
												Vehicle_Mileage__c,
												Asset__r.Vehicle_Model__r.Name
										from	Service__c
										where	Customer_Account__c in :m_mapAccounts.keySet()
									];

		for (Service__c sService : arrService)
		{
			string strModel;

			if (null == (strModel = sService.Asset__r.Vehicle_Model__r.Name))
			{
				strModel = 'No Model name found';
				continue;
			}

			SP_Service sSPService = new SP_Service (	sService.Customer_Account__c,
														sService.Asset__r.Name,				// = VIN
														sService.Service_Date__c,
														sService.Vehicle_Mileage__c,
														strModel.toUppercase()
													);
			m_liServices.add(sSPService);
			m_setVINs.add(sSPService.VIN);
		}

		// Sort by VIN and date order
		m_liServices = doSPServiceSort(m_liServices);


		// Convert the Set of VINs to a list so we can iterate through it
		list<string> li_VINs = new list<string>();
		li_VINs.addAll(m_setVINs);

		// Combine these two collections into a map
		for (Integer i = 0; i < li_VINs.size(); i++)
		{
			m_liServicesForVIN =	new list<SP_Service>();
			
			for (Integer j = 0; j < m_liServices.size(); j++)
			{
				SP_Service sSPService = m_liServices.get(j);
				if (sSPService.VIN == li_VINs.get(i))
				{
					m_liServicesForVIN.add(sSPService);
				}
			}

			if (m_liServicesForVIN != null)
			{
				m_mapVINToServices.put(li_VINs.get(i), m_liServicesForVIN);
			}
		}

		// Now read through the map, inspecting all services for each VIN
		for (Integer i = 0; i < li_VINs.size(); i++)
		{
			m_liServicesForVIN		= m_mapVINToServices.get(li_VINs.get(i));

			// This shouldn't happen - but just in case...
			if (m_liServicesForVIN == null || m_liServicesForVIN.size() == 0)
				continue;

			// Always include the first service
			SP_Service sService = m_liServicesForVIN.get(0);

			if (!m_mapModelServiceAverage.containsKey(sService.Model))
			{
				continue;
			}

			Account sAccount		= m_mapAccounts.get(sService.AccountId);

			sAccount.Service_Value__c += getServiceCost(sService.Model, sService.Mileage);

			m_mapAccounts.remove(sService.AccountId);
			m_mapAccounts.put(sService.AccountId, sAccount);

			// Check date intervals for subsequent services
			for (Integer j = 1; j < m_liServicesForVIN.size(); j++)
			{
				datetime dtNineMonthsSinceLastService;
		
				SP_Service sService1 = m_liServicesForVIN.get(j-1);
				SP_Service sService2 = m_liServicesForVIN.get(j);
				
				if (sService1.ServiceDate.Month() < 4)
				{
					dtNineMonthsSinceLastService = sService1.ServiceDate.addMonths(9);
				}
				else
				{
					dtNineMonthsSinceLastService = sService1.ServiceDate.addMonths(-3).addYears(1);
				}
		
				if (!(sService2.ServiceDate < dtNineMonthsSinceLastService))
				{
					sAccount		= m_mapAccounts.get(sService.AccountId);
					sAccount.Service_Value__c += getServiceCost(sService2.Model, sService2.Mileage);
					m_mapAccounts.remove(sService.AccountId);
					m_mapAccounts.put(sService.AccountId, sAccount);
				}
			}
		}
		
		for (Integer k = 0; k < m_mapAccounts.size(); k++)
		{
			Account sAccount = m_mapAccounts.get(li_AccountIdsIn.get(k));
			if (sAccount != null)
			{
				m_liAccountsToUpdate.add(sAccount);
			}
		}

		if (!m_liAccountsToUpdate.isEmpty())
		{
			update m_liAccountsToUpdate;
		}

		if (!m_liCases.isEmpty())
		{
			insert m_liCases;
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Get service cost data
	private decimal getServiceCost(string strModel, decimal dMileage)
	{
		// If we do not have a value for the mileage, return the average cost
		if (dMileage == null || dMileage == 0)
		{
			return m_mapModelServiceAverage.get(strModel);
		}

		// Is this a model with a 10k interval service?
		if (m_mapModel10kServiceCost.containsKey(strModel))
		{
			integer i = dMileage.intValue()/10000;

			// See if we are less than 5000k over the service interval and round DOWN if we are
			if (((dMileage.intValue() - (i * 10000)) < 5000) && i != 0)
			{
				i -= 1;
			}
		
			if (i > m_mapModel10kServiceCost.get(strModel).size() -1)
			{
				// Mileage not in list - use the average
				return m_mapModelServiceAverage.get(strModel);
			}
			else
			{
				return m_mapModel10kServiceCost.get(strModel).get(i);
			}
		}

		// Is this a model with a 15k interval service?
		if (m_mapModel15kServiceCost.containsKey(strModel))
		{
			integer i = dMileage.intValue()/15000;

			// See if we are less than 5000k over the service interval and round DOWN if we are
			if (((dMileage.intValue() - (i * 15000)) < 5000) && i != 0)
			{
				i -= 1;
			}

			if (i > m_mapModel15kServiceCost.get(strModel).size() -1)
			{
				// Mileage not in list - use the average
				return m_mapModelServiceAverage.get(strModel);
			}
			else
			{
				return m_mapModel15kServiceCost.get(strModel).get(i);
			}
		}

		// error - model not found
		return 0;

	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	 Sort method
	public SP_Service[] doSPServiceSort(SP_Service[] li_SPSin)
	{
		map<string, SP_Service>					map_DateServiceIn	= new map<string, SP_Service>();
		list<string>							li_VINDate			= new list<string>();
		list<SP_Service>						li_SPSout			= new list<SP_Service>();

		for (integer i = 0; i < li_SPSin.size(); i++)
		{
			// Use a combination of VIN and Date to uniquely identify a service
			// This separates one customer's multiple VINs serviced on same day
			string strVINDate = li_SPSin[i].VIN + string.valueOf(li_SPSin[i].ServiceDate);
			map_DateServiceIn.put(strVINDate, li_SPSin[i]);
			li_VINDate.add(strVINDate);
		}

		li_VINDate.sort();

		// Build sorted list
		for (integer j = 0; j < li_VINDate.size(); j++)
		{
			// Get the service record
			SP_Service sSPS = map_DateServiceIn.get(li_VINDate[j]);
			li_SPSout.add(sSPS);
		}

		return li_SPSout;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Build service cost data
	private void buildServiceCostData()
	{
		// IS 200 service costs
		m_li10KServiceCost = new list<decimal>{	283.37, 311.97, 1101.77, 609.13, 283.37, 1130.37, 283.37, 769.18, 1101.77, 484.45, 283.37, 1367.03,
												283.37, 311.97, 1421.87, 769.18, 283.37, 1130.37, 283.37, 781.61, 283.37};
		m_mapModel10kServiceCost.put('IS200', m_li10KServiceCost);
		m_mapModelServiceAverage.put('IS200', 645.60);

		// IS 300
		m_li10KServiceCost = new list<decimal>{	310.02, 338.62, 1128.42, 632.04, 310.02, 1157.02, 310.02, 777.79, 1128.42, 638.92, 310.02, 1389.94, 
												310.02, 338.62, 1373.17, 935.09, 310.02, 1171.32, 310.02, 932.34, 290.02};
		m_mapModel10kServiceCost.put('IS300', m_li10KServiceCost);
		m_mapModelServiceAverage.put('IS300', 685.80);

		// RX 330
		m_li10KServiceCost = new list<decimal>{	365.05, 407.95, 1175.75, 572.87, 365.05, 1218.65, 365.05, 825.87, 1175.75, 651.93, 365.05, 1383.57, 
												365.05, 407.95, 1494.75, 1014.52, 365.05, 1218.65, 365.05, 831.15, 1175.75, 407.95};
		m_mapModel10kServiceCost.put('RX330', m_li10KServiceCost);
		m_mapModelServiceAverage.put('RX330', 767.16);

		// LX 470
		m_li10KServiceCost = new list<decimal>{	371.40, 677.09, 1231.60, 932.76, 371.40, 1776.32, 371.40, 1051.56, 1231.60, 941.75, 371.40, 1792.96, 
												371.40, 677.09, 1621.00, 1332.61, 371.40, 1507.29, 371.40, 1197.42, 1231.60, 657.09};
		m_mapModel10kServiceCost.put('LX470', m_li10KServiceCost);
		m_mapModelServiceAverage.put('LX470', 942.97);

		// LX 570
		m_li10KServiceCost = new list<decimal>{	456.76, 513.96, 1318.06, 826.86, 456.76, 1360.96, 456.76, 1074.36, 1318.06, 900.28, 456.76, 1673.86, 
												456.76, 588.76, 1318.06, 1364.21, 456.76, 1646.96, 456.76, 1113.08};
		m_mapModel10kServiceCost.put('LX570', m_li10KServiceCost);
		m_mapModelServiceAverage.put('LX570', 910.74);

		// LS 400
		m_li10KServiceCost = new list<decimal>{	338.29, 506.59, 1285.39, 809.53, 309.69, 1453.69, 338.29, 955.22, 1256.79, 712.73, 338.29, 1756.63, 
												309.69, 506.59, 1703.39, 988.83, 338.29, 1482.29, 309.69, 982.06, 318.29};
		m_mapModel10kServiceCost.put('LS400', m_li10KServiceCost);
		m_mapModelServiceAverage.put('LS400', 809.54);

		// LS 430
		m_li10KServiceCost = new list<decimal>{	306.75, 335.35, 1234.05, 628.94, 306.75, 1262.65, 306.75, 788.99, 1234.05, 541.49, 306.75, 1556.24, 
												306.75, 335.35, 1637.75, 788.99, 306.75, 1262.65, 306.75, 835.08, 1234.05, 315.35};
		m_mapModel10kServiceCost.put('LS430', m_li10KServiceCost);
		m_mapModelServiceAverage.put('LS430', 753.47);

		// ES 300
		m_li10KServiceCost = new list<decimal>{	422.25, 450.85, 1373.75, 634.55, 422.25, 1422.35, 422.25, 894.70, 1373.75, 566.13, 422.25, 1586.05, 
												422.25, 450.85, 1692.75, 894.70, 422.25, 1402.35, 422.25, 749.83, 1373.75, 430.85};
		m_mapModel10kServiceCost.put('ES300', m_li10KServiceCost);
		m_mapModelServiceAverage.put('ES300', 848.67);

		// These models are serviced at 15,000 km intervals

		// IS 250
		m_li15KServiceCost = new list<decimal>{400.41, 1323.98, 460.91, 1323.98, 559.31, 1869.58, 400.41, 1323.98, 475.21, 1633.03, 400.41, 1869.58, 400.41, 1323.98, 727.06, 1323.98};
		m_mapModel15kServiceCost.put('IS250', m_li15KServiceCost);
		m_mapModelServiceAverage.put('IS250', 988.51);

		// IS 250C
		m_li15KServiceCost = new list<decimal>{400.41, 1323.98, 460.91, 1323.98, 559.31, 1869.58, 400.41, 1323.98, 475.21, 1633.03, 400.41, 1869.58, 400.41, 1323.98, 727.06, 1323.98};
		m_mapModel15kServiceCost.put('IS250C', m_li15KServiceCost);
		m_mapModelServiceAverage.put('IS250C', 988.51);

		// IS 350
		m_li15KServiceCost = new list<decimal>{400.41, 1323.98, 460.91, 1323.98, 559.31, 1869.58, 400.41, 1323.98, 475.21, 1633.03, 400.41, 1869.58, 400.41, 1323.98, 727.06, 1323.98};
		m_mapModel15kServiceCost.put('IS350', m_li15KServiceCost);
		m_mapModelServiceAverage.put('IS350', 988.51);

		// RX 400H
		m_li15KServiceCost = new list<decimal>{450.85, 1238.45, 559.75, 1238.45, 668.10, 1648.53, 450.85, 1238.45, 559.75, 2124.61, 450.85, 1648.53, 450.85, 1238.45, 888.65, 1238.45};
		m_mapModel15kServiceCost.put('RX400H', m_li15KServiceCost);
		m_mapModelServiceAverage.put('RX400H', 1005.85);

		// RX 350
		m_li15KServiceCost = new list<decimal>{353.00, 730.30, 475.46, 730.30, 420.21, 1068.36, 353.00, 730.30, 475.46, 982.86, 353.00, 1068.36, 353.00, 730.30, 728.02, 730.30};
		m_mapModel15kServiceCost.put('RX350', m_li15KServiceCost);
		m_mapModelServiceAverage.put('RX350', 642.64);

		// RX 450H
		m_li15KServiceCost = new list<decimal>{381.60, 626.90, 489.95, 626.90, 927.75, 1265.45, 395.90, 626.90, 504.25, 1426.05, 395.90, 1265.45, 381.60, 626.90, 931.60, 626.90};
		m_mapModel15kServiceCost.put('RX450H', m_li15KServiceCost);
		m_mapModelServiceAverage.put('RX450H', 718.75);

		// GS 300
		m_li15KServiceCost = new list<decimal>{414.71, 1487.76, 525.93, 1441.35, 816.98, 2146.23, 414.71, 1416.81, 525.93, 2022.37, 414.71, 2146.23, 414.71, 1416.81, 1264.25, 1416.81};
		m_mapModel15kServiceCost.put('GS300', m_li15KServiceCost);
		m_mapModelServiceAverage.put('GS300', 1142.89);

		// GS 430
		m_li15KServiceCost = new list<decimal>{362.35, 1275.79, 422.85, 1275.79, 575.42, 1566.36, 362.35, 1275.79, 422.85, 2145.56, 362.35, 1542.43, 362.35, 1347.29, 774.52, 508.89};
		m_mapModel15kServiceCost.put('GS430', m_li15KServiceCost);
		m_mapModelServiceAverage.put('GS430', 911.43);

		// GS 450H
		m_li15KServiceCost = new list<decimal>{371.81, 1331.01, 473.23, 1331.01, 457.61, 1903.23, 371.81, 1331.01, 473.23, 1818.26, 371.81, 1903.23, 371.81, 1402.51, 974.78, 1402.51};
		m_mapModel15kServiceCost.put('GS450H', m_li15KServiceCost);
		m_mapModelServiceAverage.put('GS450H', 1018.05);

		// GS 460
		m_li15KServiceCost = new list<decimal>{525.56, 1537.84, 678.82, 1481.49, 805.73, 2224.32, 525.56, 1452.86, 678.82, 2077.36, 525.56, 2224.32, 525.56, 1452.86, 1323.09, 1452.86};
		m_mapModel15kServiceCost.put('GS460', m_li15KServiceCost);
		m_mapModelServiceAverage.put('GS460', 1218.29);

		// LS 460
		m_li15KServiceCost = new list<decimal>{647.66, 1919.62, 865.46, 1919.62, 983.66, 2727.02, 647.66, 1919.62, 865.46, 2464.62, 647.66, 2727.02, 647.66, 1919.62, 1396.16, 795.22};
		m_mapModel15kServiceCost.put('LS460', m_li15KServiceCost);
		m_mapModelServiceAverage.put('LS460', 1443.36);

		// LS 600HL
		m_li15KServiceCost = new list<decimal>{528.53, 1813.33, 835.03, 1813.33, 864.53, 2695.13, 528.53, 1813.33, 835.03, 2430.93, 528.53, 2695.13, 528.53, 1813.33, 1356.93, 1684.63};
		m_mapModel15kServiceCost.put('LS600HL', m_li15KServiceCost);
		m_mapModelServiceAverage.put('LS600HL', 1422.8);

		// SC 430
		m_li15KServiceCost = new list<decimal>{516.19, 1527.86, 587.14, 1527.86, 676.24, 1833.55, 516.19, 1527.86, 587.14, 2315.46, 516.19, 1833.55, 516.19, 1527.86, 753.79, 1527.86};
		m_mapModel15kServiceCost.put('SC430', m_li15KServiceCost);
		m_mapModelServiceAverage.put('SC430', 1143.18);

		// ISF
		m_li15KServiceCost = new list<decimal>{387.12, 571.37, 526.97, 571.37, 630.44, 1300.82, 387.12, 571.37, 526.97, 1048.66, 387.12, 1300.82, 387.12, 571.37, 1004.26, 571.37};
		m_mapModel15kServiceCost.put('ISF', m_li15KServiceCost);
		m_mapModelServiceAverage.put('ISF', 671.52);


	}

	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 
	public static testMethod void testSP_HistoricalServiceDataProcessing()
	{
	// Create records to be used in this test method

		Product2 sProduct1 = new Product2();
		sProduct1.Name					= 'IS200';
		insert sProduct1;

		Product2 sProduct2 = new Product2();
		sProduct2.Name					= 'SC430';
		insert sProduct2;

		Asset__c sAsset1 = new Asset__c();
		sAsset1.Name					= 'ABC12345678';
		sAsset1.Vehicle_Model__c		= sProduct1.Id;
		insert sAsset1;

		Asset__c sAsset2 = new Asset__c();
		sAsset2.Name					= 'XYZ98765432';
		sAsset2.Vehicle_Model__c		= sProduct2.Id;
		insert sAsset2;

		Asset__c sAsset3 = new Asset__c();
		sAsset3.Name					= 'DDDD8765432';
		sAsset3.Vehicle_Model__c		= sProduct1.Id;
		insert sAsset3;

		Asset__c sAsset4 = new Asset__c();
		sAsset4.Name					= 'JJJJ8765432';
		sAsset4.Vehicle_Model__c		= sProduct2.Id;
		insert sAsset4;

		list<Account>	li_Account		= new list<Account>();

		Account sAccount1 = new Account();
		sAccount1.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccount1.LastName				= 'TestLast1';
		sAccount1.PersonEmail			= 'test1@email.com';
		li_Account.add(sAccount1);

		Account sAccount2 = new Account();
		sAccount2.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccount2.LastName				= 'TestLast2';
		sAccount2.PersonEmail			= 'test2@email.com';
		li_Account.add(sAccount2);

		Account sAccount3 = new Account();
		sAccount3.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccount3.LastName				= 'TestLast3';
		sAccount3.PersonEmail			= 'test3@email.com';
		li_Account.add(sAccount3);

		insert li_Account;

		// Now we need to hit the dataase to get the ids of the Accounts we just created
		Account[] arrAccount =	[
									select	Id
									from	Account
									where	LastName = :'TestLast1'
									or		LastName = :'TestLast2'
									or		LastName = :'TestLast3'
									limit	3
								];

		list<id>		li_AccountIds	= new list<id>();

		for (Account sAccount : arrAccount)
		{
			li_AccountIds.add(sAccount.Id);
		}

		// Create a number of Service records
		list<Service__c> liService = new list<Service__c>();

		// Two customers, each with one vehicle, all services to be counted
		for (integer i=1; i<4; i++)
		{
			Service__c sService1			= new Service__c(); 
			sService1.Customer_Account__c	= sAccount1.Id;
			sService1.Asset__c				= sAsset1.Id;
			sService1.Vehicle_Mileage__c	= 7000 * i;
			sService1.Service_Date__c		= system.today().addYears(-(4-i));

			liService.add(sService1);

			Service__c sService2			= new Service__c(); 
			sService2.Customer_Account__c	= sAccount2.Id;
			sService2.Asset__c				= sAsset2.Id;
			sService2.Vehicle_Mileage__c	= 12345 * i;
			sService2.Service_Date__c		= system.today().addYears(-(4-i));

			liService.add(sService2);
			
		}

		// Now one customer with two vehicles, not all services to be counted
		// First service - counted
		Service__c sService3			= new Service__c(); 
		sService3.Customer_Account__c	= sAccount3.Id;
		sService3.Asset__c				= sAsset3.Id;
		sService3.Vehicle_Mileage__c	= 12345;
		sService3.Service_Date__c		= date.newInstance(2008, 1, 1);
		liService.add(sService3);

		// 4999 km past due - count as 30,000km service
		Service__c sService4			= new Service__c(); 
		sService4.Customer_Account__c	= sAccount3.Id;
		sService4.Asset__c				= sAsset3.Id;
		sService4.Vehicle_Mileage__c	= 34999;
		sService4.Service_Date__c		= date.newInstance(2008, 10, 2);
		liService.add(sService4);

		// 5000 km past due - count as 40,000 service
		Service__c sService5			= new Service__c(); 
		sService5.Customer_Account__c	= sAccount3.Id;
		sService5.Asset__c				= sAsset3.Id;
		sService5.Vehicle_Mileage__c	= 35000;
		sService5.Service_Date__c		= date.newInstance(2009, 8, 3);
		liService.add(sService5);

		// Outside mileage range - use average
		Service__c sService6			= new Service__c(); 
		sService6.Customer_Account__c	= sAccount3.Id;
		sService6.Asset__c				= sAsset3.Id;
		sService6.Vehicle_Mileage__c	= 300000;
		sService6.Service_Date__c		= date.newInstance(2010, 12, 31);
		liService.add(sService6);

		// Different vehicle - should count first service
		Service__c sService7			= new Service__c(); 
		sService7.Customer_Account__c	= sAccount3.Id;
		sService7.Asset__c				= sAsset4.Id;
		sService7.Vehicle_Mileage__c	= 121000;
		sService7.Service_Date__c		= date.newInstance(2007, 10, 1);
		liService.add(sService7);			

		// Mileage is zero - use average cost for model
		// Also, date is > 9 months since last service for this vehicle but < 9 months since last service for this customer
		Service__c sService8			= new Service__c(); 
		sService8.Customer_Account__c	= sAccount3.Id;
		sService8.Asset__c				= sAsset4.Id;
		sService8.Vehicle_Mileage__c	= 0;
		sService8.Service_Date__c		= date.newInstance(2008, 9, 1);
		liService.add(sService8);

		// Service < 9 months since last
		Service__c sService9			= new Service__c(); 
		sService9.Customer_Account__c	= sAccount3.Id;
		sService9.Asset__c				= sAsset4.Id;
		sService9.Vehicle_Mileage__c	= 265000;
		sService9.Service_Date__c		= date.newInstance(2009, 4, 1);
		liService.add(sService9);

		// Service > 9 months since last, same day as other vehicle for same customer
		Service__c sService10			= new Service__c(); 
		sService10.Customer_Account__c	= sAccount3.Id;
		sService10.Asset__c				= sAsset4.Id;
		sService10.Vehicle_Mileage__c	= 165000;
		sService10.Service_Date__c		= date.newInstance(2010, 12, 31);
		liService.add(sService10);

		insert liService;

		// Finally, do some testing!!

		// Set up argument and return parameters
		SP_Args						 	oArgs;
		SP_Ret 							oRet;
		SP_HistoricalServiceDataProcessing	oProcessor;
		
		// Test
		oArgs		= new SP_Args(li_AccountIds);
		oProcessor 	= new SP_HistoricalServiceDataProcessing(oArgs);
		oRet		= oProcessor.procMain();

		// Check value placed in Account records
		Account acct1 = [
							select	Service_Value__c 
							from	Account
							where	Id = :sAccount1.Id
						];

		Account acct2 = [
							select	Service_Value__c 
							from	Account
							where	Id = :sAccount2.Id
						];

		Account acct3 = [
							select	Service_Value__c 
							from	Account
							where	Id = :sAccount3.Id
						];

//		system.assertEquals(878, acct1.Service_Value__c, 'Test case 1 failed');
//		system.assertEquals(2631, acct2.Service_Value__c, 'Test case 2 failed');
//		system.assertEquals(5827, acct3.Service_Value__c, 'Test case 3 failed');

	}
}