global class GuestServicesWebService {
	global class OnDemandWSResponse {
        webservice Boolean success {get;set;}
        webservice Id recordId {get;set;} // already COD ID
        webservice String errCode {get;set;}
        webservice String errorDesc {get;set;}
        webservice String caseNumber {get;set;}
        webservice String redemptionType {get;set;}
        webservice Integer redemptionsRemaining {get;set;}
        webservice Id partnerId {get;set;}
        webservice String offerDescription {get;set;}
        webservice Date redemptionExpiration {get;set;}
        
        global OnDemandWSResponse(){
            success = false;
            recordId = null;
            partnerId = null;
            errCode = null;
            errorDesc = null;
            caseNumber = null;
            redemptionType = null;
            redemptionsRemaining = null;
            offerDescription = null;
            redemptionExpiration = null;
        }
    }
    
    global class VoucherAvailabilityWSResponse {
        webservice Boolean success {get;set;}
        webservice Id recordId {get;set;}  // vooID
        webservice Id partnerId {get;set;}
        webservice String voucherType {get;set;}
        webservice Integer vouchersAvailable {get;set;}
        webservice Date vouchersExpiration {get;set;}
        webservice String rego {get;set;}
        webservice String model {get;set;}
        webservice String errCode {get;set;}
        webservice String errorDesc {get;set;}
        webservice String caseNumber {get;set;}
        global VoucherAvailabilityWSResponse(){
            success = false;
            recordId = null;
            errCode = null;
            errorDesc = null;
            caseNumber = null;
            partnerId = null;
        	voucherType = null;
        	vouchersAvailable = null;
        	vouchersExpiration = null;
            rego = null;
        	model = null;
        }
    }
    
    global class PartnerBranchesWSResponse {
        webservice Boolean success {get;set;}
        webservice Id recordId {get;set;}
        webservice Id partnerId {get;set;}
        webservice String partnerName {get;set;}
        webservice String partnerCategory {get;set;}
        webservice String offerType {get;set;}
        webservice String offerId {get;set;}
        webservice String partnerStatus {get;set;}
        webservice Integer vehicleOfferLimit {get;set;}
        webservice Id branchId {get;set;}
        webservice String branchName {get;set;}
        webservice String branchStreet {get;set;}
        webservice String branchCity {get;set;}
        webservice String branchState {get;set;}
        webservice String branchPostalCode {get;set;}
        webservice String branchPhone {get;set;}
        webservice String branchLocation {get;set;}
        webservice String branchValetEntryPoint {get;set;}
        webservice Decimal branchLatitude {get;set;}
        webservice Decimal branchLongitude {get;set;}
        webservice String branchValetDescription {get;set;}
        webservice String branchStatus {get;set;}
        webservice String errCode {get;set;}
        webservice String errorDesc {get;set;}
        webservice String caseNumber {get;set;}
        
        global PartnerBranchesWSResponse(){
            success = false;
            recordId = null;
            errCode = null;
            errorDesc = null;
            caseNumber = null;
            partnerId = null;
        	partnerName = null;
        	partnerCategory = null;
        	offerType = null;
            offerId = null;
            partnerStatus = null;
        	vehicleOfferLimit = null;
            branchId = null;
            branchName = null;
            branchStreet = null;
            branchCity = null;
            branchState = null;
            branchPostalCode = null;
            branchPhone = null;
            branchLocation = null;
            branchValetEntryPoint = null;
            branchLatitude = null;
            branchLongitude = null;
            branchValetDescription = null;
            branchStatus = null;
        }
    }
    
    webservice static List<OnDemandWSResponse> getOnDemandAvailability(Id guestAcctId, String gigyaId, Id codId){
        return OnDemandHandler.getOnDemandAvailability(guestAcctId, gigyaId, codId);
    }
    
    webservice static OnDemandWSResponse writeOnDemandTransactions(Id sfAcctId, String gigyaId, Id codId, String redemptionType, String redemptionLocation, Integer redemptionCredits, 
                                                                   Datetime redStartDateTime, Decimal redStartOffset, String redStartText, 
                                                                   Datetime redEndDateTime, Decimal redEndOffset, String redEndText, 
                                                                   String redemptionModel, String bookingReference, String redemptionDescription, 
                                                                   String bookingStatus, String redemptionRego){
        return OnDemandHandler.writeOnDemandTransactions(sfAcctId, gigyaId, codId, redemptionType, redemptionLocation, redemptionCredits, 
                                                         redStartDateTime, redStartOffset, redStartText, 
                                                         redEndDateTime, redEndOffset, redEndText, 
                                                         redemptionModel, bookingReference, redemptionDescription, 
                                                         bookingStatus, redemptionRego);
    }
    
    webservice static OnDemandWSResponse editOnDemandTransactions(String bookingReference, String redemptionLocation, Datetime redStartDateTime, Decimal redStartOffset, String redStartText, 
                                                                   Datetime redEndDateTime, Decimal redEndOffset, String redEndText, String redemptionModel, String redemptionDescription,String redemptionRego){
        return OnDemandHandler.editOnDemandTransactions(bookingReference, redemptionLocation, redStartDateTime, redStartOffset, redStartText, 
                                                                  redEndDateTime, redEndOffset, redEndText, redemptionModel, redemptionDescription, redemptionRego);
    }
    webservice static List<VoucherAvailabilityWSResponse> getGuestVoucherAvailability(String gigyaId, Id sfVOId){
        return OnDemandHandler.getGuestVoucherAvailability(gigyaId, sfVOId);
    }
    
    webservice static List<PartnerBranchesWSResponse> getPartnersandBranches (){
        return OnDemandHandler.getPartnersandBranches();
    }
    
    webservice static OnDemandWSResponse cancelOnDemandTransaction(Id sfAcctId, String gigyaId, String bookingReference, Boolean creditCustomer){
        return OnDemandHandler.cancelOnDemandTransaction(sfAcctId, gigyaId, bookingReference, creditCustomer);
    }
}