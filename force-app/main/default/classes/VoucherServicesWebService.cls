/*
 * 700 - Required fields
 * 800 - DML error
 * 601 - No Partner Account for the provided username and password combination
 * 602 - No partner branch found for Partner Branch ID provided
 * 603 - partner Branch ID provided is invalid
 * 604 - No active Partner Branch Account found
 * 605 - No Active Offers Found
 * 606 - No Active Vehicle Ownerships found for the Rego
 * 607 - Vehicle not enrolled in program
 * 608 - Vouchers are expired
 * 609 - No available vouchers
 * */
global class VoucherServicesWebService {
    global class VoucherServiceResult {
        webservice Boolean success {get;set;}
        webservice Id recordId {get;set;}
        webservice String errCode {get;set;}
        webservice String errorDesc {get;set;}
        webservice String caseNumber {get;set;}
        
        global VoucherServiceResult(){
            success = false;
            recordId = null;
            errCode = null;
            errorDesc = null;
            caseNumber = null;
        }
    }
    
    global class VoucherAvailabilityResult {
        webservice Id recordId {get;set;}
        webservice String model {get;set;}
        webservice Date voucherExpiryDate {get;set;}
        webservice Integer vouchersRemaining {get;set;}
        webservice Boolean noCompAllowed {get;set;}
        webservice String caseNumber {get;set;}
        webservice String errCode {get;set;}
        webservice String errorDesc {get;set;}
        webservice Boolean success {get;set;}
        
        global VoucherAvailabilityResult() {
            recordId = null;
            model = null;
            voucherExpiryDate= null;
            vouchersRemaining = null;
            noCompAllowed = false;
            caseNumber = null;
            errCode = null;
            errorDesc = null;
            success = false;
        }
    }
    
    webservice static VoucherServiceResult attendantLogin(String partnerBranchUserName, String partnerBranchPassword){
        return VoucherServicesWSHandler.attendantLogin(partnerBranchUserName, partnerBranchPassword);
    }
    
    
    webservice static VoucherServiceResult resendPassword(String partnerBranchIdentifier){
        return VoucherServicesWSHandler.resendPassword(partnerBranchIdentifier);
    }
    
    
    webservice static VoucherServiceResult redeemVoucher(Id partnerBranchId, String voucherType, Id vOOfferDetailID, Boolean compFlag, String compComment,String errCode){
        return VoucherServicesWSHandler.redeemVoucher(partnerBranchId, voucherType, vOOfferDetailID, compFlag, compComment, errCode);
    }
    
    
    webservice static List<VoucherAvailabilityResult> getVoucherAvailability(Id partnerBranchId, String regoNumber, String voucherType){
        return VoucherServicesWSHandler.getVoucherAvailability(partnerBranchId, regoNumber, voucherType);
    }

}