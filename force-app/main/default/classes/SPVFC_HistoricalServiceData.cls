public with sharing class SPVFC_HistoricalServiceData
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Exception Classes & Enums
	public class SP_Exception extends Exception{}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Singular
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Collections


	/***********************************************************************************************************
		Constructor
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Our Constructor
	public SPVFC_HistoricalServiceData()
	{

	}


	/***********************************************************************************************************
		Access Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public Pagereference startHistoricalDataBatch()
	{
		SP_HistoricalServiceDataBatch objSP_HistoricalServiceDataBatch = new SP_HistoricalServiceDataBatch();
        ID batchprocessid = Database.executeBatch(objSP_HistoricalServiceDataBatch);

//		string strSchedule = '0 30 * * * ?';
//		System.Schedule('SP_HistoricalServiceDataSchedule', strSchedule, new SP_HistoricalServiceDataSchedule());

		return null;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	public pageReference actionCancel()
	{
		pageReference pageRedirect;

		pageRedirect = new PageReference('/o');
	   	pageRedirect.setRedirect(true);
   		return pageRedirect;
	}


	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 
	public static testMethod void testSPVFC_WarrantyData()
	{
		SPVFC_HistoricalServiceData hsd = new SPVFC_HistoricalServiceData();

		Test.startTest(); 
		hsd.startHistoricalDataBatch();
		Test.stopTest(); 
		
		hsd.actionCancel();
	}
	
}