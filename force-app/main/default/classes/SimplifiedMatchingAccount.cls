global class SimplifiedMatchingAccount 
{
	private static string OWNER_TYPE			= 'Owner';
	private static string PROSPECT_TYPE			= 'Prospect';
	
	private static string MATCH					= 'Match';
	private static string MISMATCH				= 'Mismatch';
	
	private static string RULE_01				= 'YYYYY';
	private static string RULE_02				= 'YYYYN';
	private static string RULE_03				= 'YYYNY';
	private static string RULE_04				= 'YYYNN';
	private static string RULE_05				= 'YYNYY';
	private static string RULE_06				= 'YYNYN'; 
	private static string RULE_07				= 'YYNNY';
	private static string RULE_08				= 'YYNNN';
	
	private static string RULE_09				= 'YNYYY';
	private static string RULE_10				= 'YNYYN';
	private static string RULE_11				= 'YNYNY';
	private static string RULE_12				= 'YNYNN';
	private static string RULE_13				= 'YNNYY';
	private static string RULE_14				= 'YNNYN';
	private static string RULE_15				= 'YNNNY';
	private static string RULE_16				= 'YNNNN';	
		 
	private static string RULE_17				= 'NYYYY';
	private static string RULE_18				= 'NYYYN';
	private static string RULE_19				= 'NYYNY';
	private static string RULE_20				= 'NYYNN';
	private static string RULE_21				= 'NYNYY';
	private static string RULE_22				= 'NYNYN';
	private static string RULE_23				= 'NYNNY';
	private static string RULE_24				= 'NYNNN';
	
	private static string RULE_25				= 'NNYYY';
	private static string RULE_26				= 'NNYYN';
	private static string RULE_27				= 'NNYNY';
	private static string RULE_28				= 'NNYNN';
	private static string RULE_29				= 'NNNYY';
	private static string RULE_30				= 'NNNYN';
	private static string RULE_31				= 'NNNNY';
	private static string RULE_32				= 'NNNNN';	
	
	private static string NO					= 'N';
	private static string YES					= 'Y';		
	
	private static Integer PHONE_FORMAT_COUNT	= 9; // added by Tuan [23-Feb-2015]
	
	// Method to get non-null value for a string
	private static string getValue(String strValue)
	{
		if(strValue != null)
			return strValue;
		else
			return '';  
	}
	
	// Method to format a phone
	private static string getFormattedPhone(string phoneNumber, integer formatNumber)
	{
		if (phoneNumber == null)
			phoneNumber = '';
		else
		{	
	   		phoneNumber = phoneNumber.trim();
	   		
	   		if (phoneNumber != '' && (phoneNumber.length() < 4 || phoneNumber.substring(0, 4) == '****'))
	   			phoneNumber = '';
   		
	   		if(phoneNumber != '' && phoneNumber.length()> 5)
		    {  
		    	Pattern phonePattern = Pattern.compile('[+-/(/)/ ]');
		        phoneNumber = phonePattern.matcher(phoneNumber).replaceAll('');
		        
		        if(phoneNumber.length() > formatNumber) {
		        	phoneNumber = phoneNumber.substring(phoneNumber.length() - formatNumber);
					//phoneNumber = '0'+ phoneNumber;  // added by Tuan [23-Feb-2015] 
		        }
		        	
		        system.debug('##### Phone : ' + phoneNumber);     
		    }
		}
		
		return phoneNumber;
	}	
	
	// 
	private static list<string> getFormattedPhoneCombinations(string phoneNumber)
	{
		phoneNumber = getFormattedPhone(phoneNumber, 6);
		
		list<string> allPhoneCombinations = new list<string>();
		
		allPhoneCombinations.add(phoneNumber);
		
		if (phoneNumber != '')
		{
			for (list<string> phoneCombiList : getPhoneCombinations(phoneNumber))
			{
				string phoneCombi = '';
				
				for (string combiPart : phoneCombiList)
				{
					if (phoneCombi == '')
						phoneCombi = combiPart;
					else
						phoneCombi += (' ' + combiPart);
				}
				
				allPhoneCombinations.add(phoneCombi);
				allPhoneCombinations.add(phoneCombi.replaceAll(' ', '-'));
			}
		}
		return allPhoneCombinations; 		
	}
	
	// Method to get the different format of the given phone number
	private static list<list<string>> getPhoneCombinations(string phoneNumber)
	{
		list<list<string>> mainList = new list<list<string>>();
		
		for (integer i = 0; i < phoneNumber.length(); i++)
		{
			if (i > phoneNumber.length() - 3)
				break;
					
			string strPart1 = phoneNumber.substring(0, i + 1);
			
			list<string> twoParts = new list<string>();
			
			twoParts.add(strPart1);
			twoParts.add(phoneNumber.substring(i + 1));
			
			mainList.add(twoParts);
			
			if (strPart1.length() > 2)
			{
				list<list<string>> twoPartsOfPart1 = getPhoneCombinations(strPart1);
				
				for (list<string> twoPartLists : twoPartsOfPart1)
				{
					twoPartLists.add(phoneNumber.substring(i + 1));
					mainList.add(twoPartLists);
				}
					
			}
		}	
		
		return 	mainList;
	}
	
	// Method to get the Ids of all accounts having the specified VIN
	private static set<id> matchVIN(string strVIN)
	{
		set<id>		set_VINIds	= new set<Id>();
		list<Id>	li_VINIds	= new list<Id>{};

		if(strVIN != null && strVIN != '')
		{
			for (Asset__c[] arrAsset :	[
											select	Id
											from	Asset__c 
											where	Name = :strVIN
										])
			{
				for(Asset__c sAsset : arrAsset)
				{
					li_VINIds.add(sAsset.Id);
				}
			}

			// If we don't have an Asset that matches the input VIN, we are probably being called from RDR integration
			// In any case we need to return an empty set of VIN ids
			if (li_VINIds.isEmpty())
			{
				return set_VINIds;
			}

			for (Vehicle_Ownership__c[] arrVO :	[
													select	AssetID__c,
															Customer__c
													from	Vehicle_Ownership__c
													where	AssetID__c in :li_VINIds
													and		Status__c = 'Active'
												])
			{
				for(Vehicle_Ownership__c sVO : arrVO)
				{
					set_VINIds.add(sVO.Customer__c);
				}
			}
		}       
		return set_VINIds; 
	}
	
	// Method to create a Case in case of possible matches
	private static void createCase(set<ID> matchIds, string strVIN, string strLastName, 
							string strEmail, string strMobile, string strStreet,
							string strCity, string strPostCode, string matchRule)
	{
		string potentialMatches = ''; 
		for (ID accId : matchIds)
		{
			if (potentialMatches == '')
				potentialMatches = accId;
			else
				potentialMatches += (',' + accId);
		}
		
		Account acc = getLatestAccount(matchIds);
		
		Case sCase = new Case();

		sCase.RecordTypeId		= SPCachecustomerMatchingMetadata.getDefaultCaseRecordType();		// Customer Matching
		sCase.Type				= SPCachecustomerMatchingMetadata.getDefaultCaseType();				// Data Verification
		sCase.OwnerId			= acc.OwnerId;														// Owner of the matching Account
		sCase.Status			= SPCachecustomerMatchingMetadata.getDefaultCaseStatus();			// In Progress
		sCase.Origin			= SPCachecustomerMatchingMetadata.getDefaultCaseOrigin();			// Customer Matching
		sCase.AccountId			= acc.Id;
		sCase.Subject			= 'Customer matching';
		sCase.Description		= 	'Data in - VIN: ' + strVIN +
									' Last Name: ' + strLastName +
									' Email: ' + strEmail +
									' Mobile: ' + strMobile +
									' Street: ' + strStreet +
									' City: ' + strCity +
									' PostCode: ' + strPostCode +
									' Potential Matches: ' + potentialMatches +
									' Matching Rule: ' + matchRule;
		
		insert sCase;
		
		system.debug('######### newCase ' + sCase);
	}
	
	// Method to get the street number from street string
	private static string getStreetNumberFromStreet(string strStreet)
	{
		list<string> liAddressParts = strStreet.split(' ');
					
		string strUnitNumber = '';
					
		if (liAddressParts.size() == 2)
			strUnitNumber = liAddressParts[0];
		else if (liAddressParts.size() > 2)
		{
			for (integer i = 0; i < liAddressParts.size() - 2; i++)
			{
				if (!string.isBlank(liAddressParts[i]))
					strUnitNumber += liAddressParts[i].trim();
			}
		}
		
		return strUnitNumber;
	}
	
	// Method to get the final matched account from the individual matched account Ids
	private static string getMatchedAccountIds(list<set<ID>> matchedAccIdsForEachField, map<ID, Account> mapAccounts, 
												string strVIN, string strFirstName, string strLastName, 
												string strEmail, string strMobile, string strStreet,
												string strCity, string strPostCode, boolean createCase)
	{
		// Map of all rules to the decisions
		map<string, string> map_RuleToResult = new map<string, string>();
		 
	 	map_RuleToResult.put(RULE_01, MATCH);
	 	map_RuleToResult.put(RULE_02, MATCH);
	 	map_RuleToResult.put(RULE_03, MATCH);
	 	map_RuleToResult.put(RULE_04, MISMATCH);
	 	map_RuleToResult.put(RULE_05, MATCH);
	 	map_RuleToResult.put(RULE_06, MATCH);
	 	map_RuleToResult.put(RULE_07, MATCH);
 		map_RuleToResult.put(RULE_08, MISMATCH);
 
 		map_RuleToResult.put(RULE_09, MATCH);
 		map_RuleToResult.put(RULE_10, MATCH);
 		map_RuleToResult.put(RULE_11, MATCH);
 		map_RuleToResult.put(RULE_12, MISMATCH);
 		map_RuleToResult.put(RULE_13, MATCH);
 		map_RuleToResult.put(RULE_14, MISMATCH);
 		map_RuleToResult.put(RULE_15, MISMATCH);
		map_RuleToResult.put(RULE_16, MISMATCH);
 
 		map_RuleToResult.put(RULE_17, MATCH);
 		map_RuleToResult.put(RULE_18, MATCH);
 		map_RuleToResult.put(RULE_19, MATCH);
 		map_RuleToResult.put(RULE_20, MISMATCH);
 		map_RuleToResult.put(RULE_21, MATCH);
 		map_RuleToResult.put(RULE_22, MATCH);
 		map_RuleToResult.put(RULE_23, MISMATCH);
 		map_RuleToResult.put(RULE_24, MISMATCH);
 
 		map_RuleToResult.put(RULE_25, MATCH);
 		map_RuleToResult.put(RULE_26, MATCH);
 		map_RuleToResult.put(RULE_27, MISMATCH);
 		map_RuleToResult.put(RULE_28, MISMATCH);
 		map_RuleToResult.put(RULE_29, MISMATCH);
 		map_RuleToResult.put(RULE_30, MISMATCH);
 		map_RuleToResult.put(RULE_31, MISMATCH);
 		map_RuleToResult.put(RULE_32, MISMATCH);		 
		 		
		set<ID> set_resultantMatches = null;
		string matchedRule = '';
		
		// For each individual matched Id set	
		for (integer i = 0; i < matchedAccIdsForEachField.size(); i++)
		{
			// Matched Id set
			set<ID> thisFieldMatches = matchedAccIdsForEachField[i];
			
			// If there is no matched Ids fot a parameter, result is NO			
			if (thisFieldMatches == null || thisFieldMatches.isEmpty())
				matchedRule += NO;
			// Otherwise
			else
			{	
				// If there is matched Id for any of the previous param
				if (set_resultantMatches != null)
				{
					set<ID> set_resultantMatchesCloned = set_resultantMatches.clone();
					// Collect the intersection of the previous resultant matched Id set and matched Id set of this parameter
					set_resultantMatches.retainAll(thisFieldMatches);
					// If the intersection is empty, there is no match for this param
					// So, keep the existing previous resultant matched Id set
					if (set_resultantMatches.isEmpty())
					{
						matchedRule += NO;
						set_resultantMatches = set_resultantMatchesCloned;
					}
					// If the intersection is not empty it means that there is a match. So, the result is YES
					// Set the intersection as the resultant matched Id set
					else
						matchedRule += YES;
				}
				// For the first param, since it has matches, the result is always YES
				// Set 
				else
				{
					matchedRule += YES;
					// Set the matched Id set as the resultant matched Id set
					set_resultantMatches = thisFieldMatches;
				}
			}
		}
		
		if (set_resultantMatches == null)
			set_resultantMatches = new set<ID>();
				
		if (matchedRule != '')
		{
			// In case of Rule 29, we have to consider First Name and Street Number
			if (matchedRule == RULE_29)
			{
				set<ID> setMatches = new set<ID>();
				
				// Find the records with same first name among the resultant matched records for Rule 29
				for (Id accId : set_resultantMatches)
				{
					if (mapAccounts.get(accId).FirstName.toUpperCase() == strFirstName.toUpperCase())
						setMatches.add(accId);
				}
				
				set_resultantMatches = setMatches;
				
				// If there is no matched record for first name, it is a mismatch
				if (set_resultantMatches.isEmpty())
				{
					system.debug('############# matchedRule ' + matchedRule + ' (No First Name Match)');
					system.debug('############# Result Mismatch (No First Name Match)');
				}
				// If thereis a match for first name, find the records with same street number among first name matched records
				else
				{
					set<ID> setStNumMatches = new set<ID>();
					
					string streetNumber = getStreetNumberFromStreet(strStreet);
					
					for (Id accId : set_resultantMatches)
					{
						Account acct = mapAccounts.get(accId);
						
						if ((!string.isBlank(acct.BillingStreet) && getStreetNumberFromStreet(acct.BillingStreet).toUpperCase() == streetNumber.toUpperCase()) ||
							(!string.isBlank(acct.ShippingStreet) && getStreetNumberFromStreet(acct.ShippingStreet).toUpperCase() == streetNumber.toUpperCase()))
							setStNumMatches.add(accId);
					}	
					
					set_resultantMatches = setStNumMatches;
					
					// If there is no match for Street Number, it is a mismatch (Two personss with same name can live in the same street)
					if (set_resultantMatches.isEmpty())
					{
						system.debug('############# matchedRule ' + matchedRule + ' (First Name Match, No Street Number Match)');
						system.debug('############# Result Mismatch (First Name Match, No Street Number Match)');	
					}	
					// Otherwise, it is a match
					else
					{
						system.debug('############# matchedRule ' + matchedRule + ' (First Name and Street Number Match)');
						system.debug('############# Result Match (First Name and Street Number Match)');	
					}
				}
			}
			// For all other rules
			else
			{
				system.debug('############# matchedRule ' + matchedRule);
				
				string strResult = map_RuleToResult.get(matchedRule);
				
				system.debug('############# Result ' + strResult);
				
				// If it is a mismatch, fo certain rules, a case must be created, if necessary
				if (strResult == MISMATCH)
				{
					if (set_resultantMatches.size() > 0 && createCase && 
						(matchedRule == RULE_04 || matchedRule == RULE_08 ||
						 matchedRule == RULE_12 || matchedRule == RULE_14 ||
						 matchedRule == RULE_15 || matchedRule == RULE_20 ||
						 matchedRule == RULE_23	|| matchedRule == RULE_24 ||
						 matchedRule == RULE_27 || matchedRule == RULE_28))
						 createCase(set_resultantMatches, strVIN, strLastName, strEmail, strMobile, strStreet, strCity, strPostCode, matchedRule);
						 
					set_resultantMatches.clear();
				}
			}
		}
		
       if (set_resultantMatches.isEmpty())
       {
        	system.debug('############ No Matching Account');        	
        	return null;
        }
        else
		{   
			system.debug('############# Final Matched Accounts ' + set_resultantMatches);
			return getLatestAccount(set_resultantMatches).Id;
 		}
	}	
	
	// Method to get the latest among the matched ones
	private static Account getLatestAccount(set<ID> set_resultantMatches)
	{
		Account prospect = null;
		Account other = null;
		
		// First 	Pref: Last Updated Owner
		// Second 	Pref: Last Updated Prospect
		     
    	for (Account sAccount : [select  Id, Type, OwnerId
                        		from    Account
                        		where   Id in :set_resultantMatches
                        		order by LastModifiedDate desc])
       	{
       		if (sAccount.Type == OWNER_TYPE)
       		{
       			system.debug('############ Matching Owner: ' + sAccount.Id);           			
       			return sAccount;
       		}
       		else if (sAccount.Type == PROSPECT_TYPE && prospect == null)
       			prospect = sAccount;
       		else if (other == null)
       			other = sAccount;
       	}  
       	
       	if (prospect != null)
       	{
       		system.debug('############ Matching Prospect: ' + prospect.Id);
        	return prospect;
       	}
       	else
       	{
         	system.debug('############ Matching Other Account: ' + other.Id);
       		return other;
       	}		
	}	
	
	private static void checkAndSetAccountForAddressMatch(string strStreet, ID accountId, map<string, set<id>>	mapStreetToAcct)
	{
		string strStreetName;
		list<string> li_StreetParts = strStreet.split(' ');
		
		if (!li_StreetParts.isEmpty())
		{
			if (li_StreetParts.size() <= 2)
			{
				strStreetName = li_StreetParts[li_StreetParts.size()  - 1].toUpperCase();
			}
			else
			{
				integer streetPartIndx = li_StreetParts.size() - 2;
				strStreetName = li_StreetParts[streetPartIndx].toUpperCase();
			}
							
			system.debug('###### ST NAME ' + strStreetName);
							
			set<ID> accIds = mapStreetToAcct.get(strStreetName);
			if (accIds == null)
				accIds = new set<ID>();
								
			accIds.add(accountId);

			mapStreetToAcct.put(strStreetName, accIds);		
		}
	}
	
	// Method to get the records with match for address
	private static set<id> matchAddress(list<Account> listCityAndPostCodeMatchedAccounts, string strStreet)
	{
		map<string, set<id>>		mapStreetToAcct	= new map<string, set<id>>();	
		set<id>		set_AddressIds = new set<id>{};


				for(Account sAccount : listCityAndPostCodeMatchedAccounts)
				{
					// Get the second string only from the address
					string strStreetName;
					
					

					if (sAccount.ShippingStreet != null)
					{
						
						/*list<string> li_ShippingStreetParts = sAccount.ShippingStreet.split(' ');
				
						if (!li_ShippingStreetParts.isEmpty())
						{
							if (li_ShippingStreetParts.size() <= 2)
							{
								strStreetName = li_ShippingStreetParts[li_ShippingStreetParts.size()  - 1].toUpperCase();
							}
							else
							{
								integer streetPartIndx = li_ShippingStreetParts.size() - 2;
								strStreetName = li_ShippingStreetParts[streetPartIndx].toUpperCase();
							}
							
							system.debug('###### ST NAME ' + strStreetName);
							
							set<ID> accIds = mapStreetToAcct.get(strStreetName);
							if (accIds == null)
								accIds = new set<ID>();
								
							accIds.add(sAccount.Id);

							mapStreetToAcct.put(strStreetName, accIds);

						}*/
						
						checkAndSetAccountForAddressMatch(sAccount.ShippingStreet, sAccount.Id, mapStreetToAcct);
					}

					if (sAccount.BillingStreet != null)
					{
						/*list<string> li_BillingStreetParts = sAccount.BillingStreet.split(' ');

						if (!li_BillingStreetParts.isEmpty())
						{
							if (li_BillingStreetParts.size() <= 2)
							{
								strStreetName = li_BillingStreetParts[li_BillingStreetParts.size() - 1].toUpperCase();
							}
							else
							{
								integer streetPartIndx = li_BillingStreetParts.size() - 2;
								strStreetName = li_BillingStreetParts[streetPartIndx].toUpperCase();
							}
							
							system.debug('###### ST NAME ' + strStreetName);
							
							
							set<ID> accIds = mapStreetToAcct.get(strStreetName);
							if (accIds == null)
								accIds = new set<ID>();
								
							accIds.add(sAccount.Id);

							mapStreetToAcct.put(strStreetName, accIds);							
						}*/
						checkAndSetAccountForAddressMatch(sAccount.BillingStreet, sAccount.Id, mapStreetToAcct);
					}
				}
			
		
		
		system.debug('######### mapStreetToAcct ' + mapStreetToAcct);

		list<string> li_StreetParts = strStreet.split(' ');
		
		system.debug('######### li_StreetParts ' + li_StreetParts);

		if (!li_StreetParts.isEmpty())
		{
			string strStreetIn;
					
			if (li_StreetParts.size() <= 2)
			{
				strStreetIn = li_StreetParts[li_StreetParts.size() - 1].toUpperCase();
				system.debug('######### strStreetIn ' + strStreetIn);
			}
			else
			{
				strStreetIn = li_StreetParts[li_StreetParts.size() - 2].toUpperCase();
				system.debug('######### strStreetIn ' + strStreetIn);
			}

			if (mapStreetToAcct.containsKey(strStreetIn))
			{
				system.debug('######### streetAccId ' + mapStreetToAcct.get(strStreetIn));
				set_AddressIds.addAll(mapStreetToAcct.get(strStreetIn));
//				m_mapMatchingAccts.put(sAccount.Id, sAccount);
//				m_mapAcctToOwner.put(sAccount.Id, sAccount.OwnerId);
			}
		}

		return set_AddressIds; 
	}	
	
/*		Matching rules

		Action depends on the sequence/combination of field matches - same VIN, same email, same mobile, same last name
		and similar shipping address


					E	M
		T C			m	o	L	A
		e a		V	a	b	a	d
		s s		I	i	P	s	d
		t e		N	l	h	t	r	Action
		=========================================
		  1		Y	Y	Y	Y	Y	Customer match
		  2		Y	Y	Y	Y	N	Same customer
		  3		Y	Y	Y	N	Y	Same customer
		  4		Y	Y	Y	N	N	Raise Case
		  5		Y	Y	N	Y	Y	Same customer
		  6		Y	Y	N	Y	N	Same customer
		  7		Y	Y	N	N	Y	Same customer
		  8		Y	Y	N	N	N	Raise Case
				....
		  9		Y	N	Y	Y	Y	Same customer
		 10		Y	N	Y	Y	N	Same customer
		 11		Y	N	Y	N	Y	Same customer
		 12		Y	N	Y	N	N	Raise Case
		 13		Y	N	N	Y	Y	Customer match	
		 14		Y	N	N	Y	N	Raise Case
		 15		Y	N	N	N	Y	Raise Case
		 16		Y	N	N	N	N	Different customer
				....
		 17		N	Y	Y	Y	Y	Customer match
		 18		N	Y	Y	Y	N	Same customer
		 19		N	Y	Y	N	Y	Same customer
		 20		N	Y	Y	N	N	Raise Case
		 21		N	Y	N	Y	Y	Same customer
		 22		N	Y	N	Y	N	Same customer
		 23		N	Y	N	N	Y	Raise Case
		 24		N	Y	N	N	N	Raise Case
				....
		 25		N	N	Y	Y	Y	Customer match
		 26		N	N	Y	Y	N	Same customer
		 27		N	N	Y	N	Y	Raise Case
		 28		N	N	Y	N	N	Raise Case
		 29A	N	N	N	Y	Y	Different customer, if No match for First Name	
		 29B	N	N	N	Y	Y	Different customer, if match for First Name, but no match for Street Number
		 29C	N	N	N	Y	Y	Same customer, 		if match for First Name and Street Number
		 30		N	N	N	Y	N	Different customer
		 31		N	N	N	N	Y	Different customer
		 32		N	N	N	N	N	Different customer

*/	
	public static string customerMatching(String VIN, String firstName, String lastName, String email, String mobile, 
									String city, String postcode, String street, boolean createCase)
	{
		
		string strVIN			= getValue(VIN);
		string strFirstName 	= getValue(firstName);
		string strLastName		= getValue(lastName);
		string strEmail			= getValue(email);
		string strMobile		= getValue(mobile);
		string strCity			= getValue(city);
		string strPostcode		= getValue(postcode);
		string strStreet		= getValue(street);

		// We may be given an unprocessed name string so we have to deal with it here
			
		// Get the last string of a multi-string name and set that as the last name
		list<string> li_NameParts = strLastName.split(' ');
		strLastName = li_NameParts[li_NameParts.size()-1];

		// Name may also be the Account's Company Name so save that too
		string strCompanyName = getValue(lastName);
		
		system.debug('####### strCompanyName ' + strCompanyName);
		
		strMobile = getFormattedPhone(strMobile, PHONE_FORMAT_COUNT);
		
		    	
    	string strWherePart = '';
    	
    	if (strEmail != '')
    		strWherePart = 'PersonEmail=:strEmail';
    		
    	if (strMobile != '')	
    	{
    		if (strWherePart != '')
    			strWherePart += ' or ';
    			
    		string strPhonePart = '';    
    		for (string strPhoneCombi : getFormattedPhoneCombinations(strMobile))
    		{
    			string strPart = 'PersonHomePhone like \'%' + strPhoneCombi + '\' or  CM_Mobile__pc like \'%' + strPhoneCombi  +
    						   '\' or PersonOtherPhone like \'%' + strPhoneCombi + '\' or Phone like \'%' + strPhoneCombi + 
    						   '\' or Work_Phone__c like \'%' + strPhoneCombi + '\'';
    						   
    			if (strPhonePart == '')
    				strPhonePart = strPart;
    			else
    				strPhonePart += ' or ' + strPart;
    		}
    			
    		strWherePart += strPhonePart;
    	}		
  		
    	if (strLastName != '' && strCompanyName != null)
    	{
     		if (strWherePart != '')
    			strWherePart += ' or ';
    			
    		strWherePart += ('LastName = :strLastName or Company__c = :strCompanyName');
    	}
      	
    	if(strCity != '' && strPostcode != '' && strStreet != '')
    	{
    		if (strWherePart != '')
    			strWherePart += ' or ';

			 strWherePart += ('(ShippingCity=:strCity and ShippingPostalCode=:strPostcode) or ' +
			 				  '(BillingCity=:strCity and BillingPostalCode=:strPostcode)'); 		
    	}
    	
    	if (strWherePart == '')
    		return null;
    	
    	string strMainQuery = 'select Id,PersonEmail,' +
    						  'PersonHomePhone,CM_Mobile__pc,PersonOtherPhone,Phone,Work_Phone__c,' +
    						  'FirstName,LastName,Company__c,ShippingCity,ShippingPostalCode,BillingCity,BillingPostalCode, '+
    						  'ShippingStreet,BillingStreet ' + 
    						  'from Account where IsPersonAccount=true and (' + strWherePart + ')';
		
		system.debug('###### strMainQuery ' + strMainQuery);
		

		// Go find ids of all matching customer records
		set<ID> setVINIds			= matchVIN(strVIN);			// Customer ids that match the input VIN
		set<ID> setEmailIds 		= new set<ID>();
		set<ID> setMobileIds 		= new set<ID>();
		set<ID> setLastNameIds 		= new set<ID>();	
		set<ID> setAddressIds 		= new set<ID>();	
														
		// Customer ids that match the input Address
  		list<Account> li_MatchedAccounts = (list<Account>)Database.query(strMainQuery);
  		
    	list<Account> listCityAndPostCodeMatchedAccounts = new list<Account>();
    	
    	for (Account acc : li_MatchedAccounts)
    	{
    		if (strEmail != '' && strEmail == acc.PersonEmail)
    			setEmailIds.add(acc.Id);
    			    			
			if (strMobile != '' &&
    			(strMobile == getFormattedPhone(acc.PersonHomePhone, PHONE_FORMAT_COUNT) || strMobile == getFormattedPhone(acc.CM_Mobile__pc, PHONE_FORMAT_COUNT) || 
    			strMobile == getFormattedPhone(acc.PersonOtherPhone, PHONE_FORMAT_COUNT) || strMobile == getFormattedPhone(acc.Phone, PHONE_FORMAT_COUNT) ||
    			strMobile == getFormattedPhone(acc.Work_Phone__c, PHONE_FORMAT_COUNT)))
    			setMobileIds.add(acc.Id);
    			
    		if ((strLastName != '' && acc.LastName == strLastName) || (strCompanyName != '' && strCompanyName == acc.Company__c))
    			setLastNameIds.add(acc.Id);
    			
    		if (strCity != '' && strPostcode != '' && 
    		   ((strCity == acc.ShippingCity && strPostcode == acc.ShippingPostalCode) || 
    		    (strCity == acc.BillingCity && strPostcode == acc.BillingPostalCode)))		
				listCityAndPostCodeMatchedAccounts.add(acc);
    	}
    	
    	if (!listCityAndPostCodeMatchedAccounts.isEmpty())
    		setAddressIds = matchAddress(listCityAndPostCodeMatchedAccounts, strStreet);

		
		list<set<ID>> matchedAccIdsForEachField = new list<set<ID>>();
		
		matchedAccIdsForEachField.add(setVINIds);
		matchedAccIdsForEachField.add(setEmailIds);
		matchedAccIdsForEachField.add(setMobileIds);
		matchedAccIdsForEachField.add(setLastNameIds);
		matchedAccIdsForEachField.add(setAddressIds);
		
		return getMatchedAccountIds(matchedAccIdsForEachField, new map<ID, Account>(li_MatchedAccounts), strVIN, strFirstName, strLastName,
								 strEmail, strMobile, strStreet, strCity, strPostcode, createCase);
	}
	

}