public with sharing class SPCacheTWAMetadata
{
	private static SPCacheTWAMetadata__c CacheTWAMetadata;
	
	/***********************************************************************************************************
		Lookup Cached information
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	public static string getCaseOriginDealerWeb()
	{
		if (CacheTWAMetadata == null)
			LoadMetadata(); 
			
		return CacheTWAMetadata.CaseOriginDealerWeb__c;
	}

	public static string getCaseOriginWeb()
	{
		if (CacheTWAMetadata == null)
			LoadMetadata(); 
			
		return CacheTWAMetadata.CaseOriginWeb__c;
	}

	public static string getCaseOriginIPad()
	{
		if (CacheTWAMetadata == null)
			LoadMetadata(); 
			
		return CacheTWAMetadata.CaseOriginIPad__c;
	}



	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	private static void LoadMetadata()
	{
		CacheTWAMetadata = SPCacheTWAMetadata__c.getInstance();
	}
	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	public static testmethod void TestSPCacheTWAMetadata()
	{
		SPCacheTWAMetadata.getCaseOriginWeb();
		CacheTWAMetadata = null;

		SPCacheTWAMetadata.getCaseOriginDealerWeb();
		CacheTWAMetadata = null;

		SPCacheTWAMetadata.getCaseOriginIPad();
		CacheTWAMetadata = null;



	}
}