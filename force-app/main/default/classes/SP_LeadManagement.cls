// Class to handle all use cases related to Lead Management
// Author: Shabu, SP on 14.03.2013

public class SP_LeadManagement 
{
	public static void doSyncNotification_LeadToEnformUser(map<id, Lead> oldMap, map<id, Lead> newMap)
	{
		map<ID, Lead> map_LeadIDToLead = new map<ID, Lead>();
		
		set<id> set_ChangedLeadIds	= new set<id>();
		set<id> set_ChangedEnformUserIds	= new set<id>(); 
		
		for (Lead sOldLead : oldMap.values())
		{
			Lead sNewLead = newMap.get(sOldLead.Id);
			
			if	(	sOldLead.FirstName != sNewLead.FirstName ||
					sOldLead.LastName != sNewLead.LastName ||
					sOldLead.MobilePhone != sNewLead.MobilePhone ||
					sOldLead.Email != sNewLead.Email 
				)
			{
				set_ChangedLeadIds.add(sOldLead.Id);
				map_LeadIDToLead.put(sNewLead.Id, sNewLead);
			}
		}
		
		if (set_ChangedLeadIds.isEmpty())
		{
			system.debug('######## There is no Leads changed by a non-Intelematics User');
			return;
		}
		
		List<Enform_User__c> li_SecondaryEnformUsersUpd = [	select	id, Lead_Id__c
															from	Enform_User__c
															where	Lead_Id__c in :set_ChangedLeadIds
														  ];
		
		// Now look for Enform Users
		for (Enform_User__c sEnformUser : li_SecondaryEnformUsersUpd)
		{
			set_ChangedEnformUserIds.add(sEnformUser.Id);
				
			sEnformUser.First_Name__c = map_LeadIDToLead.get(sEnformUser.Lead_Id__c).FirstName;
			sEnformUser.Last_Name__c = map_LeadIDToLead.get(sEnformUser.Lead_Id__c).LastName;
			sEnformUser.Mobile_Phone__c = map_LeadIDToLead.get(sEnformUser.Lead_Id__c).MobilePhone;
			sEnformUser.Email__c = map_LeadIDToLead.get(sEnformUser.Lead_Id__c).Email;				
		}

		
		if (set_ChangedEnformUserIds.isEmpty())
		{
			system.debug('######## There is no Enform Users associated with Leads changed by the non-Intelematics User');
			return;			
		}
		
		if (li_SecondaryEnformUsersUpd.size() > 0)
		{
			SP_Utilities.EnformUpdatedByLeadTrigger = true;
			update li_SecondaryEnformUsersUpd;
		}		

		// Call the web service from a future method
		if (system.isBatch() || system.isFuture() || system.isScheduled())
			SP_EnformUtilities.notifyIntelematicsNoFuture(set_ChangedEnformUserIds, 'update', false);
		else
			SP_EnformUtilities.notifyIntelematics(set_ChangedEnformUserIds, 'update', false);
	}
	
	public static void doSyncNotification_LeadToEnformUser(map<ID, Lead> leadMap, string action)
	{
		list<ID> li_EnformUserIds = new list<ID>();
		list<ID> li_EnformUserAccountIds = new list<ID>();
		list<ID> li_OwnerIds = new list<ID>();
		list<ID> li_LeadIds = new list<ID>();
		
		set<id> set_ChangedLeadIds	= new set<id>();
		set<id> set_ChangedEnformUserIds	= new set<id>(); 
				
		// First look for changed fields on an Owner record
		for (Lead sLead : leadMap.values())
			set_ChangedLeadIds.add(sLead.Id);
		
		if (set_ChangedLeadIds.isEmpty())
		{
			system.debug('######## There is no Leads deleted by a non-Intelematics User');
			return;
		}
		
		// Now look for Enform Users
		for (Enform_User__c [] arrEnformUsers :	[	select	id, Lead_ID__c, Owner_ID__c, Owner_ID__r.OwnerId
													
													
													from	Enform_User__c
													where	Lead_Id__c in :set_ChangedLeadIds
												])
		{
			for (Enform_User__c sEnformUser : arrEnformUsers)
			{
				//set_ChangedEnformUserIds.add(sEnformUser.Id);
				li_EnformUserIds.add(sEnformUser.Id);
				li_EnformUserAccountIds.add(sEnformUser.Owner_ID__c);
				li_OwnerIds.add(sEnformUser.Owner_ID__r.OwnerId);
				li_LeadIds.add(sEnformUser.Lead_ID__c);
			}
		}
		
		if (li_EnformUserIds.isEmpty())
		{
			system.debug('######## There is no Enform Users associated with Leads deleted by the non-Intelematics User');
			return;			
		}		

		// Call the web service from a future method
		if (system.isBatch() || system.isFuture() || system.isScheduled())
			SP_EnformUtilities.notifyIntelematicsNoFuture(li_EnformUserIds, li_EnformUserAccountIds, li_OwnerIds, li_LeadIds, false);
		else
			SP_EnformUtilities.notifyIntelematics(li_EnformUserIds, li_EnformUserAccountIds, li_OwnerIds, li_LeadIds, false);
	}	
		

	////////// TEST METHODS ///////////////////
	
	/*@isTest(SeeAllData=true)
	public static void testMethodTriggerNotifyIntelematicsOnChange()
	{

		Test.startTest();
		
		List<Lead> testLeads = new List<Lead>();
		
		Lead testLeadWithEU = new Lead(LastName='Test1');
		testLeads.add(testLeadWithEU);
		
		Lead testLeadWithOutEU = new Lead(LastName='Test2');
		testLeads.add(testLeadWithOutEU);	
		
		insert testLeads;
		
		Enform_User__c testEU = new Enform_User__c(Lead_ID__c = testLeadWithEU.Id,
												   Mobile_Phone__c = '045678923',
												   Email__c = 'aaa@aa.com',
												   First_Name__c = 'First',
												   Last_Name__c = 'Last');
		insert testEU;
		
		testLeadWithEU.LastName = 'Test3';
		testLeadWithOutEU.LastName = 'Test4';
		
		update testLeads;
		delete testLeads;
		
		Test.stopTest();

	}*/
}