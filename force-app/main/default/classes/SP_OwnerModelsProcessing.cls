public with sharing class SP_OwnerModelsProcessing
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Exception Classes & Enums
	public class SP_Exception extends Exception{}

	public enum BatchParam {ALL, ALL_ASYNC}

	private static integer iQueryLimit = 200;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class used to pass arguments into the Batch
	public class SP_Args
	{
		public BatchParam 	BatchParam 		{get; set;}
		public List<id>		ProcessList		{get; set;}
		public boolean 		UseSavePoint	{get; set;}
		public boolean 		AllOrNone		{get; set;}

		public SP_Args(list<id> theProcessList)
		{
			this.ProcessList 	= theProcessList;
			this.UseSavePoint 	= true;
			this.AllOrNone 		= false;
		}

		public SP_Args(list<id> theProcessList, BatchParam theBatchParam)
		{
			this.ProcessList 	= theProcessList;
			this.BatchParam		= theBatchParam;
			this.UseSavePoint 	= true;
			this.AllOrNone 		= false;
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class used to pass information back to what called the batch
	public class SP_Ret
	{
		public boolean  	InError							{get; set;}
		public List<string> ErrorList						{get; set;}
				
		public SP_Ret()
		{
			this.InError = false;
			this.ErrorList = new List<string>();
		}
	}

	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Singular
	SP_Args 	mArgs;
	SP_Ret		mRet;
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Collections

	// Map of Account Ids to a set of Model names
	map<id, set<string>>	map_AccounttoOwnerModels	= new map<id, set<string>>();

	list<Account>			li_AccountstoUpdate			= new list<Account>();
		
	/***********************************************************************************************************
		Constructor
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Our Constructor
	public SP_OwnerModelsProcessing(SP_Args oArgs)
	{
		mRet 	= new SP_Ret();
		mArgs 	= oArgs;
	}
	
	/***********************************************************************************************************
		Access Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// The one and only entry point for the processor
	public SP_Ret ProcMain()
	{
		Savepoint savePoint;
		
		if(mArgs.UseSavePoint)
			savePoint = Database.setSavepoint();
		
		try
		{
			ProcessAccountObject(mArgs.ProcessList);
		}
		catch(Exception e)
		{
			mRet.InError = true;
			mRet.ErrorList.add(e.getMessage());
						
			if(savePoint != null)
				Database.rollback(savePoint);
			
			throw new SP_Exception('Fatal Exception - All changes have been rolled back - ' + e.getMessage());
		}
		
		return mRet;
	}
	
	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Process Account Objects
	private void ProcessAccountObject(list<id> li_AccountIdsIn)
	{
		// Get all the VO records for these Accounts
		for (Vehicle_Ownership__c [] arrVOs :	[	select	id,
															Customer__c,
															 AssetID__r.Vehicle_Model__r.Name
													from	Vehicle_Ownership__c
													where	Customer__c in :li_AccountIdsIn
												])
		{
			for (Vehicle_Ownership__c sVO : arrVOs)
			{
				string strModelName = sVO.AssetID__r.Vehicle_Model__r.Name;
				
				if (!map_AccounttoOwnerModels.containsKey(sVO.Customer__c))
				{
					set<string> set_OwnerModels = new set<string>();
					set_OwnerModels.add(strModelName);
					map_AccounttoOwnerModels.put(sVO.Customer__c, set_OwnerModels);
				}
				else
				{
					set<string> set_OwnerModels = map_AccounttoOwnerModels.get(sVO.Customer__c);
					set_OwnerModels.add(strModelName);
					map_AccounttoOwnerModels.put(sVO.Customer__c, set_OwnerModels);
				}
			}
		}

		// Now get all the Account records
		for (Account [] arrAccounts :	[	select	id,
													Models_Owned__c
											from	Account
											where	Id in :li_AccountIdsIn
										])
		{		
			for (Account sAccount : arrAccounts)
			{
				if (map_AccounttoOwnerModels.containsKey(sAccount.Id))
				{
					set<string> set_OwnerModels = map_AccounttoOwnerModels.get(sAccount.Id);
					list<string> li_OwnerModels = new list<string>();
		
					for (string strOwnerModels : set_OwnerModels)
					{
						li_OwnerModels.add(strOwnerModels);
					}
		
					li_OwnerModels.sort();
		
					sAccount.Models_Owned__c = '';
					
					for (string strOwnerModels : li_OwnerModels)
					{
						sAccount.Models_Owned__c += strOwnerModels;
						sAccount.Models_Owned__c += ' ';
					}
		
					// Back up one place to remove the trailing space
					sAccount.Models_Owned__c = sAccount.Models_Owned__c.substring(0, sAccount.Models_Owned__c.length() - 1);
				}

				li_AccountstoUpdate.add(sAccount);
			}
		}

		if (!li_AccountstoUpdate.isEmpty())
		{
			update li_AccountstoUpdate;
		}
	}

	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 
	public static testMethod void testSP_OwnerModelsProcessing()
	{

	}
}