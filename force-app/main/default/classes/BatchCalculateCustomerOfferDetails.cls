/* DATE: September 2019
 * AUTHOR: D BANEZ / MCCORKELL
 * CLASS SUMMARY: Batch class to recalculate counts and dates for Customer offer detail; Created to handle VO's 
 * 			that expire at future dates (cannot be handled by time based workflows)
 * INTENDED SCHEDULE: Daily at midnight AEST
 * * TEST CLASS: BatchCalculateCustomerOfferDetailsTest
 * */
public without sharing class BatchCalculateCustomerOfferDetails implements Database.Batchable<sObject>, Schedulable, Database.Stateful {
    Id customerOfferId = null;
    public BatchCalculateCustomerOfferDetails(Id codId){
        this.customerOfferId = codId;
    }
    
    public void execute(SchedulableContext sc) {
        Database.executeBatch(this);
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){ 
        Id CUSTOMERVOUCHERRECORDTYPEID = Schema.SObjectType.Voucher__c.getRecordTypeInfosByDeveloperName().get('Customer_Voucher').getRecordTypeId();
        Id VOVOUCHERRECORDTYPEID = Schema.SObjectType.Voucher__c.getRecordTypeInfosByDeveloperName().get('VO_Voucher').getRecordTypeId();
        Id CUSTOMERVEHICLEOFFERRECORDTYPEID = Schema.SObjectType.VO_Offer_Detail__c.getRecordTypeInfosByDeveloperName().get('Customer_Vehicle_Offer').getRecordTypeId();
        
        List<Customer_Offer_Detail__c> codList = new List<Customer_Offer_Detail__c>();
        if (customerOfferId == null){ // run from schedule
            return Database.getQueryLocator([select Id from Customer_Offer_Detail__c limit 50000]);
        }
        else { // if there is an id provided
            return Database.getQueryLocator([select Id from Customer_Offer_Detail__c where Id = :customerOfferId]);
        }
    }
    
    public void execute(Database.BatchableContext BC, List<Customer_Offer_Detail__c> scope) {
    	Set<Id> codIdSet = new Set<Id>();
        for (Customer_Offer_Detail__c cod : scope){
            codIdSet.add(cod.Id);
        }
        //CustomerOfferDetailTriggerHandler.recalculateCODCounts(codIdSet);
        GuestVoucherServicesUtilities.recalculateCODCounts(codIdSet);
    }
    
    public void finish(Database.BatchableContext BC) {
        
    }
}