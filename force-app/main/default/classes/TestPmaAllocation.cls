@isTest

private class TestPmaAllocation 
{
    static testMethod void TestPmaAllocation()
    {
        PMA__c pma = [select id, Dealer_Name__c, Postcode__c from PMA__c limit 1] ;
        
        Account acc_new = new Account (RecordTypeId = '012900000000THwAAM', Salutation = 'Ms', FirstName = 'First Name', 
                                        LastName = 'Lastname', BillingPostalCode = '2000', PersonMobilePhone = '123');
        insert acc_new;
        
        //acc_new.BillingPostalCode = '3000';
        //update acc_new;
        
        Asset__c[] assets = new Asset__c[]{};
        
        for(Integer y=0; y < 4; y++)
        {
        	Asset__c asset = new Asset__c(Name = 'TESTVIN');
            assets.add(asset);
        }
        
        insert assets;
        
        Vehicle_Ownership__c vo1 = new Vehicle_Ownership__c (Customer__c = acc_new.Id, AssetID__c = assets[0].Id, Status__c = 'Inactive');
    	
    	insert vo1;
    	

    	acc_new.Prior_Lexus_Owner__c = True;
    	acc_new.Sports_Interest__pc = 'Fishing';
    	update acc_new;
        
    }
    

}