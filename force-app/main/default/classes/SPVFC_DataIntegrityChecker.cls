public with sharing class SPVFC_DataIntegrityChecker
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Exception Classes & Enums
	public class SP_Exception extends Exception{}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Singular

	public CSV_Header__c		mp_CSVHeader			{get; set;}
	public CSV_Data__c			m_CSVData;

	// CSV file
	public string				mp_strFileName			{get;set;}
	transient public Blob		mp_strFileContent		{get;set;}
	public string				mp_strDescription		{get;set;}
	public boolean				mp_bNoAttachmentBody	{get;set;}

	public string				mp_strVINinError		{get;set;}

	public boolean				mp_bCSVInvalidHeader	{get; set;}
	public boolean				mp_bInvalidDate			{get; set;}

	public boolean				mp_bInError				{get; set;}
	public boolean				m_bIsTest;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Collections

	public list<CSV_Data__c>	m_liCSVData			= new list<CSV_Data__c>();

	// CSV input file
	public List<List<String>>	m_liResults			= new	List<List<String>>();

	// CSV Error Log
	public list<SPcsvErrorLog>	mp_liErrorLog		{get; set;}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Internal classes

	public class SPcsvErrorLog
	{
		public string LineNumber			{get; set;}
		public string Expected				{get; set;}
		public string Actual				{get; set;}
		public string ErrorMessage			{get; set;}
		
		public SPcsvErrorLog(string strLineNumber, string strExpected, string strActual, string strErrorMessage)
		{
			LineNumber	= strLineNumber;
			Expected	= strExpected;
			Actual		= strActual;
			ErrorMessage	= strErrorMessage;
		}
	}



	/***********************************************************************************************************
		Constructor
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Our Constructor
	public SPVFC_DataIntegrityChecker()
	{
		mp_CSVHeader = new CSV_Header__c();
		m_bIsTest = false;
	}


	/***********************************************************************************************************
		Access Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public Pagereference startBatch()
	{
		// Validate input first
		mp_bNoAttachmentBody = false;
		
        if (mp_strFileContent == null)
        {
            mp_bNoAttachmentBody = true;
            return null;
        }
	
		// Now read the file and write CSV_Data records
		processFile();
		
		if (mp_bCSVInvalidHeader || mp_bInvalidDate)
		{
			return null;
		}

		SP_DataIntegrityCheckerBatch objSP_DataIntegrityCheckerBatch = new SP_DataIntegrityCheckerBatch();
		ID batchprocessid = Database.executeBatch(objSP_DataIntegrityCheckerBatch, 10);

		PageReference pageRedirect = new PageReference('/apex/SPVFP_DataIntegrityChecker');
	   	pageRedirect.setRedirect(true);
   		return pageRedirect;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	public pageReference actionCancel()
	{
		pageReference pageRedirect;

		pageRedirect = new PageReference('/o');
	   	pageRedirect.setRedirect(true);
   		return pageRedirect;
	}


	/***********************************************************************************************************
		Access Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public pageReference processFile()
	{
		mp_liErrorLog		= new List<SPcsvErrorLog>();
		mp_bCSVInvalidHeader = false;

		if (!m_bIsTest)
		{
			mp_strFileName = mp_strFileContent.toString();

			// Parse the input file and RETAIN column headers
			m_liResults = parseCSV(mp_strFileName, false);
		}

		// Get first list - column header names
		list<string> li_Header = m_liResults.remove(0);

		// Pad out to 16 columns
		for (integer i = li_Header.size(); i < 16; i++)
		{
			li_Header.add('');
		}

		// Validate column header names
		if (li_Header.get(0) != 'dealer_code')
			writeToErrorLog('Header', '"dealer_code"', li_Header.get(0), 'Invalid column header - please check csv file format');

		if (li_Header.get(1) != 'dealer_name')
			writeToErrorLog('Header', 'dealer_name', li_Header.get(1), 'Invalid column header - please check csv file format');

		if (li_Header.get(2) != 'branch_code')
			writeToErrorLog('Header', 'branch_code', li_Header.get(2), 'Invalid column header - please check csv file format');

		if (li_Header.get(3) != 'branch_name')
			writeToErrorLog('Header', 'branch_name', li_Header.get(3), 'Invalid column header - please check csv file format');

		if (li_Header.get(4) != 'customer_code')
			writeToErrorLog('Header', 'customer_code', li_Header.get(4), 'Invalid column header - please check csv file format');

		if (li_Header.get(5) != 'customer_name')
			writeToErrorLog('Header', 'customer_name', li_Header.get(5), 'Invalid column header - please check csv file format');

		if (li_Header.get(6) != 'customer_address')
			writeToErrorLog('Header', 'customer_address', li_Header.get(6), 'Invalid column header - please check csv file format');

		if (li_Header.get(7) != 'customer_suburb')
			writeToErrorLog('Header', 'customer_suburb', li_Header.get(7), 'Invalid column header - please check csv file format');

		if (li_Header.get(8) != 'customer_postcode')
			writeToErrorLog('Header', 'customer_postcode', li_Header.get(8), 'Invalid column header - please check csv file format');

		if (li_Header.get(9) != 'customer_state')
			writeToErrorLog('Header', 'customer_state', li_Header.get(9), 'Invalid column header - please check csv file format');

		if (li_Header.get(10) != 'customer_phone_number')
			writeToErrorLog('Header', 'customer_phone_number', li_Header.get(10), 'Invalid column header - please check csv file format');

		if (li_Header.get(11) != 'service_date')
			writeToErrorLog('Header', 'service_date', li_Header.get(11), 'Invalid column header - please check csv file format');

		if (li_Header.get(12) != 'vehicle_mileage')
			writeToErrorLog('Header', 'vehicle_mileage', li_Header.get(12), 'Invalid column header - please check csv file format');

		if (li_Header.get(13) != 'vehicle_vin')
			writeToErrorLog('Header', 'vehicle_vin', li_Header.get(13), 'Invalid column header - please check csv file format');

		if (li_Header.get(14) != 'vehicle_line')
			writeToErrorLog('Header', 'vehicle_line', li_Header.get(14), 'Invalid column header - please check csv file format');

		if (li_Header.get(15) != 'invoice_total')
			writeToErrorLog('Header', 'invoice_total', li_Header.get(15), 'Invalid column header - please check csv file format');

		// Error validating the column headers?
		if (!mp_liErrorLog.isEmpty())
		{
			mp_bCSVInvalidHeader = true;

			system.debug('*** mp_liErrorLog ***' + mp_liErrorLog);

			return null;
		}

		// Write the Header record		
		try
		{
			mp_CSVHeader.Comments__c = mp_strDescription;
			insert mp_CSVHeader;
		}
		catch (Exception e)
		{
			return null;
		}		

		// Header validated ok so process the rest of the data
		m_liCSVData = new list<CSV_Data__c>();
		mp_bInvalidDate = false;

		for (list<string> li_Data : m_liResults)
		{
			system.debug('*** li_Data ***' + li_Data);
			
			// Validate Service date - this is frequently in the wrong format thanks to Excel!
			if (li_Data.get(11).length() != 8)
			{
				mp_bInvalidDate = true;
			}
			
			string strNumericTest = li_Data.get(11).replace('/', '');

			// Check that what is left is numeric
			strNumericTest	= strNumericTest.replace('0', 'Z');
			strNumericTest	= strNumericTest.replace('1', 'Z');
			strNumericTest	= strNumericTest.replace('2', 'Z');
			strNumericTest	= strNumericTest.replace('3', 'Z');
			strNumericTest	= strNumericTest.replace('4', 'Z');
			strNumericTest	= strNumericTest.replace('5', 'Z');
			strNumericTest	= strNumericTest.replace('6', 'Z');
			strNumericTest	= strNumericTest.replace('7', 'Z');
			strNumericTest	= strNumericTest.replace('8', 'Z');
			strNumericTest	= strNumericTest.replace('9', 'Z');

			if (strNumericTest != 'ZZZZZZ')
			{
				mp_bInvalidDate = true;
			}

			if (mp_bInvalidDate)
			{
				mp_strVINinError = li_Data.get(13);
				return null;
			}

			// Pad out to 16 columns
			for (integer i = li_Data.size(); i < 16; i++)
			{
				li_Data.add('');
			}

			m_CSVData = new CSV_Data__c();

			m_CSVData.CSV_Header__c			= mp_CSVHeader.Id;
			m_CSVData.Dealer_Code__c		= li_Data.get(0);
			m_CSVData.Dealer_Name__c		= li_Data.get(1);
			m_CSVData.Branch_Code__c		= li_Data.get(2);
			m_CSVData.Branch_Name__c		= li_Data.get(3);
			m_CSVData.Customer_Code__c		= li_Data.get(4);
			m_CSVData.Customer_Name__c		= li_Data.get(5);
			m_CSVData.Customer_Address__c	= li_Data.get(6);
			m_CSVData.Customer_Suburb__c	= li_Data.get(7);
			m_CSVData.Customer_Postcode__c	= li_Data.get(8);
			m_CSVData.Customer_State__c		= li_Data.get(9);
			m_CSVData.Customer_Phone__c		= li_Data.get(10);
			m_CSVData.Service_Date__c		= li_Data.get(11);
			m_CSVData.Mileage__c			= li_Data.get(12);
			m_CSVData.VIN__c				= li_Data.get(13);
			m_CSVData.Vehicle_Line__c		= li_Data.get(14);
			m_CSVData.Invoice_Total__c		= li_Data.get(15);

			// Write the data to the working list

			m_liCSVData.add(m_CSVData);
		}

		try
		{
			insert m_liCSVData;
		}
		catch (Exception e)
		{
			
		}

		return null;

	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public void writeToErrorLog(string strLineNumber, string strExpected, string strActual, string strErrorMessage)
	{
		SPcsvErrorLog sErrorLog	= new SPcsvErrorLog	(
														strLineNumber,
														strExpected,
														strActual,
														strErrorMessage
													);
		mp_liErrorLog.add(sErrorLog);		
	}


	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static List<List<String>> parseCSV(String contents,Boolean skipHeaders)
	{

		List<List<String>> allFields = new List<List<String>>();

		// replace instances where a double quote begins a field containing a comma
		// in this case you get a double quote followed by a doubled double quote
		// do this for beginning and end of a field
		contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');

		// now replace all remaining double quotes - we do this so that we can reconstruct
		// fields with commas inside assuming they begin and end with a double quote
		contents = contents.replaceAll('""','DBLQT');

		// we are not attempting to handle fields with a newline inside of them
		// so, split on newline to get the spreadsheet rows
		List<String> lines = new List<String>();

		try
		{
			lines = contents.split('\n');
		}
			catch (System.ListException e)
		{
			System.debug('Limits exceeded?' + e.getMessage());
		}

		Integer num = 0;
		for(String line : lines)
		{
			// check for blank CSV lines (only commas)
			if (line.replaceAll(',','').trim().length() == 0)
				break;

			List<String> fields = line.split(',');   
			List<String> cleanFields = new List<String>();
			String compositeField;
			Boolean makeCompositeField = false;

			for(String field : fields)
			{
				if (field.startsWith('"') && field.endsWith('"'))
				{
					field = field.replaceAll('"','');
					cleanFields.add(field.replaceAll('DBLQT','"'));
				}
				else if (field.startsWith('"'))
				{
					makeCompositeField = true;
					compositeField = field;
				}
				else if (field.endsWith('"'))
				{
					compositeField += ',' + field;
					cleanFields.add(compositeField.replaceAll('DBLQT','"'));
					makeCompositeField = false;
				}
				else if (makeCompositeField)
				{
					compositeField +=  ',' + field;
				}
				else
				{
					cleanFields.add(field.replaceAll('DBLQT','"'));
				}
			}

			allFields.add(cleanFields);
		}

		if (skipHeaders)
			allFields.remove(0);

		return allFields;

		}


	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 
	public static testMethod void testSPVFC_DataIntegrityChecker()
	{

		SPVFC_DataIntegrityChecker dic = new SPVFC_DataIntegrityChecker();

        // Attachment is required!
        blob blobTestBlob               = blob.valueOf('This is a test Blob!\nTest data');
        dic.mp_strFileContent			= blobTestBlob;
        dic.mp_strDescription			= 'Test Attachment Description';

        list<string> li_data = new list<string>();
		li_data.add('dealer_code');
        dic.m_liResults.add(li_data);

		li_data = new list<string>();
        for (integer i = 0; i < 16; i++)
        {
			li_data.add('Data' + i);
        }
        dic.m_liResults.add(li_data);

		dic.m_bIsTest = true;

		Test.startTest(); 
		dic.startBatch(); 

		SPVFC_DataIntegrityChecker dic2 = new SPVFC_DataIntegrityChecker();

        // Attachment is required!
        dic2.mp_strFileContent			= blobTestBlob;
        dic2.mp_strDescription			= 'Test Attachment Description';
        
        li_data = new list<string>();
		li_data.add('dealer_code');
		li_data.add('dealer_name');
		li_data.add('branch_code');
		li_data.add('branch_name');
		li_data.add('customer_code');
		li_data.add('customer_name');
		li_data.add('customer_address');
		li_data.add('customer_suburb');
		li_data.add('customer_postcode');
		li_data.add('customer_state');
		li_data.add('customer_phone_number');
		li_data.add('service_date');
		li_data.add('vehicle_mileage');
		li_data.add('vehicle_vin');
		li_data.add('vehicle_line');
		li_data.add('invoice_total');
        dic2.m_liResults.add(li_data);

		li_data = new list<string>();
        for (integer i = 0; i < 16; i++)
        {
			li_data.add('Data' + i);
        }
        dic2.m_liResults.add(li_data);

		dic2.m_bIsTest = true;
 
		dic2.startBatch();
		Test.stopTest(); 
		
		dic2.actionCancel();
	}
	
}