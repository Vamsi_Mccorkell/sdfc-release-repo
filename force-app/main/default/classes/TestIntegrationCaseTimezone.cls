/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestIntegrationCaseTimezone {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
       
	    Account newpacc = new Account();
	    newpacc.FirstName = 'testPersonacc';
	    newpacc.LastName='Test'; 
	    newpacc.RecordTypeId = '012900000000THwAAM'; 
	    newpacc.PersonEmail='sqwarepeg@gmail.com'; 
	    //newpacc.Preferred_Dealer__c = newacc.Id;
	    newpacc.BillingPostalCode = '1234';
	    newpacc.Salutation = 'Miss';
	    newpacc.PersonMobilePhone = '1234';
	    Database.SaveResult paccsr = Database.Insert(newpacc );
	    system.assertEquals( paccsr.isSuccess(), true);
	    

        
        List<Configuration__c> configs = new List<Configuration__c>{};
        for(Integer i = 285; i< 290; i++){
        	Configuration__c con = new Configuration__c (Name = '' + i, Description__c = 'description test' + i, Owner_Or_Vehicle__c = 'test Subject' + i);
        	configs.add(con);
        }
        insert configs;
        
        Case c1 = new Case();
        c1.RecordTypeId = '012900000000YXq';
        c1.Status = 'In Progress';
        c1.Origin = 'Integration';
        c1.Type = 'Roadside Assistant';
        c1.Breakdown_Code__c = '285';
        Database.SaveResult paccsrc1 = Database.Insert(c1);
	    system.assertEquals( paccsrc1.isSuccess(), true);
        
/*
        Case c2 = new Case();
        c2.RecordTypeId = '012900000000Uvh';
        c2.Status = 'In Progress';
        c2.Origin = 'Email';
        c2.Type = 'Personal Preview/Test Drive';
        c2.Model__c ='GS300';
        c2.Description = 'This is a description';
        Database.SaveResult paccsrc2 = Database.Insert(c2);
	    system.assertEquals( paccsrc2.isSuccess(), true);
*/        
        
        Case c3 = new Case();
        c3.RecordTypeId = '012900000000YXq';
        c3.Status = 'In Progress';
        c3.Origin = 'Integration';
        c3.Type = 'Roadside Assistant';
        c3.Breakdown_Code__c = '286';
        c3.Integration_Arrival_Time__c='0:20';
        c3.Integration_Completion_Time__c='0:18';
        c3.Integration_Receipt_Time__c ='0:15';
        c3.Integration_date__c = '20102010';
        Database.SaveResult paccsrc3 = Database.Insert(c3);
	    system.assertEquals( paccsrc3.isSuccess(), true);
	    
	    Case c4 = new Case();
        c4.RecordTypeId = '012900000000YXq';
        c4.Status = 'In Progress';
        c4.Origin = 'Integration';
        c4.Type = 'Roadside Assistant';
        c4.Breakdown_Code__c = '287';
        c4.Integration_Arrival_Time__c='110:20';
        c4.Integration_Completion_Time__c='110:18';
        c4.Integration_Receipt_Time__c ='0:1556';
        c4.Integration_date__c = '08/08/2008';
        Database.SaveResult paccsrc4 = Database.Insert(c4);
	    system.assertEquals( paccsrc4.isSuccess(), true);
	    
	    Case c5 = new Case();
        c5.RecordTypeId = '012900000000YXq';
        c5.Status = 'In Progress';
        c5.Origin = 'Integration';
        c5.Type = 'Roadside Assistant';
        c5.Breakdown_Code__c = '288';
        c5.Integration_Arrival_Time__c=null;
        c5.Integration_Completion_Time__c= null;
        c5.Integration_Receipt_Time__c = '20102010';
        c5.Integration_date__c = '';
        Database.SaveResult paccsrc5 = Database.Insert(c5);
	    system.assertEquals( paccsrc5.isSuccess(), true);
	    
	    Case c6 = new Case();
	    c6 = [select Id, Origin, Type, Breakdown_Code__c from Case where Id = :c3.Id];
	   	c6.Origin = 'Integration';
        c6.Type = 'Roadside Assistant';
        c6.Breakdown_Code__c = '240';
        update c6;
	    
	    Case c7 = new Case();
	    c7 = [select Id, Origin, Type, Breakdown_Code__c from Case where Id = :c3.Id];
	   	c7.Origin = 'Integration';
        c7.Type = 'Roadside Assistant';
        c7.Breakdown_Code__c = '211';
        update c7;

        
    }
}