global class MatchingAccount_WS
{
	webservice static String MatchingAccount(	String strVIN1,
												String strfirstName1, String strlastName1,
												String strEmail1, String strMobile1, 
												String strCity1, String strPostcode1, 
												String strStreet1)
	{
		/*String acctId = '';

		MatchingAccount ma = new MatchingAccount();
		acctId = ma.customerMatching(	strVIN1,
										strfirstName1,
										strlastName1,
										strEmail1,
										strMobile1,
										strCity1,
										strPostcode1,
										strStreet1
									);
								
		return acctId;*/
									
		return SimplifiedMatchingAccount.customerMatching(	strVIN1,
										strfirstName1,
										strlastName1,
										strEmail1,
										strMobile1,
										strCity1,
										strPostcode1,
										strStreet1, false
									);
	}

	webservice static String MatchingAccountCreateCase(	String strVIN1,
														String strfirstName1, String strlastName1,
														String strEmail1, String strMobile1, 
														String strCity1, String strPostcode1, 
														String strStreet1,
														Boolean bCreateCase)
	{
		/*String acctId = '';

		MatchingAccount ma = new MatchingAccount();
		acctId = ma.customerMatching(	strVIN1,
										strfirstName1,
										strlastName1,
										strEmail1,
										strMobile1,
										strCity1,
										strPostcode1,
										strStreet1,
										bCreateCase
									);
		return acctId;*/
		
		return SimplifiedMatchingAccount.customerMatching(	strVIN1,
										strfirstName1,
										strlastName1,
										strEmail1,
										strMobile1,
										strCity1,
										strPostcode1,
										strStreet1,
										bCreateCase
									);		
	}

}