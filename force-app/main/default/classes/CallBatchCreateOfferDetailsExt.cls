public with sharing class CallBatchCreateOfferDetailsExt {
/* DATE: MARCH 2020
 * AUTHOR: VD / MCCORKELL
 * CLASS SUMMARY: Calls Batch class to call the GuestVoucherServicesUtilities create OD records; Only currently used for Offer Triggers
 * * TEST CLASS: CallBatchCreateOfferDetailsTest
 * */
    Id batchId;
    public Boolean batchStatusBool {get;set;}
    public String batchStatus {get;set;}
    public Boolean pollerBool {get;set;}
    
    Offer__c sfOffer {get;set;}
   
    public CallBatchCreateOfferDetailsExt(ApexPages.StandardController stdCon){
        this.sfOffer = (Offer__c)stdCon.getRecord();
        batchStatusBool = false;
        pollerBool = false;
      }
    
      public void callCreateOfferDtlBatch() {
        batchStatusBool = true;
        BatchCreateCODVINODVOODRecords OfferBatchObj = new BatchCreateCODVINODVOODRecords(sfOffer.Id);
        batchId = Database.executeBatch(OfferBatchObj);
          system.debug('batch Id '+ batchId );
         
        //Update Batch Id
        
        List <Offer__c> OfferToUpdateList = [SELECT Id, Offer_Dtl_Batch_Id__c FROM Offer__c where Id=:sfOffer.Id];
        if(OfferToUpdateList.size() == 1){
            
            for (Offer__c sOffer : OfferToUpdateList){
        	    sOffer.Offer_Dtl_Batch_Id__c = batchId;
                system.debug('batch Id inside for '+ sOffer.Offer_Dtl_Batch_Id__c );
            }
         	update OfferToUpdateList;
        }
          
        
        checkBatchStatus();
      }
   
    public void checkBatchStatus() {
        AsyncApexJob job = [SELECT Id, Status FROM AsyncApexJob WHERE Id =: batchId];
        batchStatus = job.Status;
        if(batchStatus == 'Completed') {
            pollerBool = false;
        } else {
            pollerBool = true;
        }
    }

       
    public PageReference runBatch(){
      //  BatchCreateCODVINODVOODRecords OfferBatchObj = new BatchCreateCODVINODVOODRecords(sfOffer.Id);
      //  batchId = Database.executeBatch(OfferBatchObj);
              
        return new PageReference('/' + sfOffer.Id);
    }
    
}