/* DATE: September 2019
 * AUTHOR: D BANEZ / MCCORKELL
 * CLASS SUMMARY: Voucher Services Handler class for Voucher Services WS methods. Used in VoucherServicesWebService
 * WEB SERVICE USING THIS CLASS: VoucherServicesWebService
 * TEST CLASS: VoucherServicesWSHandlerTest
 * */
global class VoucherServicesWSHandler {
	global static VoucherServicesWebService.VoucherServiceResult attendantLogin(String partnerBranchUserName, String partnerBranchPassword){
    	VoucherServicesWebService.VoucherServiceResult vsr = new VoucherServicesWebService.VoucherServiceResult();
        // check for required fields
        String errDesc = '';
        Integer requiredFieldsErrCounter = 0;
        if (partnerBranchUserName == null || partnerBranchUserName == ''){
            errDesc += 'Partner Branch User Name;';
            requiredFieldsErrCounter++;
        }
        if (partnerBranchPassword == null || partnerBranchPassword == ''){
            errDesc += 'Partner Branch Password;';
            requiredFieldsErrCounter++;
        }
        if (requiredFieldsErrCounter > 0){
            vsr.success = false;
            vsr.recordId = null;
            vsr.errCode = '700';
            vsr.errorDesc = 'REQUIRED INPUTS MISSING: ' + errDesc;
        }
        else {
            try {
                Id PARTNERRECORDID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Branch').getRecordTypeId();
                List<Account> partnerAcctList = [select Id, Name from Account
                                      where RecordTypeId = :PARTNERRECORDID
                                      and Partner_Branch_Username__c = :partnerBranchUserName
                                      and Partner_Branch_Password__c = : partnerBranchPassword limit 1];
                if (partnerAcctList.size() > 0){
                    vsr.success = true;
                    vsr.recordId = partnerAcctList[0].Id;
                }
                else {
                    vsr.success = false;
                    vsr.recordId = null;
                    vsr.errCode = '601';
                    vsr.errorDesc = 'No Partner Account for the provided username and password combination';
                }
            }
            catch (Exception e){
                vsr.success = false;
                vsr.errCode = '800';
                vsr.errorDesc = 'System error querying  Account record: ' + e.getMessage();
                vsr.caseNumber = createErrorCase(null, partnerBranchUsername, partnerBranchPassword, null, null, '800 - ' + vsr.errorDesc);
            }
        }
        
        return vsr;
    }
    
    global static VoucherServicesWebService.VoucherServiceResult resendPassword(String partnerBranchIdentifier){
    	VoucherServicesWebService.VoucherServiceResult vsr = new VoucherServicesWebService.VoucherServiceResult();
        Id PARTNERRECORDID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Branch').getRecordTypeId();
        // check for required fields
        String errDesc = '';
        Integer requiredFieldsErrCounter = 0;
        if (partnerBranchIdentifier == null){
           errDesc += 'partnerBranchIdentifier';
           requiredFieldsErrCounter++;
        }
        if (requiredFieldsErrCounter > 0){
            vsr.success = false;
            vsr.errCode = '700';
            vsr.errorDesc = 'REQUIRED INPUTS MISSING: ' + errDesc;
        }
        else {
            List<Account> partnerAcctList = [select Id, Name, Partner_Branch_Password_Reminder_Flag__c 
                                                from Account where RecordTypeId = :PARTNERRECORDID 
                                                and Type = 'Partner Branch' and Status__c = 'Active' 
                                             	and (Partner_Branch_Username__c = :partnerBranchIdentifier or Partner_Branch_Email__c = :partnerBranchIdentifier) limit 1];
            if (partnerAcctList.size() > 0){
                Account acctUpdate = new Account(Id = partnerAcctList[0].Id);
                acctUpdate.Partner_Branch_Password_Reminder_Flag__c = true;
                try {
                    update acctUpdate;
                    vsr.success = true;
                    vsr.recordId = partnerAcctList[0].Id;
                }
                catch (DmlException e) {
                    vsr.success = false;
                    vsr.errCode = '800';
                    vsr.errorDesc = 'System error inserting updating Account record: ' + e.getMessage();
                    vsr.caseNumber = createErrorCase(null, null, null, partnerBranchIdentifier, null, '800 - ' + vsr.errorDesc);
                } 
            }
            else {
                vsr.success = false;
                vsr.errCode = '602';
                vsr.errorDesc = 'No partner branch found for Partner Branch Identifier provided';
            }
            
        }
        return vsr;
    }
    
    global static VoucherServicesWebService.VoucherServiceResult redeemVoucher(Id partnerBranchId, String voucherType, Id vOOfferDetailID, Boolean compFlag, String compComment, String errCode){
    	VoucherServicesWebService.VoucherServiceResult vsr = new VoucherServicesWebService.VoucherServiceResult();
        // check for required fields
        String errDesc = '';
        Integer requiredFieldsErrCounter = 0;
        if (partnerBranchId == null){
           errDesc += 'partnerBranchId';
           requiredFieldsErrCounter++;
        }
        if (voucherType == null){
           errDesc += 'voucherType';
           requiredFieldsErrCounter++;
        }
        if (vOOfferDetailID == null){
           errDesc += 'vOOfferDetailID';
           requiredFieldsErrCounter++;
        }
        if (requiredFieldsErrCounter > 0){
            vsr.success = false;
            vsr.errCode = '700';
            vsr.errorDesc = 'REQUIRED INPUTS MISSING: ' + errDesc;
        }
        else {
            // check if the ID's are valid
            List<Account> partnerAcctList = [select Id from Account where Id = :partnerBranchId limit 1];
            List<VO_Offer_Detail__c> voOfferDetailsList = [select Id, Vin_Offer_Detail__c from VO_Offer_Detail__c where Id = :vOOfferDetailID limit 1];
            if (partnerAcctList.size() == 0){ // Id did not return an account
                vsr.success = false;
            	vsr.errCode = '603';
            	vsr.errorDesc = 'Partner Branch ID is invalid';
                vsr.caseNumber = createErrorCase(partnerBranchId, null, null, null, vOOfferDetailID, '603 - Partner Branch ID invalid');
            }
            else if (voOfferDetailsList.size() == 0){ // Id did not return vo Offer Details
                vsr.success = false;
            	vsr.errCode = '604';
            	vsr.errorDesc = 'VO Offer Details ID is invalid';
                vsr.caseNumber = createErrorCase(partnerBranchId, null, null, null, vOOfferDetailID, '604 - VO Offer Details ID is invalid');
            }
            else { // if both Id's are correct
                Id VALETVOUCHERRECORDTYPEID = Schema.SObjectType.Voucher__c.getRecordTypeInfosByDeveloperName().get('VO_Voucher').getRecordTypeId();
                Voucher__c voucher = new Voucher__c(RecordTypeId = VALETVOUCHERRECORDTYPEID);
                voucher.Voucher_Type__c = voucherType;
                voucher.Voucher_Redemption_Date__c = system.now();
                voucher.Partner_Branch__c = partnerBranchId;
                voucher.VO_Offer_Details__c = vOOfferDetailID;
                voucher.Vin_Offer_Detail__c = voOfferDetailsList[0].Vin_Offer_Detail__c;
                voucher.Valid_Voucher_Flag__c = true;
                voucher.Comp_Flag__c = compFlag;
                voucher.Comp_Comment__c = compComment;
                try {
                    insert voucher;
                    vsr.recordId = voucher.Id;
                    vsr.success = true;
                    if (compFlag){ // if comp flag is true, create a case
                        vsr.caseNumber =  createCompCase(partnerBranchId, voucher.Id, 'Complimentary Voucher is issued: ' + compComment, errCode);
                    }
                }
                catch (DmlException e){
                    vsr.success = false;
                    vsr.errCode = '800';
                    vsr.errorDesc = 'Error creating Voucher Record: ' + e.getMessage();
                    vsr.caseNumber = createErrorCase(partnerBranchId, null, null, null, vOOfferDetailID, '800 - ' + vsr.errorDesc);
                }
                
            }
            
        }
        return vsr;
    }
    
    global static List<VoucherServicesWebService.VoucherAvailabilityResult> getVoucherAvailability(Id partnerBranchId, String regoNumber, String voucherType){
    	List<VoucherServicesWebService.VoucherAvailabilityResult> varList = new List<VoucherServicesWebService.VoucherAvailabilityResult>();
        // check for required fields
        String errDesc = '';
        Integer requiredFieldsErrCounter = 0;
        if (partnerBranchId == null){
           errDesc += 'partnerBranchId';
           requiredFieldsErrCounter++;
        }
        if (regoNumber == null || regoNumber == ''){
           errDesc += 'regoNumber';
           requiredFieldsErrCounter++;
        }
        if (voucherType == null || voucherType == ''){
           errDesc += 'voucherType';
           requiredFieldsErrCounter++;
        }
        if (requiredFieldsErrCounter > 0){
            VoucherServicesWebService.VoucherAvailabilityResult var = new VoucherServicesWebService.VoucherAvailabilityResult();
            var.success = false;
            var.errCode = '700';
            var.errorDesc = 'REQUIRED INPUTS MISSING: ' + errDesc;
            varList.add(var);
        }
        else {
            try {
                Id PARTNERBRANCHACCTID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Branch').getRecordTypeId();
                List<Account> partnerBranchAcctList = [select Id, ParentId from Account
                                                       where RecordTypeId = :PARTNERBRANCHACCTID
                                                       and Status__c = 'Active' and ParentId != null
                                                       and Id = :partnerBranchId limit 1 ];
                if (partnerBranchAcctList.size() == 0){
                    VoucherServicesWebService.VoucherAvailabilityResult var = new VoucherServicesWebService.VoucherAvailabilityResult();
                    var.success = false;
                    var.errCode = '605';
                    var.errorDesc = 'No active Partner Branch Account found';
                    varList.add(var);
                    return varList;
                }
                else {
                    // get offers based on partner branch
                    Map<Id, Offer__c> offerMap = new Map<Id, Offer__c>([select Id, Offer_Type__c
                                                                        from Offer__c where Status__c = 'Active'
                                                                        and Offer_Type__c = :voucherType
                                                                        and Partner_Account__c = : partnerBranchAcctList[0].ParentId]);
                    if (offerMap.keySet().size() > 0){
                        String cleanRego = regoNumber.trim();
                        cleanRego = cleanRego.replace(' ', '');
                        cleanRego = cleanRego.replace('-', '');
                        
                        // Get a map of VO's for the Rego provided
                        Map<Id, Vehicle_Ownership__c> voMap = new Map<Id, Vehicle_Ownership__c>(
                            [select Id, Rego_clean__c from Vehicle_Ownership__c
                             where Status__c = 'Active'
                             and Rego_Clean__c = :cleanRego]);
                        
                        if (voMap.keySet().size() > 0){
                            // now get the list of VOOD
                            List<VO_Offer_Detail__c > voodList = [select Id, Vehicle_Ownership__r.Model__c, Vehicle_Ownership__r.No_Comp__c,
                                                                  Voucher_Expiration__c, Vouchers_Available__c  from VO_Offer_Detail__c 
                                                                  where Offer__c in :offerMap.keySet() and Offer__c != null
                                                                  and Vehicle_Ownership__c in :voMap.keySet() and Vehicle_Ownership__c != null];
                            
                            if (voodList.size() > 0){
                                for (VO_Offer_Detail__c vood : voodList){
                                    VoucherServicesWebService.VoucherAvailabilityResult var = new VoucherServicesWebService.VoucherAvailabilityResult();
                                    var.recordId = vood.Id;
                                    var.model = vood.Vehicle_Ownership__r.Model__c;
                                    var.voucherExpiryDate = vood.Voucher_Expiration__c;
                                    var.vouchersRemaining = Integer.valueOf(vood.Vouchers_Available__c);
                                    var.noCompAllowed = vood.Vehicle_Ownership__r.No_Comp__c;
                                    var.success = true;
                                    if (vood.Voucher_Expiration__c < system.today()){
                                        var.success = false;
                                        var.errCode = '609';
                                        var.errorDesc = 'Vouchers are expired';
                                    }
                                    else if(vood.Vouchers_Available__c <= 0){
                                        var.success = false;
                                        var.errCode = '610';
                                        var.errorDesc = 'No available vouchers';
                                    }
                                    varList.add(var);
                                }
                            }
                            else {
                                VoucherServicesWebService.VoucherAvailabilityResult var = new VoucherServicesWebService.VoucherAvailabilityResult();
                                var.success = false;
                                var.errCode = '608';
                                var.errorDesc = 'Vehicle not enrolled in program';
                                varList.add(var);
                            }
                            
                        }
                        else {
                            VoucherServicesWebService.VoucherAvailabilityResult var = new VoucherServicesWebService.VoucherAvailabilityResult();
                            var.success = false;
                            var.errCode = '607';
                            var.errorDesc = 'No Active Vehicle Ownerships found for the Rego: ' + cleanRego;
                            varList.add(var);
                        }
                        
                    }
                    else { // no active offers for Partner found
                        VoucherServicesWebService.VoucherAvailabilityResult var = new VoucherServicesWebService.VoucherAvailabilityResult();
                        var.success = false;
                        var.errCode = '606';
                        var.errorDesc = 'No Active Offers Found';
                        varList.add(var);
                    }
                }
            }
            catch (Exception e){
                VoucherServicesWebService.VoucherAvailabilityResult var = new VoucherServicesWebService.VoucherAvailabilityResult();
                var.success = false;
                var.errCode = '800';
                var.errorDesc = 'Error querying voucher availability: ' + e.getMessage();
                var.caseNumber = createErrorCase(partnerBranchId, null, null, null, null, '800 - ' + var.errorDesc + ' | rego number: ' + regoNumber + ' | voucher type: ' + voucherType);
                varList.add(var);
            }
            
        }
        return varList;
    }
    
    private static String createCompCase(Id partnerBranchId, Id voucherId, String caseCreateReason, String errCode){
        Id GENERALENQUIRYRTYPEID = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('General_Enquiry').getRecordTypeId();
        Case newCase = new Case(RecordTypeId = GENERALENQUIRYRTYPEID);
        newCase.Type = 'Data Verification';
        newCase.Origin = 'Valet Voucher';
        newCase.Status = 'In Progress';
        newCase.AccountId = partnerBranchId;
        newCase.Subject = 'MyLexus Valet Complimentary Voucher Issued';
        String description = caseCreateReason;
        description += ' | Account ID: ' + partnerBranchId;
        description += ' | Voucher ID: ' + voucherId;
        description += ' | Error Code: ' + errCode;
        newCase.Description = description;
        try {
            if (!Test.isRunningTest()) {
                insert newCase;
            	// get case number
            	newCase = [select Id, CaseNumber from Case where Id = :newCase.Id limit 1];
            }
            return newCase.CaseNumber;
        }
        catch (DmlException e){
            system.debug(LoggingLevel.ERROR, '## Error inserting case: ' + e.getMessage());
            return null;
        }
    }
    private static String createErrorCase(Id partnerBranchId, String partnerBranchUsername, String partnerBranchPassword, String partnerBranchIdentifier, Id voOfferDetailsId, String caseCreateReason){
        Id GENERALENQUIRYRTYPEID = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('General_Enquiry').getRecordTypeId();
        Case newCase = new Case(RecordTypeId = GENERALENQUIRYRTYPEID);
        newCase.Type = 'Data Verification';
        newCase.Origin = 'Valet Voucher';
        newCase.Status = 'In Progress';
        newCase.AccountId = partnerBranchId;
        newCase.Subject = 'MyLexus Valet Voucher Customer Issue';
        String description = caseCreateReason;
        description += ' | Account ID: ' + partnerBranchId;
        description += ' | VO Offer Details ID: ' + voOfferDetailsId;
        description += ' | Partner Branch UserName: ' + partnerBranchUsername;
        description += ' | Partner Branch Password: ' + partnerBranchPassword;
        description += ' | Partner Branch Identifier: ' + partnerBranchIdentifier;
        newCase.Description = description;
        try {
            insert newCase;
            // get case number
            newCase = [select Id, CaseNumber from Case where Id = :newCase.Id limit 1];
            return newCase.CaseNumber;
        }
        catch (DmlException e){
            system.debug(LoggingLevel.ERROR, '## Error inserting case: ' + e.getMessage());
            return null;
        }
    }
}