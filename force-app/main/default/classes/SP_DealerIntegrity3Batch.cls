global with sharing class SP_DealerIntegrity3Batch implements Database.Batchable<sObject>
{
	public class SP_DealerIntegrity3BatchException extends Exception{}

	global Database.QueryLocator start(Database.BatchableContext BC)
	{

		string	strQuery;

		if (!test.isRunningTest())
		{
			strQuery =		
				' select	Id'	+
				' from 		Account' +
				' where 	RecordTypeId = ' + '\'' + SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType() + '\'' +
				//' and		Owner_Type__c = ' + '\'' + 'Current' + '\'' +
				//' and		Preferred_Dealer__c <> null' + 
				//' and		Servicing_Dealer__c <> null' +
				' and 		Status__c  = \'Active\'' + 
				' and 		Override_Preferred_Dealer__c = false';
		}
		else
		{
			strQuery =		
				' select	Id'	+
				' from 		Account' +
				' where 	RecordTypeId = ' + '\'' + SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType() + '\'' +
				' and		Owner_Type__c = ' + '\'' + 'Current' + '\'' +
				' and		FirstName = ' + '\'' + 'Test' + '\'' ;
		}

		return Database.getQueryLocator(strQuery);
	}


	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		list<id> theProcessList = new list<id>();

		for(sobject s : scope)
		{
			theProcessList.add((id)s.get('Id'));
		}

		SP_DealerIntegrity3Processing.SP_Args oArgs = new SP_DealerIntegrity3Processing.SP_Args(theProcessList);

		SP_DealerIntegrity3Processing processor = new SP_DealerIntegrity3Processing(oArgs);
		SP_DealerIntegrity3Processing.SP_Ret oRet = processor.ProcMain();
	}


	global void finish(Database.BatchableContext BC)
	{
		system.debug('Finished');
	}

	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	 @isTest(SeeAllData=true)    
	public static void testBatch() 
	{
		// Set up some database records
        Account sAccount;

        sAccount			= new Account();
        sAccount.FirstName	= 'Test';
        sAccount.LastName	= 'Account';
        sAccount.RecordTypeId = SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
        insert sAccount;

		SP_DealerIntegrity3Batch oDealerIntegrity3Batch = new SP_DealerIntegrity3Batch();
		oDealerIntegrity3Batch.start(null);

		oDealerIntegrity3Batch.execute(null, new list<SObject>{sAccount});

		oDealerIntegrity3Batch.finish(null);


	}
}