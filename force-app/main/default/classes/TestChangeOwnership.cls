@isTest

private class TestChangeOwnership 
{  
  static testMethod void TestChangeOwnership()
  {

        // Preparing Test Data
        Account ac1 = new Account(Salutation = 'Ms', FirstName = 'Test1', LastName = 'Test1', Company__c = 'Sqware Peg', 
            BillingPostalCode = '2001', BillingState = 'NSW', PersonMobilePhone = '0405939315', RecordTypeId = '012900000000THwAAM');
            
        Account ac2 = new Account(Salutation = 'Ms', FirstName = 'Test2', LastName = 'Test2', Company__c = 'Sqware Peg', 
            BillingPostalCode = '2002', BillingState = 'NSW', PersonMobilePhone = '0405939316', RecordTypeId = '012900000000THwAAM');
        
        Account ac3 = new Account(Salutation = 'Ms', FirstName = 'Test3', LastName = 'Test3', Company__c = 'Sqware Peg', 
            BillingPostalCode = '2003', BillingState = 'NSW', PersonMobilePhone = '0405939317', RecordTypeId = '012900000000THwAAM');
        
        Account[] accts = new Account[]{ac1, ac2, ac3};
        insert accts;
        
    	Asset__c[] assets = new Asset__c[]{};
        
        for(Integer y=0; y < 4; y++)
        {
        	Asset__c asset = new Asset__c(Name = 'TESTVIN');
            assets.add(asset);
        }
        
        insert assets;
        
        Warranty__c[] wars = new Warranty__c[]{};
        
        for(Integer z=0; z < 2; z++)
        {
        	Warranty__c war = new Warranty__c(Warranty_type__c = 'Factory Warranty', AssetId__c = assets[0].Id, Customer_Account__c = accts[0].Id);
            wars.add(war);
        }
        
        insert wars;
    	
    	
    	// Invoke Trigger
    	Vehicle_Ownership__c vo1 = new Vehicle_Ownership__c (Customer__c = accts[0].Id, AssetID__c = assets[0].Id, Status__c = 'Inactive');
    	
    	insert vo1;
/*    	
    	Vehicle_Ownership__c vo2 = new Vehicle_Ownership__c (Customer__c = accts[1].Id, AssetID__c = assets[0].Id, Status__c = 'Active', 
    															Start_Date__c = Date.valueOf('2008-07-07'), Vehicle_Type__c = 'A', Vehicle_Rego_Number__c = 'AAAAAA');
		insert vo2;
		
		Vehicle_Ownership__c vo22 = new Vehicle_Ownership__c (Customer__c = accts[1].Id, AssetID__c = assets[1].Id, Status__c = 'Active', 
    															Start_Date__c = Date.valueOf('2008-08-07'), Vehicle_Type__c = 'A', Vehicle_Rego_Number__c = 'AAAAAB');
		insert vo22;
		
		Vehicle_Ownership__c vo21 = new Vehicle_Ownership__c (Customer__c = accts[1].Id, AssetID__c = assets[2].Id, Status__c = 'Active', 
    															Start_Date__c = Date.valueOf('2008-06-07'), Vehicle_Type__c = 'A', Vehicle_Rego_Number__c = 'AAAAAC');
		insert vo21;

    	
        // Asserting results
        Vehicle_Ownership__c[] voQueries = [SELECT Customer__c, Customer__r.Encore_Expiration_date__c, AssetID__c, Status__c  
    										FROM Vehicle_Ownership__c 
                                    		WHERE AssetID__c = :assets[0].Id];
        
        
        System.assertEquals('Inactive', voQueries[0].Status__c);
	    System.assertEquals('Active', voQueries[1].Status__c);
        
        Vehicle_Ownership__c vo3 = new Vehicle_Ownership__c (Customer__c = accts[2].Id, AssetID__c = assets[0].Id, Status__c = 'Active',
    															Start_Date__c = Date.valueOf('2009-07-07'),Vehicle_Type__c = 'A', Vehicle_Rego_Number__c = 'BBBBBB');
        
        insert vo3;
        
         Vehicle_Ownership__c vo31 = new Vehicle_Ownership__c (Customer__c = accts[2].Id, AssetID__c = assets[3].Id, Status__c = 'Active',
    															Start_Date__c = Date.valueOf('2009-08-07'),Vehicle_Type__c = 'A', Vehicle_Rego_Number__c = 'BBBBBA');
        
        insert vo31;

		 //Vehicle_Ownership__c vo32 = new Vehicle_Ownership__c (Customer__c = accts[2].Id, AssetID__c = assets[2].Id, Status__c = 'Active',
    	//														Start_Date__c = Date.valueOf('2009-09-07'),Vehicle_Type__c = 'B', Vehicle_Rego_Number__c = 'BBBBBC');
        
        //insert vo32;
        
		vo3.Start_Date__c = Date.valueOf('2009-10-01');
		update vo3;
        
        Vehicle_Ownership__c[] voQueries2 = [SELECT Customer__c, Customer__r.Encore_Expiration_date__c, AssetID__c, Status__c, End_Date__c  
    										FROM Vehicle_Ownership__c 
                                    		WHERE AssetID__c = :assets[0].Id];
        
        //System.assertEquals('Inactive', voQueries2[1].Status__c);
        //System.assertEquals(Date.valueOf('2009-07-07'), voQueries2[1].End_Date__c);
        
        
        Warranty__c[] warQueries = [SELECT Customer_Account__c, Customer_Account__r.Name  FROM Warranty__c 
        							WHERE AssetId__c =:assets[0].Id OR AssetId__c =:assets[1].Id];
        
        for(Integer j=0; j < warQueries.size(); j++)
        {
	        //System.assertEquals('Test3 Test3', warQueries[0].Customer_Account__r.Name);
	        //System.assertEquals('Test1 Test1', warQueries[0].Customer_Account__r.Name);
        }  
*/
  }

}