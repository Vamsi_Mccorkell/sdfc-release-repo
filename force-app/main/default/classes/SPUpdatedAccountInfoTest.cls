// D BANEZ Jan 2020 - temporary test methods to deploy changes to SPUpdateAccountInfo trigger on VO (for De Rdr Project)
@IsTest (SeeAllData=true)
public class SPUpdatedAccountInfoTest {
    static testmethod void testUpdateAccountInfo(){
        // setup test data
        Vehicle_Ownership__c vo = [select	Id,
												Customer__c,
												Start_Date__c,
												Branch_of_Dealership__c,
												Status__c,
												End_Date__c,
												AssetID__r.Factory_Warranty_Expiration_Date__c,
												Vehicle_Type__c
										from	Vehicle_Ownership__c
										where	Status__c = 'Active' and Valid_for_Offers__c = false and AssetID__r.Factory_Warranty_Expiration_Date__c != null limit 1];
        
        
        vo.Status__c = 'Inactive';
        update vo;
        vo.Status__c = 'Active';
        update vo;
        Vehicle_Ownership__c cloneVo = vo.clone(false, false, false, false);
        insert cloneVo;

        
    }
}