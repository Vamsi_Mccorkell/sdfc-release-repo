@isTest
private class SP_TestCMUtils_Basic_Pt3
{
//	This test class excercises cases 17 - 24 of the Customer Matching Rules
//	using the Basic call to the utility

   static testMethod void myTestCustomerMatchingUtils()
    {
		/*String strlastName = '';
		String strEmail = ''; 
		String strMobile = ''; 
		String strCity = ''; 
		String strPostcode = ''; 
		String strStreet = '';

		// Create some test data
		Asset__c asst = new Asset__c	(
											Name = 'ABC12345678'
										);
		insert asst;

		Asset__c asst2 = new Asset__c	(
											Name = 'ABC123DIFF8'
										);
		insert asst2;

		Account acct1 = new Account();
		acct1.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		acct1.LastName				= 'TestLast1';
		acct1.PersonEmail			= 'test1@email.com';
		acct1.PersonMobilePhone		= '194837465';
		acct1.ShippingCity			= 'Sydney';
		acct1.ShippingStreet		= 'George';
		acct1.ShippingPostalCode	= '2001';
		acct1.BillingCity			= 'Melbourne';
		acct1.BillingStreet			= 'Collins';
		acct1.BillingPostalCode		= '3001';

		insert acct1;



		Vehicle_Ownership__c vo = new Vehicle_Ownership__c	(
																Customer__c	= acct1.Id,
																AssetID__c = asst.Id
															);
		insert vo;*/

/*		Matching rules

		Action depends on the sequence/combination of field matches - same VIN, same email, same mobile, same last name
		and similar shipping address

					E	M
		T C			m	o	L	A
		e a		V	a	b	a	d
		s s		I	i	P	s	d
		t e		N	l	h	t	r	Action
		=========================================
		  1		Y	Y	Y	Y	Y	Customer match
		  2		Y	Y	Y	Y	N	Same customer - update address
		  3		Y	Y	Y	N	Y	Same customer - update name
		  4		Y	Y	Y	N	N	Raise Case
		  5		Y	Y	N	Y	Y	Same customer - update mobile
		  6		Y	Y	N	Y	N	Same customer - update address and mobile
		  7		Y	Y	N	N	Y	Same customer - update name and mobile
		  8		Y	Y	N	N	N	Raise Case
				....
		  9		Y	N	Y	Y	Y	Same customer - update email
		 10		Y	N	Y	Y	N	Same customer - update address and email
		 11		Y	N	Y	N	Y	Same customer - update name and email
		 12		Y	N	Y	N	N	Raise Case
		 13		Y	N	N	Y	Y	Customer match	<--- RULE CHANGE 02DEC10 was raise Case
		 14		Y	N	N	Y	N	Raise Case
		 15		Y	N	N	N	Y	Raise Case
		 16		Y	N	N	N	N	Different customer
				....
		 17		N	Y	Y	Y	Y	Customer match
		 18		N	Y	Y	Y	N	Same customer - update address
		 19		N	Y	Y	N	Y	Same customer - update name
		 20		N	Y	Y	N	N	Raise Case
		 21		N	Y	N	Y	Y	Same customer - update mobile
		 22		N	Y	N	Y	N	Same customer - update address and mobile
		 23		N	Y	N	N	Y	Raise Case
		 24		N	Y	N	N	N	Raise Case
				....
		 25		N	N	Y	Y	Y	Customer match
		 26		N	N	Y	Y	N	Same customer - update address and email
		 27		N	N	Y	N	Y	Raise Case
		 28		N	N	Y	N	N	Raise Case
		 29		N	N	N	Y	Y	Customer match	<--- RULE CHANGE 02DEC10 was raise Case
		 30		N	N	N	Y	N	Different customer
		 31		N	N	N	N	Y	Different customer
		 32		N	N	N	N	N	Different customer

*/

		/*// Test case 17
		string strResult17 = MatchingAccount_WS.MatchingAccountCreateCase(
			'ABC123DIFF8', 'DummyFirstName',							// VIN does NOT match
			'TestLast1', 'test1@email.com', '194837465', 'Sydney', '2001', 'George', false);

		// Exact match
		system.debug('*** strResult17 ***' + strResult17);
		system.assertEquals(acct1.Id, strResult17, 'Test case 17 failed');

		// Test case 18
		string strResult18 = MatchingAccount_WS.MatchingAccountCreateCase(
			'ABC123DIFF8', 'DummyFirstName',							// VIN does NOT match
			'TestLast1', 'test1@email.com', '194837465', 'Hobart', '', '', false);

		// Check that address has been marked for change
		system.debug('*** strResult18 ***' + strResult18);
		system.assertEquals(acct1.Id, strResult18, 'Test case 18 failed');

		// Test case 19
		string strResult19 = MatchingAccount_WS.MatchingAccountCreateCase(
			'ABC123DIFF8', 'DummyFirstName',							// VIN does NOT match
			'TestLastDifferent1', 'test1@email.com', '194837465', 'Melbourne', '3001', 'Collins', false);

		// Check that last name has been marked for change
		system.debug('*** strResult19 ***' + strResult19);
		system.assertEquals(acct1.Id, strResult19, 'Test case 19 failed');

		// Test case 20
		string strResult20 = MatchingAccount_WS.MatchingAccountCreateCase(
			'ABC123DIFF8', 'DummyFirstName',							// VIN does NOT match
			'TestLastDifferent1', 'test1@email.com', '194837465', 'Hobart', '', '', false);

		// Check that null is returned
		system.debug('*** strResult20 ***' + strResult20);
		system.assertEquals(null, strResult20, 'Test case 20 failed');

		// Test case 21
		string strResult21 = MatchingAccount_WS.MatchingAccountCreateCase(
			'ABC123DIFF8', 'DummyFirstName',							// VIN does NOT match
			'TestLast1', 'test1@email.com', '222222222', 'Melbourne', '3001', 'Collins', false);

		// Check that mobile has been marked for change
		system.debug('*** strResult21 ***' + strResult21);
		system.assertEquals(acct1.Id, strResult21, 'Test case 21 failed');

		// Test case 22
		string strResult22 = MatchingAccount_WS.MatchingAccountCreateCase(
			'ABC123DIFF8', 'DummyFirstName',							// VIN does NOT match
			'TestLast1', 'test1@email.com', '222222222', 'Hobart', '', '', false);

		// Check that address and mobile have been marked for change
		system.debug('*** strResult22 ***' + strResult22);
		system.assertEquals(acct1.Id, strResult22, 'Test case 22 failed');*/
/*
		// Test case 23
		string strResult23 = MatchingAccount_WS.MatchingAccountCreateCase(
			'ABC123DIFF8', 'DummyFirstName',							// VIN does NOT match
			'TestLastDifferent1', 'test1@email.com', '222222222', 'Sydney', '2001', 'George', false);

		// Check that null is returned
		system.debug('*** strResult23 ***' + strResult23);
		system.assertEquals(null, strResult23, 'Test case 23 failed');

		// Test case 24
		string strResult24 = MatchingAccount_WS.MatchingAccountCreateCase(
			'ABC123DIFF8', 'DummyFirstName',							// VIN does NOT match
			'TestLastDifferent1', 'test1@email.com', '222222222', 'Hobart', '', '', false);

		// Check that null is returned
		system.debug('*** strResult24 ***' + strResult24);
		system.assertEquals(null, strResult24, 'Test case 24 failed');
*/
    }

}