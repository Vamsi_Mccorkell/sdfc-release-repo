public with sharing class SPCacheVehicleTypeMetadata
{
	private static SPCacheVehicleTypeMetadata__c CacheVehicleTypeMetadata;
	
	/***********************************************************************************************************
		Lookup Cached information
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	public static string getDealerTradeIn()
	{
		if (CacheVehicleTypeMetadata == null)
			LoadMetadata(); 
			
		return CacheVehicleTypeMetadata.DealerTradeIn__c;
	}

	public static string getDemonstration()
	{
		if (CacheVehicleTypeMetadata == null)
			LoadMetadata(); 
			
		return CacheVehicleTypeMetadata.Demonstration__c;
	}

	public static string getNewVehicle()
	{
		if (CacheVehicleTypeMetadata == null)
			LoadMetadata(); 
			
		return CacheVehicleTypeMetadata.NewVehicle__c;
	}

	public static string getPre_Owned()
	{
		if (CacheVehicleTypeMetadata == null)
			LoadMetadata(); 
			
		return CacheVehicleTypeMetadata.Pre_Owned__c;
	}

	public static string getPrivateSale()
	{
		if (CacheVehicleTypeMetadata == null)
			LoadMetadata(); 
			
		return CacheVehicleTypeMetadata.PrivateSale__c;
	}

	public static string getWriteOff()
	{
		if (CacheVehicleTypeMetadata == null)
			LoadMetadata(); 
			
		return CacheVehicleTypeMetadata.WriteOff__c;
	}


	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	private static void LoadMetadata()
	{
		CacheVehicleTypeMetadata = SPCacheVehicleTypeMetadata__c.getInstance();
	}
	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	public static testmethod void TestSPCacheVehicleTypeMetadata()
	{
		SPCacheVehicleTypeMetadata.getDealerTradeIn();
		CacheVehicleTypeMetadata = null;

		SPCacheVehicleTypeMetadata.getDemonstration();
		CacheVehicleTypeMetadata = null;

		SPCacheVehicleTypeMetadata.getNewVehicle();
		CacheVehicleTypeMetadata = null;

		SPCacheVehicleTypeMetadata.getPre_Owned();
		CacheVehicleTypeMetadata = null;

		SPCacheVehicleTypeMetadata.getPrivateSale();
		CacheVehicleTypeMetadata = null;

		SPCacheVehicleTypeMetadata.getWriteOff();
		CacheVehicleTypeMetadata = null;

	}
}