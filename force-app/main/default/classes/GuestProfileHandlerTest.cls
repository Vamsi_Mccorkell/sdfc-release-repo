// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for account creating accounts need to view the custom settings data
@IsTest (SeeAllData=true)
public class GuestProfileHandlerTest {
	public static Account customer;
    public static Vehicle_Ownership__c vo;
    public static Asset__c v1;
    
    static testmethod void getCustomerDetailsRequiredFields(){
        MyLexusWebService.GuestProfileResponse groutput = GuestProfileHandler.getCustomerDetails(null,null,null);
        system.assertEquals('700', groutput.errCode);
    }
    
    static testmethod void getCustomerDetailsnoMatch(){
        MyLexusWebService.GuestProfileResponse groutput = GuestProfileHandler.getCustomerDetails('GIGYANOTEXISTING',null,null);
        // 615 No existing account with the gigya ID provided
        system.assertEquals(false, groutput.success);
        system.assertEquals('615', groutput.errCode);
    }
    
    static testmethod void getCustomerDetailSuccessful(){
        dataSetup();
        MyLexusWebService.GuestProfileResponse groutput = GuestProfileHandler.getCustomerDetails(customer.Guest_Gigya_ID__c,null,null);
        system.assertEquals(true, groutput.success);
        system.assertEquals(customer.Id, groutput.recordId);
        system.assertEquals(customer.LastName, groutput.outputAcct.LastName);
        system.assertEquals(customer.Stripe_Customer_ID__c, groutput.outputAcct.stripeCustomerId);
    }
    
    static testmethod void getCustomerDetailAcctIdVOIdSuccessful(){
        dataSetup();
        MyLexusWebService.GuestProfileResponse groutput = GuestProfileHandler.getCustomerDetails(null,customer.Id,vo.Id);
        system.assertEquals(true, groutput.success);
        system.assertEquals(customer.Id, groutput.recordId);
        system.assertEquals(customer.LastName, groutput.outputAcct.LastName);
    }
    
    static testmethod void getCustomerDetailMagicLinkExpired(){
        dataSetup();
        vo.MagicLink_Sent_Date__c = system.today() - 20;
        update vo;
        MyLexusWebService.GuestProfileResponse groutput = GuestProfileHandler.getCustomerDetails(null,customer.Id,vo.Id);
        system.assertEquals(false, groutput.success);
        system.assertEquals(null, groutput.recordId);
        system.assertEquals('614', groutput.errCode);
        
    }
    
    static testmethod void getCustomerDetailHasExistingGR(){
        dataSetup();
        MyLexusWebService.GRResponse wsoutput = GuestRegistrationHandler.guestRegistration(null, null, null, null, customer.Guest_Gigya_ID__c, customer.Id, vo.Id);
        // 610 An account is found with GR records but no Gigya ID
        String gigya = customer.Guest_Gigya_ID__c;
        customer.Guest_Gigya_ID__c = null;
        update customer;
        wsoutput = GuestRegistrationHandler.guestRegistration(null, null, null, null, gigya, customer.Id, vo.Id);
        
        MyLexusWebService.GuestProfileResponse groutput = GuestProfileHandler.getCustomerDetails(null,customer.Id,vo.Id);
        system.assertEquals(false, groutput.success );
        system.assertEquals('610', groutput.errCode );
        
    }
    
    static testmethod void writeCustomerDetailsRequiredFields(){
        dataSetup();
        MyLexusWebService.GuestProfileResponse groutput = GuestProfileHandler.writeCustomerDetails(null);
        system.assertEquals('700', groutput.errCode);
        MyLexusWebService.GuestAccount gaTest = new MyLexusWebService.GuestAccount();
        gaTest.gigyaId = null;
        groutput = GuestProfileHandler.writeCustomerDetails(gaTest);
        system.assertEquals('700', groutput.errCode);
    }
    
    static testmethod void writeCommunicationPreferencesRequiredFields(){
        dataSetup();
        MyLexusWebService.GuestProfileResponse groutput = GuestProfileHandler.writeCommunicationPreferences(null);
        system.assertEquals('700', groutput.errCode);
        MyLexusWebService.GuestAccount gaTest = new MyLexusWebService.GuestAccount();
        gaTest.gigyaId = null;
        groutput = GuestProfileHandler.writeCommunicationPreferences(gaTest);
        system.assertEquals('700', groutput.errCode);
    }
    
    static testmethod void writeCustomerDetailsNoMatch(){
        dataSetup();
        MyLexusWebService.GuestAccount gaTest = new MyLexusWebService.GuestAccount();
        gaTest.gigyaId = 'NOTEXISTINGGIGYAID';
        MyLexusWebService.GuestProfileResponse groutput = GuestProfileHandler.writeCustomerDetails(gaTest);
        // 615 No existing account with the gigya ID provided
        system.assertEquals(false, groutput.success);
        system.assertEquals('615', groutput.errCode);
    }
    
    static testmethod void writeCommunicationPreferencesNoMatch(){
        dataSetup();
        MyLexusWebService.GuestAccount gaTest = new MyLexusWebService.GuestAccount();
        gaTest.gigyaId = 'NOTEXISTINGGIGYAID';
        MyLexusWebService.GuestProfileResponse groutput = GuestProfileHandler.writeCommunicationPreferences(gaTest);
        // 615 No existing account with the gigya ID provided
        system.assertEquals(false, groutput.success);
        system.assertEquals('615', groutput.errCode);
    }
    
    static testmethod void writeCustomerDetailsSuccessful(){
        dataSetup();
        MyLexusWebService.GuestAccount gaTest = new MyLexusWebService.GuestAccount();
        gaTest.gigyaId = customer.Guest_Gigya_ID__c;
        gaTest.firstName = customer.FirstName + 't';
        gaTest.lastName = customer.LastName + 't';
        gaTest.email = customer.PersonEmail + '.nz';
        gaTest.mobilePhone = customer.PersonMobilePhone + '1';
        gaTest.homeStreet = customer.ShippingStreet + 't';
        gaTest.homeSuburb = customer.ShippingCity + 't';
        gaTest.homeState = customer.ShippingState + 't';
        gaTest.homePostCode = customer.ShippingPostalCode + 't';
        gaTest.postalSameAsHome = true;
        gaTest.interests = customer.Leisure_Time_Activities__pc + 'test;';
        
        MyLexusWebService.GuestProfileResponse groutput = GuestProfileHandler.writeCustomerDetails(gaTest);
        system.assertEquals(true, groutput.success);
        system.assertEquals(customer.Id, groutput.recordId);
        system.assertEquals(customer.LastName + 't', groutput.outputAcct.lastName);
        List<Case> returnedCaseList = [select Id from Case where Subject = 'Update Details from Ownership app'
                                      and AccountId = :customer.Id and Vehicle_Ownership__c = :vo.Id];
        system.assertEquals(1, returnedCaseList.size());
        
    }
    
    static testmethod void writeCustomerDetailsSuccessfulPostal(){
        dataSetup();
        MyLexusWebService.GuestAccount gaTest = new MyLexusWebService.GuestAccount();
        gaTest.gigyaId = customer.Guest_Gigya_ID__c;
        gaTest.firstName = customer.FirstName + 't';
        gaTest.lastName = customer.LastName + 't';
        gaTest.email = customer.PersonEmail + '.nz';
        gaTest.mobilePhone = customer.PersonMobilePhone + '1';
        gaTest.homeStreet = customer.ShippingStreet + 't';
        gaTest.homeSuburb = customer.ShippingCity + 't';
        gaTest.homeState = customer.ShippingState + 't';
        gaTest.homePostCode = customer.ShippingPostalCode + 't';
        gaTest.postalSameAsHome = false;
        gaTest.postalStreet = customer.BillingStreet + 'x';
        gaTest.postalSuburb = customer.BillingCity + 'x';
        gaTest.postalState = customer.BillingState + 'x';
        gaTest.postalPostCode = customer.BillingPostalCode + 'x';
        MyLexusWebService.GuestProfileResponse groutput = GuestProfileHandler.writeCustomerDetails(gaTest);
        system.assertEquals(customer.Id, groutput.recordId);
        system.assertEquals(customer.BillingCity + 'x', groutput.outputAcct.postalSuburb);
    }
    static testmethod void writeCommunicationPreferencesSuccessful(){
        dataSetup();
        MyLexusWebService.GuestAccount gaTest = new MyLexusWebService.GuestAccount();
        gaTest.gigyaId = customer.Guest_Gigya_ID__c;
        gaTest.doNotContact = true;
        gaTest.productInfoOptOut = true;
        gaTest.doNotCall = true;
        gaTest.benefitEventsOptOut = true;
        gaTest.preferredDealer = 'TEST DEALER';
        
        MyLexusWebService.GuestProfileResponse groutput = GuestProfileHandler.writeCommunicationPreferences(gaTest);
        system.assertEquals(true, groutput.success);
        system.assertEquals(customer.Id, groutput.recordId);
        system.assertEquals(true, groutput.outputAcct.doNotContact);
        system.assertEquals(true, groutput.outputAcct.productInfoOptOut);
        system.assertEquals(true, groutput.outputAcct.doNotCall);
        system.assertEquals(true, groutput.outputAcct.benefitEventsOptOut);
        
    }
    private static void dataSetup(){
        Id OWNERRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        customer = new Account(FirstName = 'Henry', LastName = 'Sample', PersonEmail = 'sample@henry.com',
                               Guest_Gigya_ID__c = 'GIGYA1111', RecordTypeId = OWNERRECORDTYPEID);
        
        insert customer;
        
        v1 = new Asset__c(Name = 'VIN1111');
        insert v1;
        
        List<Vehicle_Ownership__c> voList = new List<Vehicle_Ownership__c>();
        vo = new Vehicle_Ownership__c(Customer__c = customer.Id, AssetID__c = v1.Id, Start_Date__c = system.today(),
                                     Status__c = 'Active', Vehicle_Type__c = 'New Vehicle', MagicLink_Sent_Date__c = system.today());
        voList.add(vo);
                
        insert voList;
        
    }
}