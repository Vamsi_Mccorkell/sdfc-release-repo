global with sharing class SP_ServiceIntegrationSchedule implements Schedulable
{

	global void execute(SchedulableContext SC) 
	{
        SP_ServiceIntegrationBatch objSP_ServiceIntegrationBatch = new SP_ServiceIntegrationBatch();
        ID batchprocessid = Database.executeBatch(objSP_ServiceIntegrationBatch, 10);		

//		System.Schedule('SP_ServiceIntegrationSchedule', '0 30 * * * ?', new SP_ServiceIntegrationSchedule());
	}


    public static testMethod void testSP_ServiceIntegrationSchedule() 
	{
		Test.startTest(); 
		
		System.Schedule('SP_ServiceIntegrationSchedule', '20 30 8 10 2 ?', new SP_ServiceIntegrationSchedule());

		Test.stopTest();
    }

}