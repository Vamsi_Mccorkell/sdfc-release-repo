// Class to handle all use cases related to Enform User
// Author: Shabu, SP on 02.04.2013

public class SP_EnformUserManagement  
{
	// Check each Enform User for the validity of mandatory fields and Account/Lead lookup
	public static void doValidateEnformUser(List<Enform_User__c> li_enformUsers, string action)
	{
		SP_EnformUtilities.ValidateEnformUsers(li_enformUsers, action);
	} 
	
	// Update accounts related to primary enform users and leads associated with secondary enform users
	public static void doUpdateAccountsAndLeadsForEnformUsers(map<ID, Enform_User__c> newMap, map<ID, Enform_User__c> oldMap)
	{
		for (Enform_User__c newEU : newMap.values())
		{
			Enform_User__c oldEU = oldMap.get(newEU.Id);
			
			if ((oldEU.Owner_Prospect_ID__c == null && newEU.Owner_Prospect_ID__c != null) ||
				(oldEU.Lead_ID__c == null && newEU.Lead_ID__c != null))
				return;
		}		
		if (system.isBatch() || system.isFuture() || system.isScheduled())
			SP_EnformUtilities.UpdateAccountsAndLeadsForEnformUsersNoFuture(newMap.keySet());
		else
			SP_EnformUtilities.UpdateAccountsAndLeadsForEnformUsers(newMap.keySet());
	}
	
	// Create leads for secondary enform users
	public static void doCreateLeadFromEnformUsers(map<ID, Enform_User__c> newMap) 
	{
		if (system.isBatch() || system.isFuture() || system.isScheduled())
			SP_EnformUtilities.createLeadFromEnformUsersNoFuture(newMap.keySet());
		else
			SP_EnformUtilities.createLeadFromEnformUsers(newMap.keySet());
	}

	/*@isTest(SeeAllData=true)
	public static void testMethodTriggerNotifyIntelematicsOnChange()
	{
		Test.startTest();
		
		List<Enform_User__c> testEnfUsers = new List<Enform_User__c>();
	
		Account sAccountWithEU = new Account();
		sAccountWithEU.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccountWithEU.LastName				= 'TestLast1';
		sAccountWithEU.PersonEmail			= 'test1@email.com';
		
		insert sAccountWithEU;
		
		Enform_User__c testEUAcc = new Enform_User__c(Owner_Id__c = sAccountWithEU.Id,
												   Mobile_Phone__c = '045678923',
												   Email__c = 'aaa@aa.com',
												   First_Name__c = 'First',
												   Last_Name__c = 'Last',
												   IsPrimaryUser__c = true);
		testEnfUsers.add(testEUAcc);
		
		Lead testLeadWithEU = new Lead(LastName='Test1');
		insert testLeadWithEU;
		
		Enform_User__c testEULead = new Enform_User__c(Lead_ID__c = testLeadWithEU.Id,
												   Mobile_Phone__c = '045678923',
												   Email__c = 'aaa@aa.com',
												   First_Name__c = 'First',
												   Last_Name__c = 'Last');
												   
		testEnfUsers.add(testEULead);
		
		Enform_User__c testEUAccForValid = new Enform_User__c(Lead_ID__c = testLeadWithEU.Id,
													Mobile_Phone__c = '045678923',
												   	Email__c = 'aaa@aa.com',
												   	IsPrimaryUser__c = true);
		testEnfUsers.add(testEUAccForValid);
		
		Enform_User__c testEULeadForValid = new Enform_User__c(Owner_Id__c = sAccountWithEU.Id,
													Mobile_Phone__c = '045678923',
												   	Email__c = 'aaa@aa.com',
												   	IsPrimaryUser__c = true);
		testEnfUsers.add(testEULeadForValid);		
		
		insert testEnfUsers;
		
		Test.stopTest();
	}*/	

}