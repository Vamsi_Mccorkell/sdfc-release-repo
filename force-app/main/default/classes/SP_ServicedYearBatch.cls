global with sharing class SP_ServicedYearBatch implements Database.Batchable<sObject>
{
	public class SP_ServicedYearBatchException extends Exception{}

	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		string	strQuery;

		if (!test.isRunningTest())
		{
			strQuery =		
				' select	Id'	+
				' from 		Asset__c';
		}
		else
		{
			strQuery =		
				' select	Id'	+
				' from 		Asset__c' +
				' where 	Name = ' + '\'' + 'TestABC12345678' + '\'' +
				' limit		1';
		}

		return Database.getQueryLocator(strQuery);
	}


	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		list<id> theProcessList = new list<id>();

		for(sobject s : scope)
		{
			theProcessList.add((id)s.get('Id'));
		}

		SP_ServicedYearProcessing.SP_Args oArgs = new SP_ServicedYearProcessing.SP_Args(theProcessList);

		SP_ServicedYearProcessing processor = new SP_ServicedYearProcessing(oArgs);
		SP_ServicedYearProcessing.SP_Ret oRet = processor.ProcMain();
	}


	global void finish(Database.BatchableContext BC)
	{
		system.debug('Finished');
	}

	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static testMethod void testBatch() 
	{
		// Set up some database records
		Product2 sProduct1 = new Product2();
		sProduct1.Name					= 'IS200';
		insert sProduct1;

		Asset__c sAsset1 = new Asset__c();
		sAsset1.Name					= 'TestABC12345678';
		sAsset1.Vehicle_Model__c		= sProduct1.Id;
		insert sAsset1;

		list<Account> li_Accounts = new list<Account>();

		Account sAccount = new Account();
		sAccount.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccount.LastName				= 'TestLast1';
		sAccount.PersonEmail			= 'test1@email.com';
		li_Accounts.add(sAccount);

		Account sAccount2 = new Account();
		sAccount2.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccount2.LastName				= 'TestLast2';
		sAccount2.PersonEmail			= 'test2@email.com';
		li_Accounts.add(sAccount2);

		insert li_Accounts;

		list<Service__c> li_Services = new list<Service__c>();

		Service__c sService1 = new Service__c(); 
		sService1.Customer_Account__c	= sAccount.Id;
		sService1.Asset__c				= sAsset1.Id;
		sService1.Vehicle_Mileage__c	= 123000;
		sService1.Service_Date__c		= system.today().addYears(-3);
		li_Services.add(sService1);
		
		Service__c sService2 = new Service__c(); 
		sService2.Customer_Account__c	= sAccount.Id;
		sService2.Asset__c				= sAsset1.Id;
		sService2.Vehicle_Mileage__c	= 187000;
		sService2.Service_Date__c		= system.today().addYears(-2);
		li_Services.add(sService2);

		insert li_Services;

		SP_ServicedYearBatch oServicedYearBatch = new SP_ServicedYearBatch();
		oServicedYearBatch.start(null);
		oServicedYearBatch.execute(null, new list<SObject>{sAsset1});
		oServicedYearBatch.finish(null);

	}
}