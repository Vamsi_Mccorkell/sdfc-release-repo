// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for  creating accounts need to view the custom settings data
@IsTest (SeeAllData=true)
public class VoucherServicesWebServiceTest {
	// DEVELOPER NOTES: These are just all coverage test methods; The assertion test methods are in the corresponding
    // test method classes for each of the handler classes
    public static Account partnerbranch;
    public static VO_Offer_Detail__c vood;
    
    static testmethod void attendantLogin(){
        dataSetup();
        VoucherServicesWebService.attendantLogin(partnerbranch.Partner_Branch_Username__c, partnerbranch.Partner_Branch_Password__c);
    }
    
    
    static testmethod void resendPassword(){
        dataSetup();
        VoucherServicesWebService.resendPassword(partnerbranch.Partner_Branch_Username__c);
    }
    
    static testmethod void redeemVoucher(){
        dataSetup();
        VoucherServicesWebService.redeemVoucher(partnerbranch.Id, 'Voucher Type', vood.Id, true, 'Comp Test', 'Error Code');
    }
    
    static testmethod void getVoucherAvailability(){
        dataSetup();
        VoucherServicesWebService.getVoucherAvailability(partnerbranch.Id, 'regoNumber', 'voucherType');
    }
    
    
    private static void dataSetup(){
        Id OWNERRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        Id PARTNERBRANCHRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Branch').getRecordTypeId();
        partnerbranch = new Account(Name = 'Partner Branch', RecordTypeId = PARTNERBRANCHRECORDTYPEID,
                                   Partner_Branch_Username__c = 'pusername', Partner_Branch_Password__c = 'password', BillingCity = 'Test', BillingState = 'Test');
        insert partnerbranch;
        vood = new VO_Offer_Detail__c();
        
        
    }
}