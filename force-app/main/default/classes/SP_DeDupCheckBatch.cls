global with sharing class SP_DeDupCheckBatch implements Database.Batchable<sObject>
{
	public class SP_DeDupCheckBatchException extends Exception{}

	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		string	strQuery =	
	
			' select	Id'	+
			' from 		Duplicate_Check_Job__c' +
			' where 	Active__c = true ' +
			' and 		In_Error__c = false';

			
		return Database.getQueryLocator(strQuery);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		list<id> theProcessList = new list<id>();

		for(sobject s : scope)
		{
			theProcessList.add((id)s.get('Id'));
		}

		SP_DeDupCheckProcessing.SP_Args oArgs = new SP_DeDupCheckProcessing.SP_Args(theProcessList);

		SP_DeDupCheckProcessing processor = new SP_DeDupCheckProcessing(oArgs);
		SP_DeDupCheckProcessing.SP_Ret oRet = processor.ProcMain();
	}


	global void finish(Database.BatchableContext BC)
	{
		system.debug('Finished');
	}

	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static testMethod void testBatch() 
	{

	}
}