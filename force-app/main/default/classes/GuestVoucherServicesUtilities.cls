/* DATE: September 2019
 * AUTHOR: D BANEZ / MCCORKELL
 * CLASS SUMMARY: Utility class for all Offer Detail records creation (used in various trigger handlers)
 * TRIGGER HANDLERS USING THIS CLASS: CODTriggerHandler, VOODTriggerHandler, VOTriggerHandler, AssetTriggerHandler, VoucherTriggerHandler
 * TEST CLASS: GuestVoucherServicesUtilitiesTest
 * */
public class GuestVoucherServicesUtilities {
    public static Boolean createODRecordsHasRun = false;
    public static void createCODVINODVOODRecords(Set<Id> offerIdSet, Set<Id> vinIdSet, Set<Id> voIdSet){
        createODRecordsHasRun = true;
        // all required lists to iterate
        List<Offer__c> offerList = new List<Offer__c>();
        List<Vehicle_Ownership__c> voList = new List<Vehicle_Ownership__c>();
        List<Asset__c> assetList = new List<Asset__c>();
        List<Account> customerList = new List<Account>();
        
        // sets to check existing OD junction objects
        Set<String> offerVINSet = new Set<String>();
        Set<String> offerCustomerSet = new Set<String>();
        Set<String> offerVOSet = new Set<String>();
        
        // Sets for Offer, COD and VINOD parents
        Set<Id> vinodIdSet = new Set<Id>();
        Set<Id> codIdSet = new Set<Id>();
        
        
        // All Maps required
        Map<String, Vin_Offer_Detail__c> offerVinVinodMap = new Map<String, Vin_Offer_Detail__c>();
        Map<Id, String> customerIdEncoreTierMap = new Map<Id, String>();
        Map<Id, Date> customerIdExpiryDateMap = new Map<Id, Date>();
        Map<String, Id> customerIdofferIdCODIdMap = new Map<String, Id>();
        
        
        // Lists to create
        List<Vin_Offer_Detail__c> vinodListToCreate = new List<Vin_Offer_Detail__c>();
        List<Customer_Offer_Detail__c> codListToInsert = new List<Customer_Offer_Detail__c>();
        List<VO_Offer_Detail__c> voodListToInsert = new List<VO_Offer_Detail__c>();
        
        
        // Record type definitions
        Id CUSTOMEROFFERVINODRECORDTYPEID = Schema.SObjectType.Vin_Offer_Detail__c.getRecordTypeInfosByDeveloperName().get('Customer_Offer').getRecordTypeId();
        Id VEHICLEOFFERVINODRECORDTYPEID = Schema.SObjectType.Vin_Offer_Detail__c.getRecordTypeInfosByDeveloperName().get('Vehicle_Offer').getRecordTypeId();
        Id VEHICLEOFFERRECORDTYPEID = Schema.SObjectType.VO_Offer_Detail__c.getRecordTypeInfosByDeveloperName().get('Vehicle_Offer').getRecordTypeId();
        Id CUSTOMEROFFERRECORDTYPEID = Schema.SObjectType.VO_Offer_Detail__c.getRecordTypeInfosByDeveloperName().get('Customer_Vehicle_Offer').getRecordTypeId();
        
        Date cutoffDate = system.today().addDays(-Integer.valueOf(Label.VOCutOffDays));
        
        // Get all required lists 
        
        // 1. Get a list of Offers and their related records
        if (offerIdSet == null){
            offerIdSet = new Set<Id>();
            offerList = [select Id, Offer_Class__c,Customer_Platinum_Limit__c, Customer_Basic_Limit__c,
                     Vehicle_Offer_Basic_Limit__c, Vehicle_Offer_Platinum_Limit__c,
                     (select Id, Offer__c, Customer__c from Customer_Offer_Details__r),
                     (select Id, Offer__c, Vehicle_Ownership__c from VO_Offer_Details__r),
                     (select Id, Offer__c, VIN__c from Vin_Offer_Details__r)
                     from Offer__c
                     where Status__c = 'Active'];
        }
        else {
            offerList = [select Id, Offer_Class__c,Customer_Platinum_Limit__c, Customer_Basic_Limit__c,
                     Vehicle_Offer_Basic_Limit__c, Vehicle_Offer_Platinum_Limit__c,
                     (select Id, Offer__c, Customer__c from Customer_Offer_Details__r),
                     (select Id, Offer__c, Vehicle_Ownership__c from VO_Offer_Details__r),
                     (select Id, Offer__c, VIN__c from Vin_Offer_Details__r)
                     from Offer__c
                     where Id in :offerIdSet];
        }
        
        if (voIdSet == null){
            voIdSet = new Set<Id>();
            if (vinIdSet == null){
                vinIdSet = new Set<Id>();
                voList = [select Id, Valid_for_Offers__c, Asset_Encore_Tier__c, Customer__c,
                          Valet_Voucher_Expiry_Date__c, AssetID__c
                          from Vehicle_Ownership__c 
                          where (Asset_Encore_Tier__c = 'Basic' or Asset_Encore_Tier__c = 'Platinum')
                          and Valet_Voucher_Expiry_Date__c >= TODAY
                          and Status__c ='Active'
                         and SystemModStamp >= :cutoffDate];
                
            }
            else {
                voList = [select Id, Valid_for_Offers__c, Asset_Encore_Tier__c, Customer__c,
                                            Valet_Voucher_Expiry_Date__c, AssetID__c
                                             from Vehicle_Ownership__c where AssetID__c in :vinIdSet
                                            and (Asset_Encore_Tier__c = 'Basic' or Asset_Encore_Tier__c = 'Platinum')
                                              and Valet_Voucher_Expiry_Date__c >= TODAY
                                              and Status__c ='Active'
                         					and SystemModStamp >= :cutoffDate];
            }
            
        }
        else {
            voList = [select Id, Valid_for_Offers__c, Asset_Encore_Tier__c, Customer__c,
                                            Valet_Voucher_Expiry_Date__c, AssetID__c
                                             from Vehicle_Ownership__c where Id in :voIdSet];
        }
        /************ UNIQUE SET FOR OD JUNCTION RECORDS DEFINITION *************/
        // if there is an offer
        for (Offer__c o : offerList){
            offerIdSet.add(o.Id);
            for (Vin_Offer_Detail__c vinod : o.Vin_Offer_Details__r){
                offerVINSet.add(String.valueOf(vinod.Offer__c) + String.valueOf(vinod.VIN__c));
            }
            if (o.Offer_Class__c == 'Customer'){
            	for (Customer_Offer_Detail__c cood : o.Customer_Offer_Details__r){
                    offerCustomerSet.add(String.valueOf(cood.Offer__c) + String.valueOf(cood.Customer__c));    
                }
            }
            for (VO_Offer_Detail__c vod : o.VO_Offer_Details__r){
            	offerVOSet.add(String.valueOf(vod.Offer__c) + String.valueOf(vod.Vehicle_Ownership__c));    
            }
        }
        
        // Preparing for vinod and cod
        // Get all accounts for this list of VO
        for (Vehicle_Ownership__c vo : voList){
            voIdSet.add(vo.Id);
            vinIdSet.add(vo.AssetID__c);
            if (vo.Asset_Encore_Tier__c == 'Platinum' || vo.Asset_Encore_Tier__c == 'Basic'){
                customerIdEncoreTierMap.put(vo.Customer__c, vo.Asset_Encore_Tier__c); 
                customerIdExpiryDateMap.put(vo.Customer__c, vo.Valet_Voucher_Expiry_Date__c);
            }
        }
        
        /************ VINOD CREATION *************/
        // Check if the VINOD is already created; if not, create it
        // do this only if there are VO's valid for offer underneath it
        if (voList.size() > 0){
            for (Id vinId : vinIdSet){
                for (Offer__c o : offerList){
                    if (!offerVINSet.contains(String.valueOf(o.Id) + String.valueOf(vinId))){
                        Vin_Offer_Detail__c vinod = new Vin_Offer_Detail__c();
                        vinod.Offer__c = o.Id;
                        vinod.VIN__c = vinId;
                        if (o.Offer_Class__c == 'Customer'){
                            vinod.RecordTypeId =  CUSTOMEROFFERVINODRECORDTYPEID;  
                        }
                        else if (o.Offer_Class__c == 'Vehicle'){
                            vinod.RecordTypeId =  VEHICLEOFFERVINODRECORDTYPEID;  
                        }
                        vinodListToCreate.add(vinod);
                    }
                }
            }
        }
        
        if (vinodListToCreate.size() > 0){
            insert vinodListToCreate;
        }
        
        // create a map of existing VINOD's
        List<Vin_Offer_Detail__c> vinodExistingList = [select Id, Offer__c, VIN__c, Total_Customer_Credits_Available__c,
                                                       Total_Vehicle_Vouchers_Available__c from Vin_Offer_Detail__c
                                                      where Offer__c in :offerIdSet and VIN__c in :vinIdSet];
        
        for (Vin_Offer_Detail__c vinod : vinodExistingList){
            offerVinVinodMap.put(String.valueOf(vinod.Offer__c) + String.valueOf(vinod.VIN__c), vinod);
            vinodIdSet.add(vinod.Id);
        }
        
        /********  END OF VINOD CREATION **********/
        
        /******** COD CREATION *************/
        
        // now check if the Offer already is linked; if not create it
        for (Id acctId : customerIdEncoreTierMap.keySet()){
            for (Offer__c o : offerList){
                if (o.Offer_Class__c == 'Customer'){
                    if (!offerCustomerSet.contains(String.valueOf(o.Id) + String.valueOf(acctId))){ // if it is not yet existing
                        if (customerIdEncoreTierMap.containsKey(acctId)){
                            Customer_Offer_Detail__c newCOD = new Customer_Offer_Detail__c();
                            newCOD.Customer__c = acctId;
                            newCOD.Offer__c = o.Id;
                            if (customerIdExpiryDateMap.containsKey(acctId)){
                                newCOD.First_Credit_Expiration__c = customerIdExpiryDateMap.get(acctId);
                            }
                            if (customerIdEncoreTierMap.get(acctId) == 'Platinum' && o.Customer_Platinum_Limit__c > 0){
                                newCOD.Total_Credits_Allowed__c = o.Customer_Platinum_Limit__c;
                                codListToInsert.add(newCOD);
                            }
                            else if (customerIdEncoreTierMap.get(acctId) == 'Basic' && o.Customer_Basic_Limit__c > 0){
                                newCOD.Total_Credits_Allowed__c = o.Customer_Basic_Limit__c;
                                codListToInsert.add(newCOD);
                            }
                        }
                    }
                }
            }
        }
        
        if (codListToInsert.size() > 0){
            insert codListToInsert;
        }
        List<Customer_Offer_Detail__c> codExistingList = [select Id, Customer__c, Offer__c from Customer_Offer_Detail__c
                                                          where Offer__c in :offerIdSet];
        
        for (Customer_Offer_Detail__c cod : codExistingList){
            customerIdofferIdCODIdMap.put(String.valueOf(cod.Customer__c) + String.valueOf(cod.Offer__c), cod.Id);
        }
        
        /********  END OF COD CREATION **********/
        
        /********  VOOD CREATION **********/
        // Check if the VOOD exists, otherwise create it
        for (Vehicle_Ownership__c vo : voList){
            for (Offer__c o : offerList){
                if (!offerVOSet.contains(String.valueOf(o.Id) + String.valueOf(vo.Id))){ // if it is not yet existing
                    VO_Offer_Detail__c newVOD = new VO_Offer_Detail__c();
                    newVOD.Offer__c = o.Id; 
                    newVOD.Vehicle_Ownership__c = vo.Id;
                    newVOD.VIN__c = vo.AssetID__c;
                    if (offerVinVinodMap.containsKey(String.valueOf(o.Id) + String.valueOf(vo.AssetID__c))){
                        newVOD.Vin_Offer_Detail__c = offerVinVinodMap.get(String.valueOf(o.Id) + String.valueOf(vo.AssetID__c)).Id;
                    }
                    if (o.Offer_Class__c == 'Customer'){
                        newVOD.RecordTypeId = CUSTOMEROFFERRECORDTYPEID;
                        if (customerIdofferIdCODIdMap.containsKey(String.valueOf(vo.Customer__c) + String.valueOf(o.Id))){
                            newVOD.CustomerOfferDetail__c = customerIdofferIdCODIdMap.get(String.valueOf(vo.Customer__c) + String.valueOf(o.Id)); // associate the COD
                            codIdSet.add(newVOD.CustomerOfferDetail__c);
                        }
                        if (vo.Asset_Encore_Tier__c == 'Platinum' && o.Customer_Platinum_Limit__c > 0){
                            if (newVOD.Vin_Offer_Detail__c == null) {
                                newVOD.Credit_Offer_Limit__c = o.Customer_Platinum_Limit__c;
                            }
                            else {
                                newVOD.Credit_Offer_Limit__c = offerVinVinodMap.get(String.valueOf(o.Id) + String.valueOf(vo.AssetID__c)).Total_Customer_Credits_Available__c;
                            }
                            voodListToInsert.add(newVOD);
                        }
                        else if (vo.Asset_Encore_Tier__c == 'Basic' && o.Customer_Basic_Limit__c > 0){
                            if (newVOD.Vin_Offer_Detail__c == null) {
                                newVOD.Credit_Offer_Limit__c = o.Customer_Basic_Limit__c;
                            }
                            else {
                                newVOD.Credit_Offer_Limit__c = offerVinVinodMap.get(String.valueOf(o.Id) + String.valueOf(vo.AssetID__c)).Total_Customer_Credits_Available__c;
                            }
                            voodListToInsert.add(newVOD);
                        }
                    }
                    else if (o.Offer_Class__c == 'Vehicle'){
                        newVOD.RecordTypeId = VEHICLEOFFERRECORDTYPEID; 
                        if (vo.Asset_Encore_Tier__c == 'Platinum' && o.Vehicle_Offer_Platinum_Limit__c > 0){
                            if (newVOD.Vin_Offer_Detail__c == null) {
                                newVOD.Voucher_Offer_Limit__c = o.Vehicle_Offer_Platinum_Limit__c;
                            }
                            else {
                                newVOD.Voucher_Offer_Limit__c = offerVinVinodMap.get(String.valueOf(o.Id) + String.valueOf(vo.AssetID__c)).Total_Vehicle_Vouchers_Available__c;
                            }
                            
                            voodListToInsert.add(newVOD);
                        }
                        else if (vo.Asset_Encore_Tier__c == 'Basic' && o.Vehicle_Offer_Basic_Limit__c > 0){
                            if (newVOD.Vin_Offer_Detail__c == null) {
                                newVOD.Voucher_Offer_Limit__c = o.Vehicle_Offer_Basic_Limit__c;
                            }
                            else {
                                newVOD.Voucher_Offer_Limit__c = offerVinVinodMap.get(String.valueOf(o.Id) + String.valueOf(vo.AssetID__c)).Total_Vehicle_Vouchers_Available__c;
                            }
                            voodListToInsert.add(newVOD);
                        }
                    }
                }
            }
        }
        
        if (voodListToInsert.size() > 0){
            insert voodListToInsert;
        }
        
        /********  END OF VOOD CREATION **********/
        
        /***** RECALCULATE   *****/
        if (codIdSet.size() > 0){
            recalculateCODCounts(codIdSet);
        }
        
        
        // recalculate
        if (vinIdSet.size() > 0){
            recalculateVINODCounts(vinodIdSet);
        }
    }
    
    public static void recalculateCODCounts(Set<Id> codIdSet){
        Id CUSTOMERVOUCHERRECORDTYPEID = Schema.SObjectType.Voucher__c.getRecordTypeInfosByDeveloperName().get('Customer_Voucher').getRecordTypeId();
        Id CUSTOMERVEHICLEOFFERRECORDTYPEID = Schema.SObjectType.VO_Offer_Detail__c.getRecordTypeInfosByDeveloperName().get('Customer_Vehicle_Offer').getRecordTypeId();
        
        // Get COD for counts at the COD level
        List<Customer_Offer_Detail__c> codList = new List<Customer_Offer_Detail__c> ();
        if (codIdSet.size() > 0){
            codList = [select Id,Total_Credits_Available__c,
                                            (select Id, Customer_Credits_Available__c, Voucher_Expiration__c,
                                             Include_in_COD_Computation__c, Adjusted_Credit_Offer_Limit__c 
                                             from VO_Offer_Details__r 
                                            where RecordTypeId = :CUSTOMERVEHICLEOFFERRECORDTYPEID
                                             and Include_in_COD_Computation__c = TRUE
                                            order by Voucher_Expiration__c asc, Vehicle_Ownership__r.CreatedDate asc),
                                            (select Id, Customer_Credits_Consumed__c, VO_Offer_Details__r.Voucher_Expiration__c from Vouchers__r
                                            where RecordTypeId = :CUSTOMERVOUCHERRECORDTYPEID
                                            and VO_Offer_Details__r.Include_in_COD_Computation__c = TRUE)
                                            from Customer_Offer_Detail__c  
                                            where Id in :codIdSet];
        }
        else {
            codList = [select Id,Total_Credits_Available__c,
                       (select Id, Include_in_COD_Computation__c , Customer_Credits_Available__c, Vehicle_Ownership__r.CreatedDate, Voucher_Expiration__c,
                        Adjusted_Credit_Offer_Limit__c 
                        from VO_Offer_Details__r 
                        where RecordTypeId = :CUSTOMERVEHICLEOFFERRECORDTYPEID
                        and Include_in_COD_Computation__c = TRUE
                        order by Voucher_Expiration__c asc, Vehicle_Ownership__r.CreatedDate asc),
                       (select Id, Customer_Credits_Consumed__c, VO_Offer_Details__r.Voucher_Expiration__c
                        from Vouchers__r
                        where RecordTypeId = :CUSTOMERVOUCHERRECORDTYPEID
                       and VO_Offer_Details__r.Include_in_COD_Computation__c = TRUE)
                       from Customer_Offer_Detail__c];
        }
        
        List<Customer_Offer_Detail__c> codToUpdateList = new List<Customer_Offer_Detail__c>();
       	Date todaysDate = system.today();
        for (Customer_Offer_Detail__c cod : codList){
            Decimal creditsAllowed = 0;
            for (VO_Offer_Detail__c vood : cod.VO_Offer_Details__r){
                if (vood.Voucher_Expiration__c >= todaysDate){
                	creditsAllowed += vood.Adjusted_Credit_Offer_Limit__c;    
                }
            }
            Decimal creditsConsumed = 0;
            for (Voucher__C voucher : cod.Vouchers__r){
                if (voucher.VO_Offer_Details__r.Voucher_Expiration__c >= todaysDate){
                	creditsConsumed += voucher.Customer_Credits_Consumed__c;    
                }
            }
         	cod.Total_Credits_Allowed__c = creditsAllowed;
            cod.Total_Customer_Credits_Consumed__c = creditsConsumed;
            // get First Credit Expiry
            Date firstExpiryDate = null;
            for (VO_Offer_Detail__c vood : cod.VO_Offer_Details__r){
                // the listing is ordered in ascending based on voucher expiration
                if (firstExpiryDate == null){ // if first item, assign the first item
                    firstExpiryDate = vood.Voucher_Expiration__c;
                }
                else if (vood.Voucher_Expiration__c < todaysDate && vood.Voucher_Expiration__c > firstExpiryDate){
                    firstExpiryDate = vood.Voucher_Expiration__c;
                }
                else if (vood.Voucher_Expiration__c >= todaysDate && firstExpiryDate < todaysDate){ // first expiry at this point is still in the past
                    firstExpiryDate = vood.Voucher_Expiration__c;
                }
            }
            
            cod.First_Credit_Expiration__c = firstExpiryDate;
            codToUpdateList.add(cod);
        }
        
        if (codToUpdateList.size() > 0){
            update codToUpdateList;
        }
    }
    
    public static void recalculateVINODCounts(Set<Id> vinodIdSet){
        // Get COD for counts at the COD level
        List<Vin_Offer_Detail__c> vinodList = new List<Vin_Offer_Detail__c> ();
        if (vinodIdSet.size() > 0){
            vinodList = [select Id,Total_Customer_Credits_Consumed__c, Total_Vehicle_Vouchers_Consumed__c,
                         Total_Customer_Credits_Available__c , Total_Vehicle_Vouchers_Available__c,
                         (select Id, RecordTypeId, Customer_Credits_Consumed__c from Vouchers__r),
                         (select Id, RecordTypeId, Credit_Offer_Limit__c, Voucher_Offer_Limit__c, 
                          Customer_Credits_Consumed__c, Vouchers_Redeemed__c from VO_Offer_Details__r)
                         from Vin_Offer_Detail__c  
                         where Id in :vinodIdSet];
        }
        else {
            vinodList = [select Id,Total_Customer_Credits_Consumed__c, Total_Vehicle_Vouchers_Consumed__c,
                         Total_Customer_Credits_Available__c , Total_Vehicle_Vouchers_Available__c,
                         (select Id, RecordTypeId, Customer_Credits_Consumed__c from Vouchers__r),
                         (select Id, RecordTypeId, Credit_Offer_Limit__c, Voucher_Offer_Limit__c, 
                          Customer_Credits_Consumed__c, Vouchers_Redeemed__c from VO_Offer_Details__r)
                         from Vin_Offer_Detail__c];
        }
        
        Id CUSTOMERVOUCHERRECORDTYPEID = Schema.SObjectType.Voucher__c.getRecordTypeInfosByDeveloperName().get('Customer_Voucher').getRecordTypeId();
        Id VOVOUCHERRECORDTYPEID = Schema.SObjectType.Voucher__c.getRecordTypeInfosByDeveloperName().get('VO_Voucher').getRecordTypeId();
        
        List<Vin_Offer_Detail__c> vinodToUpdateList = new List<Vin_Offer_Detail__c>();
        for (Vin_Offer_Detail__c vinod : vinodList){
            Decimal creditsConsumed = 0;
            Decimal vouchersRedeemed = 0;
            for (Voucher__C voucher : vinod.Vouchers__r){
                if (voucher.RecordTypeId == CUSTOMERVOUCHERRECORDTYPEID){
                	creditsConsumed += voucher.Customer_Credits_Consumed__c;    
                }
                else if (voucher.RecordTypeId == VOVOUCHERRECORDTYPEID){
                    vouchersRedeemed++;
                }
            }
         	vinod.Total_Customer_Credits_Consumed__c = creditsConsumed;
            vinod.Total_Vehicle_Vouchers_Consumed__c = vouchersRedeemed;
            vinodToUpdateList.add(vinod);
        }
        
        if (vinodToUpdateList.size() > 0){
            update vinodToUpdateList;
        }
    }
    
    
}