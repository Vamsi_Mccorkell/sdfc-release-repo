global with sharing class SP_ServiceIntegrationBatch implements Database.Batchable<sObject>
{
	public class SP_ServiceIntegrationBatchException extends Exception{}

	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		string	strQuery =		
			' select	Id'	+
			' from 		Service__c' +
			' where 	Ready_For_Processing__c = true' +
			' limit 20000';

		return Database.getQueryLocator(strQuery);
	}


	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		list<id> theProcessList = new list<id>();

		for(sobject s : scope)
		{
			theProcessList.add((id)s.get('Id'));
		}

		SP_ServiceIntegrationProcessing.SP_Args oArgs = new SP_ServiceIntegrationProcessing.SP_Args(theProcessList);

		SP_ServiceIntegrationProcessing processor = new SP_ServiceIntegrationProcessing(oArgs);
		SP_ServiceIntegrationProcessing.SP_Ret oRet = processor.ProcMain();
	}


	global void finish(Database.BatchableContext BC)
	{
		system.debug('Finished');

	}

	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static testMethod void testBatch() 
	{
	// Create records to be used in this test method

		Product2 sProduct1 = new Product2();
		sProduct1.Name					= 'IS200';
		insert sProduct1;

		Product2 sProduct2 = new Product2();
		sProduct2.Name					= 'SC430';
		insert sProduct2;

		Asset__c sAsset1 = new Asset__c();
		sAsset1.Name					= 'ABC12345678';
		sAsset1.Vehicle_Model__c		= sProduct1.Id;
		insert sAsset1;

		Asset__c sAsset2 = new Asset__c();
		sAsset2.Name					= 'XYZ98765432';
		sAsset2.Vehicle_Model__c		= sProduct2.Id;
		insert sAsset2;

		Asset__c sAsset3 = new Asset__c();
		sAsset3.Name					= 'DDDD8765432';
		sAsset3.Vehicle_Model__c		= sProduct1.Id;
		insert sAsset3;

		Asset__c sAsset4 = new Asset__c();
		sAsset4.Name					= 'JJJJ8765432';
		sAsset4.Vehicle_Model__c		= sProduct2.Id;
		insert sAsset4;

		list<Account>	li_Account		= new list<Account>();

		Account sAccount1 = new Account();
		sAccount1.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccount1.LastName				= 'TestLast1';
		sAccount1.PersonEmail			= 'test1@email.com';
		li_Account.add(sAccount1);

		Account sAccount2 = new Account();
		sAccount2.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccount2.LastName				= 'TestLast2';
		sAccount2.PersonEmail			= 'test2@email.com';
		li_Account.add(sAccount2);

		Account sAccount3 = new Account();
		sAccount3.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		sAccount3.LastName				= 'TestLast3';
		sAccount3.PersonEmail			= 'test3@email.com';
		li_Account.add(sAccount3);

		insert li_Account;

		// Now we need to hit the dataase to get the ids of the Accounts we just created
		Account[] arrAccount =	[
									select	Id
									from	Account
									where	LastName = :'TestLast1'
									or		LastName = :'TestLast2'
									or		LastName = :'TestLast3'
									limit	3
								];

		list<id>		li_AccountIds	= new list<id>();

		for (Account sAccount : arrAccount)
		{
			li_AccountIds.add(sAccount.Id);
		}

		// Create a number of Service records
		list<Service__c> liService = new list<Service__c>();

		// Two customers, each with one vehicle, all services to be counted
		for (integer i=1; i<4; i++)
		{
			Service__c sService1			= new Service__c(); 
			sService1.Customer_Account__c	= sAccount1.Id;
			sService1.Asset__c				= sAsset1.Id;
			sService1.Vehicle_Mileage__c	= 7000 * i;
			sService1.Service_Date__c		= system.today().addYears(-(4-i));
			sService1.Ready_For_Processing__c = true;

			liService.add(sService1);

			Service__c sService2			= new Service__c(); 
			sService2.Customer_Account__c	= sAccount2.Id;
			sService2.Asset__c				= sAsset2.Id;
			sService2.Vehicle_Mileage__c	= 12345 * i;
			sService2.Service_Date__c		= system.today().addYears(-(4-i));
			sService2.Ready_For_Processing__c = true;

			liService.add(sService2);
			
		}

		// Now one customer with two vehicles, not all services to be counted
		// First service - counted
		Service__c sService3			= new Service__c(); 
		sService3.Customer_Account__c	= sAccount3.Id;
		sService3.Asset__c				= sAsset3.Id;
		sService3.Vehicle_Mileage__c	= 12345;
		sService3.Service_Date__c		= date.newInstance(2008, 1, 1);
		sService3.Ready_For_Processing__c = true;
		liService.add(sService3);

		// 4999 km past due - count as 30,000km service
		Service__c sService4			= new Service__c(); 
		sService4.Customer_Account__c	= sAccount3.Id;
		sService4.Asset__c				= sAsset3.Id;
		sService4.Vehicle_Mileage__c	= 34999;
		sService4.Service_Date__c		= date.newInstance(2008, 10, 2);
		sService4.Ready_For_Processing__c = true;
		liService.add(sService4);

		// 5000 km past due - count as 40,000 service
		Service__c sService5			= new Service__c(); 
		sService5.Customer_Account__c	= sAccount3.Id;
		sService5.Asset__c				= sAsset3.Id;
		sService5.Vehicle_Mileage__c	= 35000;
		sService5.Service_Date__c		= date.newInstance(2009, 8, 3);
		sService5.Ready_For_Processing__c = true;
		liService.add(sService5);

		// Outside mileage range - use average
		Service__c sService6			= new Service__c(); 
		sService6.Customer_Account__c	= sAccount3.Id;
		sService6.Asset__c				= sAsset3.Id;
		sService6.Vehicle_Mileage__c	= 300000;
		sService6.Service_Date__c		= date.newInstance(2010, 12, 31);
		sService6.Ready_For_Processing__c = true;
		liService.add(sService6);

		// Different vehicle - should count first service
		Service__c sService7			= new Service__c(); 
		sService7.Customer_Account__c	= sAccount3.Id;
		sService7.Asset__c				= sAsset4.Id;
		sService7.Vehicle_Mileage__c	= 121000;
		sService7.Service_Date__c		= date.newInstance(2007, 10, 1);
		sService7.Ready_For_Processing__c = true;
		liService.add(sService7);			

		// Mileage is zero - use average cost for model
		// Also, date is > 9 months since last service for this vehicle but < 9 months since last service for this customer
		Service__c sService8			= new Service__c(); 
		sService8.Customer_Account__c	= sAccount3.Id;
		sService8.Asset__c				= sAsset4.Id;
		sService8.Vehicle_Mileage__c	= 0;
		sService8.Service_Date__c		= date.newInstance(2008, 9, 1);
		sService8.Ready_For_Processing__c = true;
		liService.add(sService8);

		// Service < 9 months since last
		Service__c sService9			= new Service__c(); 
		sService9.Customer_Account__c	= sAccount3.Id;
		sService9.Asset__c				= sAsset4.Id;
		sService9.Vehicle_Mileage__c	= 265000;
		sService9.Service_Date__c		= date.newInstance(2009, 4, 1);
		sService9.Ready_For_Processing__c = true;
		liService.add(sService9);

		// Service > 9 months since last, same day as other vehicle for same customer
		Service__c sService10			= new Service__c(); 
		sService10.Customer_Account__c	= sAccount3.Id;
		sService10.Asset__c				= sAsset4.Id;
		sService10.Vehicle_Mileage__c	= 165000;
		sService10.Service_Date__c		= date.newInstance(2010, 12, 31);
		sService10.Ready_For_Processing__c = true;
		liService.add(sService10);

		list<id> liServiceIds = new list<id>();

		insert liService;

		SP_ServiceIntegrationBatch oServiceIntegrationBatch = new SP_ServiceIntegrationBatch();
		oServiceIntegrationBatch.start(null);
		oServiceIntegrationBatch.execute(null, liService);
		oServiceIntegrationBatch.finish(null);
	}
}