/*******************************************************************************
@author:        Donnie Banez
@date:          March 2019
@description:   Trigger Handler for Guest Relationship Trigger
@Revision(s):    
@Test Methods:  GuestRelationshipTriggerHandlerTest
********************************************************************************/
public with sharing class GuestRelationshipTriggerHandler {
    public static TriggerAutomations__c automationEnabled  = TriggerAutomations__c.getInstance(); 
    public static Boolean triggerEnabled = automationEnabled.Guest_Relationship__c; 
    
    public static void mainEntry(Boolean isBefore, Boolean isAfter, Boolean isInsert, Boolean isUpdate,Boolean isDelete, Boolean isUnDelete, 
                    List<SObject> newList, List<SObject> oldList, Map<ID, SObject> newmap, Map<ID, SObject> oldmap)
    {
        if (Test.isrunningTest())  {
            triggerEnabled = true;
        }
        
        if (triggerEnabled){
            try{
                // INSERT TRIGGER
                if(isInsert){
                    // Before
                    if(isBefore){
                        
                    }            
                    // After
                    if(isAfter){
  						createNewGRAndEndExisting(newList);
                    }
                }
                // Update TRIGGER
                if(isUpdate){
                    // Before
                    if(isBefore){
                        cannotEditStatusOfGR((Map<Id, Guest_Relationship__c>) oldMap, newList);
                    }            
                    // After
                    if(isAfter){
  						
                    }
                }
                // Delete TRIGGER
                if(isDelete){
                    // Before
                    if(isBefore){
                        
                    }            
                    // After
                    if(isAfter){
   
                    }
                }
                // UNDELETE Trigger
                if(isUnDelete){
                    if (isBefore){
                        
                    }
                    if (isAfter){
  
                    }
                }  
            } catch (Exception ex){
                String errorString = ex.getStackTraceString()+'--'+ ex.getTypeName()+'--'+ ex.getMessage();
                system.debug('### Guest Relationship Trigger Handler Exception Thrown = ' + errorString);
                newList[0].addError(errorString);
            }
        }
    }
    
    public static void cannotEditStatusOfGR(Map<Id, Guest_Relationship__c> oldMap, List<Guest_Relationship__c> newList){
        for (Guest_Relationship__c gr : newList){
            Guest_Relationship__c oldGR = oldMap.get(gr.Id);
            if (oldGR.GR_Status__c != gr.GR_Status__c){
                gr.addError(Label.GRStatusChangeError);
            }
        }
    }
    
    public static void createNewGRAndEndExisting(List<Guest_Relationship__c> newList){
      	// Method that updates all old GR records with an end date
        Set<Id> parentAcctNewIdSet = new Set<Id>();
        Set<Id> parentAcctUnverifiedGRSet = new Set<Id>();
        Set<Id> parentAcctVerifiedGRSet = new Set<Id>();
        Set<Id> grIdSet = new Set<Id>();
        List<Account> newAccountList = new List<Account>();
        List<Account> verifiedAcctList = new List<Account>();
        List<Account> unverifiedAcctList = new List<Account>();
        
        List<Account> accountUpdateList = new List<Account>();
        List<Vehicle_Ownership__c> voUpdateList = new List<Vehicle_Ownership__c>();
        List<Guest_Relationship__c> otherGRUpdateList = new List<Guest_Relationship__c>();
        
        for (Guest_Relationship__c gr : newList){
            grIdSet.add(gr.Id);
            // set gr status and gigya id on account
            Account newAcct = new Account(Id = gr.Account__c);
            newAcct.GR_Status__c = gr.GR_Status__c;
            if (gr.GR_Status__c != 'Deleted' && gr.GR_Status__c != 'Error') {
                newAcct.Guest_Gigya_ID__c = gr.Gigya_ID__c;
            }
            accountUpdateList.add(newAcct);
            
            if (gr.GR_Status__c == 'New'){
                parentAcctNewIdSet.add(gr.Account__c);
            }
            if (gr.GR_Status__c == 'Verified' && gr.VehicleOwnership__c != null){
                parentAcctVerifiedGRSet.add(gr.Account__c);
                Vehicle_Ownership__c vo = new Vehicle_Ownership__c(Id = gr.VehicleOwnership__c);
                vo.Verified_Guest_ID__c = gr.Gigya_ID__c;
                voUpdateList.add(vo);
                // set gr status and gigya id on account
            }
            else if (gr.GR_Status__c == 'Unverified'){
                parentAcctUnverifiedGRSet.add(gr.Account__c);
            }
        } 
        
        // For GR Status = New, deactivate all Active GR's
        if (parentAcctNewIdSet.size() > 0){
            newAccountList = [select Id,
                            (select Id, Account__c, End_Date__c, GR_Status__c , VehicleOwnership__c from Guest_Relationships__r
                             where End_Date__c = null order by Start_Date__c desc)
                           	from Account where Id in :parentAcctNewIdSet];
            
            for (Account a : newAccountList){
                for (Guest_Relationship__c gr : a.Guest_Relationships__r){
                    if (!grIdSet.contains(gr.Id)){ // if it is not part of the things being updated
                        gr.End_Date__c = system.now();
                        otherGRUpdateList.add(gr);
                    }
                }
            }
        }
        
        // For Verified GR's
        system.debug(LoggingLevel.ERROR, '## parentAcctVerifiedGRSet: ' + parentAcctVerifiedGRSet);
        if (parentAcctVerifiedGRSet.size() > 0){
            verifiedAcctList = [select Id,
                                (select Id, Account__c, GR_Status__c , End_Date__c, VehicleOwnership__c from Guest_Relationships__r
                             	where End_Date__c = null 
                                order by Start_Date__c desc
                                )
                                from Account where Id in :parentAcctVerifiedGRSet];
            
            List<Guest_Relationship__c> grList = new List<Guest_Relationship__c>();
            for (Account a : verifiedAcctList){
                grList.addAll(a.Guest_Relationships__r);
            }
            
            for (Guest_Relationship__c insertedGR : newList){
                for (Guest_Relationship__c existingGR : grList){
                    if (insertedGR.Id != existingGR.Id){ // only apply it on GR records that are not part of the trigger
                        if (existingGR.VehicleOwnership__c != null && existingGR.Account__c == insertedGR.Account__c && existingGR.VehicleOwnership__c == insertedGR.VehicleOwnership__c){
                        	existingGR.End_Date__c = system.now();
                            otherGRUpdateList.add(existingGR);    
                        }
                        else if (existingGR.VehicleOwnership__c == null && existingGR.Account__c == insertedGR.Account__c){
                            existingGR.End_Date__c = system.now();
                            otherGRUpdateList.add(existingGR); 
                        }
                    }
                }
            }
        }
        
        // Unverify the VO for Unverified GR's
        if (parentAcctUnverifiedGRSet.size() > 0){
            unverifiedAcctList = [select Id,
                                  (select Id, Account__c, GR_Status__c, End_Date__c, VehicleOwnership__c from
                                   Guest_Relationships__r
                                   where End_Date__c = null
                                   order by Start_Date__c desc)
                                  from Account where Id in :parentAcctUnverifiedGRSet];
            
            List<Guest_Relationship__c> grList = new List<Guest_Relationship__c>();
            for (Account a : unverifiedAcctList){
                grList.addAll(a.Guest_Relationships__r);
            }
            
            for (Guest_Relationship__c insertedGR : newList){
                for (Guest_Relationship__c existingGR : grList){
                    if (insertedGR.Id != existingGR.Id){ // only apply it on GR records that are not part of the trigger
                        if (existingGR.VehicleOwnership__c != null && existingGR.Account__c == insertedGR.Account__c && existingGR.VehicleOwnership__c == insertedGR.VehicleOwnership__c){
                        	existingGR.End_Date__c = system.now();
                            otherGRUpdateList.add(existingGR); 
                            if (existingGR.GR_Status__c == 'Verified'){
                                Vehicle_Ownership__c vo = new Vehicle_Ownership__c(Id = existingGR.VehicleOwnership__c);
                                vo.Verified_Guest_ID__c = null;
                                voUpdateList.add(vo);
                            }
                        }
                        else if (existingGR.VehicleOwnership__c == null && existingGR.Account__c == insertedGR.Account__c){
                            existingGR.End_Date__c = system.now();
                            otherGRUpdateList.add(existingGR); 
                        }
                    }
                }
            }
        }
        
        if (otherGRUpdateList.size() > 0){
            update otherGRUpdateList;
        }
        
        if (voUpdateList.size() > 0){
            update voUpdateList;
        }
        
        if (accountUpdateList.size() > 0){
            update accountUpdateList;
        }
        
        
        
    }
    
   
}