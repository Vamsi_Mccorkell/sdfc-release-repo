global with sharing class SP_DealerIntegrityBatch implements Database.Batchable<sObject>
{
	public class SP_DealerIntegrityBatchException extends Exception{}

	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		// First thing we do is delete any existing Dealer Exception records

		// List of exception records to be deleted
		list<Dealer_Exception__c> li_DEsToDelete = new list<Dealer_Exception__c>();

		// Get all existing Dealer_Exception__c records and delete them (so we can run reports on a single data set)
		li_DEsToDelete =	[
								select	id
								from	Dealer_Exception__c
							];

		if (!li_DEsToDelete.isEmpty())
		{
			delete li_DEsToDelete;
		}

		string	strQuery;

		if (!test.isRunningTest())
		{
			strQuery =		
				' select	Id'	+
				' from 		Account' +
				' where 	RecordTypeId = ' + '\'' + SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType() + '\'' +
				' and		Owner_Type__c = ' + '\'' + 'Current' + '\'' ;
		}
		else
		{
			strQuery =		
				' select	Id'	+
				' from 		Account' +
				' where 	RecordTypeId = ' + '\'' + SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType() + '\'' +
				' and		Owner_Type__c = ' + '\'' + 'Current' + '\'' +
				' and		FirstName = ' + '\'' + 'Test' + '\'' ;
		}

		return Database.getQueryLocator(strQuery);
	}


	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		list<id> theProcessList = new list<id>();

		for(sobject s : scope)
		{
			theProcessList.add((id)s.get('Id'));
		}

		SP_DealerIntegrityProcessing.SP_Args oArgs = new SP_DealerIntegrityProcessing.SP_Args(theProcessList);

		SP_DealerIntegrityProcessing processor = new SP_DealerIntegrityProcessing(oArgs);
		SP_DealerIntegrityProcessing.SP_Ret oRet = processor.ProcMain();
	}


	global void finish(Database.BatchableContext BC)
	{
		system.debug('Finished');
	}

	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static testMethod void testBatch() 
	{
		// Set up some database records
        Account sAccount;

        sAccount			= new Account();
        sAccount.FirstName	= 'Test';
        sAccount.LastName	= 'Account';
        sAccount.RecordTypeId = SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
        insert sAccount;

		SP_DealerIntegrityBatch oDealerIntegrityBatch = new SP_DealerIntegrityBatch();
		oDealerIntegrityBatch.start(null);

		oDealerIntegrityBatch.execute(null, new list<SObject>{sAccount});

		oDealerIntegrityBatch.finish(null);


	}
}