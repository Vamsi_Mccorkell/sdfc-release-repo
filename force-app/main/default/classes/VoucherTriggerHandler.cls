/*******************************************************************************
@author:        Donnie Banez
@date:          February 2018
@description:   Trigger Handler for Voucher Trigger
@Revision(s):    
@Test Methods:  VoucherTriggerHandlerTest
********************************************************************************/
public with sharing class VoucherTriggerHandler {
    public static TriggerAutomations__c automationEnabled  = TriggerAutomations__c.getInstance(); 
    public static Boolean triggerEnabled = automationEnabled.Voucher__c; 
    public static void mainEntry(Boolean isBefore, Boolean isAfter, Boolean isInsert, Boolean isUpdate,Boolean isDelete, Boolean isUnDelete, 
                    List<SObject> newList, List<SObject> oldList, Map<ID, SObject> newmap, Map<ID, SObject> oldmap)
    {
        if (Test.isrunningTest())  {
            triggerEnabled = true;
        }
        
        if (triggerEnabled){
            try{
                // INSERT TRIGGER
                if(isInsert){
                    // Before
                    if(isBefore){
                        evaluatePurchasePrice(newList);
                    }            
                    // After
                    if(isAfter){
  						calculateRollUps(newList);
                    }
                }
                // Update TRIGGER
                if(isUpdate){
                    // Before
                    if(isBefore){
                        
                    }            
                    // After
                    if(isAfter){
  						calculateRollUps(newList);
                    }
                }
                // Delete TRIGGER
                if(isDelete){
                    // Before
                    if(isBefore){
                        preventVoucherDeletion(oldList);
                    }            
                    // After
                    if(isAfter){
   						
                    }
                }
                // UNDELETE Trigger
                if(isUnDelete){
                    if (isBefore){
                        
                    }
                    if (isAfter){
  						
                    }
                }  
            } catch (Exception ex){
                String errorString = ex.getStackTraceString()+'--'+ ex.getTypeName()+'--'+ ex.getMessage();
                system.debug('### Voucher Trigger Handler Exception Thrown = ' + errorString);
                newList[0].addError(errorString);
            }
        }
        
    }
    /*
    * Author: D G BANEZ
    * Purpose: Evaluate purchase price
    */
    public static void evaluatePurchasePrice (List<Voucher__c> newList){
        
        Set<Id> partnerBranchIdSet = new Set<Id>();
        for (Voucher__c vc : newList){
            partnerBranchIdSet.add(vc.Partner_Branch__c);
        }
        List<Branch_Offer_Detail__c> voucherPriceList = [select Id, Partner_Branch__c, Offer_Type__c, Voucher_Price_Type__c,
            Voucher_Percent_Discount__c, Voucher_Price__c from Branch_Offer_Detail__c 
            where Partner_Branch__c in :partnerBranchIdSet];
        
        for (Voucher__c vc : newList){
            Integer voucherTypeCount = 0;
            Integer fullValueCount = 0;
            Integer fixedDiscountCount = 0;
            Integer percentDiscountCount = 0;
            
            for (Branch_Offer_Detail__c  vp : voucherPriceList){
                if (vc.Partner_Branch__c == vp.Partner_Branch__c && vc.Voucher_Type__c == vp.Offer_Type__c){
                    voucherTypeCount++;
                    if (vp.Voucher_Price_Type__c == 'Full Value' && vp.Voucher_Price__c != null){
                        vc.Voucher_Price__c = vp.Voucher_Price__c;
                        fullValueCount++;
                    }
                    else if (vp.Voucher_Price_Type__c == 'Fixed Discount' && vc.Service_Price__c != null && vp.Voucher_Price__c != null){
                        vc.Voucher_Price__c = vc.Service_Price__c - vp.Voucher_Price__c;
                        fixedDiscountCount++;
                    }
                    else if (vp.Voucher_Price_Type__c == 'Percent Discount' && vc.Service_Price__c != null && vp.Voucher_Percent_Discount__c != null){
                        vc.Voucher_Price__c = vc.Service_Price__c - (vc.Service_Price__c * vp.Voucher_Percent_Discount__c/100);
                        percentDiscountCount++;
                    }
                    else {
                        vc.Voucher_Price__c = 0;
                        break;
                    }
                }
            }
            
            // now check if there are multiple entries, if yes set price to zero
            if (voucherTypeCount > 1 || fullValueCount > 1 || fixedDiscountCount > 1 || percentDiscountCount > 1){ 
                vc.Voucher_Price__c = 0;
            }
            
        }
    }
    
    public static void calculateRollUps(List<Voucher__c> newList){
        /*PURPOSE: Calculates the Credits Remainin in the COOD for very voucher created for offer class customer
         * */
        Id CUSTOMERVOUCHERRECORDTYPEID = Schema.SObjectType.Voucher__c.getRecordTypeInfosByDeveloperName().get('Customer_Voucher').getRecordTypeId();
        Id VOVOUCHERRECORDTYPEID = Schema.SObjectType.Voucher__c.getRecordTypeInfosByDeveloperName().get('VO_Voucher').getRecordTypeId();
        Id CUSTOMERVEHICLEOFFERRECORDTYPEID = Schema.SObjectType.VO_Offer_Detail__c.getRecordTypeInfosByDeveloperName().get('Customer_Vehicle_Offer').getRecordTypeId();
        
        Set<Id> codIdSet = new Set<Id>();
        Set<Id> voodIdSet = new Set<Id>();
        Set<Id> vinodIdSet = new Set<Id>();
        for (Voucher__c voucher : newList){
            voodIdSet.add(voucher.VO_Offer_Details__c); // required for all RT's
            vinodIdSet.add(voucher.Vin_Offer_Detail__c);
            if (voucher.RecordTypeId == CUSTOMERVOUCHERRECORDTYPEID){
                codIdSet.add(voucher.CustomerOfferDetail__c);
            }
        }
        // Get COD for counts at the COD level
        if (codIdSet.size() > 0){
             GuestVoucherServicesUtilities.recalculateCODCounts(codIdSet);
        }
        // Get VINOD counts 
        if (vinodIdSet.size() > 0){
            //VinOfferDetailTriggerHandler.recalculateVINODCounts(vinIdSet);
            GuestVoucherServicesUtilities.recalculateVINODCounts(vinodIdSet);
        }
        
        // now get the VOOD's to compute for vouchers redeemed
        List<VO_Offer_Detail__c> voodList = [select Id, Vouchers_Redeemed__c, Customer_Credits_Consumed__c,
                                             Most_Recent_Credit_Consumption__c, Most_Recent_Voucher_Redemption__c,
                                             (select Id, Valid_Voucher_Flag__c, RecordTypeId,
                                              Comp_Flag__c, Voucher_Redemption_Date__c, CreatedDate,
                                              Customer_Credits_Consumed__c from Vouchers__r)
                                             from VO_Offer_Detail__c
                                            where Id in : voodIdSet];
        
        for (VO_Offer_Detail__c vood : voodList){
        	Decimal vouchersRedeemed = 0;
            Decimal customerCreditsConsumed = 0;
            Boolean hasCompVoucher = false;
            Datetime mostRecentVoucherRedemption = null;
            Datetime mostRecentCreditConsumption = null; 
            for (Voucher__c voucher : vood.Vouchers__r){
                if (voucher.RecordTypeId == VOVOUCHERRECORDTYPEID){
                    if (voucher.Valid_Voucher_Flag__c) {
                        if (!voucher.Comp_Flag__c){
                        	vouchersRedeemed++;
                    	}
                    	else if (voucher.Comp_Flag__c){
                        	hasCompVoucher = true;
                        }
                        if (mostRecentVoucherRedemption == null || voucher.Voucher_Redemption_Date__c > mostRecentVoucherRedemption){
                            mostRecentVoucherRedemption = voucher.Voucher_Redemption_Date__c;
                        }
                    }
                }
                else if (voucher.RecordTypeId == CUSTOMERVOUCHERRECORDTYPEID){
                    if (voucher.Valid_Voucher_Flag__c){
                        customerCreditsConsumed += (voucher.Customer_Credits_Consumed__c == null) ? 0 : voucher.Customer_Credits_Consumed__c;
                        if (mostRecentCreditConsumption == null || voucher.CreatedDate > mostRecentCreditConsumption){
                            mostRecentCreditConsumption = voucher.CreatedDate;
                        }
                    }
                    
                }
            }
            vood.Vouchers_Redeemed__c = vouchersRedeemed;
            vood.Customer_Credits_Consumed__c = customerCreditsConsumed;
            vood.Comp_Flag__c = hasCompVoucher;
            vood.Most_Recent_Voucher_Redemption__c = mostRecentVoucherRedemption;
            vood.Most_Recent_Credit_Consumption__c = mostRecentCreditConsumption;
        }
        
        if (voodList.size() > 0){
            update voodList;
        }
    }
    
    public static void preventVoucherDeletion(List<Voucher__c> oldList){
        for (Voucher__c voucher : oldList){
            if (!voucher.Delete_Record__c){
                voucher.addError(Label.VoucherDeletionError);
            }
        }
    }
}