/*******************************************************************************
@author:        Donnie Banez
@date:          October 2018
@description:   Trigger Handler for Lead Trigger.
@Revision(s):    
@Test Methods:  LeadTriggerHandlerTest
********************************************************************************/
public with sharing class LeadTriggerHandler {
    public static TriggerAutomations__c automationEnabled  = TriggerAutomations__c.getInstance(); 
    public static Boolean leadTriggerEnabled = automationEnabled.Lead__c; 
    public static Id PROSPECTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
    public static Id DEALERRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dealer Account').getRecordTypeId();
    public static Id CASESALESENQUIRYRTYPEID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Sales Enquiry').getRecordTypeId();
    
    
    public static void mainEntry(Boolean isBefore, Boolean isAfter, Boolean isInsert, Boolean isUpdate,Boolean isDelete, Boolean isUnDelete, 
                    List<SObject> newList, List<SObject> oldList, Map<ID, SObject> newmap, Map<ID, SObject> oldmap)
    {
        // In a test context always set this to true        
        if (Test.isrunningTest())  {
            leadTriggerEnabled = true;
        }
        // Perform business logic
        if(leadTriggerEnabled){
            try{
                // INSERT TRIGGER
                if(isInsert){
                    // Before
                    if(isBefore){
                       cleanAndSupplementData(null, newList); 
                       findMatchingIndividual(null, newList); 
                       determineSourceCampaign(newList); 
                    }            
                    // After
                    if(isAfter){
                       createCaseFromLead(null, newList); 
                    }
                }
                // Update TRIGGER
                if(isUpdate){
                    // Before
                    if(isBefore){
                        
                    }            
                    // After
                    if(isAfter){
                        
                    }
                }
                // Delete TRIGGER
                if(isDelete){
                    // Before
                    if(isBefore){
                        
                    }            
                    // After
                    if(isAfter){
                                            
                    }
                }
                // UNDELETE Trigger
                if(isUnDelete){}  
            } catch (Exception ex){
                // 29 JAN 2019: TEMPORARY FIX -> DONT SHOW ERROR WHEN AFTER INSERT ERROR
                if (!isAfter){
                	String errorString = ex.getStackTraceString()+'--'+ ex.getTypeName()+'--'+ ex.getMessage();
                	system.debug(LoggingLevel.ERROR, '### Lead Trigger Handler Exception Thrown = ' + errorString);
                	newList[0].addError(errorString);
                }
            }
        }
    }
    
    
    public static void cleanAndSupplementData(Map<Id, Lead> oldMap, List<Lead> newList){
        // This method cleans and supplements lead information 
        Set<String> accountNameSet = new Set<String>();
        for (Lead l : newList){
            Lead oldLead = null;
            if (oldMap != null){
               oldLead = oldMap.get(l.Id); 
            }
            if (runTriggerLogic(oldLead, l)){
                // check for kanji characters
                if (hasSpecialCharacters(l)){
                    l.Status = 'Process Error';
                    l.Lead_Trigger_Log__c += 'Lead has special characters';
                }
                else {
                    if (l.Source_Mobile__c != null) l.MobilePhone = cleanPhoneNumber(l.Source_Mobile__c);
                    if (l.Preferred_Dealer__c != null){ // preferred dealer now provided; format is State - Dealer Name
                        if (!hasValidState(l.Preferred_Dealer__c)){ // if state is not valid
                            l.Lead_Trigger_Log__c += 'Invalid State';
                            l.FB_Lead_Error_Type__c = 'Invalid State';
                        }
                        else {
                            l.State = l.Preferred_Dealer__c.substringBefore(' - ');
                        }
                        accountNameSet.add(l.Preferred_Dealer__c.substringAfter(' - '));
                    }
                    else {
                        l.FB_Lead_Error_Type__c = 'Preferred Dealer is blank';
                    }
                }
            }
        }
        List<Account> dealerAccountList = [select Id, Name from Account 
                                           where Name in :accountNameSet
                                          and IsPersonAccount = false
                                          and Status__c = 'Active'
                                          and RecordTypeId = :DEALERRECORDTYPEID];
        
        Map<String, Id> dealerNameIdMap = new Map<String, Id>();
        
        for (Account a : dealerAccountList){
            dealerNameIdMap.put(a.Name, a.Id);
        }
        for (Lead l : newList){
            Lead oldLead = null;
            if (oldMap != null){
               oldLead = oldMap.get(l.Id); 
            }
            if (runTriggerLogic(oldLead, l)){
                if (l.Preferred_Dealer__c != null){
                    String dealerName = l.Preferred_Dealer__c.substringAfter(' - ');
                    if (dealerNameIdMap.containsKey(dealerName)){
                        l.Selected_Dealer__c = dealerNameIdMap.get(dealerName);
                        }
                    else {
                        l.Lead_Trigger_Log__c += 'Dealer does not match an account';
                        l.FB_Lead_Error_Type__c = 'Dealer does not match an account';
                        
                    }
                }
            }
        }
        
    }
    
    public static Boolean hasValidState(String preferredDealer){
        String stateToCheck = preferredDealer.substringBefore(' - ');
        Set<String> validStates = new Set<String>{'ACT', 'NSW', 'NT', 'QLD', 'SA', 'TAS', 'VIC', 'WA'};
        return validStates.contains(stateToCheck);
    }
    
    public static void findMatchingIndividual(Map<Id, Lead> oldMap, List<Lead> newList){
        List<Account> accountsToCreateList = new List<Account>();
        List<Lead> leadsWithNoMatchedAcctList = new List<Lead>();
        Set<String> postCodeToSearchinPMASet = new Set<String>();
        for (Lead l : newList){
            Lead oldLead = null;
            if (oldMap != null){
                oldLead = oldMap.get(l.Id);
            }
            if (runTriggerLogic(oldLead, l)){
                l.Lead_Trigger_Log__c = '';
                String returnedAcctId = SimplifiedMatchingAccount.customerMatching(	null,
										l.FirstName,
										l.LastName,
										l.Email,
										l.MobilePhone,
										l.City, 
										l.PostalCode,
										l.Street, false
									);
                if (returnedAcctId != null){
                    l.MatchedPersonAccount__c = returnedAcctId;
                    l.Lead_Trigger_Log__c += 'Matched Person Account found';
                    // mark Lead as processed
                	l.Status = 'Processed';
                }
                else {
                    // create an Account
                    l.Lead_Trigger_Log__c += 'No Match found, creating a new one';
                    leadsWithNoMatchedAcctList.add(l);
                }
                
                // Get post codes
                postCodeToSearchinPMASet.add(l.PostalCode); 
            }
        }
        // get a PMA Mapping
        List<PMA__c> dealerPMAList = [select Id, Location__c, Postcode__c 
                                            from PMA__c where Postcode__c in :postCodeToSearchinPMASet];
        Map<String, String> postCodeLocationMap = new Map<String, String>();
        for (PMA__c pma : dealerPMAList){
            postCodeLocationMap.put(pma.Postcode__c, pma.Location__c);
        }
        
        for (Lead l : leadsWithNoMatchedAcctList){
            if (postCodeLocationMap.containsKey(l.PostalCode)){
                accountsToCreateList.add(createLinkedAccount(l, postCodeLocationMap.get(l.PostalCode)));    
            }
            else {
                l.Lead_Trigger_Log__c += '; No post code entry in PMA';
                accountsToCreateList.add(createLinkedAccount(l, null));
            }
            
        }
        if (accountsToCreateList.size() >0){
            try {
                insert accountsToCreateList;
            }
            catch (DmlException e){
                newList[0].Lead_Trigger_Log__c += String.valueOf(e.getMessage());
                newList[0].addError('There is an error processing this lead: ' + e.getMessage());
            }
        }
        // now re-assign back to l.Matched Account
        if (accountsToCreateList.size() > 0 && leadsWithNoMatchedAcctList.size() > 0){
            for (Integer i = 0; i < leadsWithNoMatchedAcctList.size(); i++){
                leadsWithNoMatchedAcctList[i].MatchedPersonAccount__c = accountsToCreateList[i].Id;
                leadsWithNoMatchedAcctList[i].Status = 'Processed';
            }
        }
        // now do another loop to assign the Location / city to the lead as well
        for (Lead l : newList){
            if (postCodeLocationMap.containsKey(l.PostalCode)){
                String location = postCodeLocationMap.get(l.PostalCode);
                l.City = (location != null) ? ((location.toUpperCase() != 'MULTIPLE') ? location : null) : null;
            }
        }
    }
    
    public static void determineSourceCampaign(List<Lead> newList){
        String campaignNameToFind = generateCampaignName();
        Campaign matchedCampaign = new Campaign();
        List<Campaign> campaignList = [select Id from Campaign where Name = :campaignNameToFind];
        if (campaignList.size() > 0){
            matchedCampaign = campaignList[0];
        }
        else {
            matchedCampaign.Name = campaignNameToFind;
            matchedCampaign.Type = 'Advertisement';
            matchedCampaign.Status = 'Planned';
            
            insert matchedCampaign;
        }
        
        if (matchedCampaign.Id != null){
           for (Lead l :newList){
           	  if (l.Source_Campaign__c == null) l.Source_Campaign__c = matchedCampaign.Id; 
           } 
        }
    }
    
    private static String generateCampaignName(){
        String derivedCampaignName = '';
        Datetime timeNow = System.now();
        //yyyy.mm Facebook MMM e.g. 2018.11 Facebook November
        String yyymm = timeNow.format('yyyy.MM');
        String monthName = timeNow.format('MMMM');
        derivedCampaignName = yyymm + ' Facebook ' + monthName;
        return derivedCampaignName;
    }

    public static void createCaseFromLead(Map<Id, Lead> oldMap, List<Lead> newList){
        List<Case> caseToCreateList = new List<Case>();
        Set<Id> acctIdSet = new Set<Id>();
        for (Lead l : newList){
            if (l.MatchedPersonAccount__c != null) acctIdSet.add(l.MatchedPersonAccount__c);
        }
        Map<Id, Account> acctIdAcctMap = new Map<Id,Account>([select Id, PersonContactId from Account
                                                             where Id in :acctIdSet]);
		for (Lead l : newList){
            if (l.MatchedPersonAccount__c != null && l.LeadSource == 'Facebook'){
                caseToCreateList.add(generateCaseFromLead(l, acctIdAcctMap));    
            }
            
        }
        if (caseToCreateList.size() > 0){
            try {
                insert caseToCreateList;
            }
            catch (DmlException e){
                // JAN 29 2018 D BANEZ - temporary fix dont do anything if case fails
                //newList[0].addError('There is an error processing this lead: ' + e.getMessage());
            }
        }
    }
    private static Account createLinkedAccount(Lead l, String location){
        Account a = new Account();
        a.RecordTypeId = PROSPECTRECORDTYPEID;
        a.Prospect_Status__pc = 'General Enquiry'; 
        a.PersonLeadSource = 'Facebook';
        a.FirstName = l.FirstName;
        a.LastName = l.LastName;
        a.PersonEmail = l.Email;
        a.PersonMobilePhone = l.MobilePhone;
        a.BillingPostalCode = l.PostalCode;
        a.BillingCountry = l.Country;
        a.Preferred_Dealer__c = l.Selected_Dealer__c;
        a.BillingState = l.State;
        a.BillingCity = (location != null) ? ((location.toUpperCase() != 'MULTIPLE') ? location : null) : null;
        return a;
    }
    
    private static Case generateCaseFromLead(Lead l, Map<Id, Account> acctIdAcctMap){
        Case c = new Case();
        c.RecordTypeId = CASESALESENQUIRYRTYPEID;
        c.AccountId = l.MatchedPersonAccount__c;
        c.SourceLead__c = l.Id;
        c.Status = 'New';
        if (acctIdAcctMap != null && acctIdAcctMap.containsKey(l.MatchedPersonAccount__c)){
            c.ContactId = acctIdAcctMap.get(l.MatchedPersonAccount__c).PersonContactId;
        }
        if (l.FB_Lead_Error_Type__c == null){
        	//c.Type = 'Personal Preview/Test Drive';
            c.Type = 'Facebook Lead';
            c.Comments = 'Autoconvert ';
        	c.Comments += (l.FB_Campaign_Name__c != null) ? l.FB_Campaign_Name__c : '';
        }
        else {
            c.Type = 'Facebook Error';
            c.OwnerId = Label.FBCaseOwnerDefaultID; // Lexus australia
            c.Comments = 'Error: ' + l.FB_Lead_Error_Type__c;
        }
        
        c.Campaign_Source__c = l.Source_Campaign__c;
        //c.Subject = (l.FB_Campaign_Name__c  != null) ? l.FB_Campaign_Name__c  : 'FB Lead (FB Campaign Name is blank)';
        String todaysDate = system.now().format('dd/MM/yy');
        c.Model__c = (l.FB_Form_Name__c != null) ? String.valueOf(l.FB_Form_Name__c).left(2) : '';
        c.Subject = 'Test Drive Request – Facebook - ' + c.Model__c + ' – ' + l.Selected_Dealer_Name__c + ' - ' + todaysDate;
        c.Source__c = l.LeadSource;
        c.Origin = l.LeadSource;
        c.Selected_Dealer__c = l.Selected_Dealer__c;
        c.Selected_Dealer_Name__c = l.Selected_Dealer_Name__c;
        c.Description = (l.FB_Campaign_Name__c != null) ? l.FB_Campaign_Name__c : '';
        system.debug(LoggingLevel.ERROR, '## c:' + c);
        return c;
    }  
    
    private static Boolean runTriggerLogic(Lead oldLead, Lead l){
        Boolean runLogic = false;
        if (l.LeadSource == 'Facebook' && l.Status != 'Processed' && l.Status != 'Process Error'){
            runLogic = true;
        }
        return runLogic;
    }
    
    private static Boolean hasSpecialCharacters(Lead l){
        Boolean hasSpecialCharacters = false;
        Matcher m = Pattern.compile('[\\u0000-\\u007F]*').matcher(l.LastName);
        hasSpecialCharacters = !m.matches();
        if (l.FirstName != null) m = Pattern.compile('[\\u0000-\\u007F]*').matcher(l.FirstName);
        if (!hasSpecialCharacters) hasSpecialCharacters = !m.matches();
        if (l.MobilePhone != null) m = Pattern.compile('[\\u0000-\\u007F]*').matcher(l.MobilePhone);
        if (l.Source_Mobile__c != null) m = Pattern.compile('[\\u0000-\\u007F]*').matcher(l.Source_Mobile__c);
        if (!hasSpecialCharacters) hasSpecialCharacters = !m.matches();
        if (l.PostalCode != null) m = Pattern.compile('[\\u0000-\\u007F]*').matcher(l.PostalCode);
        if (!hasSpecialCharacters) hasSpecialCharacters = !m.matches();
        return hasSpecialCharacters;
    }
    
    private static String cleanPhoneNumber(String phoneNum){
        system.debug(LoggingLevel.ERROR, 'Phone num: ' + phoneNum);
        String cleanPhone = null;
        //Remove ‘p:+’ 
        String temp = phoneNum.replace('p:+', '');
        system.debug(LoggingLevel.ERROR, 'Phone num after replacing: ' + cleanPhone);
        // swap out leading 6104 with 614
        if (temp.startsWith('6104')){
            String regExp = '6104';
            cleanPhone = temp.replaceFirst(regExp, '614');
        }
        else {
            cleanPhone = temp;
        }
        return cleanPhone;
    }
}