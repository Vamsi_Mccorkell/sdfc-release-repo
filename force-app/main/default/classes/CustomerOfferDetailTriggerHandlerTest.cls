// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for creating accounts need to view the custom settings data
@IsTest (SeeAllData=true)
public class CustomerOfferDetailTriggerHandlerTest {
	static Offer__c newOffer;
    static Account customer;
    static testmethod void checkDuplicateCOD(){
        dataSetup();
        Customer_Offer_Detail__c cod = new Customer_Offer_Detail__c();
        cod.Customer__c = customer.Id;
        cod.Offer__c = newOffer.Id;
        cod.First_Credit_Expiration__c = system.today()+5;
        cod.Total_Credits_Allowed__c = 10;
        cod.Total_Customer_Credits_Consumed__c = 0;
        try {
            insert cod;
        }
        catch (DmlException e){
            system.assertEquals(true, e.getMessage().contains(Label.DuplicateCODError ));
        }
        List<Customer_Offer_Detail__c> codList = [select Id from Customer_Offer_Detail__c where Offer__c = :newOffer.Id];
        
        
    }
    static void dataSetup(){
        Id PARTNERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner').getRecordTypeId();
        Id CUSTOMERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        Account partnerAcct = new Account(Name = 'Partner Account', RecordTypeId = PARTNERACCTRECORDTYPEID, Partner_Class__c = 'Customer');
        insert partnerAcct;
        
        Vehicle_Ownership__c validForOfferVO = [select Id, Customer__c, AssetID__c from Vehicle_Ownership__c 
                          where Status__c = 'Active' and Valid_for_Offers__c = false limit 1]; // try to get 1 that isn't valid for offer yet
        Asset__c asst = new Asset__c(Id = validForOfferVO.AssetID__c);
        asst.Original_Sales_Date__c = system.today();
        asst.Encore_Tier__c = 'Platinum';
        update asst;    
        customer = [select Id, Guest_Gigya_ID__c from Account where Id = :validForOfferVO.Customer__c limit 1];
        customer.Guest_Gigya_ID__c = 'TESTGIGYA';
        update customer;
        validForOfferVO.Valet_Voucher_Enabled__c = true;
        validForOfferVO.Voucher_T_C_Accepted__c = true;
        validForOfferVO.Voucher_T_C_Accepted_Date__c = system.today();
        validForOfferVO.Asset_RDR_Date__c = system.today();
        update validForOfferVO;
        
        newOffer = new Offer__c(Partner_Account__c = partnerAcct.Id, Offer_Type__c = 'Car Hire', Status__c = 'Inactive', Offer_Class__c = 'Customer', Customer_Platinum_Limit__c = 10);
        insert newOffer;
        newOffer.Status__c = 'Active';
        update newOffer;
    }
}