// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for account creating accounts need to view the custom settings data
@IsTest (SeeAllData=true)
public class GuestServicesWebServiceTest {
	// DEVELOPER NOTES: These are just all coverage test methods; The assertion test methods are in the corresponding
	// test method classes for each of the handler classes
	public static Account customer;
    public static Customer_Offer_Detail__c cod;
    static testmethod void getOnDemandAvailability(){
        dataSetup();
        GuestServicesWebService.getOnDemandAvailability(customer.Id, customer.Guest_Gigya_ID__c, null);
    }
    
    static testmethod void writeeditOnDemandTransactionsAndCancelOnDemand(){
        dataSetup();
        Datetime startDt = system.now();
        Datetime endDt = startDt + 2;
        GuestServicesWebService.writeOnDemandTransactions(customer.Id, customer.Guest_Gigya_ID__c, cod.Id, 'Car Hire', 'Location', 1, startDt, 12, 'start date text', endDt, 12, 'end date text', 'Model', 
                                                          'B001', 'Test Desription', 'Booked', 'REGO111');
        GuestServicesWebService.editOnDemandTransactions('REGO111', 'redemptionLocation', startDt, 12, 'redStartText', 
                                                                   endDt, 12, 'redEndText', 'redemptionModel', 'redemptionDescription','redemptionRego');
        GuestServicesWebService.cancelOnDemandTransaction(customer.Id, customer.Guest_Gigya_ID__c, 'B001', true);
    }
    
    static testmethod void getGuestVoucherAvailability(){
        dataSetup();
        GuestServicesWebService.getGuestVoucherAvailability(customer.Guest_Gigya_ID__c, null);
    }
    
    static testmethod void getPartnersandBranches(){
        dataSetup();
        GuestServicesWebService.getPartnersandBranches();
    }
    
    private static void dataSetup(){
        List<Account> newAcctList = new List<Account>();
        Id PARTNERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner').getRecordTypeId();
        Id CUSTOMERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        Account partnerAcct = new Account(Name = 'Partner Account', RecordTypeId = PARTNERACCTRECORDTYPEID, Partner_Class__c = 'Customer');
        newAcctList.add(partnerAcct);
        
        customer = new Account(FirstName = 'Test', LastName = 'Customer', RecordTypeId = CUSTOMERACCTRECORDTYPEID, Guest_Gigya_ID__c = 'TESTGIGYA');
        newAcctList.add(customer);
        
        insert newAcctList;
        
        Offer__c newOffer = new Offer__c(Partner_Account__c = partnerAcct.Id, Status__c = 'Inactive', Offer_Class__c = 'Customer', Customer_Platinum_Limit__c = 10,
                               Offer_Type__c = 'Car Hire');
        insert newOffer;
        newOffer.Status__c = 'Active';
        update newOffer;
        
        cod = new Customer_Offer_Detail__c();
        cod.Customer__c = customer.Id;
        cod.Offer__c = newOffer.Id;
        cod.First_Credit_Expiration__c = system.today()+5;
        cod.Total_Credits_Allowed__c = 10;
        cod.Total_Customer_Credits_Consumed__c = 0;
        insert cod;
    }
}