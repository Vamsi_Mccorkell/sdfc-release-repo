public class SP_PotentiateWebServices
{
	@future (callout=true)
	public static void TriggerHotAlertActionedRequest(string AlertType, string VIN, string Actioned, string Comment, string ServiceDate)
	{
		boolean bParameterError = false;

		if (AlertType == null || AlertType == '')
			bParameterError = true;

		if (VIN == null || VIN == '')
			bParameterError = true;

		if (AlertType == '2')
		{
			if (ServiceDate == null || ServiceDate == '')
				bParameterError = true;
		}

		if (!bParameterError)
		{
			// send the request
			string strURL = SPCachePotentiateMetadata.getServiceLocation();
			strURL += '/LEXUS/ACTION_HOT_ALERT.php';
			strURL += '?VIN=' + VIN;
			strURL += '&TYPE=' + AlertType;
			strURL += '&ACTIONED=' + Actioned;
			strURL += '&COMMENT=' + Comment;
	
			if (AlertType == '2')
				strURL += '&SERVICE_DATE=' + ServiceDate;
	
			Http 			http;
			HttpRequest		httpRequest;
			HttpResponse 	httpResponse;

			try
			{
				http = new Http();
				
				httpRequest = new HttpRequest();
				
				httpRequest.setEndpoint(strURL);
				httpRequest.setMethod('GET');
				httpRequest.setCompressed(true);

				httpResponse = http.send(httpRequest);
			}
			catch(Exception cwse)
			{
				system.debug('Website HTTP Request - ' + httpRequest);
							
				if (httpResponse != null)
					System.Debug(httpResponse.getBody());
			}
		}
	} 

	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	public static testmethod void TestSP_PotentiateWebServices()
	{
		// Create some test data
		string strAlertType		= '1';
		string strVIN			= 'J999999999999';
		string strActioned		= '0';
		string strComments		= 'Test%20comments';
		string strServiceDate;

		string strAlertType2	= '2';
		string strVIN2			= 'J999999999999';
		string strActioned2		= '0';
		string strComments2		= 'Test%20comments';
		string strServiceDate2	= '01/02/2011';

		test.starttest();

		SP_PotentiateWebServices.TriggerHotAlertActionedRequest	(	strAlertType, 
																	strVIN,
																	strActioned,
																	strComments,
																	strServiceDate
																);

		SP_PotentiateWebServices.TriggerHotAlertActionedRequest	(	strAlertType2, 
																	strVIN2,
																	strActioned2,
																	strComments2,
																	strServiceDate2
																);
		test.stoptest();
	}

}