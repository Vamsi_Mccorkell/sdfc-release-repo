@isTest
private class SP_TestQASAddressUpdate_Trigger
{
//	This test class excercises the SP_QASAddressUpdate trigger

    static testMethod void myTestQASAddressUpdate_Trigger()
    {
		// Create some test data
		Product2 sProduct = new Product2();
		sProduct.Name								= 'IS200';
		insert sProduct;

		Asset__c sAsset = new Asset__c();
		sAsset.Name									= 'ABC12345678';
		sAsset.Vehicle_Model__c						= sProduct.Id;
		insert sAsset;

		Service__c sService = new Service__c();
		sService.Asset__c				= sAsset.Id;
		sService.Integration_Customer_Address__c	= '83 York Street';
		sService.Integration_Customer_Suburb__c		= 'Sydney';
		sService.Integration_Customer_Postcode__c	= '2000';
		insert sService;
		
		// Inserting a Service record will fire the trigger SP_ServiceIntegration
		// This will create an Address_For_Validation record
		// The asynchronous QAS process will validate the address and create an Address_After_Validation record
		// We will create one manually now for test purposes

		Address_For_Validation__c sAFV =	[	select	id
												from	Address_For_Validation__c
												where	Source_Id__c = :sService.Id
											];


		// Now do some testing
		Address_After_Validation__c sAAV = new Address_After_Validation__c();
		sAAV.Address_For_Validation__c				= sAFV.Id;
		sAAV.Confidence__c							= '9';
		sAAV.Street_Address__c						= '83 York St';
		sAAV.Suburb__c								= 'Sydney';
		sAAV.Postcode__c							= '2000';
		insert sAAV;

    }

}