// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for account creating accounts need to view the custom settings data
@IsTest (SeeAllData=true)
public class VehicleManagementHandlerTest {
    public static Account customer; 
    public static Vehicle_Ownership__c vo;
    static testmethod void getGuestVehiclesRequiredFields(){
        MyLexusWebService.GuestVehicleWSResponse wsoutput = VehicleManagementHandler.getGuestVehicles(null);
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('700', wsoutput.errCode);
    }
    
    static testmethod void getGuestVehiclesNoAccountIdGigyaMatch(){
        MyLexusWebService.GuestVehicleWSResponse wsoutput = VehicleManagementHandler.getGuestVehicles('OTHERGIGYAID');
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('615', wsoutput.errCode);
    }
    
    static testmethod void getGuestVehiclesNoAccountIdNoActiveGuest(){
        dataSetup();
        delete [select Id from Guest_Relationship__c where Account__c = :customer.Id]; // remove all GR's
        MyLexusWebService.GuestVehicleWSResponse wsoutput = VehicleManagementHandler.getGuestVehicles(customer.Guest_Gigya_ID__c);
        system.assertEquals(false, wsoutput.success);
        system.assertEquals('616', wsoutput.errCode);
    }
    
    static testmethod void getGuestVehiclesSuccessful(){
        dataSetup();
        
        Guest_Relationship__c gr = new Guest_Relationship__c();
        gr.Account__c = customer.Id;
        gr.Asset__c = vo.AssetID__c;
        gr.GR_Status__c = 'Verified';
        gr.VehicleOwnership__c = vo.Id;
        gr.Gigya_ID__c = customer.Guest_Gigya_ID__c;
        insert gr;
        MyLexusWebService.GuestVehicleWSResponse wsoutput = VehicleManagementHandler.getGuestVehicles(customer.Guest_Gigya_ID__c);
        system.assertEquals(true, wsoutput.success);
        system.assertEquals(null, wsoutput.errCode);
    }
    
    
    private static void dataSetup(){
        vo = [select Id, AssetID__c, Customer__c from Vehicle_Ownership__c where Status__c = 'Active' and Customer__r.Guest_Gigya_ID__c = null
              and Customer__r.GR_Status__c != 'Verified' limit 1];
        
        Id OWNERRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        customer = [select Id, Guest_Gigya_ID__c, GR_Status__c from Account where Id = :vo.Customer__c limit 1];
        customer.Guest_Gigya_ID__c = 'GIGYA1111';
        customer.GR_Status__c = 'Verified';
        update customer;
    }
}