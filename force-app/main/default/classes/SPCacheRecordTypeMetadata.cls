public with sharing class SPCacheRecordTypeMetadata
{
	private static SPCacheRecordTypeMetadata__c CacheRecordTypeMetadata;
	
	/***********************************************************************************************************
		Lookup Cached information
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	public static string getAccount_Dealer()
	{
		if (CacheRecordTypeMetadata == null)
			LoadMetadata(); 
			
		return CacheRecordTypeMetadata.Account_Dealer__c;
	}

	public static string getAccount_Lexus()
	{
		if (CacheRecordTypeMetadata == null)
			LoadMetadata();
			
		return CacheRecordTypeMetadata.Account_Lexus__c;
	}

	public static string getPersonAccountOwnerRecordType()
	{
		if (CacheRecordTypeMetadata == null)
			LoadMetadata();
			
		return CacheRecordTypeMetadata.PersonAccountOwnerRecordType__c;
	}

	public static string getPersonAccountProspectRecordType()
	{
		if (CacheRecordTypeMetadata == null)
			LoadMetadata();
			
		return CacheRecordTypeMetadata.PersonAccountProspectRecordType__c;
	}

	public static string getWebUserId()
	{
		if (CacheRecordTypeMetadata == null)
			LoadMetadata();
			
		return CacheRecordTypeMetadata.WebUserId__c;
	}

	public static string getAPIUserProfileID()
	{
		if (CacheRecordTypeMetadata == null)
			LoadMetadata();
			
		return CacheRecordTypeMetadata.APIUserProfileID__c;
	}

	public static string getLexusAustraliaRecordID()
	{
		if (CacheRecordTypeMetadata == null)
			LoadMetadata();
			
		return CacheRecordTypeMetadata.LexusAustraliaRecordID__c;
	}

	public static string getPortMacquarieRecordID()
	{
		if (CacheRecordTypeMetadata == null)
			LoadMetadata();
			
		return CacheRecordTypeMetadata.PortMacquarieRecordID__c;
	}

	public static string getIntegrationUserID()
	{
		if (CacheRecordTypeMetadata == null)
			LoadMetadata();
			
		return CacheRecordTypeMetadata.IntegrationUserID__c;
	}

	public static string getSystemAdminProfileID()
	{
		if (CacheRecordTypeMetadata == null)
			LoadMetadata();
			
		return CacheRecordTypeMetadata.SystemAdminProfileID__c;
	}

	public static string getCSAProfileID()
	{
		if (CacheRecordTypeMetadata == null)
			LoadMetadata();
			
		return CacheRecordTypeMetadata.CSAProfileID__c;
	}

	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	private static void LoadMetadata()
	{
		CacheRecordTypeMetadata = SPCacheRecordTypeMetadata__c.getInstance();
	}
	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	public static testmethod void TestSPCacheRecordTypeMetadata()
	{
		SPCacheRecordTypeMetadata.getAccount_Dealer();
		CacheRecordTypeMetadata = null;

		SPCacheRecordTypeMetadata.getAccount_Lexus();
		CacheRecordTypeMetadata = null;

		SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		CacheRecordTypeMetadata = null;

		SPCacheRecordTypeMetadata.getPersonAccountProspectRecordType();
		CacheRecordTypeMetadata = null;

		SPCacheRecordTypeMetadata.getWebUserId();
		CacheRecordTypeMetadata = null;

		SPCacheRecordTypeMetadata.getAPIUserProfileID();
		CacheRecordTypeMetadata = null;

		SPCacheRecordTypeMetadata.getLexusAustraliaRecordID();
		CacheRecordTypeMetadata = null;

		SPCacheRecordTypeMetadata.getPortMacquarieRecordID();
		CacheRecordTypeMetadata = null;

		SPCacheRecordTypeMetadata.getIntegrationUserID();
		CacheRecordTypeMetadata = null;

		SPCacheRecordTypeMetadata.getSystemAdminProfileID();
		CacheRecordTypeMetadata = null;

		SPCacheRecordTypeMetadata.getCSAProfileID();
		CacheRecordTypeMetadata = null;

	}
}