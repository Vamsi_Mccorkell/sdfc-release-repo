/* DATE: MARCH 2020
 * AUTHOR: D BANEZ / MCCORKELL
 * CLASS SUMMARY: Batch class to call the GuestVoucherServicesUtilities create OD records; Only currently used for Offer Triggers
 * * TEST CLASS: BatchCreateCODVINODVOODRecordsTest
 * */
public without sharing class BatchCreateCODVINODVOODRecords implements Database.Batchable<sObject>, Schedulable, Database.Stateful {
    Id OfferId = null;

    public BatchCreateCODVINODVOODRecords(Id sfOfferId){
        this.OfferId = sfOfferId;
    }
    
    public void execute(SchedulableContext sc) {
        Database.executeBatch(this);
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){ 
        // we need to batch the VO's as this is the one growing, not the Offer
        if (!Test.isRunningTest()){
            return Database.getQueryLocator([select Id, Valid_for_Offers__c, Asset_Encore_Tier__c, Customer__c,
                          Valet_Voucher_Expiry_Date__c, AssetID__c
                          from Vehicle_Ownership__c 
                          where (Asset_Encore_Tier__c = 'Basic' or Asset_Encore_Tier__c = 'Platinum')
                          and Valet_Voucher_Expiry_Date__c >= TODAY
                          and Status__c ='Active'
                                        and AssetID__c != null]);
        }
        else {
            return Database.getQueryLocator([select Id, Valid_for_Offers__c, Asset_Encore_Tier__c, Customer__c,
                          Valet_Voucher_Expiry_Date__c, AssetID__c
                          from Vehicle_Ownership__c 
                          where (Asset_Encore_Tier__c = 'Basic' or Asset_Encore_Tier__c = 'Platinum')
                          and Valet_Voucher_Expiry_Date__c >= TODAY
                          and Status__c ='Active'
                                        and AssetID__c != null limit 10]);
        }
        
        
    }
    
    public void execute(Database.BatchableContext BC, List<Vehicle_Ownership__c> scope) {
        Set<Id> voIdSet = new Set<Id>();
        Set<Id> vinIDSet = new Set<Id>();
        Set<Id> offerIdSet = new Set<Id>();
        
    // Add Id to execute for one offer only.
        List<Offer__c> offerList = [select Id from Offer__c where Status__c = 'Active' and Id = :OfferId];
        for (Offer__c o : offerList){
            offerIdSet.add(o.Id);
        }
    	
        for (Vehicle_Ownership__c vo : scope){
            voIdSet.add(vo.Id);
            vinIDSet.add(vo.AssetID__c);
        }
        if (offerIdSet.size() > 0){
            GuestVoucherServicesUtilBatch.createCODVINODVOODRecords(offerIdSet, vinIDSet, voIdSet); 
        }
    }
    
    public void finish(Database.BatchableContext BC) {
        
    }
}