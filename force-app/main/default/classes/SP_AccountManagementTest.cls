@isTest
public class SP_AccountManagementTest 
{
	private static Account CreateTestEUOwnerAccount(Enform_User__c enformUser)
	{
		Account TestEUOwnerAccount = new Account();
		TestEUOwnerAccount.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
		TestEUOwnerAccount.FirstName					= 'TestFirst1';
		TestEUOwnerAccount.LastName				= 'TestLast1';
		TestEUOwnerAccount.PersonEmail			= 'test1@email.com';
		TestEUOwnerAccount.PersonMobilePhone = '12335';	
		
		insert TestEUOwnerAccount;
		if (enformUser != null)
		{
			enformUser.Owner_ID__c = TestEUOwnerAccount.Id;
			insert enformUser;
		}
		
		return TestEUOwnerAccount;
	}
	
	private static Account CreateTestEUOtherAccount(Enform_User__c enformUser)
	{
		Account TestEUOtherAccount = new Account();
		TestEUOtherAccount.RecordTypeId			= SPCacheRecordTypeMetadata.getPersonAccountProspectRecordType();
		TestEUOtherAccount.LastName				= 'TestLast2';
		TestEUOtherAccount.PersonEmail			= 'test2@email.com';
		TestEUOtherAccount.PersonMobilePhone = '12336';	
		
		insert TestEUOtherAccount;
		if (enformUser != null)
		{
			enformUser.Owner_ID__c = TestEUOtherAccount.Id;		
			insert enformUser;
		}
		
		return TestEUOtherAccount;
	}
	
	private static Enform_User__c CreateTestEnformUser()
	{
		Enform_User__c testEU = new Enform_User__c(Mobile_Phone__c = '045678923',
												   Email__c = 'aaa@aa.com',
												   First_Name__c = 'First',
												   Last_Name__c = 'Last',
												   IsPrimaryUser__c = true);
		return testEU;
	}
	
	private static Vehicle_Ownership__c CreateVOForOwner(boolean IsEnformed, Date origSalesDate, ID owner)
	{
		Asset__c testAsset = CreateTestAsset(origSalesDate);
		
		Vehicle_Ownership__c testVO = new Vehicle_Ownership__c(Enform_Registered__c = IsEnformed,
																Customer__c = owner,
																Status__c = 'Active',
																AssetID__c = testAsset.Id);
		insert 	testVO;
		
		return testVO;
	}	
	
	private static Asset__c CreateTestAsset(Date origSalesDate)
	{
		Asset__c testAsset = new Asset__c(Name = 'test1', Original_Sales_Date__c = origSalesDate);
		insert testAsset;	
		
		return testAsset;
	}	
	
	// Logged In user is not Intelematics - Negative
	// Account (Not owner) related to enform user - Update
	
	// Result 
	
	// Account updated
	// No Trace of web service call out in log and you can not see  UNIT TESTING: CANNOT MAKE CALLOUT in log
	@isTest(SeeAllData=true)
    public static void TestCase001() 
    {
    	SPCacheLexusEnformMetadata__c metaData = SPCacheLexusEnformMetadata__c.getInstance();
    	metaData.Intelematics_User__c = null;
    	
       	Test.startTest();
       	
    	Enform_User__c enfUser = CreateTestEnformUser();
    	Account OwnerAcc = CreateTestEUOtherAccount(enfUser);
    	
    	OwnerAcc.LastName = 'Test3';

		update OwnerAcc;
		
		Test.stopTest();
		
		Account acc = [select LastName from Account where Id = :OwnerAcc.Id];
		system.assertEquals(acc.LastName, OwnerAcc.LastName);
		
    }
    
 	// Logged In user is not Intelematics - Negative
	// Account (Not owner) related to enform user - delete
	
	// Result 
	
	// Account deleted
	// No Trace of web service call out in log and you can not see  UNIT TESTING: CANNOT MAKE CALLOUT in log
	@isTest(SeeAllData=true)
    public static void TestCase002() 
    {
    	SPCacheLexusEnformMetadata__c metaData = SPCacheLexusEnformMetadata__c.getInstance();
    	metaData.Intelematics_User__c = null;
    	
       	Test.startTest();
       	
    	Enform_User__c enfUser = CreateTestEnformUser();
    	Account OwnerAcc = CreateTestEUOtherAccount(enfUser);
    	
    	ID delID = OwnerAcc.Id;

		delete enfUser;
		delete OwnerAcc;
		
		Test.stopTest();
		
		List<Account> accounts = [select LastName from Account where Id = :delID];
		system.assertEquals(accounts.size(), 0);   	
		
    }	 	
    
   	// Logged In user is not Intelematics
	// Account (Owner) related to enform user - Insert
	
	// Result
	
	// Account updated
	// The given line can bee seen in the log:
	// UNIT TESTING: CANNOT MAKE CALLOUT
	@isTest(SeeAllData=true)
    public static void TestCase003() 
    {
    	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForIA());
    	
    	SPCacheLexusEnformMetadata__c metaData = SPCacheLexusEnformMetadata__c.getInstance();
    	metaData.Intelematics_User__c = null;
    	
       	Test.startTest();
       	
    	Enform_User__c enfUser = CreateTestEnformUser();
    	Account OwnerAcc = CreateTestEUOwnerAccount(enfUser);
    	
    	OwnerAcc.LastName = 'Test3';
    	OwnerAcc.Customer_Password__pc = 'TestPW';
		
		update OwnerAcc;
		
		//Account acc = [select LastName from Account where Id = :OwnerAcc.Id];
		
		Test.stopTest();
		
		Enform_User__c eUser = [select Last_Name__c, Password__c from Enform_User__c 
								where Owner_ID__c = :OwnerAcc.Id];
		
		
		system.assertEquals(OwnerAcc.LastName, eUser.Last_Name__c);   	
		system.assertEquals(OwnerAcc.Customer_Password__pc, 'TestPW'); 
		system.assertEquals(OwnerAcc.Customer_Password__pc, eUser.Password__c);   	
    }	
    
    // Logged In user is not Intelematics
	// Account (Owner) related to enform user - Delete
	
	// Result
	
	// Account Deleted
	// The given line can bee seen in the log:
	// UNIT TESTING: CANNOT MAKE CALLOUT
	@isTest(SeeAllData=true)
    public static void TestCase004() 
    {
    	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForIA());
    	
    	SPCacheLexusEnformMetadata__c metaData = SPCacheLexusEnformMetadata__c.getInstance();
    	metaData.Intelematics_User__c = null;
    	
       	Test.startTest();
       	
    	Enform_User__c enfUser = CreateTestEnformUser();
    	Account OwnerAcc = CreateTestEUOwnerAccount(enfUser);
    	
    	ID delID = OwnerAcc.Id;
		
		delete enfUser;
		delete OwnerAcc;
		
		Test.stopTest();

		List<Account> accounts = [select LastName from Account where Id = :delID];
		system.assertEquals(accounts.size(), 0);   	
    }   
    
    // Logged In user is not Intelematics - Negative
	// Account (Owner) related to enform user - Update - No change to mandatory fields
	
	// Result
	
	// Account updated
	// The given line can not bee seen in the log:
	// UNIT TESTING: CANNOT MAKE CALLOUT
	@isTest(SeeAllData=true)
    public static void TestCase005() 
    {
    	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForIA());
    	
    	SPCacheLexusEnformMetadata__c metaData = SPCacheLexusEnformMetadata__c.getInstance();
    	metaData.Intelematics_User__c = null;
    	
       	Test.startTest();
       	
    	Enform_User__c enfUser = CreateTestEnformUser();
    	Account OwnerAcc = CreateTestEUOwnerAccount(enfUser);
    	
    	OwnerAcc.PersonTitle = 'Mr';
		
		update OwnerAcc;
		
		Test.stopTest();

		Account acc = [select PersonTitle from Account where Id = :OwnerAcc.Id];
		system.assertEquals(acc.PersonTitle, OwnerAcc.PersonTitle);   	
    }	   
    
   	// Logged In user is not Intelematics - Negative
	// Account (Owner) not related to enform user - Insert
	
	// Result
	
	// Account updated
	// The given line can not be seen in the log:
	// UNIT TESTING: CANNOT MAKE CALLOUT
	@isTest(SeeAllData=true)
    public static void TestCase006() 
    {
    	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForIA());
    	
    	SPCacheLexusEnformMetadata__c metaData = SPCacheLexusEnformMetadata__c.getInstance();
    	metaData.Intelematics_User__c = null;
    	
       	Test.startTest();

    	Account OwnerAcc = CreateTestEUOwnerAccount(null);
    	
    	OwnerAcc.LastName = 'Test3';
		
		update OwnerAcc;
		
		Test.stopTest();

		Account acc = [select LastName from Account where Id = :OwnerAcc.Id];
		system.assertEquals(acc.LastName, OwnerAcc.LastName);   	
    }	  
    
     // Logged In user is not Intelematics - Negative
	// Account (Owner) not related to enform user - Delete
	
	// Result
	
	// Account Deleted
	// The given line can not bee seen in the log:
	// UNIT TESTING: CANNOT MAKE CALLOUT
	@isTest(SeeAllData=true)
    public static void TestCase007() 
    {
    	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForIA());
    	
    	SPCacheLexusEnformMetadata__c metaData = SPCacheLexusEnformMetadata__c.getInstance();
    	metaData.Intelematics_User__c = null;
    	
       	Test.startTest();
       	
    	Account OwnerAcc = CreateTestEUOwnerAccount(null);
    	
    	ID delID = OwnerAcc.Id;
		
		delete OwnerAcc;
		
		Test.stopTest();

		List<Account> accounts = [select LastName from Account where Id = :delID];
		system.assertEquals(accounts.size(), 0);   	
    } 
    
   	// Logged In user is  Intelematics - Negative
	// Account (Owner) related to enform user - update
	
	// Result
	
	// Account updated
	// The given line can not bee seen in the log:
	// UNIT TESTING: CANNOT MAKE CALLOUT
	@isTest(SeeAllData=true)
    public static void TestCase008() 
    {
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForIA());
    	
     	Enform_User__c enfUser = CreateTestEnformUser();
    	Account OwnerAcc = CreateTestEUOwnerAccount(enfUser);
    	
   		SPCacheLexusEnformMetadata__c metaData = [select Id,Intelematics_User__c from SPCacheLexusEnformMetadata__c];
    		User currUser = [select Id from User where Id = :metaData.Intelematics_User__c];
    	
       	Test.startTest();
       	
       	system.runAs(currUser)
    	{
    	OwnerAcc.LastName = 'Test3';
		
		try
		{
		update OwnerAcc;
		}catch(Exception e)
		{
			system.debug(e.getMessage());
		}
    	}
		
		Test.stopTest();

		//Account acc = [select LastName from Account where Id = :OwnerAcc.Id];
		//system.assertEquals(acc.LastName, OwnerAcc.LastName);   
		
		//system.assertEquals(metaData.Intelematics_User__c, SPCacheLexusEnformMetadata.getIntelematicsUser());	
    }  
    
    // Test Intelematics User can update benefits_events_opt_out__c and product_information_opt_out__c
    @isTest(SeeAllData=true)
    public static void TestCase009() 
    {
        
        
        Enform_User__c enfUser = CreateTestEnformUser();
        Account OwnerAcc = CreateTestEUOwnerAccount(enfUser);
        
        OwnerAcc.benefits_events_opt_out__c = true;
        OwnerAcc.product_information_opt_out__c = true;
        
   		SPCacheLexusEnformMetadata__c metaData = [select Id,Intelematics_User__c from SPCacheLexusEnformMetadata__c];
    	User currUser = [select Id from User where Id = :metaData.Intelematics_User__c];
        
        Test.startTest();
               	system.runAs(currUser)
               	{
        update OwnerAcc;
               	}
        
        Test.stopTest();

        Account acc = [select id, benefits_events_opt_out__c, product_information_opt_out__c
                      from Account where Id = :OwnerAcc.Id];
        system.assertEquals(acc.benefits_events_opt_out__c, true);
        system.assertEquals(acc.product_information_opt_out__c, true);
        
    }
    
	@isTest(SeeAllData=true)
    public static void TestCase010() 
    {
    	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForIAAppErr());
    	
    	SPCacheLexusEnformMetadata__c metaData = SPCacheLexusEnformMetadata__c.getInstance();
    	metaData.Intelematics_User__c = null;
    	
       	Test.startTest();
       	
    	Enform_User__c enfUser = CreateTestEnformUser();
    	Account OwnerAcc = CreateTestEUOwnerAccount(enfUser);
    	
    	OwnerAcc.LastName = 'Test3';
    	OwnerAcc.Customer_Password__pc = 'TestPW';
		
		update OwnerAcc;
		
		//Account acc = [select LastName from Account where Id = :OwnerAcc.Id];
		
		Test.stopTest();
    }	
    
 	@isTest(SeeAllData=true)
    public static void TestCase011() 
    {
    	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorForIHttpErr());
    	
    	SPCacheLexusEnformMetadata__c metaData = SPCacheLexusEnformMetadata__c.getInstance();
    	metaData.Intelematics_User__c = null;
    	
       	Test.startTest();
       	
    	Enform_User__c enfUser = CreateTestEnformUser();
    	Account OwnerAcc = CreateTestEUOwnerAccount(enfUser);
    	
    	OwnerAcc.LastName = 'Test3';
    	OwnerAcc.Customer_Password__pc = 'TestPW';
		
		update OwnerAcc;
		
		//Account acc = [select LastName from Account where Id = :OwnerAcc.Id];
		
		Test.stopTest();
    }	   
    
}