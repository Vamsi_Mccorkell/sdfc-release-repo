// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for creating accounts need to view the custom settings data
@IsTest (SeeAllData=true)
public class GuestRelationshipTriggerHandlerTest {
	static Account customer;
    static Vehicle_Ownership__c vo;
    
    static testmethod void testCannotEditGR(){
        dataSetup();
        Guest_Relationship__c gr = new Guest_Relationship__c ();
        gr.Account__c = customer.Id;
        gr.GR_Status__c = 'New';
        gr.Gigya_ID__c = 'TestGigya';
        insert gr;
        gr.GR_Status__c = 'Verified';
        try {
            update gr;
        }
        catch (DmlException e){
            system.assertEquals(true, e.getMessage().contains(Label.GRStatusChangeError));
        }
        gr = [select Id, GR_Status__c from Guest_Relationship__c where Id = :gr.Id limit 1];
        system.assertEquals('New', gr.GR_Status__c);
    }
    
    static testmethod void testDeactivateAllPreviousGRs(){
        dataSetup();
        Guest_Relationship__c gr = new Guest_Relationship__c ();
        gr.Account__c = customer.Id;
        gr.GR_Status__c = 'New';
        gr.Gigya_ID__c = 'TestGigya';
        gr.End_Date__c = null;
        insert gr;
        Guest_Relationship__c gr2 = new Guest_Relationship__c ();
        gr2.Account__c = customer.Id;
        gr2.GR_Status__c = 'New';
        gr2.Gigya_ID__c = 'TestGigya';
        insert gr2;
        gr = [select Id, End_Date__c from Guest_Relationship__c where Id = :gr.Id limit 1];
        system.assertEquals(true, gr.End_Date__c != null);
    }
    
    static testmethod void testVerifiedGRs(){
        dataSetup();
        Guest_Relationship__c gr = new Guest_Relationship__c ();
        gr.Account__c = customer.Id;
        gr.GR_Status__c = 'New';
        gr.Gigya_ID__c = 'TestGigya';
        gr.End_Date__c = null;
        insert gr;
        Guest_Relationship__c gr2 = new Guest_Relationship__c ();
        gr2.Account__c = customer.Id;
        gr2.GR_Status__c = 'Verified';
        gr2.Gigya_ID__c = 'TestGigya';
        gr2.VehicleOwnership__c = vo.Id;
        insert gr2;
        gr = [select Id, End_Date__c from Guest_Relationship__c where Id = :gr.Id limit 1];
        system.assertEquals(true, gr.End_Date__c != null);
        vo = [select Id, Verified_Guest_ID__c from Vehicle_Ownership__c where Id = :vo.Id limit 1];
        system.assertEquals('TestGigya', vo.Verified_Guest_ID__c);
        Guest_Relationship__c gr3 = new Guest_Relationship__c ();
        gr3.Account__c = customer.Id;
        gr3.GR_Status__c = 'Verified';
        gr3.Gigya_ID__c = 'TestGigya';
        gr3.VehicleOwnership__c = vo.Id;
        insert gr3;
        gr2 = [select Id, End_Date__c from Guest_Relationship__c where Id = :gr2.Id limit 1];
        system.assertEquals(true, gr2.End_Date__c != null);
    }
    
    static testmethod void testUnverifiedGRs(){
        dataSetup();
        Guest_Relationship__c gr = new Guest_Relationship__c ();
        gr.Account__c = customer.Id;
        gr.GR_Status__c = 'New';
        gr.Gigya_ID__c = 'TestGigya';
        gr.End_Date__c = null;
        insert gr;
        Guest_Relationship__c gr2 = new Guest_Relationship__c ();
        gr2.Account__c = customer.Id;
        gr2.GR_Status__c = 'Verified';
        gr2.Gigya_ID__c = 'TestGigya';
        gr2.VehicleOwnership__c = vo.Id;
        insert gr2;
        gr = [select Id, End_Date__c from Guest_Relationship__c where Id = :gr.Id limit 1];
        system.assertEquals(true, gr.End_Date__c != null);
        vo = [select Id, Verified_Guest_ID__c from Vehicle_Ownership__c where Id = :vo.Id limit 1];
        system.assertEquals('TestGigya', vo.Verified_Guest_ID__c);
        Guest_Relationship__c gr3 = new Guest_Relationship__c ();
        gr3.Account__c = customer.Id;
        gr3.GR_Status__c = 'Unverified';
        gr3.Gigya_ID__c = 'TestGigya';
        gr3.VehicleOwnership__c = vo.Id;
        insert gr3;
        gr2 = [select Id, End_Date__c from Guest_Relationship__c where Id = :gr2.Id limit 1];
        system.assertEquals(true, gr2.End_Date__c != null);
        vo = [select Id, Verified_Guest_ID__c from Vehicle_Ownership__c where Id = :vo.Id limit 1];
        system.assertEquals(null, vo.Verified_Guest_ID__c);
    }
    
    static void dataSetup(){
        Id CUSTOMERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        vo = [select Id, Customer__c, AssetID__c from Vehicle_Ownership__c 
                          where Status__c = 'Active' and Valid_for_Offers__c = false limit 1]; // try to get 1 that isn't valid for offer yet
        Asset__c asst = new Asset__c(Id = vo.AssetID__c);
        asst.Original_Sales_Date__c = system.today();
        asst.Encore_Tier__c = 'Platinum';
        update asst;    
        customer = [select Id, Guest_Gigya_ID__c from Account where Id = :vo.Customer__c limit 1];
        customer.Guest_Gigya_ID__c = 'TESTGIGYA';
        update customer;
        vo.Valet_Voucher_Enabled__c = true;
        vo.Voucher_T_C_Accepted__c = true;
        vo.Voucher_T_C_Accepted_Date__c = system.today();
        vo.Asset_RDR_Date__c = system.today();
        update vo;
    }
}