// DBANEZ DEV Notes: We had to set to SeeAllData=true because other test triggers for  creating accounts need to view the custom settings data
@IsTest (SeeAllData=true)
public class GuestVoucherServicesUtilitiesTest {
	static Account partnerVehicle;
    static Account partnerCustomer;
    static Offer__c newOffer;
    static Vehicle_Ownership__c validForOfferVO;
    static Asset__c asset;
    static Account customer;
    
    static testmethod void testCreationAndRecalculation(){
        dataSetupQuery();
        newOffer = new Offer__c(Status__c = 'Inactive');
        newOffer.Offer_Class__c = 'Customer';
        newOffer.Offer_Type__c = 'Car Hire';
        newOffer.Customer_Platinum_Limit__c = 10;
        newOffer.Partner_Account__c = partnerCustomer.Id;
        insert newOffer;
        newOffer.Status__c = 'Active';
        update newOffer;
        Id CUSTOMEROFFERRECORDTYPEID = Schema.SObjectType.VO_Offer_Detail__c.getRecordTypeInfosByDeveloperName().get('Customer_Vehicle_Offer').getRecordTypeId();
        List<VO_Offer_Detail__c> voodList = [select Id, Vehicle_Ownership__c, CustomerOfferDetail__c, Offer__c from VO_Offer_Detail__c 
                                                  where Offer__c = :newOffer.Id
                                             and Vehicle_Ownership__c = :validForOfferVO.Id
                                             and RecordTypeId = :CUSTOMEROFFERRECORDTYPEID];
                                             // only filtering based on test records
                                                 
        //system.assertEquals(1, voodList.size()); // has to have one VOOD
        List<Customer_Offer_Detail__c> coodList = [select Id, First_Credit_Expiration__c, Customer__c, Offer__c, Total_Credits_Allowed__c from Customer_Offer_Detail__c 
                                                  where Offer__c = :newOffer.Id 
                                                   and Customer__c = :customer.Id];
        validForOfferVO = [select Id, Valet_Voucher_Expiry_Date__c from Vehicle_Ownership__c where Id = :validForOfferVO.Id limit 1];
        /*system.assertEquals(1, coodList.size()); // has to have 1
        system.assertEquals(coodList[0].Id, voodList[0].CustomerOfferDetail__c); // COOD must be filled up
		system.assertEquals(10, coodList[0].Total_Credits_Allowed__c); // Credits available must be same as active offer credits
        system.assertEquals(validForOfferVO.Valet_Voucher_Expiry_Date__c, coodList[0].First_Credit_Expiration__c); // Expiry date*/
    }
    
    static void dataSetupQuery(){
        // This is a workaround data setup since creating VO's on the fly calls a big SOQL that pushes the limit
        List<Account> newAcctList = new List<Account>();
        List<Vehicle_Ownership__c> voList = new List<Vehicle_Ownership__c>();
        List<Asset__c> assetList = new List<Asset__c>();
        
        Id PARTNERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner').getRecordTypeId();
        Id CUSTOMERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        partnerVehicle = new Account(Name = 'Partner Veh', RecordTypeId = PARTNERACCTRECORDTYPEID, Partner_Class__c = 'Vehicle');
        partnerCustomer = new Account(Name = 'Partner Cust', RecordTypeId = PARTNERACCTRECORDTYPEID, Partner_Class__c = 'Customer');
        newAcctList.add(partnerVehicle);
        newAcctList.add(partnerCustomer);
        insert newAcctList;
        
        
        validForOfferVO = [select Id, Customer__c, AssetID__c from Vehicle_Ownership__c 
                          where Status__c = 'Active' limit 1];
        Asset__c asst = new Asset__c(Id = validForOfferVO.AssetID__c);
        asst.Original_Sales_Date__c = system.today();
        asst.Encore_Tier__c = 'Platinum';
        update asst;    
        customer = new Account(Id = validForOfferVO.Customer__c);
        validForOfferVO.Valet_Voucher_Enabled__c = true;
        validForOfferVO.Voucher_T_C_Accepted__c = true;
        validForOfferVO.Voucher_T_C_Accepted_Date__c = system.today();
        validForOfferVO.Asset_RDR_Date__c = system.today();
        update validForOfferVO;
        
    }
}