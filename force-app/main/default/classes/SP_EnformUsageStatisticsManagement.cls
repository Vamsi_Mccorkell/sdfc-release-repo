// Class to handle all use cases related to Enform Usage Statistics
// Author: Shabu, SP on 02.04.2013
public class SP_EnformUsageStatisticsManagement 
{
	// Check each Enform Usage Statistics for the validity of mandatory fields
	public static void doValidateEnformUsageStatistics(List<Enform_Usage_Statistics__c> li_enformUsageStats)
	{
		SP_EnformUtilities.ValidateEnformUsageStatistics(li_enformUsageStats);
	}
	
	// Set the Enform App Usage Stat data in Enform User
	public static void SetEnformUsageStatisticsForEnformUser(List<Enform_Usage_Statistics__c> li_enformUserStatistics)
	{
		Date lastThreeMonthsStartDate = system.today().addMonths(-3);
		
		set<String> set_appNames = new set<String>();
		set<ID> set_enformUsers = new set<ID>();
		set<string> set_AppUserCombiKey = new set<string>();
		
		// Get all the Enform App Names and Enform Users. Since we have no way to query for a EnformUser-EnformApp
		// key, we have to query all combination of the collected enform users and apps. Then we can skip the
		// wrong combination
		for (Enform_Usage_Statistics__c eus : li_enformUserStatistics)
		{
			// Enform App Names
			set_appNames.add(eus.Enform_App__c);
			// Enform Users
			set_enformUsers.add(eus.Enform_User__c);
			// Key to skip the needless combination records
			set_AppUserCombiKey.add(String.valueOf(eus.Enform_User__c).substring(0, 15) + '-' + eus.Enform_App__c);
		}
		
		// Get all related enform users
		Map<ID, Enform_User__c> map_relatedEnformUsers = new Map<ID, Enform_User__c>([select Id from Enform_User__c
																					  where Id in :set_enformUsers]);
		
		// Get the aggregate of all combinations of Enform Users and Enform Apps with in last three months
		// Definitely there are needless combinations and using the right combination key we
		// have created earlier, we can get rid of wrong combinations																			  
		for (AggregateResult ar : [select Enform_App__c enformApp, Enform_User__c enformUser, 
										 SUM(Number_of_App_Launches__c) totalLaunches,
										 MAX(Statistics_Period_Ending__c) lastUpdDateDate
								   from Enform_Usage_Statistics__c 
								   where Enform_App__c in :set_appNames
								   and Enform_User__c in :set_enformUsers
								   and Statistics_Period_Ending__c >= :lastThreeMonthsStartDate
								   group by  Enform_User__c, Enform_App__c])
		{
			string enfApp = String.valueOf(ar.get('enformApp'));
			
			string temp = String.valueOf(ar.get('enformUser')).substring(0, 15) + '-' + enfApp;
			
			// Wrong combination			  
			if (!set_AppUserCombiKey.contains(temp))
				continue;
				
			ID enformUserID = String.valueOf(ar.get('enformUser'));
			Integer totalAppLaunches = Integer.valueOf(ar.get('totalLaunches'));
			Date lastUpdatedDate = Date.valueOf(ar.get('lastUpdDateDate'));
			
			system.debug('########## ENFORM USER: ' + enformUserID);
			system.debug('########## ENFORM APP: ' + enfApp);
			system.debug('########## TOTAL LAUNCHES: ' + totalAppLaunches);
			system.debug('########## LAST UPDATE DATE: ' + lastUpdatedDate);
				
			Enform_User__c enfUser = map_relatedEnformUsers.get(enformUserID);
			if (enfApp == 'Concierge')
			{
				enfUser.App_Launches_Concierge__c = totalAppLaunches;
				enfUser.Last_Updated_Date_Concierge__c = lastUpdatedDate;
			}
			else if (enfApp == 'Fuel Finder')
			{
				enfUser.App_Launches_Fuel_Finder__c = totalAppLaunches;
				enfUser.Last_Updated_Date_Fuel_Finder__c = lastUpdatedDate;
			}
			else if (enfApp == 'Local Search')
			{
				enfUser.App_Launches_Local_Search__c = totalAppLaunches;
				enfUser.Last_Updated_Date_Local_Search__c = lastUpdatedDate;
			}
			else if (enfApp == 'Traffic')
			{
				enfUser.App_Launches_Traffic__c = totalAppLaunches;
				enfUser.Last_Updated_Date_Traffic__c = lastUpdatedDate;
			}
			else if (enfApp == 'Weather')
			{
				enfUser.App_Launches_Weather__c = totalAppLaunches;
				enfUser.Last_Updated_Date_Weather__c =  lastUpdatedDate;
			}
				
			map_relatedEnformUsers.put(enformUserID, enfUser);
		}
		
		system.debug('########## EnformUsers ' + map_relatedEnformUsers.values());
		update map_relatedEnformUsers.values();
	}	
}