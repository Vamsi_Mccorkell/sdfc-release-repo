global with sharing class SP_HistoricalServiceDataSchedule implements Schedulable
{
	
global void execute(SchedulableContext SC) 
	{
        SP_HistoricalServiceDataBatch objSP_HistoricalServiceDataBatch = new SP_HistoricalServiceDataBatch();
        ID batchprocessid = Database.executeBatch(objSP_HistoricalServiceDataBatch);		
	}

    
    public static testMethod void testSP_HistoricalServiceDataSchedule() 
	{
		Test.startTest(); 
		
		System.Schedule('SP_HistoricalServiceDataSchedule', '20 30 8 10 2 ?', new SP_HistoricalServiceDataSchedule());

		Test.stopTest();
    }

}