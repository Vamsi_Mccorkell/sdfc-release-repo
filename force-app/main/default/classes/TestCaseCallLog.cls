@isTest

private class TestCaseCallLog 
{
	static testMethod void TestCaseCallLog()
	{
        // Insert Test Data
        Case case_new = new Case (Origin = 'Phone - Inbound');
        
        // Invoke Trigger
        insert case_new;
        
        // Assert Results
        Task tskQuery = [SELECT Subject FROM Task WHERE WhatId = :case_new.Id];
        
        System.assertEquals('Auto Call Log', tskQuery.Subject);
        
	}
}