public with sharing class SP_DeDupCheckProcessing
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Exception Classes & Enums
	public class SP_Exception extends Exception{}

	public enum BatchParam {ALL, ALL_ASYNC}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class used to pass arguments into the Batch
	public class SP_Args
	{
		public BatchParam 	BatchParam 		{get; set;}
		public List<id>		ProcessList		{get; set;}
		public boolean 		UseSavePoint	{get; set;}
		public boolean 		AllOrNone		{get; set;}

		public SP_Args(list<id> theProcessList)
		{
			this.ProcessList 	= theProcessList;
			this.UseSavePoint 	= true;
			this.AllOrNone 		= false;
		}

		public SP_Args(list<id> theProcessList, BatchParam theBatchParam)
		{
			this.ProcessList 	= theProcessList;
			this.BatchParam		= theBatchParam;
			this.UseSavePoint 	= true;
			this.AllOrNone 		= false;
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class used to pass information back to what called the batch
	public class SP_Ret
	{
		public boolean  	InError							{get; set;}
		public List<string> ErrorList						{get; set;}
				
		public SP_Ret()
		{
			this.InError = false;
			this.ErrorList = new List<string>();
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class for data from the Spreadsheet file plus the matching Account id
	public class SPData
	{
		public string		AccountId			{get; set;}
		public string		FirstName			{get; set;}
		public string		LastName			{get; set;}
		public string		Email				{get; set;}
		public string		Phone				{get; set;}
		public string		City				{get; set;}
		public string		Postcode			{get; set;}
		public string		Street				{get; set;}
		public string		InputOrMatch		{get; set;}

		public SPData(string strAccountId, string strFirst, string strLast, string strEmail, string strPhone, string strCity, string strPostcode, string strStreet, string strInputOrMatch)
		{
			AccountId		= strAccountId;
			FirstName		= strFirst;
			LastName		= strLast;
			Email			= strEmail;
			Phone			= strPhone;
			City			= strCity;
			Postcode		= strPostcode;
			Street			= strStreet;
			InputOrMatch	= strInputOrMatch;
		}	
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Singular
	SP_Args 	mArgs;
	SP_Ret		mRet;

	public string 			m_strData;
	public integer			m_iProcessedCount;
	public boolean			bEOF;
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Collections
	public map<id, Account>		m_mapAccounts	=	new map<id, Account>();
	public List<List<String>>	m_liAllResults	=	new	List<List<String>>();
	public List<List<String>>	m_liResults		=	new	List<List<String>>();

	/***********************************************************************************************************
		Constructor
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Our Constructor
	public SP_DeDupCheckProcessing(SP_Args oArgs)
	{
		mRet 	= new SP_Ret();
		mArgs 	= oArgs;
		
		bEOF = false;
	}

	/***********************************************************************************************************
		Access Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// The one and only entry point for the processor
	public SP_Ret ProcMain()
	{
		
//		Savepoint savePoint;
		
//		if(mArgs.UseSavePoint)
//			savePoint = Database.setSavepoint();
		
		try
		{
			ProcessAccountObject(mArgs.ProcessList);
		}
		catch(Exception e)
		{
			mRet.InError = true;
			mRet.ErrorList.add(e.getMessage());
			
			throw new SP_Exception('Fatal Exception - ' + e.getMessage());
		}
		
		return mRet;
	}
	
	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	private void ProcessAccountObject(list<id> li_JobIds)
	{
		// Get the job
		Duplicate_Check_Job__c sJob =	[
											select	Id,
													Active__c,
													DataList__c,
													NoMatchList__c,
													In_Error__c,
													Filename__c,
													Input_Count__c,
													Match_Count__c,
													Overflow__c
											from	Duplicate_Check_Job__c
											where	Id in :li_JobIds
											limit	1
										];
		

		Attachment sAtt =	[
								select	Id,
										Body,
										Name,
										ParentId
								from	Attachment
								where	ParentId = :sJob.Id
								limit	1
							];
		// Mark batch as In Error - clear if all good
		sJob.In_Error__c = true;
		update sJob;

		// Records processed in this pass - max 30
		m_iProcessedCount = 0;

system.debug('*** sAtt.Body.size() ***' + sAtt.Body.size());

		m_strData = sAtt.Body.toString();

		m_liAllResults = parseCSV(m_strData, true);

		// Process in chunks of 30 records, iterated by Input_Count__c
		integer iStartPos = sJob.Input_Count__c.intValue();
		integer iNumToProcess = m_liAllResults.size() - iStartPos;

		if (iNumToProcess > 20)
		{
			iNumToProcess = 20;
		}
		else
		{
			// Last batch - set EOF flag
			bEOF = true;
		}
//system.debug('*** m_liAllResults.size() *** ' + m_liAllResults.size());
//system.debug('*** iStartPos *** ' + iStartPos);
//system.debug('*** iNumToProcess *** ' + iNumToProcess);
		for (integer i = iStartPos; i < m_liAllResults.size(); i++)
		{
			m_liResults.add(m_liAllResults.get(i));
		}

		for (list<string> li_DataIn : m_liResults)
		{
system.debug('*** li_DataIn *** ' + li_DataIn);

			// Check that we have enough data - pad out if necessary
			if (li_DataIn.size() < 8)
			{
				for (integer i = li_DataIn.size(); i < 8; i++)
				{
					li_DataIn.add('"null"');
				}
			}

system.debug('*** VIN ***' + li_DataIn.get(1));
system.debug('*** Last name ***' + li_DataIn.get(2));
system.debug('*** Email ***' + li_DataIn.get(3));
system.debug('*** Phone ***' + li_DataIn.get(4));
system.debug('*** City ***' + li_DataIn.get(5));
system.debug('*** Postcode ***' + li_DataIn.get(6));
system.debug('*** Street ***' + li_DataIn.get(7));

			
			// Call the customer match rules
			MatchingAccountV2 ma = new MatchingAccountV2();
			m_mapAccounts = ma.customerMatchingV2	(
													li_DataIn.get(1).replaceAll('"',''),	// VIN
													li_DataIn.get(2).replaceAll('"',''),	// Last name
													li_DataIn.get(3).replaceAll('"',''),	// Email
													li_DataIn.get(4).replaceAll('"',''),	// Phone
													li_DataIn.get(5).replaceAll('"',''),	// City
													li_DataIn.get(6).replaceAll('"',''),	// Postcode
													li_DataIn.get(7).replaceAll('"',''),	// Street
													false									// Don't create Cases
												);

			// Check if we have returned the same account id as was input - remove if we have

			// Cast as a string - just in case we have a numeric value here
			string strId = '' + li_DataIn.get(0).replaceAll('"','');
system.debug('*** Returned id ***' + strId);
			// Check we have an id - that is, an 18 char value
			if (strId.length() == 18)
			{
				if (m_mapAccounts.containsKey(strId))
				{
					m_mapAccounts.remove(strId);
				}
			}

			// Update the input count
			sJob.Input_Count__c += 1;
			m_iProcessedCount += 1;

			if (m_mapAccounts.size() > 0)
			{
				// Back up five places
				sJob.DataList__c = sJob.DataList__c.substring(0, sJob.DataList__c.length() - 5);

				// Update the Job record
				sJob.DataList__c +=	
/*
				li_DataIn.get(0).replaceAll('"','') + ',' +
*/
									li_DataIn.get(1).replaceAll('"','') + ',' +
/*
									li_DataIn.get(2).replaceAll('"','') + ',' +
									li_DataIn.get(3).replaceAll('"','') + ',' +
									li_DataIn.get(4).replaceAll('"','') + ',' +
									li_DataIn.get(5).replaceAll('"','') + ',' +
									li_DataIn.get(6).replaceAll('"','') + ',' +
									li_DataIn.get(7).replaceAll('"','') + ',' +
*/
									'INPUT|FINAL';
/*
				// Now loop through the list of returned matching records and add to the Job record
				set<id> set_MatchingIds = m_mapAccounts.keyset();

				for (id idAccount : set_MatchingIds)
				{
					Account sAccount = m_mapAccounts.get(idAccount);

					// Concatenate values for the multiple phone number fields					
					string strPhone = 'None';
					if (sAccount.CM_Mobile__pc != null)
					{
						// Back up four places
						strPhone = strPhone.substring(0, strPhone.length() - 4);
						strPhone += sAccount.CM_Mobile__pc + ' / ';
					}
					if (sAccount.PersonHomePhone != null)
					{
						// Back up
						if (strPhone == 'None')
						{
							strPhone = strPhone.substring(0, strPhone.length() - 4);
						}
						else
						{
							strPhone = strPhone.substring(0, strPhone.length() - 3);
						}
						strPhone += sAccount.PersonHomePhone + ' / ';
					}
					if (sAccount.PersonOtherPhone != null)
					{
						// Back up
						if (strPhone == 'None')
						{
							strPhone = strPhone.substring(0, strPhone.length() - 4);
						}
						else
						{
							strPhone = strPhone.substring(0, strPhone.length() - 3);
						}
						strPhone += sAccount.PersonOtherPhone + ' / ';
					}
					if (sAccount.Work_Phone__c != null)
					{
						// Back up
						if (strPhone == 'None')
						{
							strPhone = strPhone.substring(0, strPhone.length() - 4);
						}
						else
						{
							strPhone = strPhone.substring(0, strPhone.length() - 3);
						}
						strPhone += sAccount.Work_Phone__c + ' / ';
					}
					if (sAccount.Phone != null)
					{
						// Back up
						if (strPhone == 'None')
						{
							strPhone = strPhone.substring(0, strPhone.length() - 4);
						}
						else
						{
							strPhone = strPhone.substring(0, strPhone.length() - 3);
						}
						strPhone += sAccount.Phone + ' / ';
					}

					if (strPhone != 'None')
					{
						strPhone = strPhone.substring(0, strPhone.length() - 3);
					}

					// Concatenate values for the two sets of Address fields
					string strCity = 'None';
					if (sAccount.ShippingCity != null)
					{
						// Back up four places
						strCity = strCity.substring(0, strCity.length() - 4);
						strCity += sAccount.ShippingCity + ' / ';
					}
					if (sAccount.BillingCity != null)
					{
						// Back up
						if (strCity == 'None')
						{
							strCity = strCity.substring(0, strCity.length() - 4);
						}
						else
						{
							strCity = strCity.substring(0, strCity.length() - 3);
						}
						strCity += sAccount.BillingCity + ' / ';
					}

					if (strCity != 'None')
					{
						strCity = strCity.substring(0, strCity.length() - 3);
					}

					string strPostalCode = 'None';
					if (sAccount.ShippingPostalCode != null)
					{
						// Back up four places
						strPostalCode = strPostalCode.substring(0, strPostalCode.length() - 4);
						strPostalCode += sAccount.ShippingPostalCode + ' / ';
					}
					if (sAccount.BillingPostalCode != null)
					{
						// Back up
						if (strPostalCode == 'None')
						{
							strPostalCode = strPostalCode.substring(0, strPostalCode.length() - 4);
						}
						else
						{
							strPostalCode = strPostalCode.substring(0, strPostalCode.length() - 3);
						}
						strPostalCode += sAccount.BillingPostalCode + ' / ';
					}

					if (strPostalCode != 'None')
					{
						strPostalCode = strPostalCode.substring(0, strPostalCode.length() - 3);
					}

					string strStreet = 'None';
					if (sAccount.ShippingStreet != null)
					{
						// Back up four places
						strStreet = strStreet.substring(0, strStreet.length() - 4);
						strStreet += sAccount.ShippingStreet + ' / ';
					}
					if (sAccount.BillingStreet != null)
					{
						// Back up
						if (strStreet == 'None')
						{
							strStreet = strStreet.substring(0, strStreet.length() - 4);
						}
						else
						{
							strStreet = strStreet.substring(0, strStreet.length() - 3);
						}
						strStreet += sAccount.BillingStreet + ' / ';
					}

					if (strStreet != 'None')
					{
						strStreet = strStreet.substring(0, strStreet.length() - 3);
					}


					// Now we are ready to add the matching account details to the Job record
					// Update the match count
*/
					sJob.Match_Count__c += 1;
/*
					// Back up five places
					sJob.DataList__c = sJob.DataList__c.substring(0, sJob.DataList__c.length() - 5);
	
					// Update the Job record
					sJob.DataList__c +=	sAccount.Id + ',' +
										sAccount.FirstName + ',' +
										sAccount.LastName + ',' +
										sAccount.PersonEmail + ',' +
										strPhone + ',' +
										strCity + ',' +
										strPostalCode + ',' +
										strStreet + ',' +
										'MATCH|FINAL';
				}
*/				
				// Check for overflow
				if (sJob.DataList__c.length() > 30000)
				{
					sJob.Overflow__c = true;
					break;
				}
			}
			else
			{
				// Returned null - could NOT match this record
				// Back up five places
				sJob.NoMatchList__c = sJob.NoMatchList__c.substring(0, sJob.NoMatchList__c.length() - 5);

				// Update the Job record
				sJob.NoMatchList__c +=	li_DataIn.get(0).replaceAll('"','') + ',' +
										li_DataIn.get(1).replaceAll('"','') + ',' +
										li_DataIn.get(2).replaceAll('"','') + ',' +
										li_DataIn.get(3).replaceAll('"','') + ',' +
										li_DataIn.get(4).replaceAll('"','') + ',' +
										li_DataIn.get(5).replaceAll('"','') + ',' +
										li_DataIn.get(6).replaceAll('"','') + ',' +
										li_DataIn.get(7).replaceAll('"','') + ',' +
										'INPUT|FINAL';

				// Check for overflow
				if (sJob.NoMatchList__c.length() > 30000)
				{
					sJob.Overflow__c = true;
					break;
				}
			}

system.debug('*** m_iProcessedCount *** ' + m_iProcessedCount);

			// Will hit the 200 SOQL limit of we do more than 30
			if (m_iProcessedCount > iNumToProcess - 1)
			{
				break;
			}
		}
/*
		// Back up the datalist to remove the suffix
		if (!sJob.DataList__c.endsWith('|'))
		{
			sJob.DataList__c = sJob.DataList__c.substring(0, sJob.DataList__c.length() - 6);
		}

		if (!sJob.NoMatchList__c.endsWith('|'))
		{
			sJob.NoMatchList__c = sJob.NoMatchList__c.substring(0, sJob.NoMatchList__c.length() - 6);
		}
*/
		// End of file?
		if (bEOF)
		{
			sJob.Active__c = false;
		}

		// Clear the In Error flag
		sJob.In_Error__c = false;

		// Check for overflow and start a new Job if required
		if (sJob.Overflow__c)
		{
			Duplicate_Check_Job__c sJobNew = new Duplicate_Check_Job__c();
			sJobNew				= sJob;
			sJobNew.Name		= sJob.Name + ' - from rec # ' + sJob.Input_Count__c.intValue();
			sJobNew.Active__c	= true;
			sJobNew.Overflow__c	= false;
			sJobNew.DataList__c = 'STRT|';
			sJobNew.NoMatchList__c = 'STRT|';
			insert sJobNew;

	       // Link file to the Job
	        Attachment sAttachmentNew	= new Attachment();
	        sAttachmentNew				= sAtt;
	        sAttachmentNew.ParentId		= sJobNew.Id;
	
	        insert sAttachmentNew;
	        
	        // Mark overflowed Job as complete
	        sJob.Active__c = false;
		}

		update sJob;

	}


	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static List<List<String>> parseCSV(String contents,Boolean skipHeaders)
	{
		List<List<String>> allFields = new List<List<String>>();

		// replace instances where a double quote begins a field containing a comma
		// in this case you get a double quote followed by a doubled double quote
		// do this for beginning and end of a field

		// MJ - removed - giving a 'REGEX too complicated' error with real data
		contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');

		// now replace all remaining double quotes - we do this so that we can reconstruct
		// fields with commas inside assuming they begin and end with a double quote

		// MJ - removed - giving a 'REGEX too complicated' error with real data
		contents = contents.replaceAll('""','DBLQT');

		// we are not attempting to handle fields with a newline inside of them
		// so, split on newline to get the spreadsheet rows
		List<String> lines = new List<String>();

		try
		{
			lines = contents.split('\n');
		}
		catch (System.ListException e)
		{
			System.debug('Limits exceeded?' + e.getMessage());
		}

		Integer num = 0;
		for(String line : lines)
		{
			// check for blank CSV lines (only commas)
			if (line.replaceAll(',','').trim().length() == 0)
				break;

			List<String> fields = line.split(',');   
			List<String> cleanFields = new List<String>();
			String compositeField;
			Boolean makeCompositeField = false;

			for(String field : fields)
			{
				if (field.startsWith('"') && field.endsWith('"'))
				{
					field = field.replaceAll('"','');
					cleanFields.add(field.replaceAll('DBLQT','"'));
				}
				else if (field.startsWith('"'))
				{
					makeCompositeField = true;
					compositeField = field;
				}
				else if (field.endsWith('"'))
				{
					compositeField += ',' + field;
					cleanFields.add(compositeField.replaceAll('DBLQT','"'));
					makeCompositeField = false;
				}
				else if (makeCompositeField)
				{
					compositeField +=  ',' + field;
				}
				else
				{
					cleanFields.add(field.replaceAll('DBLQT','"'));
				}
			}

			allFields.add(cleanFields);
		}

		if (skipHeaders)
			allFields.remove(0);

		return allFields;      
		}

	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 
	public static testMethod void testSP_DeDupCheckProcessing()
	{

	}
}