public with sharing class SP_DealerIntegrityProcessing
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Exception Classes & Enums
	public class SP_Exception extends Exception{}

	public enum BatchParam {ALL, ALL_ASYNC}

	private static integer iQueryLimit = 200;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class used to pass arguments into the Batch
	public class SP_Args
	{
		public BatchParam 	BatchParam 		{get; set;}
		public List<id>		ProcessList		{get; set;}
		public boolean 		UseSavePoint	{get; set;}
		public boolean 		AllOrNone		{get; set;}

		public SP_Args(list<id> theProcessList)
		{
			this.ProcessList 	= theProcessList;
			this.UseSavePoint 	= true;
			this.AllOrNone 		= false;
		}

		public SP_Args(list<id> theProcessList, BatchParam theBatchParam)
		{
			this.ProcessList 	= theProcessList;
			this.BatchParam		= theBatchParam;
			this.UseSavePoint 	= true;
			this.AllOrNone 		= false;
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Class used to pass information back to what called the batch
	public class SP_Ret
	{
		public boolean  	InError							{get; set;}
		public List<string> ErrorList						{get; set;}
				
		public SP_Ret()
		{
			this.InError = false;
			this.ErrorList = new List<string>();
		}
	}

	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Singular
	SP_Args 	mArgs;
	SP_Ret		mRet;
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Collections

	// Build a map of Dealer Accounts and their database IDs
	map<id, string>					map_DealerAccountIdtoName = new map<id, string>();

	// Accounts to process
	map<Id, Account>				map_Account						= new map<Id, Account>();

	// Account id and VO of latest selling dealer which in not private sale VO
	map<Id, Vehicle_Ownership__c>	map_VOLatestSellingDealer		= new map<Id, Vehicle_Ownership__c>();

	// List of exception records to be written
	list<Dealer_Exception__c>		li_DEsToInsert					= new list<Dealer_Exception__c>();

		
	/***********************************************************************************************************
		Constructor
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Our Constructor
	public SP_DealerIntegrityProcessing(SP_Args oArgs)
	{
		mRet 	= new SP_Ret();
		mArgs 	= oArgs;
	}
	
	/***********************************************************************************************************
		Access Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// The one and only entry point for the processor
	public SP_Ret ProcMain()
	{
		Savepoint savePoint;
		
		if(mArgs.UseSavePoint)
			savePoint = Database.setSavepoint();
		
		try
		{
			ProcessAccountObject(mArgs.ProcessList);
		}
		catch(Exception e)
		{
			mRet.InError = true;
			mRet.ErrorList.add(e.getMessage());
						
			if(savePoint != null)
				Database.rollback(savePoint);
			
			throw new SP_Exception('Fatal Exception - All changes have been rolled back - ' + e.getMessage());
		}
		
		return mRet;
	}
	
	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Process Account Objects
	private void ProcessAccountObject(list<id> li_AccountIdsIn)
	{
		Account [] arrDealerAccount = [
											select	Id,
													Name
											from	Account
											where	RecordTypeId = :SPCacheRecordTypeMetadata.getAccount_Dealer()
										];
	
		for (Account sAccount : arrDealerAccount)
		{
			map_DealerAccountIdtoName.put(sAccount.Id, sAccount.Name);
		}

		// Build a list of all Account records to be processed
		Account [] arrAccount =	[
										select	Id,
												Last_Selling_Dealer__c
										from	Account
										where	Id in :li_AccountIdsIn
										and		Batch_Processed__c = false
								];

		for (Account sAccount : arrAccount)
		{
			map_Account.put(sAccount.Id, sAccount);
		}

		// Get all Vehicle Ownerships for these Owners
		Vehicle_Ownership__c [] arrVO =	[
											select	AssetID__c,
													AssetID__r.Name,
													Customer__c,
													Branch_of_Dealership__c,
													Vehicle_Type__c,
													Start_Date__c
											from	Vehicle_Ownership__c
											where	Customer__c in :map_Account.keySet()
										];

		for (Vehicle_Ownership__c sVO : arrVO)
		{
			//for last selling dealer search only in non private sale VO	
			if (sVO.Vehicle_Type__c != 'Private Sale')
			{
				if (map_VOLatestSellingDealer.containsKey(sVO.Customer__c))
				{
					if (map_VOLatestSellingDealer.get(sVO.Customer__c).Start_Date__c
						< sVO.Start_Date__c)
					{
						map_VOLatestSellingDealer.remove(sVO.Customer__c);
						map_VOLatestSellingDealer.put(sVO.Customer__c, sVO);
					}
				}
				else
				{
					map_VOLatestSellingDealer.put(sVO.Customer__c, sVO);
				}
			}
		}

		// Now check that the Account Latest Selling Dealer matches that on the latest VO
		for (id idAccountId : map_Account.keySet())
		{
			Account sAccount = map_Account.get(idAccountId);
			
			if (map_VOLatestSellingDealer.containsKey(idAccountId))
			{
				Vehicle_Ownership__c sVO = map_VOLatestSellingDealer.get(idAccountId);
				
				if (sAccount.Last_Selling_Dealer__c != sVO.Branch_of_Dealership__c)
				{	
					Dealer_Exception__c sDE = new Dealer_Exception__c();
	
					sDE.Account__c						= sAccount.Id;
					sDE.VIN__c							= sVO.AssetID__r.Name;
					sDE.Account_Last_Selling_Dealer__c	= map_DealerAccountIdtoName.get(sAccount.Last_Selling_Dealer__c);
					sDE.VO_Last_Selling_Dealer__c		= map_DealerAccountIdtoName.get(sVO.Branch_of_Dealership__c);
	
					li_DEsToInsert.add(sDE);
				}
			}
		}

		if (!li_DEsToInsert.isEmpty())
		{
			insert li_DEsToInsert;
		}
	}

	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 
	public static testMethod void testSP_DealerIntegrityProcessing()
	{

	}
}