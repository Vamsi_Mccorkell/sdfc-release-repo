/* DATE: September 2019
 * AUTHOR: D BANEZ / MCCORKELL
 * CLASS SUMMARY: Vehicle Management Handler class for MyLexus WS methods. Used in MyLexusWS
 * WEB SERVICE USING THIS CLASS: MyLexusWebService
 * TEST CLASS: VehicleManagementHandlerTest
 * */

/* Author: D BANEZ / M&A 
*  Purpose: Web service used by MyLexus
* ERROR CODES SUMMARY:
* 700: Required fields missing
* 800: Write failed (any exception)
* 60x: Logic and business scenarios issue
    601: SF acct ID and SF VO ID provided but they do not match
    602: VO Id provided is invalid or does not exist
    603: Account matched but Gigya ID provided is not the same as the gigya Id in the account
    604: Multiple Account Matches on provided information
    605: No VIN found in the system
    606: Multiple VIN Matches found. Raised a case for investigation
    607: VIN has an active VO whose account does not match the guest account
	608: VIN exists but is not associated with a customer
	609: VO Id account does not match the provided account ID for Vehicle Verification
	610: An account is found with active GR records but no Gigya ID 
	611: No case found for case number provided
	612: Gigya ID and email do not match
	613: Multiple matches on Gigya ID and Email
	614: Registration link expired

	615: No existing account with the gigya ID provided
	616: No active Guest Vehicles for the Account

*/
global class VehicleManagementHandler {
    global static MyLexusWebService.GuestVehicleWSResponse getGuestVehicles(String gigyaId){
        MyLexusWebService.GuestVehicleWSResponse resp = new MyLexusWebService.GuestVehicleWSResponse();
        // check for required fields
        String errDesc = '';
        Integer requiredFieldsErrCounter = 0;
        if (gigyaId == null || gigyaId == ''){
            errDesc += 'gigyaId;';
            requiredFieldsErrCounter++;
        }
        if (requiredFieldsErrCounter > 0){
            resp.success = false;
            resp.recordId = null;
            resp.errCode = '700';
            resp.errorDesc = 'REQUIRED INPUTS MISSING: ' + errDesc;
        }
        else {
            try {
                List<Account> accountList = [select Id, Guest_Gigya_ID__c from Account
                                        where Guest_Gigya_ID__c = :gigyaId limit 1];
                if (accountList.size() == 0){
                    resp.success = false;
                    resp.errCode = '615';
                    resp.errorDesc = 'No existing account with the gigya ID provided';
                    Case returnedCase = createErrorCase(null, null, gigyaId, '615 - ' + resp.errorDesc);
                    resp.caseNumber = returnedCase.caseNumber;
                }
                else {
                    Id accountId = accountList[0].Id;
                    List<Guest_Relationship__c> activeGRList = [select Id, Encore_Status__c,
                                                                GR_Status__c, IsActive__c,
                                                                VehicleOwnership__r.Vehicle_Rego_Number__c,
                                                                VehicleOwnership__r.Model__c,
                                                                VehicleOwnership__r.Valet_Voucher_Expiry_Date__c,
                                                                VehicleOwnership__r.AssetID__r.Name,
                                                                VehicleOwnership__r.AssetID__r.Factory_Warranty_Expiration_Date__c, // 16 Nov Changes to add warranty expiry date
                                                                VehicleOwnership__r.AssetID__r.Fake_VIN__c, // 22 Oct 2019: D BANEZ: Added for Pilot
                                                                VehicleOwnership__c,
                                                                 Related_Case_Number__c
                                                                from Guest_Relationship__c
                                                               where Account__c = :accountId
                                                                and (IsActive__c=true or (GR_Status__c = 'Verified' and VehicleOwnership__r.Status__c = 'Active'))
                                                                //and GR_Status__c = 'Verified'
                                                               // and VehicleOwnership__r.Status__c = 'Active'
                                                               order by VehicleOwnership__r.Valet_Voucher_Expiry_Date__c desc];
                    
                    if (activeGRList.size() > 0){ // if there are active GR's
                        // get the latest GR based on voucher expiry date
                        resp.guestEncoreExpiration = activeGRList[0].VehicleOwnership__r.Valet_Voucher_Expiry_Date__c;
                        // by default encore status is Never Eligible
                        resp.guestEncoreStatus = 'Never Eligible';
                        for (Guest_Relationship__c gr : activeGRList){
                            if (gr.Encore_Status__c == 'Active'){
                                resp.guestEncoreStatus = 'Active';
                                break;
                            }
                            else if (gr.Encore_Status__c == 'Expired'){
                                resp.guestEncoreStatus = 'Expired';
                                break;
                            }
                        }
                        // assemble the vehicle list
                        List<MyLexusWebService.GuestRelationshipVehicles> vehiclesList = new List<MyLexusWebService.GuestRelationshipVehicles>();
                        for (Guest_Relationship__c gr : activeGRList){
                            MyLexusWebService.GuestRelationshipVehicles vehicle = new MyLexusWebService.GuestRelationshipVehicles();
                            vehicle.grStatus = gr.GR_Status__c;
                            vehicle.grActive = gr.IsActive__c;
                            vehicle.grRego = gr.VehicleOwnership__r.Vehicle_Rego_Number__c;
                            vehicle.grfactWrntExpDate = gr.VehicleOwnership__r.AssetID__r.Factory_Warranty_Expiration_Date__c;
                            // 22 Oct 2019: D BANEZ: Added for Pilot; If fake VIN is ticked, return a fake VIN
                            if (gr.VehicleOwnership__r.AssetID__r.Fake_VIN__c){ // if fake
                                vehicle.grVIN = Label.PilotFakeVinReplacementValue;
                            }
                            else {
                            	vehicle.grVIN = gr.VehicleOwnership__r.AssetID__r.Name;    
                            }
                            vehicle.grModel = gr.VehicleOwnership__r.Model__c;
                            vehicle.grVO = gr.VehicleOwnership__c;
                            vehicle.grCaseNumber = gr.Related_Case_Number__c;
                            vehiclesList.add(vehicle);
                        }
                        resp.grVehiclesList = vehiclesList;
                        resp.success = true;
                        resp.recordId = activeGRList[0].Id; // Record ID of the first entry
                    }
                    else {
                        resp.success = false;
                        resp.errCode = '616';
                        resp.errorDesc = 'No active Guest Vehicles for the Account';
                        //Case returnedCase = createErrorCase(null, null, gigyaId, '616 - ' + resp.errorDesc);
                        //resp.caseNumber = returnedCase.caseNumber;
                    }
                }
            }
            catch (Exception e){
                resp.success = false;
                resp.errCode = '800';
                resp.errorDesc = 'System error retrieving guest vehicles: ' + e.getMessage();
                Case returnedCase = createErrorCase(null, null, gigyaId, resp.errorDesc);
                resp.caseNumber = returnedCase.CaseNumber;
            }
        }
        return resp;
    }
    
    private static Case createErrorCase(Id sfAcctId, Id sfVOId, String gigyaId, String caseCreateReason){
        Id GENERALENQUIRYRTYPEID = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('General_Enquiry').getRecordTypeId();
        Case newCase = new Case(RecordTypeId = GENERALENQUIRYRTYPEID);
        newCase.Type = 'Data Verification';
        newCase.Origin = 'MyLexus';
        newCase.Status = 'In Progress';
        newCase.AccountId = sfAcctId;
        newCase.Subject = 'MyLexus Customer Issue';
        String description = caseCreateReason;
        description += ' | Account ID: ' + sfAcctId;
        description += ' | VO ID: ' + sfVOId;
        description += ' | Gigya ID: ' + gigyaId;
        newCase.Description = description;
        try {
            insert newCase;
            // get case number
            newCase = [select Id, CaseNumber from Case where Id = :newCase.Id limit 1];
            return newCase;
        }
        catch (DmlException e){
            system.debug(LoggingLevel.ERROR, '## Error inserting case: ' + e.getMessage());
            return null;
        }
        
    }
    
    
}