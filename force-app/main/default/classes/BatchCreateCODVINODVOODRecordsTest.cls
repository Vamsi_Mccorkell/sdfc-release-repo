@IsTest(SeeAllData=true)
public class BatchCreateCODVINODVOODRecordsTest {
	static Offer__c newOffer;
    static Account customer;
    static Account partnerAcct;
    
    static testmethod void testBatchCreatedCODVINODVOOD(){
        dataSetup();
        Set<Id> offerIdSet = new Set<Id>();
        newOffer.Status__c = 'Active';
        update newOffer; // this should call the batch via the trigger
        Test.startTest();
        BatchCreateCODVINODVOODRecords br = new BatchCreateCODVINODVOODRecords(newOffer.Id);
       	Database.executeBatch(br, 10);
       	Test.stopTest();
        //offerIdSet.add(newOffer.Id);
        
        // DEV NOTE: This just tests the batch class and just aims for coverage; It calls the GuestVoucherServicesUtilities.recalculateCODCounts(codIdSet) method
        // which is tested with assertions in the GuestVoucherServicesUtilitiesTest class
        
    }
    
    static void dataSetup(){
        List<Account> newAcctList = new List<Account>();
        List<Vehicle_Ownership__c> voList = new List<Vehicle_Ownership__c>();
        List<Asset__c> assetList = new List<Asset__c>();
        
        Id PARTNERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner').getRecordTypeId();
        Id CUSTOMERACCTRECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Customer').getRecordTypeId();
        partnerAcct = new Account(Name = 'Partner Account', RecordTypeId = PARTNERACCTRECORDTYPEID, Partner_Class__c = 'Customer');
        newAcctList.add(partnerAcct);
        
        customer = new Account(FirstName = 'Test', LastName = 'Customer', RecordTypeId = CUSTOMERACCTRECORDTYPEID);
        newAcctList.add(customer);
        
        insert newAcctList;
        
        newOffer = new Offer__c(Partner_Account__c = partnerAcct.Id, Status__c = 'Inactive', Offer_Class__c = 'Customer', Customer_Platinum_Limit__c = 10,
                               Offer_Type__c = 'Car Hire');
        insert newOffer;
        
    }
}