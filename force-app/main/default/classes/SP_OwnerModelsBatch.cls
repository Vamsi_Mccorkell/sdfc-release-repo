global with sharing class SP_OwnerModelsBatch implements Database.Batchable<sObject>
{
	public class SP_OwnerModelsBatchException extends Exception{}

	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		string	strQuery;

		if (!test.isRunningTest())
		{
			strQuery =		
				' select	Id'	+
				' from 		Account' +
				' where		RecordTypeId = ' + '\'' + SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType() + '\'' ;
		}
		else
		{
			strQuery =		
				' select	Id'	+
				' from 		Account' +
				' where 	RecordTypeId = ' + '\'' + SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType() + '\'' +
				' and		Owner_Type__c = ' + '\'' + 'Current' + '\'' +
				' and		FirstName = ' + '\'' + 'Test' + '\'' ;
		}

		return Database.getQueryLocator(strQuery);
	}


	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		list<id> theProcessList = new list<id>();

		for(sobject s : scope)
		{
			theProcessList.add((id)s.get('Id'));
		}

		SP_OwnerModelsProcessing.SP_Args oArgs = new SP_OwnerModelsProcessing.SP_Args(theProcessList);

		SP_OwnerModelsProcessing processor = new SP_OwnerModelsProcessing(oArgs);
		SP_OwnerModelsProcessing.SP_Ret oRet = processor.ProcMain();
	}


	global void finish(Database.BatchableContext BC)
	{
		system.debug('Finished');
	}

	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static testMethod void testBatch() 
	{
		// Set up some database records
        Account sAccount;

        sAccount			= new Account();
        sAccount.FirstName	= 'Test';
        sAccount.LastName	= 'Account';
        sAccount.RecordTypeId = SPCacheRecordTypeMetadata.getPersonAccountOwnerRecordType();
        insert sAccount;

		SP_OwnerModelsBatch oOwnerModelsBatch = new SP_OwnerModelsBatch();
		oOwnerModelsBatch.start(null);

		oOwnerModelsBatch.execute(null, new list<SObject>{sAccount});

		oOwnerModelsBatch.finish(null);
	}
}