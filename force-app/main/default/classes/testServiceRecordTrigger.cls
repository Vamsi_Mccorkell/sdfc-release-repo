/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class testServiceRecordTrigger {

    static testMethod void myUnitTestService() {
    	
        Account acc = new Account();
        acc.RecordTypeId= '012900000000THw';
        acc.Type = 'Customer';
        acc.LastName = 'Last Name';
        acc.BillingPostalCode = '2000';
        acc.PersonMobilePhone = '0499990000';
        acc.FirstName = 'First Name';
        acc.Salutation = 'Mr.';
        insert acc;
        
        Asset__c asset = new Asset__c();
        asset.Name = 'TEST123456789';
        insert asset;
        
        Vehicle_Ownership__c VO = new Vehicle_Ownership__c();
        VO.AssetID__c = asset.Id;
        VO.Customer__c = acc.Id;
        VO.Start_Date__c = System.today();
        VO.Vehicle_Rego_Number__c = 'ABC123';
        insert VO;
        
        Service__c service = new Service__c();
        service.Asset__c = asset.Id;
        service.Customer_Account__c = acc.Id;
        service.Service_Date__c = System.today();
        service.Invoice_Total__c = 123.45;
        insert service;
    }
}